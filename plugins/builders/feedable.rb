class Builders::Feedable < SiteBuilder
  def build
    define_resource_method :content_for_rss_feed do |_maker, view|
      view.render FeedItem.new(post: self)
    end
  end
end
