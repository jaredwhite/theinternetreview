class Builders::SeoExtractions < SiteBuilder
  def build
    # Extract first paragraph or h2 tag
    define_resource_method :summary_extension_output do
      content_lines = content.to_s.lines
      heading = nil
      paragraph = nil

      content_lines.each do |line|
        if line.start_with?("<h2")
          heading = line.match(%r!<h2.*?>(.*?)</h2>!)&.then { _1[1] }
          break
        elsif line.start_with?("<p")
          paragraph = line.match(%r!<p.*?>(.*?)</p>!)&.then { _1[1] }
          break
        end
      end

      heading || paragraph
    end

    # Find any images in content and use that
    hook :posts, :pre_render do |resource|
      next if resource.data.image && !resource.data.image.end_with?("the-internet-review-square.png")

      md_img = resource.content.match %r!\!\[.*?\]\((.*?)\)!
      img_url, _ = md_img&.captures

      unless img_url
        html_img = resource.content.match %r!<img src="(.*?)"!
        img_url, _ = html_img&.captures
      end

      if img_url && !img_url.end_with?(".gif")
        img_url = img_url.start_with?("http") ? img_url : "#{site.config.url}#{img_url}"

        cloudinary_thumbnail = "/a_exif,c_fill,g_face,h_700,q_50,w_700/"
        img_url.sub!("/w_2048,c_limit,q_65/", cloudinary_thumbnail)

        resource.data.image = img_url
      end
    end
  end
end
