class Builders::LazyImages < SiteBuilder
  def build
    inspect_html do |doc, resource|
      main = doc.query_selector('main')
      next unless main

      main.query_selector_all("img").each do |img|
        if img[:src].start_with?("./")
          img[:src] = resource.relative_url + img[:src].delete_prefix(".")
        end
        next if img[:loading]

        img[:loading] = :lazy
      end
    end
  end
end
