class Builders::Markdownable < SiteBuilder
  def build
    hook :resources, :pre_render do |resource|
      # WikiLinks! =)
      # resource.content = resource.content.gsub %r!\[\[(.+?)\]\]! do
      #   title = Regexp.last_match[1].strip
      #   found = site.resources.find { _1.data.title == title }
      #   found ? "[#{found.data.title}](#{found.relative_url})" : "[[NOT FOUND: #{title}]]"
      # end

      next unless resource.relative_path.extname == ".md" && resource.data.skip_figure_markdown != true

      resource.content = resource.content.gsub("<image-figure", '<image-figure markdown="span"')
    end
  end
end
