class Builders::Graph < SiteBuilder
  def build
    helper :graph_tags
  end

  def graph_tags
    GraphTags.new(view: helpers.view).render_in(self)
  end
end

class GraphTags
  include Streamlined::Renderable

  attr_reader :resource, :site

  def initialize(view:)
    @resource = view.resource
    @site = resource.site

    @url = view.absolute_url(resource)

    @summary = resource.summary if resource.respond_to?(:summary)
    @summary = @summary == "" ? nil : view.strip_html(@summary)

    @description = resource.data.description || resource.data.subtitle || @summary || @site.metadata.description

    if resource.data.title == "Index"
      @title = @site.metadata.title
      @description = @site.metadata.tagline
    else
      @title = resource.data.graph_title || resource.data.title
    end

    @locale = @site.locale.to_s.tr("-", "_")
    @locale = nil if @locale == "en"

    @author = site.metadata.author
    @image = view.absolute_url(resource.data.image) if resource.data.image
  end

  def template
    render { title }
    render { locale } if @locale
    render { description }
    render { author } if @author
    render { canonical }
    render { site_name }
    render { image } if @image

    if resource.data.graph_type
      render { og_custom_type }
    elsif resource.respond_to?(:date) && !(resource.respond_to?(:collection) && resource.collection.label == "pages")
      render { og_article_type }
    else
      render { og_website_type }
    end
  end

  def title = meta_property_tag "og:title", @title

  def locale = meta_property_tag "og:locale", @locale

  def description
    render meta_name_tag "description", @description
    render meta_property_tag "og:description", @description
  end

  def author = meta_name_tag "author", @author

  def canonical
    render html -> { <<~HTML
      <link rel="canonical" href="#{text -> { @url }}" />
    HTML
    }
    render meta_property_tag "og:url", @url
  end

  def site_name = meta_property_tag "og:site_name", site.metadata.title

  def image = meta_property_tag "og:image", @image

  def og_custom_type
    render meta_property_tag "og:type", resource.data.graph_type
  end

  def og_article_type
    render meta_property_tag "og:type", "article"
    render meta_property_tag "article:published_time", resource.date.iso8601
  end

  def og_website_type
    render meta_property_tag "og:type", "website"
  end

  private

  def meta_property_tag(key, value)
    html -> { <<~HTML
      <meta property="#{text -> { key }}" content="#{text -> { value }}" />
    HTML
    }
  end

  def meta_name_tag(key, value)
    html -> { <<~HTML
      <meta name="#{text -> { key }}" content="#{text -> { value }}" />
    HTML
    }
  end
end
