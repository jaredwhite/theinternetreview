---
layout: page
title: Back by Popular Demand!
icon: pocketwatch
---

**The following is copied almost verbatim from [the original Web site](http://web.archive.org/web/19961223192910/http://www.sonic.net/~jwhite). Isn't that a hoot?**
{:style="text-align: center"}

----

<strong>The Internet Review</strong> was started by Jared White in <strong>September 1996</strong> for the purpose of presenting timely information in a useful, easy to read format.

<image-figure caption="Holy Netscape Navigator, Batman!">
  <img src="/1996/NNSSsm1.gif" style="width: 525px">
</image-figure>

The Internet has opened up the way for **anybody with a computer**, modem, and Internet account to present information to the rest of the world, completely eliminating the monetary concerns of publishing, as well as removing the barrier between the big monopolistic publishers and <strong>the people at large</strong>. Often, the information from regular people is as useful (or even more useful) than the information from the monetarily-controlled media.

Well, <strong>we're regular people</strong>, and we sincerely hope that the information we have to present is informative, useful, and interesting. If it is not, we'll never know unless someone tells us, so please e-mail Jared at [jared@intuitivefuture.com](mailto:jared@intuitivefuture.com), mention [@theinternet@intuitivefuture.com](https://intuitivefuture.com/@theinternet) on Mastodon, or DM Jared on Signal **@jaredwhite.05**, and tell us what you think! <strong>Tell us what you like! Tell us what you don't like!</strong>

<image-figure caption="Gosh, the memories…">
  <img src="/images/gifs/netscape.gif" width="60" style="image-rendering: crisp-edges" />
</image-figure>

**[Start Browsing from 1996 Onward ➜]({{ site.generated_pages.select { _1.relative_path.include?("archived/") }.last.relative_url }})**
{: style="text-align: center"}

----

Putting on Michael Walthius (aka [The Keyboard Wizard](https://web.archive.org/web/19970106193807/http://www.keybdwizrd.com:80/music.html))'s **Dreaming in Stereo** album is a must if you want that mid-1990s "General MIDI on the Web" feel. I actually embedded one of his MIDI files (`kw_fnk02.mid`) as background music for the original site! My favorite of his compositions is **Chrysta's Dream**:

<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/166535877&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/michael-walthius" title="Michael Walthius" target="_blank" style="color: #cccccc; text-decoration: none;">Michael Walthius</a> · <a href="https://soundcloud.com/michael-walthius/chrystas-dream" title="Chrysta&#x27;s Dream" target="_blank" style="color: #cccccc; text-decoration: none;">Chrysta&#x27;s Dream</a></div>

----

## Fast Forward to Today…

![old i logo](/images/1996-logo.png){: style="float: right; margin-inline-start: var(--size-3); margin-block-end: var(--size-3)"}
Zooming out a bit, I ran my first pseudo-blog all about software and internet technology, [The Internet Review](http://web.archive.org/web/19961223192910/http://www.sonic.net/~jwhite) (aka "iReview"), for a short period of time in 1996-1997. Then I relaunched [a second version](https://web.archive.org/web/20000309161815/http://gaeldesign.adei.com/ireview/index.html) in 1999, again running it for a short period of time. (Sensing a pattern here? 😅)

Then in 2002, I started a new technology blog under a different name: [The Idea Basket](http://web.archive.org/web/20021007215826/http://gaeldesign.com/ib). That ran for a while under a couple of different incarnations. Eventually it too shut down, and with occasional exceptions, I never really had a "tech blog" as such again—apart from all that I've posted on my personal blog [JaredWhite.com](https://jaredwhite.com) over many years (and granted there's plenty of tech-themed content to mine).

So why am I tech blogging again now, in the Year of Our Lord 2024? Simple. I believe tech reporting by and large is in danger of becoming quite distorted in ways I haven't seen since the late 90s. **History is repeating itself.**

One the one hand, you have **outlets breathlessly regurgitating press releases** from disingenuous tech companies. We saw it with crypto, web3, NFTs, metaverses, and now generative AI. For example pundits will repeat wild claims made by GitHub about the supposed productivity boost of using Copilot, without ever stopping to think that maybe we shouldn't blindly trust data promoted by a company that says it's good to use a product made by that company? (You'd imagine we'd have learned our lesson by now after Big Tobacco, Big Pharma, Big Oil, Big…you get the idea!)

And on the other hand, you have **people cynically** throwing up their hands and claiming that the Internet is doomed, online technology is rotting our brains, social media is destroying civil society, and computers were a mistake or whatever. Yes, we've fully entered the age of "it's cool to shit on the Internet". **Sorry, I don't buy it.** The problem isn't the Internet, or the World-Wide Web, or even social media. The problem is Big Tech, corporate silos, VC money, hype cycles, bad actors backed by adversaries of secular democracies, and a bizarre lack of adequate funding of open source software and open protocols by governments and non-profits (at least here in the U.S.).

**Listen:** we can criticize garbage culture-eroding technologies peddled by smarmy CEOs without accepting the premise of the tired insult _[Luddite!](https://thenib.com/im-a-luddite/)_, AND we can revel in the wondrous world of online creativity, global collaboration, future-thinking education, and joyful discourse that is the Web—celebrating all the ways we're getting things back on track with the rise of the Fediverse and a leveling of the OS playing field (increasingly it doesn't matter which operating system or device type you happen to use…plenty of modern software runs everywhere!).

Here at **The Internet Review**, I am absolutely dedicated to presenting an _optimistic_ view of digital technology, even while holding Big Tech accountable. We can do both. We _need_ both. And I'm excited to join the fray this year along with a cohort of other amazing and hard-working pundits and reporters in the technology journalism space (who I'll be linking to with glee).

Make sure you follow [@theinternet@intuitivefuture.com](https://intuitivefuture.com/@theinternet) on Mastodon and [subscribe to our RSS feed](/feed.xml), so you don't miss a beat! And if you have a scoop you'd like to share with me privately, I'm available via email at [jared@intuitivefuture.com](mailto:jared@intuitivefuture.com) or on Signal at **@jaredwhite.05**.
