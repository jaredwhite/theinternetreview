---
layout: page
title: The Toolbox
icon: tool
---

Things you need to get started in order to become a fully-fledged netizen:

* **A personal Web site.** Often called a "weblog" or blog for short (though not every personal Web site has to be a blog—some are more like portfolios, resumes, or just a collection of fun things you enjoy making or showcasing). You can learn how to make your own Web site using languages like HTML, CSS, and JavaScript. [HTML for People](https://www.htmlforpeople.com) is a great way to get started. Or you can use a service that gives you visual building blocks to build a site in no time flat. For writers, perhaps give [Micro.blog](https://micro.blog) or [Write.as](https://write.as) a try…and for the more ambitious, [Ghost](https://ghost.org)!  
  (_Psst!_ Like Ruby? I work on a web framework called [Bridgetown](https://www.bridgetownrb.com). Check it out!)
* **A domain name.** It's best if your Web site is hosted at a name you control. My personal Web site is [jaredwhite.com](https://jaredwhite.com), because I'm Jared White! I like buying my domains at [Porkbun](https://porkbun.com)—and believe me, I own a bunch of ’em. 😅
* Chat with folks from around the world in the **fediverse** — a federated network of social Web sites that can be themed around a topic or a region or just general purpose conversation. My fediverse profile is [@jaredwhite@indieweb.social](https://indieweb.social/@jaredwhite)—feel free to stop by and say hi!
* In addition to text, it's easy to post audio online as well! If you record a series of episodes and want folks to be able to subscribe to your show, you can create a **podcast**. I like using [Buzzsprout](https://buzzsprout.com) for hosting podcasts, but there are many services out there. Just make sure it's a _real_ podcast service, and not something that locks you into their "silo" alone. (Podcasts are playable from any podcast player on any network—it's the _open_ web after all!)
* Video's still a bit tricky, sad to say. There's a Web site called YouTube that wants everyone to post their videos there, but unfortunately that isn't very well in keeping with the spirit of the internet. What I like to do these days is post my videos on my own Web site using **Bunny Stream**. [Bunny](https://bunny.net/stream/) makes it pretty easy to set up your _own_ online video service, and then you can "embed" your videos on your own Web site. Neat!
* And of course, you can't get too far with any of these endeavors without an **email address**. Don't fall for the hype and try to use Gmail! There are a number of email hosting providers out there that work great with your own domain name _and_ protect your privacy. Want to be myname@myfullname.com? No sweat! Personally I like using [FastMail](https://www.fastmail.com).
* Speaking of email, maybe you want to send an **email newsletter** out to your fans? Not to worry, [Buttondown](https://buttondown.com) has got you covered. Their text editor is best-in-class and the personalized support you'll receive is top-notch.
