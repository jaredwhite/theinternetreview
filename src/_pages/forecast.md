---
layout: page
title: Internet Forecast
icon: umbrella
---

The concepts, ideas, technologies, and movements we're tracking as the internet moves forward. (And some of the real stinkers that fit the category of "and this is why we can't have nice things…")

🚧 _Coming soon!_ 🚧
