---
layout: page
icon: pocketwatch
title: ":prototype-term-titleize Archive"
exclude_from_search: true
prototype:
  collection: posts
  term: monthly
pagination:
  per_page: 100
---

{% articles = paginator.resources.reject { _1.data.quick_news } %}
{% if articles.count.positive? %}
  <h2>Articles <img src="/images/icon-articles.png" width="64" style="image-rendering: crisp-edges; vertical-align: -0.55em; margin-inline-start: var(--size-1)"></h2>

  <ul class="unstyled">
  {% articles.each do |post| %}
    <li style="margin-block-end: var(--size-4)">
      {%@ ArticlePost post: %}
    </li>
  {% end %}
  </ul>
{% end %}

{% quick_news = paginator.resources.select { _1.data.quick_news } %}

{% if articles.count.positive? && quick_news.count.positive? %}
  <hr style="border-top-style: dashed;width: 22ch;margin: var(--size-8) auto;" />
{% end %}

{% if quick_news.count.positive? %}
  <h2>Quick News <img src="/images/icon-tv.png" width="64" style="image-rendering: crisp-edges; vertical-align: -0.5em; margin-inline-start: var(--size-1)"></h2>

  {% quick_news.each_with_index do |post, index| %}
  {%= index > 0 ? '<hr style="border-top-style: dashed;width: 22ch;margin: var(--size-8) auto;" />' : '' %}
  {%@ QuickNews post: %}
  {% end %}
{% end %}

<hr style="border-top-style: dashed;width: 22ch;margin: var(--size-8) auto;" />

<p style="display: grid; gap: var(--size-5); grid-template-columns: 1fr 1fr">
  <resource-link>
    {% previous_page = site.generated_pages[site.generated_pages.find_index { _1 == page } + 1] %}
    {% if previous_page && previous_page.relative_path.include?("archived/") %}
      {{ previous_page.data.title | sub: " Archive", "" | prepend: '<flip-it style="display: inline-block; rotate: 180deg">➜</flip-it> ' | link_to: previous_page }}
    {% end %}
  </resource-link>
  <resource-link style="text-align: right">
    {% next_page = site.generated_pages[site.generated_pages.find_index { _1 == page } - 1] %}
    {% if next_page && next_page.relative_path.include?("archived/") %}
      {{ next_page.data.title | sub: " Archive", "" | append: " ➜" | link_to: next_page }}
    {% end %}
  </resource-link>
</p>

<hr style="margin-block: var(--size-10)" />

<h2 style="margin-block: var(--size-8)">Blast from the Past</h2>

<archive-grid style="display: grid; grid-template-columns: repeat(auto-fit, 12em); justify-content: space-between">

{% page_year = page.data.monthly.split(" ").last %}
{% year = nil %}
{% monthly_archive_list.each do |archive| %}
  {% this_year = archive.split(" ").last %}
  {% if this_year != year %}
    {% unless year.nil? %}</ul></details>{% end %}
    <details {% if this_year == page_year %}open {% end %}name="years-list" style="margin-block-end: var(--size-6)"><summary style="color: white; font-weight: bold; cursor: pointer">{{ this_year }}</summary><ul class="unstyled">
  {% end %}
  <li><a href="/archived/{{ archive | slugify }}">{{ archive }}</a></li>
  {% year = this_year %}
{% end %}
</ul></details>

</archive-grid>