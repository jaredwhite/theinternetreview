---
layout: default
---

# {{ site.metadata.tagline }}
{:style="max-width: 26ch; margin-inline: auto"}

**The Internet Review** was started by <a href="/about/jared">Jared White</a> in **1996** for the purpose of presenting timely information in a useful, easy to read format. It eventually went on hiatus for about **25 years** (_seriously!_), and now it's back and **better than ever**. [More history here.](/history/)

**Got links?** Email them to [jared@intuitivefuture.com](mailto:jared@intuitivefuture.com) or mention [@theinternet@intuitivefuture.com](https://intuitivefuture.com/@theinternet) on Mastodon, and if they're relevant we'll feature them on this Web site.

----

<p style="text-align: center">
  <strong>Our Newsletter is ALIVE! <nobr>📨 Subscribe Today:</nobr></strong><br /><a href="https://buttondown.email/theinternet" target="_blank">“<strong style="color: inherit">Cycles Hyped No More</strong>”</a>
</p>

<p style="text-align: center"><a href="/feed.xml">Add Our RSS to Your Feed Reader</a> <a href="/feed.xml" class="unstyled"><img src="/images/rss-button.png" width="36" alt="RSS logo" style="image-rendering: crisp-edges;vertical-align: -.6em; margin-inline-start: var(--size-2)" /></a></p>

----

## Latest Articles <img src="/images/icon-articles.png" width="64" style="image-rendering: crisp-edges; vertical-align: -0.5em; margin-inline-start: var(--size-2)">
{:style="text-align: center"}

{% latest_articles = collections.posts.resources.reject { _1.data.quick_news }[0...4] %}
<ul class="unstyled">
  {% latest_articles.each do |post| %}
    <li style="margin-block-end: var(--size-4)">
      {%@ ArticlePost post: %}
    </li>
  {% end %}
</ul>

<p style="text-align: center; margin-block-start: var(--size-8)">
  <strong>Continue Browsing: <a href="/archived/{{ latest_articles.last.date | strftime: "%B %Y" | slugify }}">{{ latest_articles.last.date | strftime: "%B %Y" }}</a></strong>
</p>

----

## Quick News <img src="/images/icon-tv.png" width="64" style="image-rendering: crisp-edges; vertical-align: -0.5em; margin-inline-start: var(--size-1)">
{:style="text-align: center"}

{% quick_news = collections.posts.resources.select { _1.data.quick_news }[0...8] %}
{% quick_news.each_with_index do |post, index| %}
{%= index > 0 ? '<hr style="border-top-style: dashed;width: 22ch;margin: var(--size-8) auto;" />' : '' %}
{%@ QuickNews post: %}
{% end %}

<hr style="border-top-style: dashed;width: 22ch;margin: var(--size-8) auto;" />

<p style="text-align: center">
  <strong>Continue Browsing: <a href="/archived/{{ quick_news.last.date | strftime: "%B %Y" | slugify }}">{{ quick_news.last.date | strftime: "%B %Y" }}</a></strong>
</p>