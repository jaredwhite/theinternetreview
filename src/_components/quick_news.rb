class QuickNews < Bridgetown::Component
  def initialize(post:)
    @post = post
  end

  def url_hostname
    URI.parse(@post.data.link_url).host.sub("www.", "")
  end
end
