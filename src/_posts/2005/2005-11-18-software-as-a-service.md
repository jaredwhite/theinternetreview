---
date: "Friday, November 18th, 2005 at 2:36 pm"
title: "Software as a Service"
category: archived
funny_caption: "Gettin' Real SaSSy on"
icon: "vista-network"
archive_url: "https://web.archive.org/web/20081122070902/http://www.theideabasket.com/2005/11/18/software-as-a-service/"
---

It's [all in the news](https://web.archive.org/web/20081122070902/http://news.zdnet.com/2100-3513_22-5958760.html?tag=st.prev) these days --- especially after Microsoft's recent Windows & Office Live announcements: software as a service, made possible either via advertising revenue or small scheduled payments. As long as you, or the advertisers, pay, the software is available. Once the money goes bye-bye, the software goes bye-bye.

Purchasing software for a one-time fee is such an ingrained concept that I doubt it will go away entirely. But the strange thing about software is that every second that you use it, it looses value because it just brings you one step closer to that next upgrade (which of course you'll buy eventually). So the idea of "renting" your software, or even using it for free and putting up with some ads, really isn't so out there. The problem, of course, is making sure you're getting your money's worth. If I buy a piece of software for $99 and use it for two years, or I rent the software for $7.95 a month for two years, I'll have spent almost twice as much money ($190.80) renting the darn thing!

The best thing about the software-as-service concept is that it fits the Web 2.0 application model very nicely. You can't "run" a Web 2.0 application on your computer, because it's hosted. So you really are paying for the ability to use the application, rather than paying for the application itself as a product. I think we'll be seeing a lot of this sort of thing as time goes on, and I think Microsoft and other big-players know this. THAT is what the Windows/Office Live announcements were all about, not just some kind of reactionary Google-killer fluff.
