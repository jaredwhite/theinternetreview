---
date: "Sunday, December 4th, 2005 at 11:58 am"
title: "Stating the Obvious: Tagging is the Future"
category: archived
funny_caption: "Tag! You're…It? at"
icon: folders
archive_url: "https://web.archive.org/web/20090318160338/http://www.theideabasket.com/2005/12/04/stating-the-obvious-tagging-is-the-future/"
---

I'm going to say something so blindingly obvious, so "pointy-haired boss" duh, so incredibly lame, that you'll wonder why you're spending precious time reading this post. But it still needs to be said.

Tagging is the future, and will probably eliminate the need for folders in most cases.

By tagging I mean Flickr and Del.icio.us style tagging, where you have a very small number of data streams (perhaps only one) containing a huge amount of items, and the way you organize the data is through tags (or keywords as you might call them). The tagging process occurs when you save/upload/create your data, and the faster that process is, the more people will use it. And people are certainly using it, as the top Web 2.0 social apps show in significant ways.

I absolutely believe that, sooner or later, this concept will be used extensively throughout filesystems themselves with native OS support on both the GUI and CLI level. Years ago I stressed the need for keyword support in file management, and the latest tagging metaphors and "tag clouds" and whatnot that have become so popular only make this need more urgent. In fact, I have a feeling that Mac OS X 10.5 (Leopard) will introduce tag-style metadata saving and searching as part of the whole Spotlight system. This is already possible to a certain extent in the core of 10.4 Tiger (with third-parties exposing some of this functionality), so it really makes sense for Apple to expose this officially.

Microsoft has made a more public demonstration of these ideas with Vista (and some of the future WinFS possibilities), but as usual their UI work leaves something to be desired. I have no doubt it will take Apple to come up with a simple, efficient, and elegant way to create, edit, and search for tagged files. And I can hardly wait.
