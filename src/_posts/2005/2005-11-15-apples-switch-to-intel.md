---
date: "Tuesday, November 15th, 2005 at 12:01 pm"
title: "Apple’s Switch to Intel"
category: archived
funny_caption: "PC 🤝 Mac for"
icon: "386"
archive_url: "https://web.archive.org/web/20081122055540/http://www.theideabasket.com/2005/11/15/apples-switch-to-intel/"
---

This topic has been discussed to death already, and I'm sure that won't change going into next year. But I do want to comment on something which I think is meaningful not just to this business move but to everything we do in life.

This change of attitude on Apple's part is a triumph of pragmatism over ideology. The "noble" and "idealistic" thing for Apple to do would be to continue to push PowerPC as a "fine" and "superior" technology over that, that...ugh, I can't even say it without turning up my nose...joke of a processor family called x86. But instead, Apple did the unthinkable --- they realized that that joke called x86 would actually serve their needs better than PowerPC going forward, and thus they decided to do the necessary thing rather than the artificially-determined "right" thing.

Now my question to you is: how willing are you to sacrifice ideology and idealism in your own life in order to do what is necessary? When you decide to build a product, create an artwork, forge a relationship with someone, are you going to cling to an imagined superiority of purpose and not budge? Or will you settle for something that simply works in the way it needs to in order to be the most useful?

Food for thought.
