---
date: "Monday, December 19th, 2005 at 12:00 pm"
title: "The End of Wintel? Think Different."
category: archived
funny_caption: "Opportunity Knocks on"
icon: "386"
archive_url: "https://web.archive.org/web/20060621144432/http://www.theideabasket.com/2005/12/19/the-end-of-wintel-think-different/"
---

As much as I'm a gigantic Apple fan and own several Macs (happily switched from the PC in 2001), I have to say all these grandiose predictions of a formidable "Mactel" conglomerate seem pretty far-fetched. Windows will continue to be a dominant OS and Intel will continue to sell a bucket-load of processors through Dell, HP, and others. That's just the way it is, unless an epic shakeup rips through the entire industry for some reason.

However, what I do see happening is a re-alignment of innovation partnerships. Typically, attempts at "PC innovation" have been fueled by Microsoft and Intel working together on concept products shown to OEMs at computer trade shows. It hasn't worked very well. Big PC OEMs aren't interested in creating wacky new products that might fail --- they want to create cheap stuff that the masses buy and that hasn't changed much at all in the past 5+ years. The one major "innovation" that's come along as of late is the Tablet PC. Hmm, perhaps the OEMs should have been more careful after all.

Meanwhile, Apple is a company that knows how to create new computer concepts that are simple, cool, and fun to use. Intel desperately needs some real design mojo right now as it tries to distance itself from a price/performance war with AMD. An Apple-Intel design alliance could fuel significant concept product design that's lightyears ahead of Intel's work with Microsoft, and the best thing about this is that it's a win-win situation for both companies. Apple solidifies its image as the bold, hip, cutting-edge computer company that PC makers blandly copy years later, and Intel gains a new image as a cool company that looks beyond the "hardware guts" of today and sees the whole 21st-century tech picture.

However, before you go off jumping up and down with glee and anticipation (or grinding your teeth with fury if you're Michael Dell), let me remind you that Apple is an incredibly secretive company. The idea of Apple and Intel hanging around at some show together showing everyone future products is somewhat laughable. So it may be that Intel will resign itself to the fact that when these concept products are first shown, it's at a "Stevenote" in San Francisco, and it's shipping from Apple.com a few weeks later. I doubt Apple is feeling generous enough to lend its design prowess to giving other OEMs a leg up.

We'll see how this all plays out during 2006. But one thing's for sure: Intel has been courting Apple for years in an effort to gain that one prestigious customer. Now that they have Apple in their camp, it's highly unlikely they'll squander this historic opportunity.
