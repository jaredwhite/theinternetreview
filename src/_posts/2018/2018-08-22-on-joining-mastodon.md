---
date: '2018-08-22T11:14:59-07:00'
title: On Joining Mastodon
icon: fediverse
funny_caption: "Trading Birds for Big Game on"
archive_url: "https://jaredwhite.com/links/20180822/1"
---

> The "business model" (if any) and development model of Mastodon vs. App.net (may it rest in peace) is radically different…however I see them as both fulfilling the same important need on the web: aligning the goals of a social network with the needs of its community, rather than the actual paying customers of nearly all proprietary social networks: advertisers.
> 
> Mastodon is nothing without the community. And I *love* that.

So I've been on Mastodon, the open web federated social network, for a while now. I've seen it grow and grow, and lately with all the wackiness with Nazis and other unsavory characters on Twitter—combined with the mind-numbing lack of awareness on the part of Twitter's top brass—I'm seeing a ton of "influencers" in the tech/geek crowd migrating over to Mastodon. It's very exciting. What's also exciting is I decided to set up my own instance of Mastodon! It's called (*oh how I love that I was able to snag this domain name*): **OpenWeb.social**. The link lets you see my account there, and you're welcome to sign up on the instance if you want to try out Mastodon yourself. I'll be happy to show you around!

<em>[2024 Editor's Note: I ended up shutting down this instance and drifting away from Mastodon by 2020, in part due to a great many of the aforementioned "influencers" also leaving Mastodon. Bummer. It wouldn't be until 2022 that I rejoined the fediverse, this time via the **excellent** <a href="https://indieweb.social/@jaredwhite">IndieWeb.social</a> instance.]</em>