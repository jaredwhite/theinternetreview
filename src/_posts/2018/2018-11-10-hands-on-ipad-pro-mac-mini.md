---
date: '2018-11-10T07:23:53-08:00'
title: 2018 iPad Pro, Pencil 2, Mac mini Hands-on Details
icon: rainbow-apple
funny_caption: "Another Trip to the Apple Store for"
archive_url: "https://jaredwhite.com/20181110/1"
skip_figure_markdown: true
---

<image-figure>
  <iframe src="https://www.youtube-nocookie.com/embed/XYvSDdvphc8?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</image-figure>

Some impressions on Apple's October product lineup:

* MacBook Air: 😐
* Mac mini: 😃
* iPad Pro: 🤩
