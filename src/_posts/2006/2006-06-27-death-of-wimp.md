---
date: "Tuesday, June 27th, 2006 at 9:20 pm"
title: "The Death of WIMP"
category: archived
funny_caption: "Beyond the Desktop Metaphor for"
icon: vista-computer
archive_url: "https://web.archive.org/web/20060706211607/http://www.theideabasket.com/2006/06/27/the-death-of-wimp/"
---

In the past [I’ve blogged about what the “next-generation” desktop user interface might look like](/archived/2002/05/28/the-commercial-feasibility-of-a-next-generation-user-interface/). My hypothesis was that neither Microsoft nor Apple could afford to throw existing UI concepts out of the window because it wouldn’t make immediate financial or business sense. I also said it was unlikely the Linux crowd would jump to the forefront in UI innovation because they were too busy cloning Windows. No, I said, if there were to be a quantum leap ahead in the GUI space, it would come from a totally unknown source with nothing to lose and everything to gain.

Well, even though I had no idea what that source might be, I basically wasn’t too far off the beam. It looks like the death of the standard “WIMP” (Windows, Icons, Menus, Pointer) GUI is actually starting to appear on the horizon, and a new paradigm is coming into view. And what is this new paradigm, you ask? Why, nothing less than the venerable Web page.

*WHAT?!?! That’s no paradigm. Web pages have been around for years now, and they’re just interactive documents with fancy graphics. The more they act like apps, the closer they get to the “real deal” — desktop software with a traditional GUI.*

Nope, that’s not what’s happening at all. Sure, lots of Web apps over the years have tried to emulate traditional GUIs, complete with desktop-like icons, menus, windows, etc., but you know what? They all sucked, big time. The era of sluggish Java applets and pixelated, battleship-grey interfaces is over, and everyone knows it. The new era is everything we love about Web 2.0.

User-focused design. Big, happy fonts. Descriptive text that helps you learn your way around. Simplicity. Less features (as the grammatically-challenged 37signals folks would say). In-place editing and in-context actions via the power of Ajax. Tagging. Links. Striking visual effects via cutting-edge CSS and Javascript. The list goes on. And you know what virtually none of these awesome new apps use? The old paradigms of WIMP. There’s no menu bar. There are no windows (or if there are, they’re just tiny pop-up dialogs). There’s no pool of icons. Only the Pointer of WIMP is left, for the most part.

What’s replaced all that is the evolution of three components: attractive graphics design, intelligent copy and typesetting, and the hyperlink. Who knew years ago that the separation between application and document would virtually disappear as the concept of software platform shifted from a window-centered one to a page-centered one?

Now some would argue that the familiar, repeatable consistency of a WIMP-based GUI with the OS and all applications sporting the same kind of interface is far superior to the Wild-West-anything-goes world of the Web. I say otherwise. People generally learn new Web apps far easier than they learn new desktop apps — because the UI is tailored to the content. Certainly there are plenty of recognizable UI elements that you find in most Web apps. Patterns are emerging, designers are following competitors’ leads. But the main strength of this new Web 2.0 world is in its very inconsistency. We’ve all had the displeasure of living in the “everything’s the same” WIMP world with the OS telling apps how to look and feel. It stank. We all ended up with confusing, bloated, and unreliable software. On the Web, people expect stuff to Just Work. And if people can’t make it work in 1 minute, they’ll go somewhere else. Confusing, bloated software withers on the vine.

If you think the WIMP GUI is just fine, thank you, and is totally unrelated to the explosive growth of Ajax-style Web apps, that’s your prerogative, but I maintain there’s a definite comparison here. Imagine Backpack, Flickr, Amazon, Google, Wordpress, del.icio.us, etc., done WIMP-style using a traditional Mac OS X or Windows XP interface. It’d look boring as hell and be damn hard to use. Seriously.

I, for one, welcome our new Web 2.0 overlords. I enjoy my time on the Web as much as I ever have using any desktop app. And that, my friends, is where the real UI innovation is happening. Microsoft, Apple (yes, Apple), Linux distros, and virtually everyone else are adding features to their interfaces that the Web has had for years. Ironic, isn’t it — a few years ago, people were trying to figure out how to make Web pages look like desktop apps. Now people are trying to figure out how to make desktop apps look like Web pages.

I love it.
