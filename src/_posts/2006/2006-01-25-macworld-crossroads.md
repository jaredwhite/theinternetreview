---
date: "Wednesday, January 25th, 2006 at 10:52 am"
title: "Macworld ’06: Crossroads"
category: archived
funny_caption: "Anticipating the “Jesus Phone” at"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20060621073548/http://www.theideabasket.com/2006/01/25/macworld-06-crossroads/"
---

The best word I could use to descibe what Macworld felt like earlier this month in San Francisco is *crossroads*. Apple is at a major crossroads, one of several in its astonishing and dramatic ~30 year history. The success of the NeXT takeover...oops, I mean aquisition...the iMac, the release of Mac OS X, and now the iPod has given Apple a great run over the last five years. And now, the bizarre but understandable switch to Intel chips sort of marks the end of an era and the beginning of a new one. Mac OS X is well established by this time and will continue on in "upgrade" mode for years to come. The iPod has moved beyond fad and into established consumer recognition which means Apple could rest on its laurels and still sell a bucketload for quite some time yet. Macs keep on selling at a solid and respectable unit sales number, and it's expected to climb somewhat as the Intel switch really gets underway. The company is rolling in dough right now and is the darling of Wall Street.

But what's next? What will Apple do next? There are only so many rabbits you can pull out of your hat until you run out of rabbits. The third-party companies at Macworld were thriving this year, and the number of people at the event seemed very substantial. But all of this excitment and innovation is only made possible by the brilliant minds at Apple ultimately; without Macs, without iPods, without OS X, none of the wonderful stuff shown off at Macworld would be possible.

Apple is at heart a computer company, and Steve Jobs is at heart an artist. He surrounds himself with highly creative people that like to ask questions, as Steve does, concerning why things are deemed as good as they can be. Why not take your best thing and make it even better? Look at the iPod mini. It was selling like crazy and everyone loved it. So what did Apple do? Cancel the product and come out with something even more amazing.

This is what must happen to the Macintosh platform. As good as OS X is, as good as Macs are, as well-excecuted as the latest Intel Macs are in particular, they're still just "better" versions of what is available in the PC world. Apple has to prove to the world that the Mac is a platform that can shoot far ahead of its rivals and start innovating in ways nobody anticipates. We're still using GUI metaphors that were developed in the 70's. We're still using filesystems and conceptual constructs that were developed in the 50's and 60's. We're still using input devices that are in many ways little changed from products designed in the 1800's. On the one hand, this stuff is proven and it works. On the other hand, computers could be *so much more powerful*in terms of user experience than they are now. We have enough under the hood power, yet our computer interactions have hardly changed in three decades.

Apple, don't just make a better PC. Jumpstart a new era of computer design that will show once and for all why the public can, and should, Think Different.
