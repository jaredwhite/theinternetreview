---
date: "Sunday, May 14th, 2006 at 6:30 am"
title: "To Innovate: Don’t Listen to Your Customers"
category: archived
funny_caption: "I'm Not Listening on"
icon: electricity-ani
archive_url: "https://web.archive.org/web/20060614123548/http://www.theideabasket.com/2006/05/14/to-innovate-dont-listen-to-your-customers/"
---

I feel like I'm posting on the Signal vs. Noise blog with this one, but what the hey. I was just reading a [TIME article on the Nintendo Wii](https://web.archive.org/web/20060614123548/http://www.time.com/time/magazine/article/0,9171,1191861-4,00.html), which to my non-gamer brain sounds like a great product, and I was struck by the brilliance of the philosophy behind the design of the Wii. From the article:

> But the name Wii not wii-thstanding, Nintendo has grasped two important notions that have eluded its competitors. The first is, Don't listen to your customers. The hard-core gaming community is extremely vocal--they blog a lot--but if Nintendo kept listening to them, hard-core gamers would be the only audience it ever had. "\[Wii\] was unimaginable for them," Iwata says. "And because it was unimaginable, they could not say that they wanted it. If you are simply listening to requests from the customer, you can satisfy their needs, but you can never surprise them. Sony and Microsoft make daily-necessity kinds of things. They have to listen to the needs of the customers and try to comply with their requests. That kind of approach has been deeply ingrained in their minds."

Wow, another company besides Apple that actually "gets it". As the article goes on to discuss, one of the main reasons the iPod rose to dominance was precisely because it eschewed the "more features and more power is better" school of product development in favor of "simple and fun and elegant is better". Nintendo is betting on the same philosophy to help them gain a lead on their arch-rivals in the console business. I believe it will work.

And I believe this is a good time to tell the truth about innovation: it doesn't work if you are letting your customers design your products for you. Don't give customers what they want, give customers what they need. If you can figure that out better than anyone else, you've won.
