---
date: "Saturday, February 25th, 2006 at 11:12 am"
title: "PHP Rants Miss the Point"
category: archived
funny_caption: "Hot Take for"
icon: terminal
archive_url: "https://web.archive.org/web/20081122055832/http://www.theideabasket.com/2006/02/25/php-rants-miss-the-point/"
---

There has been a recent flurry of posts in the blogosphere regarding PHP's suitability as a serious Web programming language. I don't have time to link to them all --- check out the latest news entries at [PHPDeveloper.org](https://web.archive.org/web/20081122055832/http://www.phpdeveloper.org/) for a good rundown. However, I'll try to distill them down for you:

-   Most PHP apps are spaghetti code.
-   PHP's standard libraries are a mess.
-   Other languages are more sophisticated.
-   PHP doesn't have any de-facto frameworks.

Concerning point number one, the reason I think for that is because many popular PHP apps have a multi-year history going back to early PHP 4 or PHP 3 days when the language was very rough and simple script-oriented, not to mention the fact that many different developers may have added cruft on top over time. That being said, there are well-designed PHP apps out there. I've seen them, and sometimes they aren't even OOP. Drupal is a good example. Wordpress actually is pretty good IMHO --- it's not only stable, but I've found it to be easy to customize (though its DB abstraction could be much better).

Anyway, on to point 2: I totally disagree with this. PHP standard libraries are very easy to understand, there's a simple function to do almost anything, and PHP's online manual IS. THE. BEST. ONLINE. MANUAL. IN. THE. WORLD. Bar none. The user comments alone are invaluable. So even if I forget a function name or can't remember the argument order, I can find out in about ten seconds.

Point 3: it is true that PHP is still missing a lot of high-end, super-nifty features that other languages have. The one I'm mad about is the lack of named parameters, and the core devs seem highly reluctant to implement it. However, my opinion based on several years of experience with PHP is that PHP 5 is not only light-years ahead of PHP 4, it's actually pretty darn perfect for most Web applications needs. I haven't yet discovered a break-the-bank missing feature that would require me to use a different language, and the time it would take me to learn a whole new environment could never make up for the possibly increased productivity I might end up having.

Point 4: when people compare PHP with Ruby, it's almost always bare-bones PHP vs. Ruby on Rails, which is hardly a fair fight. Ruby without Rails is a non-entity as far as the Web is concerned. Rails is what makes Ruby a PHP competitor, not Ruby. So to make a valid comparison on the other side, you have to compare a good PHP framework with Rails. There are a number of growing PHP 5 frameworks out there, and frankly I'm not totally happy with any of them, so I've been building my own. However, the ideal situation is that an official company with industry clout and extensive notoriety within the PHP community develop a generic framework that everyone can base their apps on or build specialized frameworks on top of --- that that's what Zend is doing right now. Their upcoming Zend Framework is really PHP's answer to Rails, .NET, and J2EE (even though Zend claims they're not trying to compete with Java), and from what I've seen of it thus far, it looks pretty hot. Time will tell how successful it is, but my feeling is that the reasons to choose Rails over a PHP-based solution will diminish as this year goes by and PHP 5 gains a more prominent foothold and more ISP support.

So there you have it: my little plug for PHP, a language that I love dearly and look forward to using for years to come --- warts and all. Comments and rebuttals are most appreciated.
