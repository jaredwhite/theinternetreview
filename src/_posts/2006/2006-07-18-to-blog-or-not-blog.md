---
date: "Tuesday, July 18th, 2006 at 8:51 am"
title: "To Blog or Not to Blog: That is the Question"
category: archived
funny_caption: "That Pesky Human Nature at"
icon: text42
archive_url: "https://web.archive.org/web/20060818165235/http://www.theideabasket.com/2006/07/18/to-blog-or-not-to-blog-that-is-the-question/"
---

Blogging is a very personal and very immediate form of media. So much so that I sometimes fear that bloggers can get too wrapped up in a cocoon of self-glorifying reflection. You see this when prominent bloggers bristle anytime blogging or the blogosphere receives any kind of criticism. Now I love blogs, and I get a lot of my news and commentary on a variety of topics from good blogs, but I think sharp, unbalanced attacks and out-of-control egos can be a real problem sometimes.

Of course, this is really all about human nature and nothing about blogging per se, but with all the talk about blogs bringing freedom of expression to everyone and turning information into a democratic, level playing-field, we have to ask ourselves this important question: what are we now free to express?
