---
date: "Tuesday, January 17th, 2006 at 11:17 am"
title: "Ah, So That’s What I’ve Been Doing All These Years!"
category: archived
funny_caption: "Making Buttons Click for"
icon: "photoshop-7"
archive_url: "https://web.archive.org/web/20060630103827/http://www.theideabasket.com/2006/01/17/ah-so-thats-what-ive-been-doing-all-these-years/"
---

I'm almost done reading [The Inmates are Running the Asylum](https://web.archive.org/web/20060630103827/http://www.amazon.com/gp/product/0672326140/qid=1137523719/sr=8-1/ref=pd_bbs_1/002-9278811-7415260?n=507846&s=books&v=glance) by Alan Cooper. I never picked it up when it first came out a few years ago, but someone recommended it to me last month so I finally gave it a go.

It's a pretty fascinating book, although if you're a programmer (and I am, to a certain extent) you have to have a very healthy sense of humor and a modest ego, because a large part of the book is spent insulting programmers. (I know Alan Cooper hates it when people say that about the book, but it's true. But that's OK --- most programmers really are horrible at user-focused design, so I agree with him.)

I don't always agree with the forcefulness of his arguments, but there is a number of things he's very right about. The main thing he's right about is that programmers write code and create software architectures, and visual designers make things look nice and friendly --- but all that gives you is a very nice looking piece of stable software that is still incredibly obtuse, hard to use, and insulting to the person using it. What you need is an interaction designer: someone whose sole job is to craft the interaction between the software and the user. We're not talking about how it looks, or how it works under the hood, but how it presents itself to a human being and how the communication between human and computer operates. Call this the user experience, if you will.

The fun thing about this book is finding out that I've been doing interaction design for several years and didn't know it! (Though I wasn't using his techniques per se.) While I do graphics design, pushing pixels in Photoshop, and while I do programming, writing code in BBEdit, my main focus has always been on making something simple to understand for a real person. Otherwise, I'm just wasting my time. I think that's due to the fact that I started out in Web design from the get-go rather than developing software (programming exclusively) or designing printed media (visual design exclusively).

A good Web designer is essentially a good interaction designer. If your site is poorly laid out and badly organized with confusing navigational schemes and infuriating forms, people will leave in a matter of seconds. Creating a great experience isn't just icing on the cake, *it's mandatory*. People will actually prefer boring-looking sites that are simple and fun to browse to gorgeous-looking sites that are really hard to deal with. Unfortunately, some horribly-designed sites are ugly and hard to deal with! Banking sites tend to fall in this camp more often than not. Or corporate intranets (shudder).

The bottom line is that you don't design software or Web sites for "users", you design them for human beings. So if you can imagine yourself as a human who cares nothing about technology but only about getting a certain set of goals accomplished easily and in as little time as possible, you're well on your way to creating a great product.
