---
date: "Tuesday, May 30th, 2006 at 8:27 pm"
title: "Dell vs. Apple: the Battle Rages On"
category: archived
funny_caption: "Continuing That Time-Honored Tradition of Apple Bashing on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20060614123508/http://www.theideabasket.com/2006/05/30/dell-vs-apple-the-battle-rages-on/"
---

Just when I thought Apple bashing was finally fading away from general computer industry discourse, some dude publishes an editorial on OSNews.com that is fascinating in its extreme bias, misleading premise, and un-factual conclusions.

[Read my rebuttal here](https://www.osnews.com/story/14763/apple-and-dell-business-models/#comment-129271) and let me know what your thoughts are. I fully admit I'm an Apple fan (I first switched in 2001 and haven't looked back), but I think any reasonable person has to admit Apple's doing well these days and Mac marketshare is slowly but surely growing. That someone would go out of their way to state that Apple's marketshare is in fact shrinking and its business model is an endangered species --- contrasted to the notion that Dell and Microsoft are in the prime of their lives --- is mystifying to me.

<em>[2024 Editor's Note: Nearly 20 years later, [according to some statistics](https://www.statista.com/statistics/267018/global-market-share-held-by-pc-vendors/), Dell is the #3 worldwide PC vendor, and the #4 is…Apple. The Mac is still arguably a niche platform at just under 10%, but it's an **extremely** profitable business for Apple, still setting the pace of consumer expectations in the PC industry, and of course comprises part of a larger ecosystem which has made Apple one of the most valuable and successful corporations in the world. So who was right in the end? You be the judge! 😉]</em>
