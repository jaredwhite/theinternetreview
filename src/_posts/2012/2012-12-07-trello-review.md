---
title: "Trello: Sticky Notes Reimagined for the 21st Century"
description: It's "in the cloud" so you can log in and use it on the web from anywhere.
archive_url: https://jaredwhite.com/articles/trello-free-web-based-project-management-software
---

<image-figure>
  ![Trello screenshot](https://res.cloudinary.com/mariposta/image/upload/w_2048,c_limit,q_65/izykcfvbqlmx0jbrbfed.jpg)
</image-figure>

**I am a project management software junkie**. Every few months I think to myself "there's gotta be a better tool to organize everything I'm trying to work on and figure out!" Usually I end up cobbling together a home-brewed solution based on common tools like calendars, reminder lists, note-taking, and so forth. Actually, I must say I've grown quite fond of Evernote, the much-hyped cloud-based notes app. But I digress.

What I've really wanted all along is a tool that combines the functionality of sticky notes, to-do lists, and comments & social sharing all together in one **easy-to-use, visually compelling package** where I can see everything that needs my attention at a single glance.

**Enter  [Trello](http://trello.com/)**. 

Trello is like sticky notes reimagined for the 21st century. Essentially, Trello is a collection of boards, each board featuring any number of "lists" you want. Each list can have a multitude of cards (they literally look like cards). Each card can contain a **surprisingly-rich amount of structured information that you see when you click on a card**. For example, a card can contain:

-   A title
-   A description
-   Comments from multiple users
-   One or more checklists (for tracking to-do's)
-   Votes from multiple users
-   File attachments
-   Colored and named labels
-   A due date

You are free to move cards around between lists or up and down on the same list for at-a-glance prioritization. As the company states, "in one glance, **Trello tells you what's being worked on, who's working on what, and where something is in a process**." Using Trello becomes addictive because somehow creating cards and moving them around on screen is so much more satisfying than scrolling through an endless list of entries in a box like most software. Oh, and **did I mention that Trello is free?** Yeah, that one really blows my mind.

How you set up Trello is really up to you. You can literally create whatever workflow fits your brain and your project. I have a Trello workflow at my digital media agency that suits me and the consulting nature of my business very well. I have lists for Inactive Clients, Active Clients, Sales, Internal Product Development, Marketing, Operations, Contractors, and so forth. That way, I am able to keep track of all the people and features are make up the moving parts of my business.

Before Trello, I literally had sticky-notes flying around my desk or I had to pour through a linear stack of notes stored in some other software. Now with Trello, I can literally bring up the app and just **look around the screen for a moment to be reminded instantly what I should focus on next**.

And did I mention Trello is **free**?

[Go check Trello out today](http://trello.com/). It's "in the cloud" so you can log in and use it on the web from anywhere. iPhone and Android apps are also available, and a native iPad app is currently in development. I await its arrival with baited breath. Fog Creek Software, it can't come soon enough for me!
