---
title: "Why Today’s “Fast Food” Startup Economy Isn’t All It’s Cracked Up to Be"
category: archived
funny_caption: "Growing at All Costs on"
icon: handwait-ani
archive_url: "https://web.archive.org/web/20160317040154/https://blog.mariposta.com/posts/slow-startup"
---

Paul Graham says [startup = growth](https://web.archive.org/web/20160317040154/http://paulgraham.com/growth.html). **I fundamentally disagree.**

Paul Graham is a smart guy, no doubt about it. Both he and Y Combinator have contributed much good to the tech/startup sector.  But, to be quite honest, a lot of the Silicon Valley hype bubble (and Paul Graham's Y Combinator is definitely a part of that) turns me off. I've been in the tech industry for a long, long time, and I've seen a lot of companies come and go. Many of the "gone" companies were once darlings of the media, on a trajectory of rapid growth heading towards giddy heights of shimmering success. Fast forward a few years (or a few months!) and it's like nobody ever heard of them.

Other companies seem to be growing and doing well but end up losing everything they stood for in the early days to achieve that growth. **Twitter is a prime example.**Pretty much everything Twitter is doing now is antithetical to its original mission. Twitter used to pride itself that its developer ecosystem and user community defined what Twitter would be. Hashtags and retweets were invented by users, not Twitter. Now Twitter thinks it deserves to control every facet of user & developer behavior and to shut down anyone not compliant with its protocols. Let's face it, there would be no [App.net](https://web.archive.org/web/20160317040154/http://alpha.app.net/) without the bad new Twitter. (Hmm, maybe I'm glad Twitter jumped the shark.)

**Yes, Growth is not all it's cracked up to be.**

Thankfully, I'm not the only one skeptical of the Graham viewpoint. [Mark Suster thinks there's more to startups than rapid growth and the VC lens that fosters such thinking](https://web.archive.org/web/20160317040154/http://www.bothsidesofthetable.com/2012/09/22/is-going-for-rapid-growth-always-good-arent-startups-so-much-more/). In his words, "It's ok to build a company that stays small, has a few million dollars in revenue and builds careers, bank accounts and enriches client experiences." Now lest we throw the baby out with the bathwater, let it be said growth is generally a good thing. If your business isn't growing, there's probably a major problem with how it's structured.

But I believe a startup isn't defined by its percentage of growth, but by the increasing amount of financial leverage created by its growth over time. In other words, if you can build a business that over a period of time brings in an exponential rate of return on profits, you're doing very well. That's why the SaaS (Software as a Service) business model is so attractive. If the jump from 2,000 customers to 4,000 customers doubles gross income but your expenses go up only a minimal amount (maybe at most you have to beef up your servers a bit and hire one more technical support rep), and the marketing budget required to achieve that growth is modest, you have a great business on your hands. That is, if you're profitable to start with.


As co-founder of Mariposta, a SaaS company in its infancy, I definitely want to get new customers, hire more people, and grow a successful and profitable business. But I'm not interested in hyper-growth, I'm interested in sustainable, long-term growth.

<em>[2024 Editor's Note: Mariposta was an iPad-centric content publishing platform I ran from 2012-2014. Alas, like many small businesses, it never took off…I blame myself for missing the early "progressive enhancement" wave of web development.]</em>

So here's my "radical transparency" disclosure:

-   I want a company that grows slowly, not quickly. Fast growth often leaves real people high and dry. (See: Twitter.)
-   I want a company that can be content with a few $mil in revenue. More is better, of course, but the dog-eats-dog chase after higher and higher valuations just isn't one I'm interested in.
-   I want a company that financially rewards all participants in meaningful ways --- in cold hard cash, not whizbang financial gimmickry.
-   I want to be CEO of a company where I can actually remember the names of the people working for me. 

I may not end up being the next Mark Zuckerberg or Larry Page, it's true. But if I end up being the next Jason Fried or Joel Spolsky, that's totally fine by me.
