---
title: "Mac OS X Mountain Lion? Purr-fect"
category: archived
funny_caption: "What’s Next? Sea Lion? at"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20120219054958/http://jared.mariposta.com:80/morphotype/os-x-mountain-lion-purr-fect"
---

Apple today announced [the upcoming release of the next version of Mac OS X](https://web.archive.org/web/20120219054958/http://www.apple.com/macosx/mountain-lion/), codenamed Mountain Lion. Which is rather funny, when you think about it (Leopard's minor update was Snow Leopard, so Lion's update is Mountain Lion). I am assuming this will be version 10.8, but Apple isn't saying.

So, rather than me spout off a bunch of feature information you can already learn about on Apple's site, let's take a broader overview of this news and its significance in the marketplace. First, this isn't the only major operating system to be upgraded this year. Microsoft is, of course, working furiously on its bottom-up "reimagining" of Windows, known simply as Windows 8. Now, unlike Mountain Lion, Windows 8 is not a minor upgrade. Windows 8 is massive. Windows 8 will essentially do away with virtually all of the interface metaphors and usage patterns of the platform that people know and...love?...and replace them with a new environment tailor-made for touch screens and mobile experiences (read: tablets). Windows 8 capitalizes on the innovation of the Windows Phone 7 platform and brings the Metro interface to the PC.

I've been very, very harsh on Microsoft over the last decade since I abandoned the Windows platform and switched to the Mac. I thought OS X ran circles around XP and have continued to cheer Apple all the way through the Mac's modern evolution alongside the iPod, iPhone, and now iPad era.

But I must give credit where credit is due. Windows 8 looks great. It literally looks great --- the Metro interface is unique and clever, and Microsoft for the first time in a really long time (perhaps ever) has an innovative selling point unlike any other company in the market. Like Metro or hate it, but you can't deny it is original. Microsoft is also making the gutsy move of pinning all new development efforts in the app space on the Metro-based platform. The Windows 8 store, the default PC desktop, browsing the web, email, etc. --- all using Metro. Yes, you will drop into a Windows 7-like environment to use certain apps like Microsoft Office and existing third-party Windows apps. But the future is Metro and everyone knows it.

So how does this all relate to Apple and OS X? Let's put it this way. By the end of this year, the world of computers (minus tablets) will be all about one thing: Windows 8 vs. Mountain Lion. That competitive landscape will be very, very intriguing because unlike any other time, OS X and Windows have almost nothing in common anymore.

Consumers will head to Best Buy, look around, and see PCs and Macs that work very differently. Ironically, the Mac interface may appear to be more familiar to previous Windows 7, Vista, or XP users than Windows 8. Will that give Apple an advantage? Or will Microsoft win back converts and encourage upgraders with the slick new touch-friendly gloss of Metro?

What Mountain Lion has going for it is sheer integration prowess with the iOS platform. With iCloud, iMessages, Notes & Reminders, folks who already own an iPhone, iPad, or iPod Touch will have a pretty seamless computing experience across all their devices. Work on a document here, it's already updated there. Get a text message there, it's also over here. Tap in a reminder on your phone, it's waiting for you on your desktop. Jot down a recipe on your iMac, and it's available on your iPhone when you're at the grocery store. Now it's clear this kind of cloud computing scenario is something Microsoft is going over as well, but Apple has the clear edge in that the number of iOS devices already in the wild is staggering. Windows Phone 7? A mere blip on the radar.

So, in my mind, that's the selling point for Apple. Consumer, you can take a chance on the strange new world of Windows 8...or you can get a Mac running Mountain Lion, feel comfortable using it right away, and enjoy a fantastic exchange of information and functionality between your computer and your phone or tablet. Microsoft = unknown, Apple = familiar.

Microsoft is going to have to do a really, really good job of educating the populace on the merits of Metro. Because if it fails to catch on in the public's imagination, the Redmond giant is going to have a major problem on its hands. Apple is currently on a roll with a clear vision that has been in play for years already. There's something to be said for consistency in the minds of consumers.
