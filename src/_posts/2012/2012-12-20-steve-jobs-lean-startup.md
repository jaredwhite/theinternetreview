---
title: "What the Lean Startup Movement Can Learn from Steve Jobs"
category: archived
funny_caption: "So…Make Everything a Cuboid? for"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20160304231158/https://blog.mariposta.com/posts/what-the-lean-startup-movement-can-learn-from-steve-jobs"
---

<image-figure>
  ![Steve Jobs](/2012/steve-jobs.jpg){: style="border: 4px solid transparent; border-radius: 4px; box-shadow: 0px 1svh 75px 10px color-mix(in srgb, white, transparent 70%)"}
</image-figure>

## MVPs, Pivots, and Validated Learning are all useful tools in a startup’s arsenal. But there’s something to be said for long-term, relentless execution of a singular idea.
{: style="font-size: var(--subheading-font-size-fluid)"}

In 1988, the year in which George H.W. Bush was elected President of the United States, Sonny Bono was elected Mayor of Palm Springs, Richard Stallman unveiled the General Public License (GPL), and Microsoft unveiled Windows 2.1 (technically, Windows/286 2.1 and Windows/386 2.1), Steve Jobs had some of his own unveiling to do.

**His company, NeXT, released the NeXT Computer --- also known as "The Cube."**

The NeXT Computer was revolutionary for its time. Just a few short years after the momentous events of Apple's release of the first Macintosh and Steve Jobs' infamous ousting by his own company, Jobs made a huge splash with a sleek, cubed, 1-foot-long on each side, black magnesium desktop machine. The NeXT Computer ran Unix, sported a fully object-oriented desktop UI, and included cutting-edge multimedia and software development features. The World-Wide-Web was invented on a NeXT Computer.

Sounds peachy, right? Actually, the reality was not so much: the NeXT Computer was a commercial failure and hardly the comeback Jobs was hoping for. It was far too expensive, the components were unreliable, and much of the connectivity and peripheral technology was simply too incompatible with emerging industry standards. In modern parlance, the NeXT Computer did not achieve product/market fit.

NeXT withdrew from the hardware market entirely in 1993 and refocused squarely on its OS platform, NeXTSTEP and later OpenStep. These technologies, through NeXT's acquisition of Apple (sorry, I mean _by_ Apple), became the basis for what we know today as Mac OS X and iOS.

## The Cube, Reborn

In 2000, shortly after the whirlwind rise of Apple as a major industry player in computing (again) due to the breakaway success of the iMac, [Steve Jobs unveiled..."The Cube."](https://www.youtube.com/watch?v=AwDOJ7HztXM) Wait, what? Another cube computer? Didn't Jobs learn his lesson the first time around?

Just like its predecessor, the Power Mac G4 Cube was a stunning piece of work. With 4" shaved off each side compared with the NeXT Cube, and housed in an enclosure of acrylic glass, this 8" cubed wonder was truly a sight to behold (and even became an exhibit at the Museum of Modern Art).

**It was also a big, big flop.**

Apple only produced the G4 Cube for one year and then discontinued the line. Just like its predecessor, this machine was too expensive, too underpowered, and had some production flaws. In other words, it did not achieve product/market fit.

It did spawn a loyal underground following, however, with a robust collection of third-party processor upgrades and other aftermarket add-ons continuing for some time after the model was discontinued. Even today, a G4 Cube in good condition on eBay can command several hundred dollars. But at the time, it was hardly the commercial success Apple, and no doubt Steve Jobs personally, had hoped for.

## Third Time's a Charm?

The year was 2005. George W. Bush was sworn in for his 2nd term as President of the United States. Three former employees of PayPal founded YouTube. Pope Benedict XVI became the successor to Pope John Paul II and the 265th pope. Deep Throat was unmasked as Mark Felt, a resident of Santa Rosa (my home town!).

And at Macworld Expo in San Francisco, [Steve Jobs unveiled...oh c'mon, not again?!?!](https://www.youtube.com/watch?v=GJpZGeihy0s)

**Dude. You really have a thing for cubes.** (Or in this case, a *cubiod*.)

Apple's latest invention, the Mac mini, bore an uncanny resemblance to previous generations of the Jobsian cube. A little smaller around the base (6.5 inches) and definitely less tall (only 2 inches), this version was decidedly miniscule for a desktop machine. While it looked cool, albeit not in the same way as the fine work-of-art G4 Cube, it certainly wasn't very powerful. With the 1.25GHz G4 processor and 256MB, the Mac mini was adequate at best. Was this destined to be yet again an underpowered "gimmick" model, like its two predecessors?

No, this time was different, and the reason was simple: **price**. The Mac mini started at $499. And a major selling point of the machine, in an uncharacteristic move for "we sell you the whole widget" Apple, was BYODKM -- as Jobs put it, Bring Your Own Display, Mouse, and Keyboard. It was a "stripped-down" Mac for the masses (and particularly the emerging Switcher market of disgruntled Windows PC refugees).

In short, the Mac mini was the Cube, repackaged in an even smaller (and simpler) form factor at a much lower price without any extra bells and whistles. Was this the magic formula that would propel Apple to stratospheric heights of unbridled success?

Well, the jury's still out on that one, but there's no question the Mac mini has been by all counts a successful product, selling well into the millions. And while in years past there has been speculation that Apple would discontinue the mini, recent updates since a significant design refresh in 2010 have propelled this product line into a high-performance, very capable computer that approaches speeds once reserved for Mac Pros. *Full disclosure: I recently purchased a brand-new, higher-end Mac mini and am frankly blown away by its power relative to my 2009 Mac Pro and 2008 MacBook Pro.* I daresay, from a bang-for-your-buck perspective, the modern Mac mini is one of Apple's strongest hardware releases.

<em>[2024 Editor's Note: 11 years after writing this, we've seen the Mac mini lineup go through some high points and low, but in today's era of Apple Silicon, it is a **dream machine**. I'm using one right now! In addition, it spawned a truly awesome professional desktop computer in the form of the Mac Studio (even closer to a pure cube in size!)—quite the legacy for this diminutive BYODKM hunk o' metal.]</em>

## What does all this have to do with Lean Startup?

My point is this: Steve Jobs exhibited a relentless, long-term drive to keep releasing products in a certain vein until they became hits. Failure was simply the signal to try harder next time. There are more examples than just the cube form-factor. The iMac itself, arguably, was really a 1998 take on the original Macintosh: a friendly, all-in-one computer perfect for consumers that wanted easy-to-use technology without having to read manuals and endure headaches following complicated setup routines. And another example of Apple doggedly pursuing a hard-to-crack market -- one we have hardly seen the culmination of yet -- is Apple TV.

I'm very concerned about entrepreneurs in the Lean Startup movement following the exact opposite approach: try out an idea, create an MVP (or just a landing page and some mockup images!), and if you can't make it work fast, pivot and try out some other idea. We have companies and products starting and ending so rapidly, it's making my head spin. Turntable.fm? Remember what they did before that? Yeah, I can't remember either.

Validated learning and all that goes with it are great for helping prioritize individual features in a product or perhaps deciding what isn't working and should be eliminated. Perhaps some of the Lean Startup techniques would have helped Apple avoid a G4 Cube-like scenario and arrive at the Mac mini sooner. But I firmly believe it's dangerous to extrapolate that process out to an existential level. If you need A/B tests and surveys to figure out what you're passionate about building, to arrive at a vision that will sustain you for years to come, then you have way bigger problems on your hands than the efficiency of the feedback loop of your MVP.

## Great entrepreneurs trust their instincts so fully they will move heaven and earth to find success for their idea.

You must discover an idea so compelling, so singularly pitch-perfect at its inception, that you will do whatever it takes to make it a reality. And if you fail, try again. And if you fail, try again. And if you fail, try again. I realize this kind of thinking simply isn't in vogue in the Valley these days, but all that says to me is that people are far too willing to believe "scientific shortcuts" will make their venture wildly successful. Sure you might be the next Apple or Google or Facebook with a methodical Lean Startup-like process of data analysis and prototype testing, but I really doubt it.

When I look at great entrepreneurs, I see people with fully-formed philosophies about *the way the world should be*, and their commercial idea is simply the productization of that philosophy. And just like how most people rarely discard deeply held beliefs, great entrepreneurs rarely discard their ideas simply because of a lack of "success" at any given moment.

Can you imagine a fledgling Steve Jobs without much of a track record walking up to a panel of investors and saying "Hey, I want you to invest in my new company. We're going to build this awesome little computer that looks like a flattened cube! Everything you need in a computer is in this little box. Just plug in a monitor, mouse, and keyboard, and Boom, you're done!" And they respond: "Wait, aren't you the guy who put out that other cube computer that was a big flop? And aren't you the same guy who released that original cube computer way back when that was also a big flop? What makes this time around any different? *(**Lahooooooser!**)*" And Steve says: "Um, well this time it's going to be a lot cheaper -- well, also less expandable I'll admit. And, um, well, that's about it I suppose." (Obviously this alternate reality Steve Jobs lacks an RDF -- Reality Distortion Field.)

Thank goodness the real Steve Jobs never gave up on his idea that there should be a computer on the market that has everything you need sans input/output devices in one little, incredibly cool-looking, box. That desktop computers could be more than giant noisy towers (or all-in-ones for that matter). That people would even be willing to switch from competing platforms just to get this box for their kid, or as a second computer, or perhaps even their first! Think about this: even with all he had going for him at NeXT, and later Apple, **it took almost two decades (17 years to be exact) to find the right formula for the computer-in-a-box**. 17 years?!?! That's an eternity in Internet time. Can you picture a modern-day startup whiz kid hanging around Palo Alto with an idea he's willing to try and fail at for 17 years? Yeah, I can't either.

<em>[2024 Editor's Note: the below reference to Evernote being a "100 year company" certainly didn't age well!]</em>

Here's the challenge, my startup friends: follow in the footsteps of Evernote CEO Phil Lubin and build a company that you believe will last 100 years. It might take you a while to get anywhere. You might run out of runway. You might have to go through some major ups and downs, sometimes big teams and sometimes just yourself in your bedroom. You might get a lot of slick investors shut doors in your face. You might be ridiculed in the press. But that's what being a real entrepreneur is all about. Remember, nobody will remember what you failed at at the end of the day if you end up succeeding. Steve Jobs, sadly, has left us. But what did he leave us with?

Apple. Pixar. Macintosh. iMac. iPod. iPhone. And a whole host of revolutionized industries that are changed forever. All the big flops Steve had? Products that truly sucked, real stinkers all? Who cares. Because Steve Jobs didn't just come out with the NeXT Computer. He came out with the Mac mini. When we think of Steve's legacy, do we remember NeXTStep?

**We remember iOS.** What's your iOS?
