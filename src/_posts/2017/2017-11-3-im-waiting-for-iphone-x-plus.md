---
title: "I'm Waiting for the iPhone X Plus"
date: "2017-11-03 15:12:51"
icon: rainbow-apple
funny_caption: 'Just Wait for the "S" on'
archive_url: https://pygmynuthatch.com/20171103/im-waiting-for-iphone-x-plus
---

I love my iPhone 7 Plus. I mean, I really, really _love_ it. I didn't care much for the larger size phone when it first came out in the iPhone 6 era, but the matte black finish on my 7 Plus looks incredible. I love typing on the large screen, and I can't imagine going back to the smaller keyboard and cramped navigation.

Which is why, as much as I'm impressed by the iPhone X (I mean, this thing is just gorgeous and packed with futuristic technology), I'm holding out for the next generation when Apple will presumably release a Plus version of this new form factor. After enjoying the wider screen real estate for browsing websites, jotting down notes, and reading documents, I can't ever go back to a narrower screen width.

However, fans of the regular size phones: rejoice! Trying out iPhone X today at the Apple store made it clear that this is far and away a better phone than iPhone 8. It feels like an 8 in the hand, yet you get a much taller screen (and of course a much better OLED display), dual cameras, better battery life, and all the rest of the features exclusive to iPhone X. If you ask me whether you should get an iPhone 8 or iPhone X, the latter wins hands down.

But if you ask me whether you should get an iPhone 8 Plus or iPhone X, the decision becomes murkier. If having the latest-gen tech and the cool factor of using the best model means the most to you, then by all means go with the iPhone X. If, like me, you've grown to love the larger, roomy Plus screens and don't want to go back to a narrower screen width, then you should get the iPhone 8 Plus—or just wait until next year. That's what I'm doing, although it won't be easy...the iPhone X is the most beautiful handheld device I've ever seen. Kudos to Apple on this one. It's a winner.