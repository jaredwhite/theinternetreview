---
date: "2017-04-18 11:03:14"
title: "Looking Ahead to WWDC and iOS 11"
icon: rainbow-apple
funny_caption: "Got Most of This…Eventually…Years Later…"
archive_url: https://pygmynuthatch.com/20170418/looking-ahead-to-wwdc-ios-11
---

It's that time of year again when Apple fans eagerly look forward to the panoply of goodies that will be offered at the Worldwide Developers Conference, taking place in San Jose, CA in June.

Here's what I personally find interesting this go around: when I contemplate the future of macOS, or iOS on the iPhone, I can hardly think of anything that I'm really hoping for. The Mac is a mature platform, and as macOS is such a powerful workhorse already, I feel like the desire for new features comes down to tiny things like the recent addition of Night Shift (which I'm a huge fan of). Regarding the iPhone, my 7 Plus is simply an incredible device with both hardware and software working together seamlessly in a way only Apple can deliver. For the most part, I don't have any complaints.

Which brings us to the iPad. Listen, I **love** my 9.7" iPad Pro (which I have mentioned numerous times here on Pygmy Nuthatch), but I agree with pretty much every Apple commentator out there that iOS on the iPad needs some major love. We haven't seen any pro-level features come to iOS/iPad since the introduction of split-view multitasking back in iOS 9. For a product that Apple claims is the future of personal computing, the pace of progress has been surprisingly slow as of late.

When thinking about what would allow me to use my iPad a lot more for the professional tasks I still use a Mac for, here are a few main suggestions:

* **Better multitasking.** The split-view side application switcher works in a pinch, but it's really not a very good experience overall. I'd much prefer a homescreen-style grid of icons (with my favorites pinned) I can use to switch to an app on the side. In addition, I would like to see some way to save "combos" of apps and switch between those (sort of like Spaces on the Mac) — so for example, I could have Ulysses and Twitter open in one combo, and then switch to Numbers and Safari in another.
* **Floating windows.** I'm not asking that iOS go full-bore Mac here and adopt anything-goes windows, but I _would_ like to see a way to pull an app out of split-view and have it float anywhere on the screen. Picture-in-picture videos already allow for this type of interaction, and I'd love to be able to do that sort of thing with a utility like a calculator, or a photo viewer, etc.
* **Terminal.** Now we're getting a little far out, but I would absolutely love to have a secure, sandboxed Unix environment I could drop into and run Ruby scripts, web servers, databases, and other command-line software. If such a thing were possible, I could do real web development directly on my iPad and not be forced to switch over to my Mac. I'm already able to get some work done occasionally using code editors and Git repo apps, but without any local servers running, my options to test code changes are limited.
* **External display.** If iOS is truly going to evolve into a pro OS for pro users on pro hardware, Apple is going to need to figure out how to translate the iOS experience to external displays. I would love to be able to plug a 27" monitor into my iPad Pro and get a larger view of all my apps. The challenge here of course is how do you interact with those apps? iOS doesn't have the concept of a mouse cursor because everything is a touchscreen. Perhaps when the iPad has an external display connected to it, there's some way to use the iPad itself as a fancy touchscreen "trackpad" of sorts to navigate the interface on the external display.

So there you have it: my top requests in descending order of likelihood for new iPad Pro hardware and iOS 11 functionality. We'll see Apple has up its sleeve come June. Hopefully they've not been sitting on their hands and are cooking up some cool surprises to wow us all.