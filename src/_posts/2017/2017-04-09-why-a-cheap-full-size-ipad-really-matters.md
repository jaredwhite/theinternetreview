---
date: '2017-04-09T12:11:14-08:00'
title: Why a Cheap, Full-Size iPad Really Matters
icon: rainbow-apple
funny_caption: "Product Line Complexity FTW!"
archive_url: "https://pygmynuthatch.com/20170409/why-a-cheap-full-size-ipad-really-matters"
---

## As an entry-level device, Apple's new iPad is a welcome step forward.

Apple recently released a new iPad—no, not the iPad we geeks were holding our collective breath for. _That_ iPad, a worthy successor to the iPad Pro ([see my review of the 9.7" model here](/2016/04/12/ipad-pro-creative-review/)), has yet to materialize. What Apple instead came out with was a cheap iPad, simply called **iPad**.

Apple...cheap? Historically those two words haven't gone together well. But that was the Apple of the Steve Jobs era. We now live in the era of Tim Cook's Apple, which seems far more willing to offer a variety of configurations and screen sizes for various device classes in order to hit a broad spectrum of price points.

The new suffix-less iPad is essentially an iPad Air 1 with slightly better upgraded internal components (such as the A9 CPU) as compared with the iPad Air 2 (A8X CPU). That means the iPad is slightly heavier and thicker than the Air 2, and the build quality of the display isn't quite as nice—although pixel-for-pixel the Retina display should look as lovely as ever under most conditions.

The main feature of this new iPad, however, is not its specs but its price. US $329 for a 32GB model, $429 for a 128GB model. The LTE cellular models add $130 on top of the base price.

Just as a reminder, when the iPad originally came out in 2010, it came with a starting cost of $499. Many people were surprised Apple even priced it that low at the time. However, the iPad hovered around $499 as its starting price ever since then—iPad mini not withstanding. (Personally I don't consider the mini-size of iPad to be a fully-fledged tablet computer.)

Fast-forward to the release of the 9.7" iPad Pro last year—the starting price for iPads seemed to be creeping upward, and the conventional wisdom was that Apple had resigned itself to the falling sales of the iPad lineup in recent years and decided to focus on boosting higher-end sales. The future of the iPad Air 2 line was thus unclear. Would Apple simply bump the specs up a bit and release an iPad Air 3? What would justify keeping the Air line around when the Pro line, only slightly more expensive, is clearly a better product?

Now the answer is obvious: the iPad has become a cleanly bifurcated category: iPad and iPad Pro. The iPad is still a great tablet, but it's mainly for casual & educational users, as well as other niche use cases such as POS machines, "smart clipboards" in various business settings, healthcare, and so forth. The iPad Pro is the flagship tablet that offers superior screen technology, better performance, and modular upgrades such as snap-on keyboards and of course Apple Pencil. I have no doubt Apple will drive the iPad Pro even further into "power computing" territory in future updates.

I can see Apple selling lots of these new, cheaper iPads as secondary devices, upgrades from much older devices, and in other contexts where low price is of great concern. I wouldn't recommend most people buy an iPad as their primary tablet, if they're serious about tablet computing. The iPad Pro is superior in every conceivable way. But as an entry-level device, Apple's new iPad is a welcome step forward.
