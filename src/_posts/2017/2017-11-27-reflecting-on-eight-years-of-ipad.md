---
date: '2017-11-27T12:04:54-08:00'
title: "Reflecting on Eight Years of iPad"
icon: rainbow-apple
funny_caption: "What’s a Computer…er, Tablet for"
archive_url: "https://pygmynuthatch.com/20171127/reflecting-on-eight-years-of-ipad"
---

Digging through the blog archives, [I found this blog post I wrote the day before the iPad was first announced in January 2010](/archived/2010/01/26/apple-tablet-rumors/). We're coming up on the eighth anniversary of the Apple tablet that reinvented the tablet computer and ushered in the age of iOS (even though iPad initially ran what was called iPhone OS until it was rebranded iOS in June 2010).

Reading my original post and remembering the incredible excitement I felt back then about how it could Change Everything, today I have mixed feelings about the iPad's legacy. On the one hand, the iPad *has* been a hugely influential and generally successful product. Recent advancements made to iPad in the form of the Pro line and iOS 11 have reinvigorated sales, and more computer users of all stripes are utilizing their iPads every day for both professional and personal use. Unlike for the iPhone, tablet competitors to the iPad never really took off, and it remains a product category unto itself and an important part of Apple's overall ecosystem.

On the other hand, the iPad arguably did not have much effect on web or software design. Efforts to launch media and apps solely for the iPad have either failed (Newsstand, The Magazine, etc.) or been relegated to relatively niche status. Web design was affected far more by the need to revamp for mobile touchscreens on iPhone and Android devices. At the mainstream level, the iPhone became a cultural phenomenon the likes of which we've never seen. The iPad never became that. "There's an app for that" transformed our lives through the delivery mechanism of the smartphone, not the tablet.

As a happy and enthusiastic user of an iPad Pro, however, I remain---like Tim Cook---bullish on the future of the iPad. I still believe that the tablet form factor is the de facto computer of tomorrow. Holding a large screen in your hands and manipulating objects on it directly with your fingers is simply the most natural way to interact with digital technology. There will always be a place for laptops and desktops, but the iPad is indeed the purest expression of the future of personal computing.