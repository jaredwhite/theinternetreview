---
date: '2017-11-06T12:04:54-08:00'
title: "iPhone X: Best Smartphone Display Ever"
icon: phone-atom
funny_caption: "Dealing in Superlatives on"
archive_url: "https://pygmynuthatch.com/20171106/iphone-x-best-smartphone-display-ever"
---

Over and over again, I've witnessed an assumption on the part of computer users and analysts that Apple uses parts from suppliers the same way any other widget maker uses parts. Therefore, any aspects of an Apple product such as an iPhone, a Mac, etc. that is superior to the competition is simply a matter of design aesthetic or software functionality.

But this is not the case. While it's true that Apple sources a lot of its hardware components from third-party manufacturers that also work with Apple's competitors, Apple often goes to great length to utilize those components in unique ways or requires the addition of Apple-specific optimizations to those components. iPhone X provides a perfect example. You'd be forgiven for assuming that because the OLED display in the iPhone X is manufactured by Samsung, it'd be basically the same as the OLED displays in Samsung's own phones.

**No so.**

> "The iPhone X is the most innovative and high performance Smartphone display that we have ever tested."
> [iPhone X OLED Display Technology Shoot-Out](http://www.displaymate.com/iPhoneX_ShootOut_1a.htm#Best_Smartphone) by Dr. Raymond M. Soneira

DisplayMate's extensive testing of the OLED panel in the iPhone X shows that Apple has put a ton of effort into the hardware and supporting software specs for this display. Even though Samsung is the manufacturer, this is not the same display you'll get when you purchase the latest high-end Samsung smartphone. It's brighter, the color is richer and more accurate, it has higher contrast and superior viewing angles, and it supports Apple's signature display features such as TrueTone to make the screen look excellent under a wide variety of lighting conditions.

So yes, kudos to Samsung's OLED manufacturing division. They've made something truly special. But Apple is equally deserving of praise for the part they've played in this remarkable breakthrough. Once again the slogan rings true: *if it's not an iPhone, well, it's not an iPhone.*
