---
title: "Review: Paint Shop Pro 4.1"
category: archived
archive_url: "http://web.archive.org/web/19961225074808fw_/http://www.sonic.net/~jwhite/reviews/psp4/index.html"
---

<h2>Introduction</h2>

<p><img src="/1996/pspreviewtitle.gif" border="0" alt="image showing the review title in a graphic format"></p>
  
  <p>As you can see, the graphic above was created with Paint Shop Pro (PSP). It took me about
  an hour to design, and a lot of that time was spent deciding where the graphics would be, how the text
  would look, etc. I used the brightness/contrast and horizontal perspective effects on the main
  background, I resampled both background images, and I used the cutout special effect on the text
  to make it look sunken in. But PSP doesn't just manipulate images. PSP is a complete image editor
  that combines features and flexibility with ease-of-use and a good, clean, simple interface.
  Besides the numerous effects, enhancers, color adjusters, and more, PSP includes quite
  a number of brushes -- just about all of which can be used with the many textures that come with
  PSP (water, clouds, sky, canvas, sidewalk), or with ones that you can create yourself! Also
  included in the package is a built-in image browser which really helps when you are looking
  through the images on your Web site (if you have a local copy).</p>
  
  <h2>The Main Window</h2>
  
  <p><b>Figure 1.0</b></p>
  <p><img src="/1996/PSPSSsm1.gif" alt="Paint Shop Pro 4.1 Screen Shot" border="0"></p>
  
  <p>In the above screen shot (which you can click on to get a larger version), you can see the main
  window in which you not only create and edit images, but also where the built-in image browser appears
  (not shown). You can have multiple images up at the same time (limited only by memory), which is
  handy for more than just being able to edit more than one image at a time -- you can also combine
  two different open images in several ways, which can lead to some pretty interesting effects (for
  example, combining one image with the red channel of the other image -- wild!). All the toolbars can
  dock on any side, or can just float in mid-air (or should I say, mid-screen?). Also present in the screen
  shot is the Histogram Window, which shows you the red, green, blue, and luminance factors of the
  currently selected image. We'll start off with a brief description of some of the paint tools, the selection
  tools, and finally, the image effects, enhancers, and color adjusters that make Paint Shop Pro really
  shine! (Please take note that not all of the tools and effects are possible in anything other than an 
  8-bit greyscale image or a 24-bit Truecolor image.)</p>
  
  <h2>Digital Paint</h2>
  
  <p>If you've got tons of money on your hands, you're going to want to use Corel PhotoPaint, Adobe
  Photoshop, or Fractal Design Painter for the most realistic pencils, pens, and brushes. But if you want
  an inexpensive, but powerful way to paint on your computer, PSP is what you're looking for. With the
  Paint Brush, you can choose from several brush types that are fairly realistic, such as pen, pencil, marker,
  crayon, chalk, charcoal, and more. What makes them special is that they all share the ability to paint with
  a <i>texture</i>, which you might use to paint clouds on a sky, or pretend you're drawing on
  construction paper. Another way to paint is to use the Airbrush. This tool doesn't have any brush
  types to choose from (of course), but it has an opacity setting (how transparent the application of the
  paint will be). This can be quite useful for using textures as backgrounds.<img align="LEFT" src="/1996/psptexex.gif" border="0" alt="" hspace="1" vspace="1">The reason the
  Airbrush is better than the Paint Brush for textural backgrounds is that the Airbrush gives the texture
  more of a misty quality, or a glowing quality (as in the image on the left), than the Paint Brush. Besides
  the Paint Brush and the Airbrush, there are also your standard set of tools, such as the Line Tool, the
  Shapes Tool, and the Flood Fill Tool. The Flood Fill tool is pretty versatile, because you can fill an area
  of one image with another open image! You can also fill an area with a number of different gradients,
  or you can just fill it with a regular solid color. The other main paint tools are the
  Clone Brush, the Color Replacer, the Retouch Tool, and the Eraser. With the Clone Brush, you simply
  right-click on an area of an image, move the cursor to another part of the image, and start drawing.
  The Clone Brush will take whatever is in the part of the image you right-clicked on -- relative to where
  you started drawing -- and paint it there. Amazing! The Color Replacer is another handy tool -- when
  you want to replace a color! It has a tolerance setting so you can replace more than just the one
  color you've already specified. The Retouch Tool is a reasonable way to touch up photographs -- however,
  it isn't as versatile as some of the other retouching tools in more expensive programs. Finally, the Eraser
  is a nifty tool, but it isn't what you would first think of it as. The Eraser tool actually <i>erases the last
  action you performed</i>, instead of just painting over your image with the current background
  color. You might not need to use it much, but it does give you more flexibility.</p>
  
  <h2>Selection Tools</h2>
  
  <p>Paint Shop Pro has some of the better selection tools I've seen around. All of the selection tools
  share the ability to <i>feather</i>, that is, make the selection a number of pixels bigger than what it
  really would be. It might not seem very useful, but believe me, it's great. I won't go into detail just now, but
  I've used it on many occasions. PSP includes a regular selection tool, with which you can select areas with
  rectangular, square, elliptical, and circular shapes. PSP also includes a freehand selection tool, so you can
  select areas with odd shapes, and the Magic Wand, which selects areas based on RGB color values,
  hues, or luminance (and the tolerance level that you specify). All the selection tools can have other
  selected areas added to them by pressing SHIFT, or deleted by pressing CTRL. You can also modify
  selections with commands on the Selections Menu, such as the overall opacity of the selection or the
  transparent color of the selection (when you select an area and want a certain color to be deselected).
  And, to top it off, you can save and load selection boundaries (which comes in handy when you've
  worked hard to get an odd selection area, and you want to be able to experiment with many
  different effects -- unfortunately, PSP only has one level of undo). Both the paint and selection tools
  are pretty versatile, but PSP's image effects are the main highlight of the program (as I've said before) --
  and in the tradition of the Miracle of Cana, I've saved the best for last.</p>
  
  <h2>Hollywood, Watch Out!</h2>
  
  <p>Well, not quite, but Paint Shop Pro does give you a wealth of various effects, many of which are
  pretty innovative. I haven't mentioned this before, but I feel that PSP is in its element with Web graphics,
  and the effects are proof. Many of the effects work hand in hand with the kind of flash that people look for
  in "cool" Web graphics, such as Add Drop Shadow (which makes text look 200% better on light
  backgrounds), Create Seamless Pattern (a must for creating your own Web page backgrounds),
  Cutout (which I used on the text in the image at the top of this page), Chisel (which makes the
  selected area look like it was chiseled out of stone), Buttonize (an effect that makes the selection
  look like a button), and, finally, Hot Wax Coating (which makes the image look like it was dipped in
  hot wax). Besides the above-mentioned special effects, PSP features many different
  <i>deformations</i>, such as ones that stretch the image into a cylinder, skew the image, add
  perspective, and many more. PSP also includes the normal sharpen, blur, and soften effects, as well
  as effects such as Dilate, Median, Emboss, Erode, Mosaic, etc. In addition to all these effects, PSP
  has a number of different color adjusters, including Brightness/Contrast, Gamma Correction (useful for
  calibrating photographs to look the same on your monitor), Highlight/Midtone/Shadow,
  Hue/Saturation/Luminance, and Red/Green/Blue. It also has several color effects, such as Colorize
  (which converts the overtones of an image to one color), Posterize (which reduces the number
  of bits in a plane, or makes images have more of a coloring book look), Solarize (which inverts all
  color over a certain threshold), and a couple of histogram functions. Last, but not least, PSP
  has all the regular image manipulators, such as Flip, Mirror, Rotate, Add Borders, Enlarge Canvas,
  Resize, Resample, and commands to increase and decrease color depths (such as converting a 256
  color image to a 16 million color image). The one main effect that I miss in PSP is an anti-aliasing
  function. You can anti-alias text as you put it in, but you can't just anti-alias a whole image. However,
  the way I get around that is, I try to design the image slightly bigger than I would like it to be, and then,
  after I'm done, I <i>resample</i> the image to the correct size. Resampling an image, as opposed to
  just resizing an image, softens an image to get rid of the quality loss when you increase or decrease
  the size of an image. It really works well when you import line art from a vector or meta graphic, and
  you want to anti-alias it before you put it up on the Web.</p>
  
  <h2>Wrapping Up</h2>
  
  <p>Contrary to what JASC says in their ads and on their Web page, Paint Shop Pro may not be the
  only image editor you will ever need -- nevertheless, PSP packs an amazing amount of features
  into a small, inexpensive package. I've worked with Corel PhotoPaint and Fractal Design Painter,
  and I still use Paint Shop Pro most of the time to create my images for iReview. Why? Because of
  PSP's fast, sleek, and easy interface -- and its intuitive way of working with effects and tools. PSP
  has come a long way from a few years ago, when, basically, PSP could only apply a small number of
  effects to an image, or convert an image from one format to another. I highly recommend downloading
  the fully-functional shareware version, and trying it out yourself -- and, hey, maybe it <i>will</i> be the
  only image editor you will ever need.<p></p>
  
  <hr>
  
  <p><b>Note:</b> If you need additional flexibility beyond that of Paint Shop Pro, I suggest getting
  Microsoft <a href="http://web.archive.org/web/19961226140646/http://www.microsoft.com/imagecomposer" target="_top">Image Composer</a>. Image Composer counteracts PSP's weaknesses, and
  visa-versa, to make a really good match for creating outstanding Web graphics. Together with Image
  Composer, PSP may very well be the only image editor you will ever need!</p>
  
  <h2>Product Information</h2>
  
  <table border="">
  
  <tbody><tr>
     <td></td>
     <td><font size="+1"><b>Paint Shop Pro 4.1</b></font></td>
  </tr>
  
  <tr>
     <td><b>Author:</b></td>
     <td><a href="http://web.archive.org/web/19961226140646/http://www.jasc.com/" target="_top">JASC, Inc.</a></td>
  </tr>
  
  <tr>
     <td><b>Software Category:</b></td>
     <td>Graphics Software</td>
  </tr>
  
  <tr>
     <td><b>Price:</b></td>
     <td>Shareware: $69.00</td>
  </tr>
  
  <tr>
     <td><b>Availability:</b></td>
     <td>Now</td>
  </tr>
  
  <tr>
     <td><b>Platforms:</b></td>
     <td>Windows 95 and Windows NT 4.0</td>
  </tr></tbody></table>
  