---
date: Mon, 06 Jan 2025 14:15:43 -0800
title: My Favorite Fediverse Bots for 2025
description: Because you can't have too many funny bots in your feed.
icon: fediverse
funny_caption: We Are The Robots on
---

One of the unsung heroes of the Fediverse (aka Mastodon and beyond) are all the great bots available for spicing up your feed. And no, I'm not talking about the nasty bad bots out there which try to slurp up all your content and steal your private details. I'm talking about content bots which humans set up to be fun and amusing!

There was a bit of a hiccup in the bot community late last year with the closing of the `botsin.space` instance where a number of bots had been located, but thankfully many of them were able to migrate to different instances and remain online. So I thought this would be a good opportunity to share some of my favorite bots. You can [download a CSV](/2025/bots-bots.csv) of the complete list and even import that into your own Mastodon account as a list! (instructions below)

Here are my top 8 favorite bots in no particular order:

* [Endless Screaming (@screem@bots.robots.rodeo)](https://bots.robots.rodeo/@scream) – AAAAAAAAAAAAAAAAAAAHHHHHHHHHH
* [levelbot (@levelbot@mastodon.social)](https://mastodon.social/@levelbot) – current existentialism levels are 57%
* [Inspirational Skeletor (@skeletor@mas.to)](https://mas.to/@skeletor) – We often give our enemies the means of our own destruction.
* [Back to the Future Bot (@greatscott@beep.town)](https://beep.town/@greatscott) – Since when did you become the physical type?
* [Fedi Everything! (@FediEverything@mastodon.moule.world)](https://mastodon.moule.world/@FediEverything) – The Fediverse should be powered by Jam Band Music!
* [Carmen Sandiego (@carmensandiego@mastodon.social)](https://mastodon.social/@carmensandiego) – OH NO! CARMEN SANDIEGO HAS STOLEN YOUR beanie baby collection!
* [It's The Weekend (@CraigWeekend@mstdn.ca)](https://mstdn.ca/@CraigWeekend) – Ladies and gentlemen…
* [Riker Googling (@RikerGoogling@mas.to)](https://mas.to/@RikerGoogling) – nipples itch after transporter

Those and over 20 others are all available [in this CSV](/2025/bots-bots.csv). Funny bots—what a concept! **Enjoy.** 😊

## Importing a List in Mastodon

When logged into your Mastodon account, click the **Preferences** link on the sidebar. Then in the new admin screen, click **Import and Export > Import**.

Select **Lists** as the import type, then choose the CSV file you wish to import. Make sure "Merge" remains selected so you don't accidentally overwrite anything.

Click the **Upload** button to begin processing the CSV. Then click **Back to Mastodon** and choose **Lists** to make sure the list you you imported is available.

It's possible you won't see any content in the list right away because you've just started following those accounts. Check back soon and you should start to see content populated there as well as showing up in your main feed.
