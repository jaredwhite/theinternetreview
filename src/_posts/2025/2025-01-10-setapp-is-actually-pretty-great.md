---
date: Fri, 10 Jan 2025 20:21:29 -0800
title: A Setapp Subscription is Pretty Great Actually
description: Given the quality and breadth of features I've seen so far with the apps I've used, I feel pretty confident recommending you give Setapp an earnest look.
icon: cdrom
funny_caption: There's an App for That!
---

I'd heard for a long time about [Setapp](https://setapp.com), either via ads on podcasts and the like or via people talking about using apps they'd downloaded through Setapp.

I honestly hadn't paid it much attention because I just assumed you _might_ get one or two apps out of it that are genuinely useful and the rest must be the detritus of an old "shareware" style store (add a signature to a PDF! measure something on your screen! _blah blah blah_).

Well…while there certainly is some of that sort of thing to be found in the Setapp catalog, I've been pleasantly surprised by how many truly awesome Mac apps you can get. (To be clear, dear reader, this is _not_ a sponsored post. I am simply writing this review as a genuine paying customer.)

The reason I initially gravitated to Setapp was because I was looking at getting [CleanShot X](https://setapp.com/apps/cleanshot), and while reviewing the company's pricing for the app I noticed it was also available via Setapp. That got me taking a closer look wondering if there might be anything else besides CleanShot X I'd be interested in using, and the more I browsed the Setapp catalog the more I realized this could be a truly great deal.

So far I've used the following apps in addition to CleanShot X:

* [Gitfox](https://setapp.com/apps/gitfox) – a Git client for managing software repositories
* [TablePlus](https://setapp.com/apps/tableplus) – a database client featuring compatibility with PostgreSQL, MySQL, and SQLite
* [Downie](https://setapp.com/apps/downie) – download media from literally thousands of Web sites…I originally got it for downloading videos off YouTube, but then I discovered I could download mixes off Mixcloud and that's _way_ cool!
* [Elmedia Player](https://setapp.com/apps/elmedia-player) – once you have audio and video files saved locally, how do you play them? This is the answer! (And the UI is far and away preferable to VLC…)
* [Meta](https://setapp.com/apps/meta) – add cover art and edit meta tags of audio files of all sorts (I'm using it to update ALAC files)

Just these alone would make me feel pretty satisfied with my monthly subscription (US $9.99), but I know I've only scratched the surface of what's on offer.

I realize many of us are suffering from "subscription fatigue" so it's not inconsequential to ask for yet another subscription to a grab-bag of apps, but given the quality and breadth of features I've seen so far with the apps I've used, I feel pretty confident recommending you give Setapp an earnest look.
