---
date: Tue, 25 Feb 2025 21:20:24 -0800
title: Framework Brings Real Excitement Back to Personal Computers
description: I was struck by how much I missed this adrenal rush of seeing a company really go for it.
icon: vista-network
funny_caption: Putting the Work in Framework for
---

I remember the heady days of the 2000s, when **Apple Computer** would push out model after model of their _computers_ on a steady basis with all sorts of extraordinary shapes, finishes, and advancements in usability and _desirability_. We all remember Steve Jobs strutting across a minimalist stage, giving another keynote for ages, pulling something out of an envelope or a pocket or out from under a drape. **What a time that was!**

I haven't felt that sort of excitement in a long time. The overly-produced "corporate Cupertino" bland vibe coming out of late-stage Tim Cook Apple [just isn't doing it for me](/2025/01/31/my-shifting-computing-habits-thanks-tim-cook/). I think I'm gonna hurl if I see another nausea-inducing "Genmoji" advertisement.

Now I fully recognize that **Framework is not Apple**, nor does it pretend to be—and obviously Nirav Patel, CEO of Framework, is not trying to channel some sort of half-baked Jobsian impression. But as I watched [the Framework launch event earlier today](https://www.youtube.com/watch?v=-8k7jTF_JCg) showing off the latest updates to the Framework 13 and 16 laptops, and particularly the first looks at a daring new Framework Desktop and a cute, colorful 12" convertible, I was struck by how much I missed this adrenal rush of seeing a company _really go for it_ and try industrial design that's new and different—something with that intangible _aha!_ factor.

Framework appears to be growing by leaps and bounds, and **they seem as surprised as anyone** that the Framework product design philosophy is resonating with so many people. I haven't yet used one in a real way, but I got to check out a friend's Framework 13 and it looks every bit as striking in person as it does in photos & videos. So I have no doubt if the Framework Desktop looks **truly killer** in a totally _computer nerd_ sort of way from the online demos, it will be impressive IRL.

As I mentioned in the link up above, I'm in the market for a new laptop this year and saving up to get a Framework 13 of my own to run **Fedora GNOME Linux**. But I'd be lying if I said I'm not feeling rather tempted to fish for an excuse to purchase that new desktop when it's eventually released in Q3. _Hey Kid…you need a new gaming PC or anything?!_ 😅
