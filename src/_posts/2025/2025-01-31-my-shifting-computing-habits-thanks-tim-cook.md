---
date: Fri, 31 Jan 2025 12:41:45 -0800
title: My Shifting Computing Habits in 2025 (Thanks…Tim Cook?)
description: I've been an Apple fanboy for nearly 25 years. That's all about to change.
icon: terminal
funny_caption: Getting Fitted for a Tux on
---

The **Mastodon -> Fediverse -> Linux** pipeline is real. 😅

But I'm getting ahead of myself.

When I first became a Mac user in 2001, there were two principal reasons:

1. Mac OS X had just been released, a truly modern operating system which looked absolutely stellar and—shocker if you were a Windows _or_ Classic Mac user—it was UNIX under the hood. (More on that in a moment!)
2. The brand new "TiBook" as it was affectionatly called (aka [PowerBook G4](https://www.apple.com/newsroom/2001/01/09Apple-Unveils-One-Inch-Thick-Titanium-PowerBook-G4/)), made every other laptop on the market look like a dinosaur.

The TiBook was my first Mac ever, and I was smitten right from the start. You must realize this was all a singular purchase for me: the MacWorld Expo in January 2001 was where Apple had announced the release date of Mac OS X 10.0 _and_ unveiled the PowerBook G4, and I wouldn't have gotten a TiBook without Mac OS X nor would I have gotten Mac OS X without stellar hardware to run it on.

You may not grasp just what a Big Deal it was in 2001 for Apple to have an entirely new operating system on the market based on Unix technologies. This was a long-awaited _and long overdue_ development, Apple having spent years in the wilderness before finally acquiring NeXT (and Steve Jobs!) and then spending yet more years figuring out what the future of the Mac platform should be.

Let me repeat the first portion of [Apple's January 2001 Mac OS X press release](https://www.apple.com/newsroom/2001/01/09Apples-Mac-OS-X-to-Ship-on-March-24/) because it's _wild_ to think just how much marketing buzz Apple expected to get merely by touting its UNIX bona fides!

> Apple® today announced that Mac® OS X, the next generation Macintosh® operating system, will ship on March 24, 2001 for a suggested retail price of US$129. Mac OS X is the world’s most advanced operating system, combining the power and openness of UNIX with the legendary ease of use and broad applications base of Macintosh.
> 
> Mac OS X features:  
> • an open source, UNIX based foundation called Darwin;

and it goes on from there. But think about it—just think about it. "The power and openness of UNIX" was _the very first marketing pitch_ for why Mac OS X was so cool! And _the very first bullet point_ in the new features list was that the foundation of Mac OS X is **open source**  and based on UNIX.

**This is why I became a Mac user.** Not because the hardware was way cool (though it was). Not because the software was way cool (though it was).

I became a Mac user because, while many of the user-facing applications and the UI were proprietary, the guts of the system was _open_. Suddenly you could install _all kinds_ of command line packages and libraries and tools that heretofore only Linux and BSD nerds were tinkering with. A Macintosh computer! _With a terminal!!_ Whodathunkit.

Install PHP, Perl, Ruby, Python, whatever you like. Heck, Apple **bundled** many of those runtimes into the system right out of the gate! They even attempted to capitalize on the popularity of then-shiny Java by allowing Java code to access Cocoa APIs.

People may look back at Steve Jobs and his instincts around open vs. proprietary and think of him as Mr. Proprietary, but in doing so they forget that the Apple of the late 90s into the 2000s regularly took steps to open up parts of their technologies, adopt open technologies (KHTML -> WebKit, Flash -> HTML5, remember?), and in many ways make **Microsoft** look like the nasty closed corporate behemoth in the room.

When the iPhone was first unveiled in the late 2000s, nobody batted an eye that it was much more locked down than the Mac, because it was (a) following in the conceptual footsteps of the massively-successful iPod, and (b) cell phones were viewed more akin to game consoles in that nobody saw them as "real computers". They were _devices_. Nobody would point to the Mac desktop on their desk running UNIX and call it a "device"…but an iPod and even an iPhone were certainly devices.

Then came the iPad in 2010, and that's where things started to get rather murky. Was the iPad a _device_? Or was it a _computer_? Should it be Mac-like? Or should it be iPhone-like?

**Here we are 15 years later, and those questions remain strangely unresolved.** Which points to a larger rot at the heart of Apple. Steve Jobs unfortunately passed away in 2011, when the iPad was still an underpowered baby of a computer, and everything that's happened since to the iPad and the Mac has been under the watchful eye of Apple CEO Timothy D. Cook.

**And what has he done to make Apple more open?**

What has he done to strengthen Apple's UNIX prowess and champion open source software?

What has he done to push the envelope of the Web forward via innovation in Safari & WebKit? In its early days, Safari was arguably the best and most innovative Web browser in the world, and I mean that quite earnestly. All of the later grumbling about how Safari had become "the new IE" and had fallen way behind Chrome/Blink, etc.? On Tim Cook's watch. All the conflicts of interest around Progressive Web Apps (PWAs) possibly impacting the value of native apps in the App Store? On Tim Cook's watch.

Now before I start to sound too grumpy here, let me throw Tim a bone. I think iPad hardware today is _phenomenal_. I'm still rocking an iPad Pro from a few years ago and it's one of my favorite pieces of Apple hardware of all time. Apple Silicon is so good, I'm still working away on a Mac mini M1 as my desktop Mac! And while I take issue with the "notch" designs that plagued iPhones for a while and still affect Mac laptops, overall I must admit the fit'n'finish of Apple hardware remains the envy of the world.

But while Apple hardware continues to impress, the software story is a whole other matter. The rollout of **Apple Intelligence** has been a fuckup of epic proportions. In a Jobsian world, all the people responsible for this sloppy shit would have been fired. Meanwhile, the UI of many aspects of Apple's core apps and OSes has been getting _worse_ not better, or simply languishing. Apple's weird attempt to bring iPad apps to the Mac via Catalyst was also largely a failure—and the theoretically revolutionary developer tech of **SwiftUI**, while successful in the aggregate, contains a flurry of sharp edges, limitations, and bugs _years_ after its initial debut (not to mention a number of SwiftUI controls just look plain ugly on the Mac for reasons I simply do not understand).

And don't get me started on the **App Store**, one of the most godawful "features" of the Apple ecosystem in recent memory. What started out as a handy way to purchase and download a handful of cool iPhone apps—and a natural evolution of the iTunes Music Store—turned out to be a nightmare of Orwellian control, vendor lock-in, and greedy capitalism run amuck. Apple, far from being the plucky upstart of the 2000s flexing its "open" software muscles, has now become as bad as Microsoft ever was—a monopoly desperately in need of regulation by antitrust rulings and legislation.

And this year, the straw that broke the camel's back: Tim Cook cozying up to that **fascist wannabe dictator** now in charge of the Executive Branch of the United States. Is this what Steve Jobs would have done? Really?

![Are we the baddies?](/2025/mitchell-and-webb-are-we-the-baddies.gif)

---

**I'm done.**

I'm not buying any more Apple hardware, at least not for the next few years. The core values, technological prowess, and futuristic user experience which first led me to Apple fanboyism in the 2000s is largely gone. And increasingly my reliance on the Apple ecosystem and the larger issues surrounding constant smartphone usage and Big Tech's insidious invasion of our local, personal lives has left me with a feeling of dread.

**This is not the computer platform I signed up for.**

And so I'm looking elsewhere. I'm hoping to make a few purchases over the next few months/years, and here's my plan in a nutshell:

* My next computer will be a [Framework Laptop](https://frame.work). I think what Framework has accomplished thus far is nothing short of a miracle, and it's literally the only PC company which has grabbed my attention. The fact they're intently focusing on providing excellent support for Fedora Linux is a sweet bonus, as I've been using Fedora GNOME in a VM on my Mac mini for months now and ready to make it this excellent OS my **daily driver**.
* My next mobile device may very well be a [Pebble](https://repebble.com). I haven't used an Apple Watch in quite some time (my OG Series 1 is in a drawer somewhere), but every time I think about buying a new Apple Watch I wonder why sticking a miniature iPhone on my wrist is the best idea. I never owned one of the original Pebbles, but if they can pull off a reboot of this concept in a cool "retro tech" fashion, I am _totally_ on board.
* My iPhone 14 Pro is still in mint condition, so I'll stick with that (and my iPad Pro) as the primary gateways to Apple world for now, but I could definitely see myself purchasing a smartphone capable of running Linux down the road. I know that side of things is not as polished as the world of desktops, but I'd be willing to tinker and experiment. (I'm *certainly* not running Android!)
* I've already been extricating myself from a total reliance on Apple Music and am in the process of rebuilding a local-first music library from scratch based on CD rips and purchased downloads. (I promise I'll be writing more about that very soon!)
* I already use Fastmail for my email and calendaring, so no need to rely on iCloud for that.
* I've kept using Dropbox as my file sync provider of choice…not ideal but it's been rock-solid for me for decades now so I'm reluctant to rock the boat.

What I still need iCloud for primarily is photography/videos, as I have a massive photo library stored there and moving away from that is not a top priority for me. But perhaps there will come a time when I need to investigate a solution that's self-hosted/open source.

So those are some of my current thought processes (all subject to change of course). I'm under no illusion I can "ditch Apple" any time in the near or even distant future, and I can't even say I want to. What I *do* want to do is leverage the hardware I already have which is all in excellent condition and perfectly usable, all while avoiding sending any more of my hard-earned money to Cupertino as much as I possibly can. Every penny I can send towards other providers of hardware and software who are more aligned with my values and my worldview is one I can feel good about.

**Congratulations Tim!** You've taken Steve Jobs' baby and turned it into a massive corporation of staggering proportions and a money-making machine the envy of all the titans of capitalism. I hope it was worth it to you that you've left people like me behind, people _who actually care_ about open source, open computing, and open developer access to platforms. (And who refuse to capitulate to American Nazis. That too!)
