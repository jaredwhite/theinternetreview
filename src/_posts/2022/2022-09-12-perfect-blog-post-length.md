---
date: 'Mon, 12 Sep 2022 16:23:48 -0700'
title: The Perfect Length for a Blog Post Is…
icon: articles
funny_caption: Counting Characters on
archive_url: https://jaredwhite.com/20220912/perfect-blog-post-length
---

…nobody knows. 😄

I literally searched DuckDuckGo for ["what is the perfect blog post length?"](https://duckduckgo.com/?q=what+is+the+perfect+blog+post+length%3F) and got a wide variety of different answers all on the first page. I suppose it entirely depends on the genre, the author, and the audience. In other words, perfection will be forever illusory.

So I took a look at what I'd written so far throughout the year on JaredWhite.com. For short ["thought" posts](https://jaredwhite.com/browse/thoughts/) like this, the average seems to be around 300 words. For longer [article-style posts](https://jaredwhite.com/browse/articles/), the average seems to be around 900.

Rather than leave it random chance, I'm going to try an experiment to see if I can keep the length of short posts a little bit shorter—say around 200 words—so that I'm more motivated to write and publish them, and conversely strive to ensure essays clock in at no less than 1000 words.

This is all part of my to design a more disciplined and appealing workflow for blogging. You'd think I would have figured this stuff out by now! 😂 (Always more to learn…)