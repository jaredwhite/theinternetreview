---
title: No (Open Web) Regrets
date: Sun, 13 Nov 2022 14:33:29 -0800
icon: fediverse
funny_caption: Reflecting on My Life, I Wish I'd Posted More on Facebook…NOT! 🤣 
archive_url: https://jaredwhite.com/20221113/no-regrets
---

**I regret a lot of things.**

I regret spending so much time contributing content to corporate social media. I regret expending my limited creative and financial resources all in the service of Big Tech.

But you know what I don't regret?

**Publishing content on my own website.** Yes, right here. And in other places I inhabit on the internet. And even on sites that no longer exist, because _thank you_ [Wayback Machine](/2013/05/10/thank-goodness-for-wayback-machine/).

It makes me think that, huh, perhaps I should spending more time publishing content in places I "own". Even if my website is technically hosted on a service I don't control, the content 100% belongs to me, and I can take it with me anywhere I want because [Cool URIs don't change](https://www.w3.org/Provider/Style/URI).

Maybe the open web would be in better shape if more people valued personal domain names as much as they value other things in life. I'm coming to realize [jaredwhite.com](https://jaredwhite.com) _[and now [theinternet.review!](https://theinternet.review)]_ is one of the most prized possessions in life.