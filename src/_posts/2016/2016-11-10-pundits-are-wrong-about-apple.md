---
title: The Pundits are Wrong about Apple. Again.
date: '2016-11-10 19:10:27'
icon: rainbow-apple
funny_caption: "Nothing New Under the Sun on"
archive_url: "https://pygmynuthatch.com/20161110/apple-pundits-wrong"
---

<image-figure>
  ![An iPad Pro with an Apple Pencil on a desk, circa 2016](https://res.cloudinary.com/mariposta/image/upload/w_2048,c_limit,q_65/IMG_0267_flrneg.jpg)
</image-figure>

## Which reminds me of another recent news event in which the pundits were catastrophically wrong...
{: style="font-size: var(--subheading-font-size-fluid)"}

I really should expect this by now. As an ardent enthusiast for all things Apple over the past fifteen years, I have been constantly taken aback by the level of hostility and vitriol directed towards Apple every time they come out with a major new product or significant upgrade.

The pundits and forum-dwelling nerds hurl accusations of all sorts: Product XYZ is underpowered. It's pretty but can't get any real work done. It's incompatible with (fill in the blank). It's overpriced. Apple's gouging. You can buy a (PC/Android/whatever) for $500/$1500 less! Apple doesn't care about professionals. Nobody's going to buy this thing. Jony Ive has lost his mind. [Apple's CEO should be fired][1]. And on and on and on it goes.

Then something curious happens: Apple sells a ton of Product XYZs and the actual people who buy the product love it. A year or two goes by, and the conventional nerd wisdom on the product subtly changes from _It Sucks!_ to _It's Cool_. Until the next big product rolls around, and we are treated to this bizarre ritual all over again.

<!--more-->
The latest case of Apple-fueled angst of course is the recently-announced 2016 MacBook Pro. If you only listened to the pundits and nerd outrage, you'd be forgiven for thinking that Apple's hardly going to sell any of these things. Surely they really, really blew it this time. And look! Apple just lowered the prices of their various USB-C adapters and the new LG 4K and 5K displays. It's a sign they're desparate! Apple's doomed.

Except that's not what's happening at all. Not only has Apple themselves gone on the records stating that preorders for the new MacBook Pros are off the charts, but now [independent e-commerce analysis][2] shows that, so far, the new laptops are selling like hotcakes. They're even blowing away sales of the smaller MacBook (the super-thin 12" one).

Why does this startling disconnect between the punditry and the actual behavior of most Apple customers remind me of another notable disconnect in the news lately? I'm talking, of course, about our recent U.S. presidential election. Listening to the pundits throughout the entire campaign season, over and over again we were assured Donald Trump had no chance of winning. He was a buffoon, an ignoramus, a total jerk. Maybe even Hitler. No way was he going to win the Republican primary. OK, well he just did that. But don't worry folks, no way is he going to win the general election. OK, well he just did that. Oops.

Now I'm not here to defend or praise Trump in any way (for the record, I voted for Gary Johnson). No, I'm simply pointing out that our news media is failing us. Too many so-called experts and elites think that their words and ideas are shaping public opinion, when the reality is that most people just aren't listening to them at all. In Apple's case, all the outrage at a lack of a 32GB RAM option, no legacy USB ports, etc., calls to fire Tim Cook, suggestions to buy a PC laptop instead, seems to be falling on deaf ears where the marketplace is concerned. In the case of the election, all the outrage at Trump's petty attitudes, outrageous statements, lack of informed policy positions, nefarious affiliations with alt-right groups, etc. seemed to have little effect on the electorate once voting day rolled around.

It's time the news media and vocal internet commentators came to grips with the fact that all their shouting and posturing really isn't doing much of anything. Certainly people are free to voice their complaints or be deeply concerned about any number of important things in our society. But don't mistake those opinions for some sort of conventional wisdom or consensus. Very often, the realm of punditry and the realm of actual behavior by the populace at large have nothing to do with each other. Take a step back and learn to recognize when the online echo chamber has created a disconnect between perception and reality.

[1]:	https://9to5mac.com/2016/11/08/jason-calacanis-tim-cook-rant/
[2]:	https://www.cnet.com/news/apple-macbook-pro-sales-already-set-to-surpass-2015-macbook/