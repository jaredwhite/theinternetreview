---
title: Happy Birthday iPad! First Impressions from 2010
date: Mon, 3 Apr 2023 08:58:49 -0700
icon: rainbow-apple
funny_caption: Ooo, the iPad is Now a Teenager! 
archive_url: https://jaredwhite.com/links/20230403/happy-birthday-ipad
---

<image-figure caption="It would be a while before it slimmed down and finally got a “stylus”." caption-right>
  ![photo of the iPad Pro with an Apple Pencil](https://res.cloudinary.com/mariposta/image/upload/w_2048,c_limit,q_65/xujiyaus9jly4vmzpgwq.jpg)
</image-figure>

A lot of people still remember the hype train and rumors flying left and right leading up to the announcement of the iPhone—the "Jesus phone" as some wags liked to call it. I too was very excited about the iPhone.

But not as excited as I was about the **iPad**.

[As I wrote in my 2010 hands on:](/archived/2010/04/02/ipad-hands-on/)

> There are many products that receive tremendous hype but when you actually
  use them, it’s a big letdown. The iPad is not one of them. I’ve been waiting ten
  years for someone to come out with a genuine tablet computer built from the ground
  up to offer a compelling user experience. Apple has delivered the goods. PC makers:
  you have a lot of ’splainin to do.

The iPad was hardly the first _tablet_ to get my heart racing. In the tumultuous period of the very early 2000s, Be had been attempting to pivot from marketing a PC OS to rival Windows and instead service the theoretical market of "internet appliances" — even naming their updated OS BeIA. BeIA was intended to run on a variety of form factors, some looking rather iMac (G3) inspired, but one looking essentially like an iPad with a few extra buttons (and an antenna!):

<image-figure>
  <img src="https://jaredwhite.com/images/BeIATabletPrototype.jpg" alt="BeIA Tablet prototype" />
</image-figure>

You can see a fascinating early look at various BeIA prototypes in this [YouTube video](https://www.youtube.com/watch?v=--bLSaBtYFs).

At the dawn of the millennium I had been a huge BeOS nerd, so you can imagine my excitement at the thought of a touchscreen, wireless, internet-capable [tablet running a variant of BeOS](https://jaredwhite.com/images/BeTabletComputerConcept.jpg). I simply couldn't _wait_ to get my hands on one once Be's hardware partners started shipping production models.

And then Be folded and the BeIA dream died. For roughly ten years—_ten!_—I remained lost in the wilderness. (Not really though, because I quickly dived headfirst into shiny new Mac OS X waters…kicking off my love affair with Apple with the gorgeous Titanium PowerBook G4. The rest is history…)

So when the rumors started making the rounds that Apple was working a tablet of its own later that decade, I was quite intrigued. Of course in those early days, most people assumed a tablet would run a touch-and/or-stylus-enabled flavor of Mac OS X, and we all saw plenty of third-party mockups of a "Mac tablet" to whet our appetites.

Then the iPhone landed, and suddenly the narrative began to shift. What if…just bear with me here…what if an Apple tablet wasn't running a stripped-down version of Mac OS X, but a beefed-up version of iPhoneOS? Using all of the touchscreen awesomeness of the iPhone experience?

And as we know now, that's **exactly** what happened. And my body was **ready** for it. (We later came to learn that Apple actually started development of the iPad first and ended up deciding to bring multitouch technology to market in the phone form factor initially with the intention of circling back around to tackle the tablet project.)

I was so incredibly excited about the upcoming launch of Apple's first tablet computer, I started a (short-lived) blog called iPad Artistry. [Here's a live report with a few photos of folks lined up at my local Apple Store at the time.](/archived/2010/04/03/ipad-day-on-location-santa-rosa/)

Once I had gotten my hands on an iPad, I quickly put it through its paces and—despite many obvious limitations—fell in love with the experience. It was only a year later when I embarked on a journey to revamp my "personal brand" and website development agency around "tablet-first computing". I ended up building a whole new CMS and website hosting platform from the ground up which launched in late 2012. I would spend the next couple of years trying to reach product/market fit and unfortunately never did so. [Mariposta](https://web.archive.org/web/20140811132849/https://www.mariposta.com/) was certainly an interesting product, but ultimately doomed to failure because the web never embraced "tablet-first computing" and instead went for the ultimately superior concept of "responsive design" — aka websites should look and function well on a wide variety of devices and form factors, scaling up and down as needed. (Apps too eventually went the responsive design route, and the dream of tablet-first app design and product marketing died a slow and painful death.)

So here we are in 2023, and while in some ways I'm disappointed the tablet ended up having far less of an impact on media and computing than I'd originally hoped (the laptop PC and the smartphone remain the canonical computing platforms for most people around the world), I nevertheless am extremely happy with my iPad Pro and use it every day to get real work done. The iPad is a fun product, a joyful product, and that I'm able to earn a living using it as a trusty companion to my desktop Mac is a noble conclusion to this story.

I still hold out hope that tablets will eventually mature into the "everyday computer" for the masses—more capable, powerful, and usable than a phone…more versatile and nimble than laptops featuring a far greater number of potential use cases and ideal scenarios. Certainly it's true my children use iPads _all the time_, and tablets are every bit as central to their lifestyle as any PC-style device.

In summary, **Happy 13th Birthday iPad!** I love you and can't wait to see what you're capable of next.