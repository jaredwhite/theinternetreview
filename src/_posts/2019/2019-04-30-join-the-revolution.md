---
date: '2019-04-30T15:41:37-07:00'
title: Join the Revolution!
funny_caption: "We're Just Getting Started on"
icon: fediverse
archive_url: https://jaredwhite.com/20190430/1
---

If all the people out there who complain about how "blogging/RSS/websites/whatever" is dead and we're all stuck with "Twitter/Facebook/whatever" would simply quit those services and use blogs, Mastodon, and other open web services, we'd be back to where we were before:

A **vibrant online content ecosystem** free of aggressive corporate dominance and a data-sucking ad-tracking hellscape.

It all starts with us: we the people. **Join the revolution!**