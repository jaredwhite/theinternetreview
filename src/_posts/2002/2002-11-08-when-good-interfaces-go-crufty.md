---
title: "When Good Interfaces Go Crufty"
category: archived
archive_url: "https://web.archive.org/web/20040229142147/http://www.theideabasket.com/modules/news/article.php?storyid=16"
---

By way of Slashdot, I found a really good article by Matthew Thomas on his Weblog called  [When Good Interfaces Go Crufty](https://web.archive.org/web/20040229142147/http://mpt.phrasewise.com/stories/storyReader$374) . It’s amazing how many of the points he brings up are ones I’ve been thinking about myself for some time. For example, I think in most cases there should be absolutely NO need to save what you’re working on. So you bring up a word processor and start typing away. Why do you have to save the document as you’re working on it? If you want to file it away, give it a custom name, etc., you can do so at anytime, but, meanwhile, you’re precious work won’t be lost in case of an emergency. And why do we have to use crummy little file pickers to open and save files? Can’t we just use a file manager to open a file and drag a document icon to an open folder to save it?

And, let me tell you, I’ve always hated the Start Menu. I put all my favorite application icons in the Quick Launch bar, or the Dock, or in a similar place in any OS I use. I’ve put the Applications folder in the OS X Dock just in case I need to launch something obscure, but that’s hardly better than the Start Menu. You’d think after over seven years of using bloated menu hierarchies to store application shortcuts, someone would figure out a better UI. I proposed a using a little two-pane folder/contents box similar to how e-mail clients are organized to the GNOME project, but nothing came of it. I guess deep menus *are* much better…

Do you think today’s mainstream interfaces are chock full of cruft? Or are they perfect paragons of human-computer interaction?