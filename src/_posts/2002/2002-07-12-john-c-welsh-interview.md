---
title: On Marketing Strategies, Media Ruckuses, and Maniacal Mac Heads
category: archived
archive_url: "https://web.archive.org/web/20020818031058/http://www.gaeldesign.com/ib/mw0702_john.php"
---

## An Interview with John C. Welch
{:style="text-align:center"}

I first came in contact with John on a mailing list run by Shawn King, host of the <a href="https://web.archive.org/web/20020818031058/http://www.yourmaclife.com/" target="_blank">Your Mac Life</a> Internet radio show. I was unfamiliar with his writings at the time, but since then I have followed both his regular column on <a href="https://web.archive.org/web/20020818031058/http://www.workingmac.com/" target="_blank">WorkingMac.com</a> as well as the technical insight he's provided on many occasions on Shawn's mailing list. John brings over ten years of experience in the business and enterprise worlds to the table, and has made a name for himself as being a champion for the Mac cause within the IT community. John is a talented speaker who has packed the hall at Macworld Expo on more than one occasion, and has written for many IT-oriented Mac publications, including <a href="https://web.archive.org/web/20020818031058/http://www.mactech.com/" target="_blank">MacTech</a> and the former MacWEEK.

On extremely short notice, John very kindly agreed to allow me to interview him in spite of his frenzied scrambling to get ready for next week's expo, so, without further adieu, on with the interview!

<strong>Jared: I know you're very busy getting ready to go to the upcoming Macworld Expo in New York, so I appreciate your taking the time to be with us here today. If you could name one word which you think describes the current mood of the Mac community, what would it be, and why?</strong>

John: The same word I always use...overactive. They love to get overly excited, overly mad, overly everything. They rather remind me of a bunch of baboons on horse stimulants.

<strong>[laughs] What a picture! But, no kidding, they act like the entire future of the company rests on how hyped up Steve Jobs' keynote is!</strong>

Well, yeah. The funny thing is, Apple isn't in the Macworld cycle anymore. There's a lot of product that gets announced as it's ready, particularly updates, although Apple's come out with some brand new stuff as well. This is really important, as the older Macworld-only cycle was hell for purchasing predictions, especially in education.

<strong>Agreed. I think Apple moving away from that cycle is a good business move. Speaking of business moves, it seems that Apple has been working hard lately to court the IT world as well as average Windows PC users. Do you think the business strategy Apple will be focusing on for this Macworld will be all about Apple's relevance in a PC-dominated marketplace rather than the "digital hub" strategy Apple has touted in the past?</strong>

Nope. They aren't really actively courting the IT world. They are just making product that works well in the IT world, which seems to be picking up on this all on its own. I think that while Apple will always focus on why people should go with 'the other guy' instead of 'the popular guy', the digital hub is the centerpiece of what they are doing, at least for now, and I don't see it being abandoned anytime soon.

<strong>I don't see it being abandoned either, but the marketing message Apple is now pushing to the PC-using masses with its new "Switch" ad campaign isn't specifically focused on the digital hub in my opinion.</strong>

It is and it isn't. The switch campaign is more of a superset of the digital hub in one sense, because it has to point out the ways that a Mac can play nice in a PC world. On the other hand, it's a subset, as most of the features of the Mac touted in the Switch campaign are a major part of the digital hub. I think they have more in common than not, but with different targets.

<strong>My feeling is that better interoperability with other computing platforms should be considered a part of Apple's digital hub concept, so you do have a point there. What new features in Jaguar (the next major release of Mac OS X) do you think will help Apple gain further acceptance in the enterprise as well as in small business environments?</strong>

Well, from what's been publicly commented on by Apple, things like SNMP, better remote administration, etc., really help in the enterprise. But they help everywhere you need to manage a bunch of computers — education for instance. Otherwise, they are just fixing stuff that's broken, refining the OS, and adding in some new features where they hope they will make sense.

<strong>I'm sure better Windows file sharing and VPN support will be important for the business market as well.</strong>

Oh they are, but they are also important in Higher Ed and SciTech
(commercial or not), which is still more important to Apple. Also, understand that especially in Higher Ed/SciTech, but also in K-12, a <i>lot</i> of the experience that Apple is getting for things like the Xserve, etc., <i>directly</i> apply to the enterprise market. But by somewhat avoiding the enterprise market, Apple can make mistakes in a friendlier environment. Obviously Apple has a lot more today to offer the enterprise market than say six years ago, but they aren't an enterprise computing company yet, and may not be for a few years, if ever. But if they keep releasing products like the Xserve and it's RAID follow on, they are going to get noticed by the enterprise market, and may find themselves as a player there, intentional or not.

<strong>Right. We'll keep our fingers crossed. Meanwhile, let's change the subject a bit. One issue I know you feel very strongly about at the moment is the so-called blacklisting of certain Mac news and/or rumor sites by IDG and Apple, who have denied the publishers of these sites press passes to Macworld Expo. In fact, you recently wrote a guest column on this very subject for <a href="https://web.archive.org/web/20020818031058/http://www.randommaccess.com/newspro/articles/1026267977.shtm" target="_blank">RandomMaccess</a>. Can you give us a brief overview of this situation as you see it currently, and do you think Apple or IDG should feel compelled to "make things right" in an attempt to stem the tide of negative PR?</strong>

IDG and/or Apple have decided that if you are going to print speculation that is only based on unsubstantiated statements from "unnamed sources", you don't get a press pass. Now, obviously, this only applies to the smaller sites. If the <a href="https://web.archive.org/web/20020818031058/http://www.nytimes.com/" target="_blank">New York Times</a> wants a press pass, they're getting one. So it's fairly tightly targeted at the <a href="https://web.archive.org/web/20020818031058/http://www.macosrumors.com/" target="_blank">Mac OS Rumors</a> of the world. It's kind of silly, but it's not violating anyone's rights. The loudest complaints are just idiotic...a press badge gives you a very small set of privileges, none of which are critical to writing good stories about Macworld. It's really more of an ego thing..."I can go here, and you can't". Big deal. If that's the reason you <i>need</i> a press badge, then you don't deserve one. I think that Apple and IDG are now in enough of a corner that anything they say will be spun to make them look bad, so they're quite sensibly riding it all out. Face it, most of these people are easily distracted anyway. <a href="https://web.archive.org/web/20020818031058/http://www.graphicpower.com/" target="_blank">GraphicPower</a> was shutting down until Scott McCarty realized he was getting major fame and play from this. So the site is now online again...and his ego is much happier these days.

<strong>I guess some people will do anything to get their fifteen minutes of fame.</strong>

That's a given...people eat live worms for that...so creating a rumpus over this is not a big surprise.

<strong>[laughs] Since I won't be going to Macworld myself, and therefore aren't in danger of being blacklisted, I'd say it's high time for some of the dreaded "S" word: Speculation! What do you think will be the most exciting product announcement to come out of Apple next week?</strong>

[laughs] Well, I'm the worst one to ask, as my favorite product announcement happened the week after the WWDC, namely the Xserve. I don't tend to get excited by keynotes, although I do enjoy the pomp they have. Quite frankly, I don't tend to think about it that much.

<strong>OK, fair enough. Personally, I can't wait — Jaguar can't come soon enough for me! Well, thanks again for letting me interrupt your crazy schedule to conduct this interview, and I hope you have a wonderful time at next week's Macworld Expo in New York.</strong>

No problem, glad I could help out. I don't know about <i>wonderful</i>, but definitely I'll have a busy time.

<strong>I'm sure you will. Take care!</strong>

You too!
