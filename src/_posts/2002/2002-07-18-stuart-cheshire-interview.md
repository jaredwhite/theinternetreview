---
title: "On Rendezvous, TiVo, and Parliamentary Titles"
category: archived
archive_url: "https://web.archive.org/web/20020802005938/http://www.gaeldesign.com/ib/mw0702_stuart.php"
---

## An Interview with Stuart Cheshire
{:style="text-align:center"}

<em>[2023 Editor's Note: what we now call Bonjour networking in Apple operating systems was originally called Rendezvous.]</em>

Soon after publishing my article entitled <a href="/archived/2002/07/01/rendezvous/">Rendezvous: It's Like a Backstage Pass to the Future</a>, an e-mail appeared in my Inbox from some guy named Stuart who was very supportive and helped me gain further understanding of a few aspects of the Rendezvous technology. It wasn't until I got to the bottom of his e-mail when I noticed his signature. Ha! No wonder he knew so much about Rendezvous — he was Chairman of the ZEROCONF Working Group and was employed at Apple! After exchanging a couple of e-mails back and forth, I asked him if I could interview him during Macworld, and to my delight, he agreed.

Stuart has been involved in a slew of computer science projects for the past number of years. He recently completed his Ph.D. in Computer Science at Stanford University, and holds B.A. and M.A. degrees from Sidney Sussex College in Cambridge, U.K. You can learn more about his background from <a href="https://web.archive.org/web/20020802005938/http://www.stuartcheshire.org/" target="_blank">his Web site</a>. One of his overriding goals is to make IP networking easier to manage and better suited for use with various kinds of computing devices, which is why he has been so instrumental in getting IETF ZEROCONF off the ground.

<strong>Jared: Thanks very much for agreeing to this interview in spite of your busy schedule and travel arrangements. You're actually in Japan at the moment, correct?</strong>

Stuart: That's right. I'm in Japan for the week-long <a href="https://web.archive.org/web/20020802005938/http://www.ietf.org/" target="_blank">IETF</a> (Internet Engineering Task Force) meeting. The IETF does most of its work via email, but three times a year we get together for face-to-face discussion. Generally two out of three meetings are in the United States, but the Internet is a worldwide phenomenon, not just an American thing, so usually at least one meeting a year is outside the USA.

At this minute I'm sitting in the IPv6 Working Group meeting with my 
PowerBook, answering your questions via AirPort.

<strong>Very cool! Now the ZEROCONF Working Group that is part of the IETF is responsible for developing and maintaining the open-standard Zeroconf networking protocols, dubbed <a href="https://web.archive.org/web/20020802005938/http://www.apple.com/macosx/jaguar/rendezvous.html" target="_blank">Rendezvous</a> by Apple. Can you give us a brief overview of these protocols and the history behind them?</strong>

The initial seeds of Zeroconf started in a Macintosh network programmers' 
mailing list called net-thinkers, back in 1997 when I was still a PhD 
student at Stanford. We were discussing the poor state of ease-of-use for 
IP networking, particularly the lack of any equivalent to the old 
AppleTalk Chooser for browsing for services. I proposed that part of the 
solution might be simply to layer the existing AppleTalk Name Binding 
Protocol (NBP) over UDP Multicast.

At the Orlando IETF meeting in December 1998 I discussed this idea with 
other people, and the following suggestion was made: trying to introduce 
an AppleTalk protocol into the IETF would not be easily accepted, but 
perhaps the existing IETF DNS packet format is semantically rich enough 
to hold exactly the same information as I proposed putting into an NBP/IP 
packet. I agreed with this suggestion — there's no need to invent a new 
IETF packet format just for the sake of it, if there's an existing packet 
format that can do the job perfectly well.

The IETF is generally populated by people who care very little for 
ease-of-use, but the Area Directors of the Internet Area were 
sufficiently far-sighted that they believed that improving ease-of-use 
should be an important priority for the IETF, even though that was very 
much a minority view back than. Even today, it remains a something of a 
minority view in the IETF. Most IETF people work for router vendors, 
ISPs, backbone providers, telephone companies, etc., and their focus is 
wide-area networking. If you work for a company that makes routers, 
you've not going to be very excited about technology that lets computers 
communicate directly, without needing a router. If you work for a company 
that sells a DHCP server, you've not going to be very excited about 
technology that lets computers communicate without needing a DHCP server. 
If you work for a company that sells DNS servers, you've not going to be 
very excited about technology that lets computers communicate without 
needing a DNS server. I'm sure you get the point.

Despite this lack of general enthusiasm, the Area Directors went ahead 
and arranged a preliminary "Birds of a Feather" session (BOF) to discuss 
these issues, under the name "Networking in the Small" (NITS). We had two 
NITS BOF meetings, in March and July of 1999. Peter Ford from Microsoft 
helped me co-chair those meetings, and we gathered enough interest to 
warrant the formation of an official IETF Working Group, under the new 
name "Zero Configuration Networking", in September 1999. At that time, 
Erik Guttman from Sun volunteered to co-chair the new Zeroconf working 
group with me, and he has been invaluable in helping keep the work 
on-track and moving forward for all this time.

The Zeroconf working group identified four requirements for "Zero 
Configuration Networking":

1. Devices need an IP address.<br>
2. Users need to be able to refer to hosts by name, not by address.<br>
3. Users need to be able to browse to find services on the network.<br>
4. Future applications will need to be able to allocate multicast addresses.

IPv6 already has self-assigned link-local addresses, but IPv4 did not, so 
we produced a specification for how IPv4 devices can obtain self-assigned 
link-local addresses.

For name lookup, we have general agreement that DNS-format packets sent 
via IP Multicast are the right solution.

For browsing, I worked out how to do the thing that was suggested to me 
back in 1998, and wrote a draft called "Discovering Named Instances of 
Abstract Services using DNS" (draft-cheshire-dnsext-nias-00.txt) which 
specifies how to do network browsing using just DNS-format query and 
response packets.

These specifications provide what we need to make dramatic ease-of-use 
improvements for local IP networking. However, these solutions do remain 
controversial with some IETF participants. Although Apple is already 
shipping Rendezvous, we are continuing to work in the IETF Zeroconf 
Working Group to continue the ongoing development of these protocols. 
Apple's intent is that as the protocol specifications continue to improve 
as a result of helpful intelligent discussion in the IETF community, we 
will be updating our Rendezvous implementation to benefit from those 
improvements. This is a fairly normal state of affairs — most 
communications protocols continue to evolve and improve over time, and 
good companies have an ongoing process of updating their implementations 
to benefit from those improvements.

<strong>Speaking of Apple, in addition to your role as Chairman for the ZEROCONF Working Group, you are also Wizard Without Portfolio at Apple Computer. I confess I have no idea what that means, so what exactly is it that you do at Apple?</strong>

It is a pun on the British parliamentary title, "Minister Without 
Portfolio", which is like "Senator at Large" in the USA, meaning someone 
with general responsibilities, not restricted to one particular area.
For me, what it means is that I try to make sure I'm always looking at
the big picture, not limiting my thinking to one particular narrow field.

<strong>Ah, I see. Steve Jobs at the Macworld keynote yesterday gave Rendezvous a significant spotlight during his Jaguar presentation. In particular, he demonstrated the integration of Rendezvous into iChat, iTunes, and several network printers soon to be released by Epson, HP, and Lexmark. This definitely proves that Rendezvous is just as useful for bringing plug-and-play, zero-configuration networking services to software and hardware devices as it is useful for networking computers themselves. Can you elaborate further on what kind of functionality Rendezvous brings to software and hardware devices that simply hasn't been possible in the past?</strong>

I can't comment on specific Apple product plans, but I think you had some 
very interesting ideas in your "<a href="me.7.1.02.php">Backstage Pass to the Future</a>" article. 
Rendezvous is not just about making current networked devices easier to 
use. It is also about making it viable to put networking (i.e. Ethernet) 
on devices that today use USB or Firewire, and it is also about making it 
viable to use networking in areas that you wouldn't have even considered 
before Rendezvous. Imagine a future world where you connect your 
television and amplifier and DVD player with just a couple of Ethernet 
cables, instead of today's spaghetti mess of composite video, S-Video, 
component video, stereo audio, 5.1 Dolby, Toslink optical audio cables, 
etc.

One of my favorite examples that I've been giving since the early days 
of Zeroconf is this: I have friends who have bought a TiVo Personal Video 
Recorder, and then liked it so much that they bought a second TiVo for 
the television in the bedroom. Now what is the problem? At night they 
turn on the bedroom television to watch a recorded episode of Seinfeld 
before they go to sleep, but they can't because it is recorded on the 
other TiVo. Imagine if any TiVo in your house could automatically 
discover and play content recorded on any other TiVo in your house. 
Sadly, I'm not aware of anyone from TiVo participating on the Zeroconf 
mailing list, so it may be a long time before we see anything like this, 
but I think you'll agree this would be a very cool product.

<strong>So you would say Rendezvous delivers almost FireWire-like ease of use for networked devices?</strong>

I would go further than that. My long-term goal, from before I even 
started at Apple, is to eliminate the need for disparate incompatible 
technologies on your computer. Right now your computer may have SCSI, 
Serial, IrDA, Bluetooth, USB, Firewire, Ethernet and AirPort, all 
communication technologies that each work a different way.

My hope is that in the future — distant future perhaps — your computer 
will only need one wired communication technology. It will provide power 
on the connector like USB and FireWire, so it can power small peripheral 
devices. It will use IP packets like Ethernet, so it provides your 
wide-area communications for things like email and Web browsing, but it 
will also use Zeroconf IP so that connecting local devices is as easy as 
USB or FireWire is today. People ask me if I'm seriously suggesting that 
your keyboard and mouse should use the same connector as your Internet 
connection, and I am. There's no fundamental reason why a 10Mb/s Ethernet 
chip costs more than a USB chip. The problem is not cost, it is lack of 
power on the Ethernet connector, and (until now) lack of 
autoconfiguration to make it work. I would much rather have a computer 
with a row of identical universal IP communications ports, where I can 
connect anything I want to any port, instead of today's situation where 
the computer has a row of different sockets, each dedicated to its own 
specialized function.

<strong>Well it sounds like you're working on some wonderful ideas and technologies there, and I'm very excited to see Apple adopting Rendezvous so readily for Jaguar. I'm certain this will prove to be an important milestone in the evolution of networked computing. Stuart, thanks again for being here, and I wish you the best of luck in your dual roles as ZEROCONF Chairman and Wizard Without Portfolio at Apple!</strong>

Thank you Jared.
