---
title: Random Thoughts on This Whole "Blog" Thing
category: archived
archive_url: "http://web.archive.org/web/20020818142752/http://gaeldesign.com/ib/be/#78107570"
---

According the fine folks at <a href="http://web.archive.org/web/20020818142752/http://www.blogger.com/" target="_blank">Blogger</a>, one of the great promises of the Internet during its early days was the fact that almost anyone could publish anything, anytime, anywhere. Unfortunately, unless you had the skills to be a Web designer, your Web site would be hard to create or manage — and it would probably look pretty bad. The truth of the matter is, running a Web site that features interesting content updated on a regular basis is no trivial task. Believe me, I've tried several times over the past few years to run a computer news site (this one isn't the first!), and it certainly takes a lot of time and effort, not to mention decent skills in the "geek" department.

But all that's starting to change. With blogs, you truly can publish anything, anytime, anywhere, because all you have to do fire up your Web browser, go to a certain Web page, type a bunch of text in a box, and click a button. And that's it! Because of its easy setup and low cost (usually free), you don't have to be a rocket scientist to get it working — unlike many other content management systems.

In spite of all this warm and fuzzy good news, however, there is a drawback to this push-button publishing world of the future. Are some people worth listening to? When you have hundreds of thousands, or even millions, of people putting whatever is in their minds up on their blogs, the likelihood of it all being interesting, or even well-written, is none too high. No offense, but I don't really want to know what some people are thinking. But then, no one told me I had to read their blogs, either.

It's a signal-to-noise ratio thing. More Web sites = more content. More content = more bad content as well as good content. Which means that it is really up to the search engines and site/blog directories to help us all find just what we're looking for. Otherwise, we'll be spending a lot of time wading through muck to get at the good stuff.
