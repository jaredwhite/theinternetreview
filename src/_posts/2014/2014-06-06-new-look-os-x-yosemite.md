---
title: The New Look of OS X Yosemite
date: '2014-06-06 19:10:27'
funny_caption: "Wow, Not Half (Dome) Bad!"
icon: rainbow-apple
---

Well, WWDC 2014 is drawing to a close, and with that comes reflection and contemplation about the impact Apple's announcements and new technologies will have on our lives as users and as developers. I myself am not a developer on Apple's platforms per se (not yet anyway), but I make my living as a web software writer using Apple products and thus the tools developers have at their disposal are of utmost interest to me.

Of greatest importance, as much as I love my iOS devices, is the Mac OS. **I use a Mac every day**, in fact I use more than one Mac almost every day (a Mac mini and a MacBook Pro). Thus every time Apple announces a new version of OS X, I prick up my ears.

**OS X is what originally converted me into an Apple customer**. I was a "PC switcher" before the concept became a phenomenon. I've used every version of OS X since the first public release in 2001.

Now it's 2014, and with that comes **the most radical change to the visuals of OS X since its inception**.

<image-figure caption="Photo credit: The Verge" caption-right>
![OS X Yosemite](/2014/os-x-yosemite.jpg)
</image-figure>

Here's my take on it (and the usual caveats apply when it comes to testing pre-release software):

* This is the first version of OS X designed for **Retina displays**. The change of system font to Helvetica Neue, the streamlined window styles, and other new/different effects being promoted, all make the most sense on a high-DPI display. On a standard display, the changes are less impressive, and in some cases a little strange. But on a Retina display, it's lovely. _Bottom line:_ those of us who have older non-Retina notebooks and care about visual quality are going to have to upgrade sooner rather than later.
* This is the first version of OS X truly designed from the ground up to comprise part of a **seamless Apple device ecosystem**. The integrations between iOS and OS X, buttressed by major enhancements to iCloud, are nothing short of astounding. Apple is banking hard on the fact that iOS users will find the idea of buying a new Mac to replace their PC compelling, and thus I believe **OS X Yosemite will contribute to a major uptick in Mac hardware sales**.
* OS X Mavericks proved in a technical sense that Apple still cared about the Mac platform despite the runaway success of iPhone + iPad, but there was still a question of if Apple cared as much about the usability and future innovation of the Mac experience as they do the iOS experience. OS X Yosemite indicates that Apple is indeed willing to throw significant engineering and design resources at the entire Mac software stack, and in fact go further in throwing resources at the development layer as well.

OS X Yosemite and iOS 8 together present Apple's vision of where computing is headed, and it's a vision that is quite different than any of Apple's competitors (namely Google and Microsoft). I believe this year's WWDC makes it abundantly clear that Apple is firmly the #1 computing platform company in the market today. **Only Apple can provide pro desktops, notebooks, tablets, smartphones, and a smart cloud that all work together in useful and delightful ways.** Microsoft can't do it: the Surface line of tablets is floundering and Windows phone is barely a blip on the radar, plus Microsoft doesn't even manufacture its own desktop PCs. Google can't do it: while no one can deny the success of the Android platform for smartphones, Google's forays into the PC market with Chromebooks never grabbed the public's attention. Amazon can't do it and likely never will: the tablets and upcoming smartphones Amazon makes are mainly screens intended to present Amazon-delivered content.

As much as we all miss Steve Jobs, time marches on. And I think it's safe to say that he left behind a company with incredible personnel, tight focus, and lots and lots of cash—and it's encouraging to see the company isn't willing to stand still and see others overtake its lead.

**Apple is back.**

_(originally published on the Echoes microblog)_
{: style="margin-block-start: var(--size-8)"}