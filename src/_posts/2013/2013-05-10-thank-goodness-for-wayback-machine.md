---
title: "Thank Goodness for the Wayback Machine"
icon: handwait-ani
funny_caption: "Gotta Get Back in Time for"
archive_url: "https://jaredwhite.com/articles/thank-goodness-for-the-wayback-machine"
---

<image-figure>
  ![Light rays](https://res.cloudinary.com/mariposta/image/upload/w_2048,c_limit,q_65/kuanlyvm6ed28wdhahqr.jpg)
</image-figure>

## If Google is Sherlock Holmes, Internet Archive is Indiana Jones

If you’ve been on the Internet a long time (I have since the early 90’s), you no doubt have left a long trail of content behind you. Websites, blogs, articles, photos, all sorts of things. If you posted items on social networks and platforms still in use, such as Facebook, Flickr, and so forth, it's still all available for viewing (albeit not always easy to get to!). However, if you go back far enough, it's highly likely your content is offline -- lost in the digital mists of time.

Unless you kept rigorous backups of all your web content in formats easily accessible on your modern-day computer (ahem, I didn't), you may think all is lost. Here's some good news: it may actually still be available! *How's that, you say?* It's thanks to an amazing, though perhaps not widely known, web service called the [Wayback Machine](http://archive.org/web/), provided by a non-profit digital library called **Internet Archive**.

## Put on your goggles and get ready to time travel

We're all used to finding stuff on the Internet using Google. Like Sherlock Holmes, Google stands ever ready to help you find exactly what you are looking for. However, Google's search engine is based on one simple premise: what it presents to you actually exists on the web, somewhere. In fact, the fresher the content, the more Google will like it.

But what if you want to find something that *doesn't* exist? What if you want to find an historical relic of a bygone age? Like Indiana Jones discovering an ancient tomb in the primordial jungle, the Wayback Machine lets you browse the Internet as it was in the past. Just type in an address of a Web site, then navigate through the calendar of "snapshots" (the various times when the Wayback Machine crawled your site and indexed it), and pick a day and time. V*oilà!* You are looking at the Web site as it existed at that exact moment.

It's fun to pick some of your favorite sites or blogs and check out what was going on years ago, but for me in particular it's most handy for finding my own stuff. As just one of many examples, [here's a tech blog I ran for a while called iReview back in 1999](http://web.archive.org/web/19990828044105/http://www.gaeldesign.adei.com/ireview/home.asp). The files for this Web site *might* be buried in a PC backup drive somewhere in my garage using a weird drive enclosure that I can't even connect to my modern-day Mac. Or they might be lost forever. I don't even know. But thanks to the [Wayback Machine](http://archive.org/web/), I'm able to uncover the evidence of my journalistic past.

## The permanence of our digital legacy is a real concern

This is a topic deserving of an entire blog post on its own, but the fact that so much of our lives is now lived in the digital world (smartphone photos, Facebook messages, email, electronic documents) -- and increasingly "the cloud" -- raises questions of how to deal with all that content in the future. 50 years from now, 200 years from now...will anyone be able to access any of this data? Yes, perhaps historians and researchers will figure it out if they're motivated enough, but will your grandkids? You may have photos of your ancestors right now in a shoebox dating from 1913...how will your descendants access that Android phone picture of you in 2113?

Here's hoping technology companies and academic organizations such as **Internet Archive** come up with better and easier ways to preserve your data in common formats -- and more importantly, keep public awareness high as to the importance of this issue. Meanwhile, I'm just grateful I can go back and read an old blog post of mine and realize my writing in 1999, um, Really. Sucked. 😅
