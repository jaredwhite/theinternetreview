---
title: "Your Next DVD: “Microsoft Inside?”"
description: Yet another indicator of the fact that Microsoft already wields far too much power over the tech industry.
category: archived
funny_caption: "HD-DVD Makes Me Blu(ray) for"
icon: cdrom
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/27/your-next-dvd-microsoft-inside/"
---

CNET News.com [reports](https://web.archive.org/web/20050308071637/http://news.com.com/2100-1041_3-5166786.html?tag=nefd_lede) that the steering committee for the [DVD Forum](https://web.archive.org/web/20050308071637/http://www.dvdforum.org/forum.shtml) has announced "provisional approval" for Microsoft's VC-9 codec (part of Windows Media) for a next-generation high-definition DVD format. Two other codecs are also being considered: the high-quality H.264 codec that was recently included in the MPEG-4 specification and the venerable MPEG-2 (at higher bitrates I imagine).

I have no doubt Microsoft's codec is good quality. However, this announcement really scares me, because it is yet another indicator of the fact that Microsoft already wields *far* too much power over the tech industry outside of the PC world specifically, and a full-fledged adoption of Microsoft technology in the upcoming HD-DVD format would allow Microsoft to gain yet another large revenue stream and a way to leverage their monopoly in emerging multimedia markets.

Typically industry-standard codecs have been co-developed by a number of companies, or at least developed by a single company already well-established and respected in the multimedia field. Microsoft is a relative newcomer that is already a convicted monopolist in its principal markets and whose chief interest in HD-DVD is to make lots of money off of it and give Microsoft a competitive advantage. I don't know about you, but I'm certainly not interested in seeing Microsoft take over yet another industry. When will the madness cease? Do we want a single company to be able to control all of the consumer technology of the future?
