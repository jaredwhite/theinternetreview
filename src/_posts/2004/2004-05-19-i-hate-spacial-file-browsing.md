---
title: "I Hate “Spacial File Browsing”…and I Have My Reasons"
category: archived
funny_caption: "Doubling Down on Columns at"
icon: folders
archive_url: "https://web.archive.org/web/20050318192822/http://www.theideabasket.com/weblog/archives/2004/05/19/i-hate-spacial-file-browsingand-i-have-my-reasons/"
---

I just read a [very annoying article](https://web.archive.org/web/20050318192822/http://www.bytebot.net/geekdocs/spatial-nautilus.html) that was linked to from OSNews. I knew it would be annoying before I read it, but I read it anyway. And I was annoyed.

Why? Because the author of the article, Colin Charles, attempted to argue the merits of a feature by saying that feature is wonderful and ground-breaking, therefore you should like it. And why, pray tell, is that feature wonderful and ground-breaking? Well, because, because it is, that's why! And John Siracusa [says so](https://web.archive.org/web/20050507155321/http://arstechnica.com/articles/paedia/finder.ars/1)!

Which means nothing to me because I never did agree with John's arguments for the so-called superiority of the spacial file browser. You see, spacial is nice when you have a small handful of files -- say, up to 20 or 30 or thereabouts -- but when you're dealing with hundreds of files in multiple locations all over multiple computer systems, it's a pain in the ass!

Some people prefer a single window in Icon View pared with a "Tree View" ala the Windows Explorer. That's not bad, but I truly believe the easiest and most efficient method of file management 95% of the time is the Column View in Mac OS X Finder (inherited from the former NeXTStep File Viewer). You see, that way you can actually see the contents of multiple folders in your hierarchy at the same time, and accessing any file or switching to any other folder anywhere in the hierarchy is as simple as a single mouse click. Coupled with Panther's wonderful and ground-breaking (hehe) Sidebar, I can access just about any file anywhere on my hard drive or on a network share with a few quick mouse clicks. All that and no window clutter either. The rare time when I need to sort files by something other than name, I just switch to List View for the folder temporarily. Works like a charm.

If you want to make certain files or folders stand out in Column View, just use labels. That makes finding certain things about 500% easier than manually arranging the on-screen positions of files and folders. You know how hard it is to clean up and arrange your desk or office -- and the number of items there rarely goes over a couple hundred. Now imagine arranging several thousand items. It's crazy. The thing most people will do is pick "Arrange by Name" or something and leave it at that. So now what you do have? A bunch of windows with big icons in them. What's so great about that?

And that's why I hate spacial file browsing. And that's why I'm annoyed every time some self-proclaimed UI expert goes on and one about how wonderful it is. It's not. It sucks. And I'll never use a file manager that is built around that metaphor! So there.
