---
title: "News.com: Who should govern the Net?"
description: Basic protocols and services that everyone relies on at all times should be open and standardized.
category: archived
funny_caption: "Logging into cyberspace on"
icon: phone-atom
archive_url: "https://web.archive.org/web/20041227215440/http://www.theideabasket.com/weblog/archives/2004/03/18/newscom-who-should-govern-the-net/"
---

Here is another case of a large commercial company with far too many ambitions in the wrong areas, and it's starting to cause strife in the industry. According to [this interview with Vint Cerf](https://web.archive.org/web/20041227215440/http://news.com.com/2008-1082_3-5174043.html?tag=nefd_lede), the organization ICANN, of which he is Chairman, is under fire from VeriSign on claims that ICANN is "standing in the way of innovation." Doesn't this sound familiar? (Microsoft's "Right to Innovate" campaign comes to mind.) Sorry VeriSign, I think the reality is that ICANN is standing in the way of obnoxious over-commercialization of basic Internet services -- which is a position I applaud whole heartedly.

There is a growing number of companies -- with Microsoft at the forefront -- that would love nothing better than to remake the Internet in their image (and their licensing fees). Thank God we have standards bodies and open source advocates that are willing to stand up to that kind of rubbish. I have no problem with commercialism and proprietary software, but basic protocols and services that everyone relies on at all times should be open and standardized, period. As the News.com headline asks, who should govern the Internet? Well, I can certainly tell you who *shouldn't* govern the Internet: any commercial company!
