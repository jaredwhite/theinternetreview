---
title: "iPod Mini Wins Editor’s Choice Award at CNET"
category: archived
funny_caption: "Colorful Aluminum for"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/20/ipod-mini-wins-editors-choice-award-at-cnet/"
---

[CNET reviews the Apple iPod Mini](https://web.archive.org/web/20050308071637/http://reviews.cnet.com/Apple_iPod_Mini__4GB__Green_/4505-6490_7-30657036.html?tag=dir) and gives it an Editor's Choice award. It seems that the "iPod brand has become synonymous with excellent portable audio players" and I'm excited that CNET and over 100,000 customers agree with that assessment. I still own an original 5GB iPod, and it's served me well all this time.

Juicy design quote from the review:

> Although it scarcely seems possible, we think the Apple iPod Mini's design surpasses even that of its photogenic older sibling. Its stylish, anodized-aluminum shell is so tough that we felt as if we could stand on the device without consequence. Apple constructs the body by hollowing out Mini-shaped aluminum tubes so that there are no seams in the construction, then applies the color during the anodization process so that it can't scratch off.

Wow. No wonder they looked and felt so amazing! (I tried one out at Macworld.)
