---
title: "Firefox: Mozilla Firebird Comes Into its Own on OS X"
category: archived
funny_caption: "A Very Good Web Browser for"
icon: firefox
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/10/firefox-mozilla-firebird-comes-into-its-own-on-os-x/"
---

Good news for all fans of alternative Web browsers (especially Mac users). Version 0.8 of the [Mozilla Firebird](https://web.archive.org/web/20050308071637/http://www.mozilla.org/products/firefox/) Web browser was just released for Windows, Mac OS X, and Linux. Only the name Firebird is no more -- Firefox it is from now on. When I tried downloading the Mac .dmg installer earlier, the FTP servers were getting pretty hammered, but once I found a suitable mirror, everything went like clockwork.

What do I think of the name...Firefox? Well, it's easy to get used to. I don't care for it as much as I did Firebird, naturally, but I can understand the need for an alternative name to avoid conflicting with the open-source Firebird database software.

The great thing about this release, from my point of view, is the really wonderful new Aqua theme (Pinstripe) that comes with Firefox by default. Not only that, but Firefox shows some serious effort on the developers' part to adhere to Apple's HIG specifications. For instance, if you close all Firefox windows (but keep Firefox running), switch to another application, and then click on the Firefox icon in the Dock, a new window is automatically opened. Previously, no window would appear -- a definite no-no in OS X-land.

Firefox is, IMHO, the first official, XUL-based Mozilla browser that really feels at home in OS X. I can now say with full conviction that Firefox is as good a browser on the Mac as it is on Windows. Which means that it is a very good Web browser indeed.
