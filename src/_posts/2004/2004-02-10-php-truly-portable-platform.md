---
title: "PHP: A Truly Portable Platform"
category: archived
funny_caption: "FTPing Text Files on"
icon: terminal
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/10/a-truly-portable-platform/"
---

I've been spending a lot of time lately copying PHP application installations from one server to another -- not the least of which is the [weblog software](https://web.archive.org/web/20050308071637/http://www.wordpress.org/)I'm using for The Idea Basket that you see before you now. I tweaked and refined the look & feel of the site via the template and stylesheet -- as well as set options to my satisfaction in the administrative control panel for the software -- all on my trusty ol' iBook. When the time came to transition the site to my hosting provider, all I had to do was export the MySQL database contents to a single text file, upload the software folder to the server, import the single text file into a newly-created database on the server, change just a few lines in one config file, and that's it! My blog was up and running on the new server. No muss, no fuss. (Of course, the Unix-underpinnings of OS X make the file transfer to a Linux-based server a pretty straightforward operation.)

The PHP experts out there might be laughing their heads off at my seemingly astonished reaction to this typical exercise, but have you ever stopped to think about how amazing this truly is? The \*AMP (normally it's LAMP: Linux, Apache, MySQL, PHP) is a truly portable application platform, and when you remove the M (MySQL) in cases of database-agnostic apps, the \*A\*P platform becomes even more flexible. Heck, if you really wanted to go bleeding edge, you could remove the A as well -- PHP can work fairly well with IIS and other popular Web servers at this point.

Sure, there are minor differences between PHP installations (and versions of the language), and the upcoming final launch of PHP 5 is going to muddy the waters considerably -- but the fact remains that we Web developers have a much easier time going cross-platform than our traditional desktop software brethren.
