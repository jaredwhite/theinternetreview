---
title: "PulpFiction: The RSS Reader of My Dreams"
category: archived
funny_caption: "Aggregating the News on"
icon: wireless
archive_url: "https://web.archive.org/web/20050318192304/http://www.theideabasket.com/weblog/archives/2004/05/09/the-rss-reader-of-my-dreams/"
---

I'm sorry, but I've never been that excited about RSS. It's boring. The readers/aggregators are boring. The hype is boring. I prefer to go to people's Web sites. It's visually much more interesting. My list of sites is nicely bookmarked or in most cases in my head (I have a pretty good memory for URLs). NetNewsWire and Shrook are good apps, don't get me wrong, but they just don't float my boat.

Enter [PulpFiction](https://web.archive.org/web/20050318192304/http://freshlysqueezedsoftware.com/products/pulpfiction/), the "Advanced News Reader/Aggregator for Mac OS X" created by Freshly Squeezed Software. I've been following its development progress on [NSLog();](https://web.archive.org/web/20050318192304/http://www.nslog.com/) for some time, but now that the product page is online and the release date is fast approaching, I wanted to say a few things about it. Bear in mind that I haven't used it yet, but I hope to soon (bwha ha ha!).

First of all, it uses Apple Mail for its interface inspiration. Which is just fine by me because I love Mail. I've always thought RSS should work more like e-mail, since both formats deal with time-sensitive information that needs to be manually organized for maximum viewing efficiency. Just sticking a million feeds in a folder and expecting me to know what to click on doesn't cut it for me. I might as well use regular Web sites and bookmarks (which is what I do now).

The appearance of the each feed's content is absolutely gorgeous, because you can use CSS/XHTML templates (either the included ones or custom ones you supply) that are displayed via WebKit (Safari's rendering engine). The result is an experience every bit as wonderful as the Web's, because, well, this IS the Web in a superficial sense. Utilizing the power of WebKit for custom information displays in OS X apps is something that I've long been waiting for (and am planning to implement in one of my own upcoming apps), and PulpFiction will make great use of this technology.

Lots of power user features (SQLite-powered database backend, labels, filters, flags, quick search, full RSS/RDF/Atom support, OPML import/export, and more) ensure that this is an app that will be hard to beat. I'm sure it will encourage a renewed surge of innovation in this space within the OS X market; in fact, I doubt there will be a better looking and slicker app on ANY platform when this is released.

My hats off to the folks at Freshly Squeezed Software. This will definitely be my first and best purchase from that company (sorry it took so long, Erik!).

**Update: It's here!** And it rocks. [Download it now](https://web.archive.org/web/20050206083455/http://freshlysqueezedsoftware.com/products/pulpfiction/). Oh, and one more thing: there's a freeware version available. Woo woo! 😃