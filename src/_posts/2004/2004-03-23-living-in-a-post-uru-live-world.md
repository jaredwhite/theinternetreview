---
title: "Living in a Post-Uru Live World"
category: archived
funny_caption: "We've Myst You Uru! from"
icon: myst
archive_url: "https://web.archive.org/web/20041227203926/http://www.theideabasket.com/weblog/archives/2004/03/23/living-in-a-post-uru-live-world/"
---

Attention all Myst fans: unless you've been living under a rock on Mars for the past several weeks, you know doubt know of the untimely demise of the online multiplayer component of Cyan World's groundbreaking new game [Uru: Ages Beyond Myst](/archived/2003/11/13/uru-one-age-at-a-time/). The closing of Live was a shock to all of us, but the one bit of good news announced durning that time (besides the very good news of the upcoming Mac release) was the development of future expansion packs that would continue the gameplay in lieu of Uru Live.

Now the time has come, the walrus said, to [download the first of the expansion packs](https://web.archive.org/web/20041227203926/http://mystworlds.ubi.com/US/) -- free of charge! Yes, in a wonderful display of generosity and "thanks" for bearing with Cyan/Ubi Soft during this period of transition, they've made "Xpack 1" available for free download (future packs will be cost a small amount of money, of course). However, the primary method of distribution they've chosen is Gigex, a game download site very much hated in some circles for its potentially invasive proprietary download manager. However, there are a number of mirrors of various sorts available -- be sure to check out the Uru forums for details.

After playing Xpack 1 for a while, I have to say I'm very excited about this new direction for Uru. While the cancellation of Live is still a very sad affair, the way in which the Uru "platform" will live on is quite encouraging. Here's hoping that Cyan's incredible vision for the Uru worlds can continue to be developed successfully for a long time to come.
