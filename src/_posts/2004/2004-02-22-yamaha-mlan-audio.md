---
title: "From Sonic State: Yamaha Debuts mLAN OS X and Network Drivers"
category: archived
funny_caption: "Routing Audio at"
icon: audio-gear
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/22/from-sonic-state-yamaha-debuts-mlan-os-x-and-network-drivers/"
---

For you pro audio lovers out there: Yamaha will soon be wrapping up development on an incredible new computer-based mLAN networking/patchbay software application that allows you to connect multiple mLAN devices together in a flexible networked configuration. In other words, just by connecting a few mLAN hardware devices (along with your Mac or PC) together using regular FireWire cables, you will have the ability to route any number of audio or MIDI channels (along with master word clock) to and from any number of devices. All in software, all in real-time -- no extra hardware cabling need apply.

The great news for OS X users out there is that Apple is building mLAN support directly into the OS via CoreAudio. In other words, mLAN is going to be completely integrated into the system ensuring the most stable and latency-free performance imaginable. We're talking 3ms or less here via FireWire -- a most impressive feat of engineering.

For more detailed information and an exclusive demo of this new technology, you can [watch the video at Sonic State](https://web.archive.org/web/20050308071637/http://sonicstate.com/news/shownews.cfm?newsid=1374) featuring Yamaha's Nick Howes. (Special thanks from me to Sonic State for hosting a QuickTime version of this video alongside RealPlayer and Windows Media.)
