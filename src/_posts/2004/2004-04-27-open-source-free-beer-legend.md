---
title: "Open Source is Free (Beer): An Urban Legend?"
category: archived
funny_caption: "Speaking Freely on"
icon: text42
archive_url: "https://web.archive.org/web/20050314201122/http://www.theideabasket.com/weblog/archives/2004/04/27/open-source-is-free-beer-an-urban-legend/"
---

An [interesting discussion](https://web.archive.org/web/20050314201122/http://nslog.com/archives/2004/04/27/open_source_mentalities.php) over at Erik Barzeski's blog [NSLog();](https://web.archive.org/web/20050314201122/http://www.nslog.com/) reminded me of my thought processes regarding the commercial viability of open source software that have occurred over the past few months. You see, Erik had a major bone to pick with a comment someone made regarding an [OSNews](https://web.archive.org/web/20050314201122/http://www.osnews.com/) article, which was this:

> The reason Open source is better, and walks the moral high ground is because we don't demand money for our work just that if you make our product better, you push it back out so that everyone benefits. We help each other, not fight each other. We know that you can build a better product if everyone helps.

Erik's main complaint was that this "moral high ground" attitude is basically full of \*\*\*\*. I don't blame him -- in fact I tend to agree -- but I'm also a huge supporter of open source software (OSS). However, even though most of the OSS I use is free as in beer, I've often thought OSS' main "selling point" (ha) was its "free speech" aspect. I never though all OSS should remain free beer as well.

Here's what I wrote into Erik's blog:

> I actually only have one complain with the original quote, and it isn't related to "morality" one way or another. It's related to the flawed assumption that open source software = free beer. It's not! Open source = free speech. Even Richard Stallman has made it clear, in terms of the GPL anyway, that you can charge any amount of money, as much as you want really, for the distribution of binaries and source code using the GPL. Of course, you can't stop someone else for distributing the software for free or less money -- but if you charge reasonable prices and provide decent commercial value for the money (tech support, attention to feedback, development speed, community involvement, etc.), then it's all right to assume most people will choose to obtain the software from you.
>
> Lots of companies are making money off of open source software. For some reason, I don't see a lot of "indie developers" using open source development methods combined with selling software for profit, but there's no reason why they couldn't if they thought it would be worth their while. The software I'm working on (don't worry, none of it will compete with FSS 😉 ) will be sold for a fee and will also be open source. I can hardly wait to see how it works out.
>
> Regards,
>
> Jared
>
> P. S. My response to the possible comment of "but if you make your software open source, other OSS projects can steal your features!" is that you can "steal" theirs as well. That's what's great about open source. It puts everyone on a level playing field.

As an aside, both PHP and MySQL, tools that benefit me greatly every day, are directly supported by commercial developers. Even though you can get the base products for free (which admittedly is all that I use), their value-added products and services cost money. And those companies are making it, too.

So I guess the bottom line is this: some software costs money and some doesn't (freeware). Some software is open source and some is closed. I don't seen the need to have a link between money/free and open/closed source whatsoever. Why can't developers be able to choose the type of licensing and pricing they like and have done with it?
