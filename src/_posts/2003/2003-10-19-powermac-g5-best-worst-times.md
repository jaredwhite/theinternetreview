---
title: "Power Mac G5 Update: Best of Times, Worst of Times"
category: archived
funny_caption: "Womp Womp on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20041211121710/http://www.theideabasket.com/modules/news/article.php?storyid=61"
---

As you know, I received my Dual 2GHz Power Mac G5 a few days ago, and I've been putting it through its paces. Let me just tell you now that it is a *fantastic* machine. I purchased the standard, "run-of-the-mill" model, with 512MB of RAM, ATI Radeon 9600 Pro video card, etc. Sure, it'd be great to have more memory, but for now it's all right. I'm sure I'll pick up another gig or so somewhere down the line --- and NOT from Apple. (Too expensive!)

But here's the bad news...I'm having some kind of power problem. After putting it in sleep mode overnight, I find it dead the next morning. Shutting it off by holding down the power button, and then turning it back on again usually doesn't work. I hear the chime sound, the power light goes on, but then nothing happens. No USB power, no video, no boot up. Sometimes I have to turn it off and on, or plug it in out of the wall/UPS several times before I can get it work. Last time this problem surfaced, nothing I did worked. I just now was able to get it to boot up --- the first time since yesterday evening.

So first thing Monday, I'm calling Apple. I'm also hearing a little "chirping" sound coming out of the power supply area and also out of the analog audio out (very audible in headphones). I've heard it might be a power supply problem, but I don't know for sure. I'll keep you posted.

It's sad this kind of issue has come up, since the Power Mac G5 is basically a great machine. Really, truly, a great machine. It's *incredibly* fast, looks great, OS X runs "like buttah" on this puppy --- after using my iMac G4 800MHz, the Power Mac feels like I've just traveled ten years into the future, speed wise. It's that dramatic of difference. I tried just about anything I could to bring the G5 down on its knees, and I couldn't. Nothing gets the better of this thing.

Truth is, I'm basically a happy guy. I love using such a sexy, powerful Mac. I just need to get this problem fixed, and FAST. So wish me luck --- Apple's gonna hear from me in just a few short hours.