---
title: 'Prediction: Future Version of MSN to Include "Microsoft Music Store"'
category: archived
funny_caption: "Bill Gates Making Music on"
icon: msn
archive_url: "https://web.archive.org/web/20040325210112/http://www.theideabasket.com/modules/news/article.php?storyid=39"
---

When Apple first unveiled the [iTunes Music Store](https://web.archive.org/web/20040325210112/http://www.applemusic.com/) before an eager, expectant public, my first thought was: "Damn! When's Microsoft gonna copy this thing?" Many fellow Mac heads were wondering the same thing. But I was reassured by a prominent journalist who regularly covers Windows news that Microsoft would *never* create its own music store; instead offering technologies that could be utilized by third-party retailers.

Well, guess what? After witnessing the success of Apple's iTMS (and watching the copycats start trickling in), it looks like Microsoft may indeed be pursuing such a strategy. In a recent meeting with industry analysts, Microsoft Chairman Bill Gates [let loose the news](https://web.archive.org/web/20040325210112/http://news.com.com/2100-1027-5055392.html?tag=nl) that Microsoft is looking into the possibility of offering its own online music store. Surprise, surprise.

My predication is that Microsoft, either exclusively or in close partnership with another company, will start offering online music downloads as part of MSN in the near future. Microsoft is slated to release a new version of MSN later this year, but I doubt they'd have a store ready that soon. But wait a while. Microsoft isn't a company to sit on the sidelines while other companies pass it by. If the pay-per-download online music biz really takes off over the long-term, you can bet that Bill & Co. will want to jump on the bandwagon *muy pronto*.
