---
title: "Noodles, Gardens, and the Metreon (a Travelog)"
category: archived
archive_url: "https://web.archive.org/web/20040117133033/http://www.theideabasket.com/modules/news/article.php?storyid=20"
---

## At the Start of MacWorld 2003

It was a pleasant, warm afternoon when I arrived in the City by the Bay. If the coldest winter Mark Twain ever spent was the summer he spent in San Francisco, then the warmest summer I ever spent was...well, you get the picture. Although I have to admit that I bumped into a fellow who mentioned that his sister spent New Year's in Arizona where it was 83 degrees. Not quite that warm here, but, still, I almost wished that I hadn't been wearing my thick wool vest.

Moscone Center is located in an up-and-coming area of San Francisco which has undergone a tremendous amount of urban planning and development in recent times. The whole square here is now being called "SoMa" — or, "South of Market". The Marriott resides in the north-west corner (more or less) of the square, and Moscone is in more of a south-central location. One of the most surprising aspects of my visit so far was exploring the Yerba Buena Gardens, an amazing urban oasis that may have been here for some time but I certainly never noticed it before! Tree-lined walkways, waterfalls, benches, gazebos, and an incredible view of the San Francisco skyline at night makes the Yerba Buena Gardens a spot to remember. And the oh-so-handy skybridge directly over to the Moscone Center makes it an easy walk to the expo floor.

The Sony Metreon is right next to the Gardens, and it's quite a trip. I didn't get a chance to explore all of the rides, the unbelievably huge IMAX theater, or the "Where the Wild Things Are" theme park, but I was able to have a nice, inexpensive dinner at a great Asian noodle place called Long Life Noodle Company (thanks for the tip, Dennis!). The architecture of the Metreon is quite "futuristic" — yet the sight of several old-fashioned street lamps in the food court just outside the noodle place did seem rather strange to me.

San Francisco is a hip, sophisticated, high-tech city, and SoMa looks like it's fast becoming the new downtown. The city planners should be commended for turning a run-down nowhere into one of the coolest places in the entire metropolis.

That's it for tonight — I'm off to bed to get a good night's sleep and I'll see you tomorrow during or just after the keynote!

'Till then,

_Jared White_