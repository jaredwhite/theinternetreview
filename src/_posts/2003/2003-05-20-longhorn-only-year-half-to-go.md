---
title: "Longhorn: Only a Year and a Half to Go!"
category: archived
archive_url: "https://web.archive.org/web/20031030185420/http://www.theideabasket.com/modules/news/article.php?storyid=32"
---

## Disclaimer

This editorial isn't directed at the many journalists, developers, and consumers who have taken a rational, objective view of Longhorn. Believe me, I have naught but the deepest admiration and respect for their coverage and comments. No, I'm writing this opinion piece to counter the growing onslaught of FUD that's percolating throughout the online tech community. If you've been unable to resist the mighty marketing machine of Redmond, then this article is for you.

## It's Not What You Think

It's astounding to me how Microsoft has completely and utterly flabbergasted, mesmerized, and stupefied, not to mention thunderstruck, such a significant audience when it comes to Longhorn, the next major desktop release of Windows. Of course it's going to be "the Best Windows Ever (tm)", but, to hear some people talk about it, you'd think that it's going to be the most incredible operating system ever designed and will leave the competition groveling in the dust. Mac OS X? Linux? Gone, baby, gone.

Not quite. The thing that I find most confounding about this whole rigmarole is that these people somehow think that Microsoft's competitors are going to sit around sipping tea and eating bonbons for the next *year and a half* before Longhorn is officially released. Yes, Longhorn won't be in our hot little hands until 2005, but it's already killed off its main competitors --- or so "they" say. Between the superlative, glowing reports of the Microsoft-loving press and the "watch out Linux, Apple is doomed, long live King Gates" ramblings of disgruntled former alternative OS supporters, it seems that far too many pundits have forgotten to switch their brains back on after drinking the Microsoft Cool-Aid. Apple Mac fans have often been accused of being taken in by Steve Jobs' "reality distortion field", but I think in this case the tables are turned. Consider this:

## Myth #1: Longhorn is Revolutionary

No, it isn't. Longhorn's database-like filesystem is simply a logical evolution of data storage and Web-like search capabilities, and Microsoft is already late to the game when you consider the benefits of the innovative filesystem found in the late-lamented BeOS. The new DirectX-based graphics rendering engine Microsoft will introduce in Longhorn bears a striking resemblance to Quartz Extreme, which Apple has been shipping in Jaguar since August last year. What about Longhorn's far more modular design compared to previous Windows versions? Hmm, sounds like UNIX to me. DVD Movie Making? Don't make me laugh.

## Myth #2: Longhorn Will Revitalize the PC Market

Everyone is looking to Longhorn now to "save" the ailing PC market because, guess what? Windows XP doesn't quite cut the mustard in that department. Too many users are still using Windows 2000, or, worse, Windows 98. Microsoft knows this, so they're trying to downplay Windows XP as being just a "simple, evolutionary" upgrade from the previous version of Windows, and now Longhorn is the polished and shiny new Windows that we've all been waiting for. There's only one problem. Microsoft's reputation is too well known. When Longhorn is actually released, and the quality and innovation are woefully behind the level of hype perpetrated by Microsoft's marketing department, then I imagine there's going to be a great deal of gnashing of teeth by more than one disappointed customer.

## Myth #3: Longhorn Means Death to DLL Hell, Blue Screens, etc.

That's what Microsoft said about Windows XP. Too bad it isn't even remotely true. Sure, XP is better than Win 9x, but it's still nowhere even close to perfect. Why should Longhorn be any different? Because all Windows programs will be written using the new .NET frameworks? (Dream on!) Because Microsoft will be forcing manufactures to certify and "clear" their device drivers with Microsoft before releasing them? Sorry, but Microsoft's already been doing that, and, not only is it a terrible burden on the manufacturers, but it hasn't helped that much anyway. Windows XP on a brand-new box with certified everything still has problems more often than not. If Longhorn fixes all this once and for all, why then I'll be hornswoggled.

## Face the Facts

Longhorn will certainly improve the Windows user experience, of that I have no doubt. And I can say in all sincerity that I believe Longhorn will go a long way in encouraging much more innovative hardware development by the major PC OEMs. However, the facts remain that Longhorn won't be coming out for quite some time yet, and, meanwhile, the competition is going to improve by leaps and bounds. Apple's on the verge of unveiling Mac OS X 10.3 "Panther", along with far more powerful hardware that will place Apple firmly in the pro market once again. In the Linux and BSD camps, new kernels and new distros are being released all the time, and the speed of development in the \*NIX desktop market has accelerated greatly in the past year. If Microsoft is betting that the Mac and \*NIX communities won't have a viable answer to Longhorn in 2005, then Microsoft is severely mistaken.

We haven't yet seen everything that will be in Longhorn. Microsoft's fabled "Aero" interface that will replaced the "XP-like" interface in the current alpha builds of Longhorn won't be shown until later this year. It's certainly possible that none of the mind-blowing aspects of Longhorn have yet been revealed. On the other hand, we know very little about Panther as well, so right now it's just a waiting game.

So please, while we're waiting, use your noggin before predicting the "doom" of Apple, Linux, or any other alternative OS. Every time any of the mainstream OSes leapfrogs the others in a particular area, the others all catch up sooner or later. That's what capitalism is all about, and the more diversity and open competition there is, the better off we consumers are. Thank you.