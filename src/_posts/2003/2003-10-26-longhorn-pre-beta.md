---
title: "Longhorn: Microsoft Shows Us the Ropes"
category: archived
funny_caption: "Playing the Longhorn Game at"
icon: vista-computer
archive_url: "https://web.archive.org/web/20041218041714/http://www.theideabasket.com/modules/news/article.php?storyid=68"
---

The official presentation of the latest pre-beta release of Longhorn, the next major version of Windows, won't take place until the start of the Professional Developers Conference (PDC) on Monday, but, meanwhile, eager Windows users have been checking out the leaked build that has been making its way around the Web for the past day or so. Screenshots are available at [Neowin.net](https://web.archive.org/web/20041218041714/http://www.neowin.net/comments.php?id=14694&category=main)and [WinBeta.org](https://web.archive.org/web/20041218041714/http://www.winbeta.org/winbeta/forums/index.php?showtopic=1036).

**Our Take:** OK, forget the ugly interface. It doesn't matter. We all know that Microsoft is working on Aero, which will be the next-generation Windows interface, not "Slate." So while it's tempting to make fun of Microsoft for putting out such a horrible UI, it's mostly pointless. The purpose of the PDC build of Longhorn is to introduce developers to some future Longhorn technologies --- NOT to demonstrate a real working version of the OS.

That being said, this build on the surface seems extremely boring. And those aren't just my words --- well known Microsoft apologist Paul Thurrott has said the same thing about Longhorn in his live [WinInfo](https://web.archive.org/web/20041218041714/http://www.winnetmag.com/windowspaulthurrott/Article/ArticleID/40623/windowspaulthurrott_40623.html) news feed. For a preview release that's supposed to get developers excited about the future of Windows and tide an anxious user base over for a while (users that may finally be thinking of abandoning ship and considering alternative operating systems), it doesn't seem to be doing a very good job.

But we haven't yet heard directly from Microsoft what's so great about Longhorn this time around, so we will just have to wait until the PDC starts on Monday before we can find out what is really going on here. After all, perhaps the old adage is true in this situation: you can't judge a book by its cover.
