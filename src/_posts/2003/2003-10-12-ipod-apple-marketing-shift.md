---
title: "New iPod Ads Signal Focus Shift for Apple Marketing"
category: archived
funny_caption: "All the Hip Cool People on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20041216154603/http://www.theideabasket.com/modules/news/article.php?storyid=56"
---

With the recent launch of [new iPod ads](https://web.archive.org/web/20041216154603/http://www.apple.com/ipod/ads/) for magazines, billboards, and TV, [Apple](https://web.archive.org/web/20041216154603/http://www.apple.com/) has shown once again that it's treading new ground with its advertising style.

Previous advertising campaigns, like [Switch](https://web.archive.org/web/20041216154603/http://www.apple.com/switch/) or the [original iTunes Music Store ads](https://web.archive.org/web/20041216154603/http://www.apple.com/music/ads/), focused on "real people" and how they like Apple products. Commercials like the one where the guy is blown out of his by the "power" of the G5 focused on the main selling point of the product itself. But now Apple has shifted to a new focus: young, hip, and cool. The focus is on both the product and the customer. Apple makes cutting edge, hip, cool products for cutting edge, young, hip, cool people. It's a message that will resonate soundly with a very lucrative market segment.

Teens and preteens are spending incredible amounts of money these days on "cool" name brand products. Several reports, [such as this one by Interbrand](https://web.archive.org/web/20041216154603/http://www.interbrand.com/start1.asp?id=143), show Apple has having very strong brand recognition overall, but that isn't good enough. Like Nike or The Gap, Apple wants to appeal to a younger target audience, and, to do that, Apple has to become the coolest, hippest, and sexiest tech company on the planet. For the same people that brought us the iMac, OS X, and now the iPod, it won't be too difficult. Apple CEO Steve Jobs is already well-recognized as one of the coolest technology mavens in the business --- but in more of an "aging hippy who likes the Beatles" sort of way. For today's kids who listen to hip-hop and shave their heads, that isn't quite the right sort of cool.

The new iPod ads are the right sort of cool. With shocking teen-friendly colors, high-contrast black and white silhouettes, and (in the TV spots) cutting-edge pop music, this new marketing campaign hits the spot. And the important thing to remember is that this is just the start of the campaign. More TV ads are rumored to be on the way, and, once the iTunes Music Store for Windows is released, you can be sure that Apple is going to get this new message out there like nothing else since the original Switch ads. Frankly, Apple may very well see more success come out of this campaign than Switch. Very exciting.

Stay tuned as we continue to cover the latest iPod and iTunes news right here at The Idea Basket.
