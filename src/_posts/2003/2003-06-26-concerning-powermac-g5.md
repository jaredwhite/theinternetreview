---
title: "Concerning Apple's New PowerMac G5"
category: archived
archive_url: "https://web.archive.org/web/20031231025845/http://www.theideabasket.com/modules/news/article.php?storyid=36"
---

Amidst the media hoopla and sprawling controversy over the relative performance benefits of the latest Penium/Xeon/Athlon/Opteron-based PCs vs. the recently announced [PowerMac G5](https://web.archive.org/web/20031231025845/http://www.apple.com/powermac), a number of interesting details about the new Apple machines have gone more-or-less unnoticed.

## Item #1: USB 2.0

I'm amazed that this isn't making front-page headlines. Apple has finally added USB 2.0 to the Mac, which is probably due to the fact that FireWire 800 is starting to establish itself and FireWire in general is obviously in no danger at this point of becoming a second-class citizen to USB 2.0 on the Mac platform. So what I'm wondering is this: what USB 2.0 devices will work on the Mac now? Are there drivers available? Does Mac OS X come with USB 2.0 drivers already? Or will the next point release of Jaguar and/or Panther ship with them included? Ah, so many questions, so few answers....

## Item #2: Has 5.1 Dolby Digital DVD support finally arrived on the Mac?

A suspicious new addition to the PowerMac line-up of ports is the new optical digital output, built for use with special Toslink cables. Apple claims it now supports 5.1 surround-sound speaker systems and even sells a set of Logitech speakers in its online Apple store as proof. But my question is, does the current Apple DVD player included with Jaguar support Dolby Digital output, or do we have to wait for Panther? Again, Apple isn't giving us all the details.

## Item #3: PCI-X...where are the cards?

It's exciting to see that Apple is adopting a technology that will vastly improve the rate of data transfer between PCI cards and the system controller. It's not so exciting to realize that this new technology can only be utilized by new hardware. In other words, all your old PCI cards, while they will work fine in PCI-X slots, won't actually *run* at PCI-X speeds. You'll see no benefit at all, so what I'm wondering is, when are we going to see PCI-X cards for the Mac platform? Unlike other recent Apple tech rollouts (FireWire 800, Rendezvous, etc.), the third-party press releases from companies vowing to support this new technology are surprisingly absent.

## Item #4: 64-bit application support...how does it work?

We know that the PowerMac G5 is 64-bit. We know that existing applications can be recompiled to support 64-bit operation with a minimum amount of effort. However, what I don't quite understand is if the 64-bit version of a given application has to be comprised of entirely different files from the 32-bit version. If so, that means there has to be two different versions of an application on every CD, two different downloads, two different updates, etc. This could get complicated. My hope is that Apple utilizes the "fat binary" technology inherited from NeXT so that applications can include both versions within a single executable file. We'll have to wait and see how this pans out.

A great many of these questions will be addressed once the PowerMac G5s are actually shipping. But, meanwhile, there are an awful lot of questions going unanswered. Maybe Apple has more information on all this buried in a tech note on their site somewhere, but I certainly haven't found it yet.

Regardless of these minor points, the PowerMac G5 is going to be an awesome machine, and Apple should be congratulated on a very impressive product launch. All the company needs to do now is to make sure these babies do indeed ship in August, or very shortly thereafter, and all will be well.
