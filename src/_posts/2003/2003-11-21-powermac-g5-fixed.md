---
title: "Despite One Minor Issue, My Power Mac G5 is Fixed"
category: archived
funny_caption: "Still Chirping Past"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20041205104447/http://www.theideabasket.com/modules/news/article.php?storyid=75"
---

You may remember [my post back on the 19th of October](/archived/2003/10/19/powermac-g5-best-worst-times/) in which I stated that my then brand-new Dual 2GHz Power Mac G5 was having trouble turning on (after the initial chime sound, I'd just get stuck on a black screen with no USB activity, and the OS wouldn't boot). Well, I'm happy to report that now, an entire month later, I finally have a working G5! Better late than never, I suppose --- but I do have to admit that the fault didn't entirely lie with Apple. You see, it turns out that the problem was linked to the ATI Radeon 9600 Pro video card. After having both the power supply and the logic board swapped out (I'm back to my old logic board now because the new one was totally defective!), the folks at my local Apple authorized repair facility finally turned to the video card. Putting a new one in did the trick. It now boots every time. In fact, I haven't had one glitch with the machine since I received it four days ago, so that's great news.

The not so good news is that I was expecting the faint "chirping" sound and crackle in the analog audio out port to go away when the new power supply was installed. Unfortunately, the problem is still there --- the new power supply unit seems to be no better at all. At this point, I'm just going to live with it until Apple figures out an official solution. The fact is that the noise is so soft that you have to have your speakers turned way up in a dead silent room or use high-end headphones to hear it at all. And I'll be using a separate USB audio interface for pro audio usage anyway, and that output seems to be clean.

So, it's mostly good news. It wasn't fun waiting for a month to get this resolved, but now that I have a rocket ship Power Mac G5 in my hands that WORKS, I'm a happy camper.
