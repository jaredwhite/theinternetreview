---
title: "New Lease on Life for Motorola's Chip Division?"
category: archived
funny_caption: "Putting All Their Chips on the Table for"
icon: powerpc
archive_url: "https://web.archive.org/web/20041211155111/http://www.theideabasket.com/modules/news/article.php?storyid=48"
---

In a not wholly unexpected turn of events (thanks to the ever present rumor mill), [Motorola has just announced](https://web.archive.org/web/20041216153241/http://www.motorola.com/mediacenter/news/detail/0,,3358_2778_23,00.html) that it is planning to spin off its Semiconductor Products Sector (SPS) in an effort to focus more strongly on Motorola's communications, wireless, and embedded systems products. Fair enough, but what does this mean for the computer industry?

From a computer tech point of view, and especially a Mac user point of view, Motorola's chip making unit has always been the most interesting aspect of the company. In recent times, however, it has also been the most frustrating. While its PowerPC chips have usually fared well in the embedded and networking markets, it hasn't always delivered solid products for the desktop market (aka Apple). Delays, technical problems, and sometimes (as in the case of the mythical Motorola "G5") outright failures have been hallmarks of the processor maker over the past several years --- and many irate Mac users have placed the blame of Apple's difficulties in competing with high-performance PCs squarely on the shoulders of Motorola and its apparent inability to keep up with the likes of AMD and Intel.

The sad thing about all this is that the G4 is still a really good chip. It's just clocked too low for the performance demands of today's desktop/notebook market. Which is why Apple has been forced to switch over to IBM to ensure that the future of the Macintosh platforms remains secure. IBM's new PPC 970, aka G5, is one monster of a chip, but because Apple is still depending on Motorola to deliver G4s for the iMac and PowerBook lines (the iBook is still using IBM's PPC 750FX chip, aka the G3), it's important that the G4 continues to be properly developed.

Which brings us to today's news of Motorola's decision to spin off its SPS. For the embedded and networking markets, I see nothing but good coming out of this scenario. The new company will be able to focus more strongly than ever on making chips for mobile phones, DSP usage, networking products, and such. But for the desktop/notebook market (mainly Apple), the future may not be so bright. I see one of two possibilities playing out in the next year or so:

1. This is, of course, the most optimistic possibility. The new company (let's call it Cool Chip Maker - CSM) does indeed go IPO, and Apple purchases some stock in the company --- possibly even puts one of its head honchos on the board of directors. CSM ramps up development of newer, high-speed PPC chips and does for Apple's consumer/prosumer line what IBM is now doing to Apple's high-end pro line. CSM and IBM together will bring about a new PowerPC revolution that will impact the entire computer industry.
2. Now for a more realistic possibility. The new company (let's call it Same 'Ol Chip Maker - SOCM) continues on much as before, and the G4 continues to languish. Apple is finally forced to turn to IBM to develop a faster G3 --- possibly with AltiVec added --- that can be re-branded as a G4, with the G5 heading up Apple's pro line for both the desktop and notebook markets (once IBM has conquered the heat and power consumption issues plaguing the G5 currently). The entire Macintosh product line may eventually be powered by future versions of the G5, but that would have to take place quite a long time from now.

It's possible that both scenarios are extreme and the truth may lie somewhere in the middle. But the fact is that the current G4 is getting very long in the tooth, and I'm afraid that unless the new chip company behaves very differently than it did when it was part of Motorola, the future of the G4 remains rather bleak.

However, the future of IBM's PowerPC chips looks bright, very bright indeed, and it was very wise of Apple to start working with IBM last year to develop a new high-end chip for the PowerMac. Now all IBM needs to do with bridge the gap between the rather slow but extremely efficient G3 and the extremely fast but rather inefficient G5 --- and Apple is, to coin a phrase, made in the shade.

And what about SPS? We know that the goal of the new company will be to grow and make more money. We don't know yet whether that means it will peruse or it will abandon the mainstream CPU market. It looks like, once again, we will have to play the waiting game, and see what the company has in store for us all.
