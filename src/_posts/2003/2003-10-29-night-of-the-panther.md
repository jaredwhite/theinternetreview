---
title: "Yes! The Night of the Panther is Here at Last"
category: archived
funny_caption: "I Am Your Performance Retribution on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20041218045720/http://www.theideabasket.com/modules/news/article.php?storyid=70"
---

I'm posting this from my iBook G3 600MHz running Mac OS X 10.3 Panther. Yes, I received my Panther order today, and I've been putting it through its paces for about a couple of hours now. So far, the hype seems to be true: Panther is, indeed, a great performer. With Jaguar, my iBook was definitely starting to feel sluggish when compared with my iMac G4 800MHz, and of course it was a joke compared to a Dual 2GHz Power Mac G5.

But now with Panther running on it, it feels pretty zippy. Now obviously there's no way that a slower G3 notebook is going to perform miracles with an OS upgrade, but the nice thing about Panther is that makes the system *feel* fast. This is something that BeOS users understand perfectly --- it's possible to have a system that feels fast even if it's not. How? By keeping the GUI responsive even when under system load. Panther improves upon Jaguar significantly in this arena. I think it's probably safe to say that most OS X performance quibbles are now a thing of the past.

I'll be writing up a in-depth review of Panther over the next week or so. Unlike some other reviews out there, I want to focus on the little things overlooked by a lot of people. You've probably heard about Exposé about a zillion times by now, but what about Samba 3.0? I don't know about you, but that's the stuff *I'm* interested in.

So now, I'll leave you in peace while I go wile away the hours playing with the latest and greatest from Apple.
