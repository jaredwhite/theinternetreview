---
title: "Apple Strikes Gold: 1 Million Songs Sold, Windows iTunes Downloaded in 3 1/2 Days"
category: archived
funny_caption: "Joke's on You Micro$oft for"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20041211121710/http://www.theideabasket.com/modules/news/article.php?storyid=61"
---

Since the launch of the iTunes Music Store for Windows last Thursday, Apple [has sold over one million songs](https://web.archive.org/web/20041218050239/http://www.apple.com/pr/library/2003/oct/20itunes.html), primarily to its new Windows-based customers --- who have also downloaded over one million copies of iTunes so far.

Recently, Mac users have been buying songs at a rate of about 600,000 per week, so one million songs in three and a half days represents a major influx of PC users. But, remember, "only" Mac users bought one million songs in just one week when the iTunes Music Store was first released, so this isn't so much a matter of marketshare difference as it is a matter of pent-up demand.

Apple looks like it has another hit on its hands. While the Windows competition in legal music downloads is already strong, and getting stronger by the day, right now iTunes is clearly the winner in this new market segment --- as is the iPod, which has captured over 31% share in portable music devices and is now the #1 player.
