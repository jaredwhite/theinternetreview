---
title: "Mac OS X 10.7 Lion Available Through $99 Developer Program"
category: archived
funny_caption: "King of the OSes on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20110416025248/http://netnotes.siteshine.com/2011/02/"
---

[Apple today released a developer preview of its upcoming major release of Mac OS X](https://web.archive.org/web/20110416025248/http://developer.apple.com/technologies/mac/whats-new.html). Dubbed "Lion" in a long list of cat-themed names, [this new 10.7 version includes a ton of features](https://web.archive.org/web/20110416025248/http://www.apple.com/macosx/lion/) that bring it up-to-par with the latest innovations in its iOS platform which powers iPhone & iPad.

**Jared's Take:** Pouring over some of the finer details on Apple's developer site and comments from developers on discussion boards, this is a definite shift in direction for the Mac platform. The always-on, always-available, "fluid" nature of the iPhone & iPad which we've grown to love and appreciate--that kind of "appliance" style reliability--is now being applied to the sometimes squirrely realm of traditional desktop computing. Features like autosave, multi-touch switching of fullscreen apps, LaunchPad, instant recall of app state, background process management, the tighter integration with Apple's App Store--all this combines to create a new and compelling experience by which future computer systems from Microsoft and others will be judged.

The power-control geek crowd may find some pause for concern here, as Apple continues the shift away from "bare metal" access to files, power utilities, and manual management of running processes towards a consumer-centric appliance model where everything under the hood is hidden away. Even I am wary of the implications, I'll admit. Still, it's clear there will always be some environment where the bare metal is accessible--simply because that's the only way you could develop the "sandboxed" apps that will live on these new iOS-style platforms. In other words, developers need developer tools, plain and simple.

I'm excited about Lion. I think this will be the most innovative Mac OS X release since 10.4 Tiger from an end-user perspective. We shall see come summer.

*From the press release:*

> Lion features Mission Control, an innovative new view of everything running on your Mac; Launchpad, a new home for all your Mac apps; full screen apps that use the entire Mac display; and new Multi-Touch™ gestures. Lion also includes the Mac App Store℠, the best place to discover, install and automatically update Mac apps. The Lion preview is available to Mac Developer Program members through the Mac App Store today, and the final version of Lion will ship to customers this summer.
>
> "The iPad has inspired a new generation of innovative features in Lion," said Philip Schiller, Apple's senior vice president of Worldwide Product Marketing. "Developers are going to love Mission Control and Launchpad, and can now start adding great new Lion features like full screen, gestures, Versions and Auto Save to their own apps."
>
> Mission Control is a powerful, entirely new feature that unifies Exposé®, Dashboard, Spaces®, and full screen apps to give you a bird's eye view of every app and window running on your Mac. With a simple swipe, your desktop zooms out to display your open windows grouped by app, thumbnails of your full screen apps as well as your Dashboard, and allows you to instantly navigate anywhere with a click.
>
> Launchpad makes it easier than ever to find and launch any app. With a single click, Launchpad displays all your Mac apps in a stunning full screen layout where you can launch, re-order or organize apps into folders. You can also arrange apps into multiple pages and swipe between them.
>
> Lion brings the full screen experience that iPad users love to the Mac. With one click, your application window goes full screen, taking advantage of your Mac's brilliant display. You can swipe from one full screen window to another and even back to your Desktop or Dashboard.
>
> New Multi-Touch gestures and fluid animations give you a natural and intuitive way to interact with your Mac. New gestures include pinching your fingers to zoom in on a web page or image, swiping left or right to turn a page or switch between full screen apps and swiping up to enter Mission Control.
>
> Lion also includes the Mac App Store, where you can find great new apps, buy them with your iTunes® account, and download and install them in just one step. Apps purchased from the Mac App Store are installed directly into Launchpad.
>
> Additional features in Lion include:
>
> -   a new version of Mail, with an elegant, widescreen layout inspired by the iPad; Conversations, which automatically groups related messages into one easy to read timeline; more powerful search; and support for Microsoft Exchange 2010;
> -   AirDrop, a remarkably simple way to copy files wirelessly from one Mac to another with no setup;
> -   Versions, which automatically saves successive versions of your document as you create it, and gives you an easy way to browse, edit and even revert to previous versions;
> -   Resume, which conveniently brings your apps back exactly how you left them when you restart your Mac or quit and relaunch an app;
> -   Auto Save, which automatically saves your documents as you work;
> -   the all new FileVault, that provides high performance full disk encryption for local and external drives, and the ability to wipe data from your Mac instantaneously; and
> -   Mac OS X Lion Server, which makes setting up a server easier than ever and adds support for managing Mac OS X Lion, iPhone®, iPad and iPod touch® devices.