---
title: "Decoding the Phenomenon of Google+"
category: archived
funny_caption: "Feeling Lucky on"
icon: world
archive_url: "https://web.archive.org/web/20120519111304/http://northbaystartup.com/2011/07/21/decoding-the-phenomenon-of-google/"
---

Time to turn our attention to a new service that's taken the web by storm:

**Google+**

It seems the collective punditry of the tech movers and shakers out there have split along fairly predictable lines: (a) Google+ is innovative with a dash of awesome, and both Facebook and Twitter are going to get thrashed, or (b) Google+ has some interesting features but once Facebook (and to a lesser extent Twitter) tweak some things on their services, Google+ will cease to stand out. In either case, everyone acknowledges Google's uphill battle in the face of Facebook's 750 million strong user base.

Here's my take on the matter, having used Google+ for a little while now (like everyone else on the new service).

**Google+ is less about Google and more about Facebook.**

Here's what I mean by that. Google's service is most likely going to continue to grow organically and exponentially and be quite successful for the company. But it's not a Facebook "killer" --- really, it's not. For one thing, Facebook can quite easily copy Google+'s main claim to fame: Circles. Circles is Google's way of allowing you to group/list your social connections in various ways, the benefit being you then control which of those "circles" you are sharing a particular item with (a status message, a photo, etc.). You can also share with individual people or with the "public" (ala Twitter). Isn't that great?

Guess what --- Facebook already has a friends list feature. All they have to do is add an interface to select which lists you want to use when sharing an item. Furthermore, Google is currently missing a way to create a group that allows multiple people to interact with it which Facebook has already. (The latest incarnation of Facebook Groups works quite well, in my opinion.)

Some of the other fluff in Google+ like Huddle (multi-way video chat) and Sparks is just that. Fluff. I don't see any of that as very compelling from a competitive perspective.

But going back to the Circles feature: here's why Facebook will copy it and why it doesn't even matter that much anyway. Nobody is going to use it! Yes, all the power users will and think it's great, but the vast majority of people just don't care. Almost everything that ends up on Facebook is of the "here's something I want to share with everyone I know" variety --- in some cases "I want to share with the world". In the few instances where there's something to share with a small number of people, it ends up in the private messages department. People just aren't going to take the time sort hundreds of people into little circles/groups/lists and then take the time every instance they post something to figure out which of all those circles they want to allow access. It takes too much brain power.

However, if I'm right in all of this, there's still one advantage Google has with Google+ and it's actually a pretty big one: it makes Facebook look weak.

You see, Facebook has enjoyed an almost unbelievable position of dominance for several years now. After the obvious decline of MySpace, Facebook gleefully took on the role of social network trend-setter for the masses. Here's how it goes: Facebook "gets social" and everyone needs to trust them to know what's best for this sort of service. And to a certain extent this mythology is grounded in reality. After all, Facebook invented the News Feed, the Like button, and Facebook Connect. Facebook was a pioneer in the Open Graph protocol which adds a social & sharing layer on top of the web at large. Facebook has innovated based not on a desperate need to ward off aggressive competition but simply to add value to the service and keep people engaged.

But now Facebook has a competitor, and it's a significant one. Google is a huge company with absurd piles of cash. Google+ is now to Facebook what Bing is to Google: the pesky competitor that Just. Won't. Go. Away. They can easily spend years on this project, trying one new thing after another until they find that secret sauce that just might outshine Facebook entirely. And that may very well be what's keeping Mr. Zuckerberg up at night.

Facebook suddenly appears vulnerable. Google+ is cool. It has that new car smell. Everyone wants to kick the tires and take it for a spin. Facebook seems...well, old. It's yesterday's news. Facebook gets social, huh? Well, maybe Google does too. Facebook as the be-all and end-all of online interactions? Well, maybe Google's slant on the future of social networks is just as important.

So again, I reiterate my previous statement. **Google+ is less about Google and more about Facebook.** Google+ as a product isn't a game changer, but in terms of potential mindshare and the volatile nature of marketplace, it hits Facebook pretty hard. And that makes industry observers like me pretty intrigued to see how Facebook responds.
