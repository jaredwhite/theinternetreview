---
date: "Wednesday, March 31st, 2010 at 4:59 pm"
title: "Kill Flash on the Desktop, Not Just for Mobile"
category: archived
funny_caption: "Its Days are Numbered at"
icon: photoshop-7
archive_url: "https://web.archive.org/web/20100504160843/http://netnotes.siteshine.com/2010/03/kill-flash-on-the-desktop-not-just-in-mobile/"
---

[TED just announced the addition of HTML5 support](https://web.archive.org/web/20100504160843/http://blog.ted.com/2010/03/tedcom_now_avai.php) to their video-rich site for mobile devices, thus allowing iPhone, iPad, etc. that don't support Flash to view and playback their media. While I applaud their move and many other companies that are introducing new HTML5 versions of their media players, I have to take exception to the approach that only mobile devices are targeted for these versions.

![iMac with a crossed out flash icon](/2010/kill-adobe-flash.jpg){: style="float: right; margin-inline-start: var(--size-3); margin-block-end: var(--size-2); border-radius: var(--radius-2)"}
Let's face facts here: Flash is not a real Web technology. It's not an open language, like HTML or CSS. You can't easily develop your own Flash renderer or generator. You can't help contribute to the future of Flash. Flash is owned by Adobe. It's a proprietary runtime, embedded onto a Web page through the use of browser plugin support which originally was intended to serve as a stopgap measure back in the days when Web technology was primitive.

These days, the standards-based Web is becoming extremely sophisticated. HTML5 is a major leap forward and one that heralds a future where open technology can handle all the media-rich, interactive applications one can dream of. Flash, while still superior from a capability side at this present moment, truly and literally will not be necessary for 90% of typical use cases in the near future. If I were Adobe, I'd be shaking in my shoes right now.

Here's what I don't understand: Flash as an animation software package is very attractive. Why doesn't Adobe just take what they have and add support for rendering out to Canvas or SVG for vector art and HTML5/CSS for other elements? It's true they've invested so much in the proprietary runtime that is Flash, but surely they must realize their days as *the solution* for multimedia on the Web are numbered.

I am proud to state that I am not a Flash developer. I build standards-based sites that work in any browser on any platform. Soon, I will be building HTML5 & CSS3 Web sites that do some pretty cool things. I can hardly wait.
