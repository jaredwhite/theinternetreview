---
date: "Tuesday, January 26th, 2010 at 12:27 pm"
title: "How the Apple Tablet Could Change the Web"
category: archived
funny_caption: "Following in the Footsteps of Moses on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20100504163840/http://netnotes.siteshine.com/2010/01/how-the-apple-tablet-could-change-the-web/"
---

I was an iPhone user pretty much from Day 1. That was back in summer of 2007. It was the first portable touchscreen computer I’d ever used, and it rocked. But let me give you even more backstory…

In the 2000-2001, a company named Be announced they would be partnering with several companies to launch “Internet Appliances” or IAs that would come in several form factors. Be had been developing a desktop OS named BeOS for several years prior but was unable to find traction in a consumer PC market dominated by Microsoft. They decided to [retool BeOS into BeIA](https://web.archive.org/web/20100504163840/http://www.birdhouse.org/beos/byte/23-sony_ces/) and give it a go in the device space.

<image-figure>
  ![BeIA tablet prototype](https://jaredwhite.com/images/BeIATabletPrototype.jpg)
</image-figure>

One of the main uses of BeIA that was demonstrated was in a wireless handheld device sporting the tablet form factor (see the photo above) The hope was that a specialized Internet-enabled OS with an interface and development stack suited to next-generation graphics and touchscreen hardware would capture the market by storm. As we all know now, BeIA never took off commercially and Be ended up getting bought by Palm, who more or less let BeOS and BeIA lanquish into complete obscurity.

Here’s the thing though: I wanted a BeIA Web tablet. I wanted it bad. I dreamed of being able to roam around the house with a book-sized slate in my hand, writing e-mail and surfing the Web wirelessly and enjoying sophisticated multimedia content. Remember, this was years before the iPhone, before the iPod even, before Mac OS X was first released. When I saw BeIA tablets, I saw the future.

Now here we are, and it’s 2010, and [Apple is very likely to announce tomorrow the first Mac OS X tablet computer](https://web.archive.org/web/20100504163840/http://www.huffingtonpost.com/2010/01/26/apple-tablet-announcement_n_436859.html). Granted, the rumors seem to indicate it will probably be running a variant of the iPhone OS, but that’s basically a variant of Mac OS X, so I like to call it the OS X Tablet. Why is this significant? It’s significant because Apple is going to parlay its incredible success with the Mac, iPod, and iPhone platforms into a new platform. It’s significant because Apple will be making the undeniable statement that **tablets are the future**. They were the future in 2000 and they are the future in 2010. The reason they never took off in the past whether it was Be or Microsoft or another company pushing the form factor is because the price, the software functionality, and the hardware design never hit that magic sweet spot.

I believe Apple will hit that sweet spot, and if this device takes off like I had hoped the BeIA devices would ten years ago, it could revolutionize how we consume content and interact with media on the Web. It could cement the sea change already underway with the iPhone and tip the scales of the Web from a mouse-and-keyboard centric interface to a touch-based interface. It could change what we think of when we think of a “Web site” and usher a new milestone in the evolution of online interactivity.

Hyperbole? Maybe. But remember: I’ve been wanting a slick Web tablet for ten years. To say I’m excited about what might be announced tomorrow is a grave understatement.

Come back tomorrow, and I’ll share my thoughts on whatever Apple has pulled out of its hat once again!