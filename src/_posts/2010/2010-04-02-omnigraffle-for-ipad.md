---
date: "April 2, 2010 – 5:13 pm"
title: "OmniGraffle for iPad: Killer App for Designers?"
category: archived
funny_caption: "Replacing Paper on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20100504230239/http://ipadartistry.com/2010/04/omnigraffle-for-ipad-killer-app-for-designers/"
---

I'd heard that OmniGroup was working hard on bringing all of their apps to the iPad, so I headed over to their site and saw that, sure enough, they have several apps ready for release. The one that interests me the most is OmniGraffle, which could prove to be a "killer app" for designers and engineers.

I used to use OmniGraffle on the Mac quite frequently, but after a while, I noticed that I was reaching for pen and paper more often. For the kind of work I do (Web design), it's just easier to doodle on a big sheet with a marker. By the time I create a mockup in OmniGraffle, I might as well have been doing the real thing in Photoshop.

On the iPad, however, it will probably be very different. I can just tap on the screen to add objects or sketch freehand. I'm most excited about that --- I envision pulling up OmniGraffle, doodling with my finger for a bit, and then setting my iPad down next to my MacBook Pro and Photoshopping a design based on the sketch. The only consideration to keep in mind is how small the iPad screen might feel when sketching. I'll probably review OmniGraffle in-depth once I can test it out.

All in all, a very exciting app and one I can hardly wait to try out.
