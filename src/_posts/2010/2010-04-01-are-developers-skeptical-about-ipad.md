---
date: "April 1, 2010 – 12:15 am"
title: "Are Developers Skeptical About the iPad?"
category: archived
funny_caption: "Kicking Off a Tablet Market on"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20100504230221/http://ipadartistry.com/2010/04/are-developers-skeptical-about-the-ipad/"
---

Shock! Gasp! [Looking at this recent survey](https://web.archive.org/web/20100504230221/http://technorati.com/technology/it/article/is-ipad-developer-enthusiasm-flagging), it would seem that developers just aren't super excited about the iPad and if there's anything out there to match the enthusiasm level we've seen with the iPhone app market, it would be Android. Except that's not really what is going on here.

> When respondents were asked what platform they were "very interested" in developing for, the numbers were striking. While 87% of respondents listed the iPhone (a strong showing, though how could it not be at this point?), Android came in second with 81%. The iPad came in a distant third, at 53%.

I think it's safe to say that a lot of developers out there are running small shops and have limited resources. When you have limited resources, you go where the biggest market is for what you want to develop. The smartphone market is where it's at right now, no doubt about it. And while Android may have a ways to go before it catches up to the iPhone in terms of marketshare, it's definitely the #2 contender right now.

Meanwhile, the tablet market is unproven. The iPad *isn't even available yet*. We still have three days to go before ordinary people are using iPads. So, obviously, the most likely developers to bring their wares to the iPad are current iPhone developers that see an immediate opportunity to make fast money. Meanwhile, if you're still working on your first iPhone app, or you haven't even started yet, you'd be silly to put that on hold and focus on iPad right now.

One other point: the survey was conducted by [Appcelerator](https://web.archive.org/web/20100504230221/http://www.appcelerator.com/), a company that produces a cross-platform SDK that compiles to native apps on Windows, Mac, and Linux on the desktop side, and iPhone, Android, and (soon) iPad on the mobile side. Now I could be wrong, but wouldn't it make sense that developers using such an SDK are less likely to be wanting to focus efforts on exploiting the capabilities of a single device? If I wanted to establish myself as a bona-fide iPad developer *extraordinaire*, I most likely would not be using a cross-platform SDK targeted at Web developers.

So, once again, don't believe everything you read on the Internet. Or, in this case, look at the statistics with a grain of salt.
