---
date: "April 14, 2010 – 8:56 am"
title: "Why iBooks Won’t Be Like iTunes"
category: archived
funny_caption: "I Hope You're…Into Books…Hahaha for"
icon: myst
archive_url: "https://web.archive.org/web/20130917050049/http://ipadartistry.com/2010/04/why-ibooks-wont-be-like-itunes/"
---

When the iTunes Music Store came to market in April 2003, there wasn't anything else like it. The simplicity and elegance of buying songs and syncing them to your iPod, combined with attractive and industry-sanctioned pricing, was a revolution.

Today, Apple has embarked on a similar journey in the publishing world with the iBooks Store, and, while it's a great service on a fantastic device (the iPad), there are some major reasons why this time Apple will be competing in a very different climate.

**E-books are already popular.** Let's give credit where credit is due: Amazon really gave a huge boost to the e-book market when they launched the Kindle platform in November 2007. Previous e-readers never caught on in the mainstream, but the Kindle was easy to use and easy to obtain content for. What's most attractive about the Kindle platform however isn't its hardware, it's the broad selection of titles and integration with Amazon's already formidable e-commerce business. And Amazon knows this, which is why the Kindle reader and store is available on a variety of computer and mobile devices, including...the iPhone, iPod Touch, and now iPad. Barnes & Noble also has a solid e-book business (though its hardware device, Nook, hasn't gotten the best of reviews), and they have an iPhone/iPod Touch reader available with an iPad version on the way. So, unlike with the original iPod/iTunes combo, Apple isn't providing a service all that much more compelling than the competition. In the end, the "winning" store will emerge based on simple economics and quality of user experience.

**Apple isn't big on books.** I still believe previous comments from Steve Jobs such as "people don't read anymore" indicate a prevailing philosophy at Apple that the future of education, literary art, and communication isn't in books. It's in interactive, multimedia experiences. Apple cares more about the App Store and the iTunes Store than the iBooks Store. iBooks exists because the iPad form factor simply begs for it, and Apple might as well have a foot in the door than cede the territory to Amazon. But it really is a me-too effort. Apple didn't "reinvent" the book, nor did they change the rules of the game in the book publishing industry other than to exert a bit of pricing control on the competition by allowing publishers to set prices.

**Books are different than music.** While it's tempting to think that a book is no different than an album (just a collection of words/just a collection of waveforms), there are fundamental differences in the business models. The idea of a book as *the way* to consume the written word is rather tenuous in this day and age. We read books, yes, but we also read newspapers & magazines which are far more likely to show up as interactive experiences in the App Store (which, surprise surprise, they already have). We read articles & blogs online. We read e-mails. We read tweets. A book is just one of a vast number of channels in which the written word plays a strong role.

**Conclusion:** we will always have books, and in many cases, it makes sense these days to go digital with books just like we have for most other media. As long as the iPad is popular and Apple is running the iBooks Store, it will be successful. But for those who are wondering if iBooks will be to the publishing world what iTunes was to the music world, I offer this opinion: it simply won't. They're two different animals entirely.

Now if you want to speculate that Apple might come out with a innovative "Book 2.0" solution at some point that combines pages of words with interactive content to form a whole new way of publishing, you might be on to something...
