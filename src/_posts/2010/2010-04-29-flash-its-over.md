---
date: "April 29, 2010 – 5:18 pm"
title: "It’s Over: Flash Lost"
category: archived
funny_caption: "HTML5 Killed the Flash Runtime Star on"
icon: photoshop-7
archive_url: "https://web.archive.org/web/20130917045953/http://ipadartistry.com/2010/04/its-over-flash-lost/"
---

I think pretty much everyone in the tech and design communities has read [Steve Jobs' Thoughts on Flash](https://web.archive.org/web/20130917045953/http://www.apple.com/hotnews/thoughts-on-flash/) open letter by now. Published early this morning, it outlines a clear case for the rationale why Apple has deliberately chosen to block Flash from the iPhone/iPod/iPad ecosystem --- not only as a plugin in their Safari mobile browser but as a development platform for "native" apps as well.

![iMac with a crossed out flash icon](/2010/kill-adobe-flash.jpg){: style="float: right; margin-inline-start: var(--size-3); margin-block-end: var(--size-2); border-radius: var(--radius-2)"}
My position is very clear (and [was prior to this letter](/archived/2010/03/31/kill-flash-on-desktop/)): I believe the Web, along with Apple's mobile platform, are better off without Flash. As a Web designer by professional, I have absolutely no interest in Flash. 80% of the interesting use cases I see on Web sites, other than video, where Flash is employed could have been developed using a native Web technology stack (HTML/CSS/Javascript/Ajax). That's been true for at least a couple of years now. (If I see another photo slideshow with fade effect using Flash, I think I'll scream!) And now with HTML5 video, another compelling reason to stick with Flash is pretty much rendered null and void. We don't need Flash on the Web anymore. Good bye, and good riddance.

In terms of Flash as a development platform, that seems ridiculous to me. If I want to develop a truly great application for the iPhone or iPad, I'll use Apple's tools. Period. And as a consumer, I want the highest quality applications available. One of the problems with the App Store isn't that too much stuff gets rejected --- it's that there's still too much garbage! I think the majority of apps are consistent with Apple's high bar of quality, but sometimes you end up with a real dog (sorry canine lovers). The idea of a bunch of Flash-generated app spam showing up on the App Store is hardly appealing to me.

So what should Adobe do? Frankly, I think they've painted themselves into a corner for reasons I don't fully understand. I like what [Michael Slater of Webvanta](https://web.archive.org/web/20130917045953/http://www.webvanta.com/post/95204-apple-reiterates-its-position-on-flash) says on the matter (as a former Adobe employee himself). He has a great solution for how Adobe could utilize the Flash development environment in a way that benefits Apple and everyone else: simply transcode Flash projects to open HTML5-powered code. Sure, there are some things a Flash runtime can do that HTML5 can't, but if Adobe created a way to render out HTML5, Javascript utilizing Canvas, SVG, and H.264 video, Flash developers would simply learn the latest paradigm and use Adobe's tools to create great multimedia experiences for mobile devices and the Web in general. Adobe makes money, developers can target Apple's platform, and the Web becomes more open. Everyone wins.

In the meantime, newsflash to Adobe: Apple won. Flash (the runtime) lost. Get over it. The end.
