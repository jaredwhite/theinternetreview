---
date: "April 2, 2010 – 5:13 pm"
title: "Hands-On with the iPad: First Impressions"
category: archived
funny_caption: "Buh-Bye Slate PCs for"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20100504230226/http://ipadartistry.com/2010/04/hands-on-with-the-ipad-first-impressions/"
---

I'm typing this in the WordPress app on the iPad. I've been using it since 10am. I've synced it up with iTunes, checked out all the native apps, and played around with some downloads from the App Store. I even took it outside. Here are some initial thoughts.

The size is perfect. Any bigger and it wouldn't feel portable, any smaller and it wouldn't have as strong a sense of immersion. I could understand the temptation to wish for a 5×7 type of mini-slate, or a "larger is better" widescreen tablet. But, really, this is ideal. It feels right in the hand, and while it does have a bit of heft to it, it doesn't feel heavy. It feels...solid. **Apple's fit and finish continues to be unmatched.**

![iPad in case](/2010/ipad-hands-on.jpg){: style="float: right; margin-inline-start: var(--size-3); margin-block-end: var(--size-2); border-radius: var(--radius-2)"}
I've encountered a few minor glitches, both in Apple's software and third-party apps. I'm not surprised --- after all, everything is 1.0. But, so far, the iPad has been very stable --- no crashes or missing data. Give this platform a couple of months, and I believe it will operate like a well-oiled machine.

The virtual keyboard in portrait mode feels like a big iPhone. In landscape mode, it's a rather odd experience. I'm actually touch-typing pretty fast, almost as fast as a real keyboard. The reason it's odd is because to use most punctuation, or numbers, I have to switch contexts (like on the iPhone), and at that point, I'm doing the hunt-and-peck routine. Still, I suspect that after a while, I'll get used to it. Bottom-line: you won't miss a real keyboard much.

You'll want to keep this in a case. Trust me --- while you might enjoy holding it bare in your hand, you're going to start feeling paranoid. Unlike with laptops, the iPad is basically just a screen that you hold like a book or a newspaper, and I don't want to be throwing this thing around and getting it all bunged up and scratched. I got the Apple case, which is functional but surprisingly clunky-looking. I'll probably get a more artistic case at some point, maybe a brown leather one.

**Display: gorgeous. Speed: "wicked fast" as Walt Mossberg would say.**

The iPad is not as usable outside as I would like. Unless you turn the brightness up to max and stay in the shadows, it's going to be very difficult to read. There's also the issue of glare, although most laptops don't fare any better. Just don't expect to chuck your paper edition of the Wall Street Journal and read it in iPad form in the bright sunlight.

Speaking of the WSJ, I downloaded their app and read some articles in the free limited edition. I only have one thing to say: I have never subscribed to a newspaper in my whole life (3 years to 30). The iPad may change my mind.

Web site browsing on the iPad is an interesting experience. I'll post more insight on this as I do further testing.

In summary, there are many products that receive tremendous hype but when you actually use them, it's a big letdown. The iPad is not one of them. I've been waiting ten years for someone to come out with a genuine tablet computer built from the ground up to offer a compelling user experience. Apple has delivered the goods. **PC makers: you have a lot of ’splainin' to do.**
