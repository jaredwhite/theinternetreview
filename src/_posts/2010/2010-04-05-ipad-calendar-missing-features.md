---
date: "April 5, 2010 – 11:36 am"
title: "iPad Calendar: Missing Some Crucial Features"
category: archived
funny_caption: "Don't Have Time for This on"
icon: handwait-ani
archive_url: "https://web.archive.org/web/20100504230234/http://ipadartistry.com/2010/04/ipads-calendar-missing-some-crucial-features/"
---

I'm pretty happy with the overall experience of the Calendar app on the iPad, but, strangely, it's missing some crucial features compared to the Mac version of iCal that makes it difficult to use for wide-ranging scheduling sessions.

These limitations show the iPad's heritage of the iPhone in terms of software, which is perhaps less annoying in a small pocket device but rather inexcusable in a broader context. My main beefs are:

1.  **Can't change event calendars after an event is created.** This has been a problem on the iPhone and now it's a bigger problem. When I am creating an event, I can choose which calendar (Home, Work, etc.) the event should use, but afterwards, there's no way to move the event to another calendar if I made a mistake. iCal can do it easily, so why can't Calendar?
2.  **Can't duplicate events.** What if I am mapping out some blocks of time on different days and thus want to duplicate an event? It's a no go on the iPad.
3.  **Subscribed calendars in iCal synced over MobileMe don't refresh quickly.** OK, this one's a little esoteric, but my wife publishes her calendars and I subscribe to them in iCal so we can stay in sync. In iCal, I can choose "Refresh" to update the calendars immediately. However, when I'm actually using my iPad, there's no way to refresh the calendars. I have to wait for a few minutes. Lame.

![iPad Calendar screenshot](/2010/ipad-calendar-week.jpg){: style="float: right; margin-inline-start: var(--size-3); margin-block-end: var(--size-2); border-radius: var(--radius-2)"}
Also, when using the Calendar in landscape mode, there's a very strange UI resize dance that happens when the keyboard disappears while selecting an event date and then when the keyboard reappears. Such a clunky interface in this particular context is very surprising for Apple.

On the plus side, iCal on the Mac seems positively utilitarian and boring now. I'd much rather use the iPad Calendar if these issues got resolved. I'm hoping Apple will make some improvements for the next OS update and turn Calendar into a killer app.
