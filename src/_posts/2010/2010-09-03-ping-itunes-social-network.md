---
date: "Friday, September 3rd, 2010 at 9:55 am"
title: "Ping: a social network for MUSIC, built right into iTunes"
category: archived
funny_caption: "Ping Pong on"
icon: audio-gear
archive_url: "https://web.archive.org/web/20110826091438/http://netnotes.siteshine.com/2010/09/ping-a-social-network-for-music-built-right-into-itunes"
---

Apple? Social network? Yeah I know, it's a bit of a culture-shock. Apple doesn't quite seem like the "warm fuzzy community builder" company. But, indeed, Apple just released [iTunes 10](https://web.archive.org/web/20110826091438/http://www.apple.com/itunes) and with it comes a whole new social network called Ping.

What is Ping all about? Like the title of this post says, and like Steve Jobs repeatedly emphasized in his [keynote address](https://web.archive.org/web/20110826091438/http://www.apple.com/apple-events/september-2010/), it is a social network all about music. ***Music***. I don't think this can be stressed highly enough. Most people who are critical of Ping right now don't understand this. In Twitter, the unit of importance is a 140 character message called a Tweet. It often comes with a link attached. Tweets can be about anything in the world. In Facebook, the unit of importance is a shared object from a friend (which can be a status update, link, photo, etc.). Again, it can be about anything.

In Ping, the unit of importance is music. That's it. It can be a song or an album or an artist, but it's all about music. I didn't understand this yesterday. I was mad I couldn't dress up my profile with more info about me, or post status updates for people to read. I didn't understand quite what the point was of being on Ping, since most of my favorite artists don't have profiles up yet. But then today, I hopped over to the iTunes Store to see if I could find an artist's Ping profile from an artist's page. It turns out the answer is an obvious yes, but before I solved that, I discovered something really amazing. I can "Like" or "Post" anything on the iTunes Store. Bingo. That's the killer app right there. So I can find stuff I like, whether I even own it or not, and then post it to my profile to share with anyone who's following me. That gives Ping an extra dimension.

Now you may be thinking there are other social networks that work like that, networks that allow people to post music they like. Sure there are. Are they built into iTunes? No. Realize this: Ping isn't something Apple just hacked together and stuck up on a Web site to try out. Ping is iTunes. iTunes is Ping. I really don't think that can be stressed enough. The #1 way to find, purchase, and enjoy music on iTunes now will be through Ping, and everyone who likes to shop on iTunes will be using Ping to share the music they like. It's that simple.

No, Facebook isn't going anywhere. Twitter isn't going anywhere. Heck, I don't think Last.fm or Soundcloud are going anywhere. It's just that Ping is going to be very successful for exactly what it is: a social network for *music* built right into iTunes.

And I think that's pretty cool.
