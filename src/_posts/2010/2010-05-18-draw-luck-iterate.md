---
title: "Draw, Luck, Iterate"
category: archived
funny_caption: "Luck of the Draw? Or Draw of the Luck? at"
icon: wireless
archive_url: "https://web.archive.org/web/20110208225818/http://netnotes.siteshine.com/2010/05/"
---

[ReadWriteWeb posted a summary of a speech by Jack Dorsey](https://web.archive.org/web/20110208225818/http://www.readwriteweb.com/start/2010/05/draw-it-out-and-other-tips-for-success-from-twitter-co-founder-jack-dorsey.php), one of the co-founders of [Twitter](https://web.archive.org/web/20110208225818/http://twitter.com/). In it, Dorsey talks about some of the principles he's learned in starting a wildly-successful company (Twitter) and another one that is still in the beginning phases but looks likely to make waves ([Square](https://web.archive.org/web/20110208225818/https://squareup.com/)).

I haven't watched the entire video, but the summary is excellent. In the past, I've spent a lot of wasted moments agonizing over business ventures not panning out, but in more recent times maturity has taught me that if you take an idea, "draw" it out, and then put it on the shelf for a while, you'll come to discover that it's either a great idea --- at some point it will make complete sense and be just the right timing to launch it --- or a poor one best left forgotten.

I recently emptied out an old box with a bunch of software ideas from 5, 6, 7 years ago. It was depressing in a way, since I still haven't released an actual software product, but on the other hand, I realized that all the effort I put into working on those as-yet-unrealized ideas has helped sharpen my skills and made me a better programmer, a better designer, and a better project manager.

So don't sweat your dreams not coming to pass. The right ones will happen at the right times and the rest --- [to borrow a phrase from Jacob](https://www.youtube.com/watch?v=r10XiLD2KHQ) --- is just progress.

[Watch Jack Dorsey's Presentation on YouTube ➜](https://www.youtube.com/watch?v=wntQQMSu_l4)
