---
date: "April 2, 2010 – 4:46 pm"
title: "iPad Awareness: With Familiarity Comes Favor"
category: archived
funny_caption: "The Next Revolution at"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20100504205035/http://ipadartistry.com/2010/04/ipad-awareness-with-familiarity-comes-favor/"
---

In all the times I've talked to various people I know (regular folk, not techies) about the iPad, I usually get a similar response: *oh yeah, I've heard about that. So what does it do again?* When I begin to explain how I'll be using it --- as a secondary display to my main computer (for when my MacBook Pro is chugging away at some task), as an ebook reader, as a home lifestyle device that is more usable than my iPhone, as a photo frame, etc. --- then I can hear the wheels beginning to turn in their heads.

Now I haven't yet used an iPad (I'll be picking mine up tomorrow), but I suspect that for many people for whom the iPad is actually an ideal computer, they simply aren't aware of what it can do for them or how much they would use it. I realize it's silly to say that considering Apple is probably going to sell millions within the first month, but I believe that there is a *huge* market out there just waiting to be tapped.

The main reason for this is simple: it's more natural to pick up a slate and touch it. It really is. We've trained ourselves to use abstractions like touchpads, sticks, trackballs, mice, keyboards, and so forth --- and in certain use cases these abstractions will be faster and more productive for those who are well trained to use them --- but for people who have very basic computer skills (or none at all, and I'm not kidding when I say there are people out there who don't know anything about computers), the iPad revolution will become their personal revolution.

In 1984, the first Macintosh came into the world and kickstarted the whole graphical user interface phenomenon. 2010 may prove to have the same level of significance. Bottom line: I think this is going to be a very interesting year in the history of technology.
