---
date: "May 4, 2010 – 12:13 pm"
title: "No Competition for the iPad? Uh Huh."
category: archived
funny_caption: "It Stands Alone for"
icon: rainbow-apple
archive_url: "https://web.archive.org/web/20130917050130/http://ipadartistry.com/2010/05/no-competition-for-the-ipad-uh-huh/"
---

<em>[2024 Editor's Note: it's wild to consider that the tablet market these 14 years later is STILL iPad…and then a distant showing of Samsung…and then everyone else. Incredible. For what it's worth, my daughter insists that her iPad is NOT a tablet. It's an iPad. That is what it is. It doesn't just define the category, it IS the category.]</em>

[Reading this CNN article by John D. Sutter](https://web.archive.org/web/20130917050130/http://www.cnn.com/2010/TECH/ptech/05/04/tablet.competition.ipad/index.html), it reminds me that the iPad marks a singular event in the history of personal computing: a hot new product coupled a robust platform for app development that has no competition. When was the last time you saw that happen? There have been multiple hardware systems, OSes, languages, development frameworks, distributors, etc. in every market segment of the computer industry for over three decades. That is, until now.

Apple has accomplished the unthinkable and created a computing device (which has sold [over 1 million in less than a month](https://web.archive.org/web/20130917050130/http://www.apple.com/pr/library/2010/05/03ipad.html)) that is simply it. If you want a next-generation touchscreen tablet, you buy an iPad. What else are you going to buy? The mythical HP Slate project is on hold while HP figures out what to do with Palm's webOS platform [they just acquired](https://web.archive.org/web/20130917050130/http://www.hp.com/hpinfo/newsroom/press/2010/100428xa.html). Microsoft has canceled Courier, a vaporware demo of a dual-screen tablet featuring an advanced touch interface. What else is there?

The most logical choice for a company wanting to compete with Apple is to take Google's Android platform and scale it up to tablet size, but the work involved in creating a usable experience and coordinating that with Android's semi-open development strategy means that a true iPad competitor will likely not be ready to show, much less release, until this year's holiday season. And, really, who's going to want to wait until 2011 to buy a tablet that probably won't even have 1/10th the breadth of applications that the iPad does right now?

What Apple needs to do to keep the momentum going is push the envelope of what the iPad hardware is capable of. The upcoming release of iPhone OS 4 this fall for the iPad will add multitasking support and some other important features that make it more usable for all-purpose computing, but the other side of the equation is what the iPad can connect with. The current dock connector is capable of doing many things, but without the right development APIs and support for third-party add-ons, the iPad will be difficult to integrate into a multi-device ecosystem. My biggest hope as a creative guy is that Apple will work with audio manufacturers to allow USB audio interfaces, MIDI connectors, mixers, etc. to integrate with the iPad. At that point, an entire community of performing artists will be glued to their iPads.

What other kinds of uses can you see for the iPad going forward?
