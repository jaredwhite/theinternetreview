---
date: "Tuesday, February 2nd, 2010 at 3:07 pm"
title: "What a Designer Needs to Know Nowadays…"
category: archived
funny_caption: "That Sure is a Lot on"
icon: photoshop-7
archive_url: "https://web.archive.org/web/20100223025132/http://netnotes.siteshine.com/2010/02/what-a-designer-needs-to-know-nowadays/"
---

[A bit of a humorous look](https://web.archive.org/web/20100223025132/http://css-tricks.com/designers-these-days/) from Chris Coyier at all the different skills, tools, and techniques designers need to know. I fear it’s only going to get more complex from here on out!

> **[Designers These Days](https://web.archive.org/web/20100223025132/http://css-tricks.com/designers-these-days/)…**  
> … have a good design sense and understand the fundamentals / design principals.  
> … know all the major design software including the entire Adobe Creative Suite.  
> … have some basic video editing skills.  
> … know HTML, CSS, and JavaScript.  
> … know enough about server-side languages (PHP, ASP, Ruby, Python, etc) to understand how they work, what they do, and the possibilities of their use.  
> … know about servers, hosting, domain registrants, DNS, etc. Setting it up, and fixing it when it breaks.  
> … know OS X really well (and enough Windows to get by) or know Windows really well (and enough OS X to get by) and know a huge variety of utility software that goes with.  
> … are good photographers.  
> … can color correct photos and work in RAW.  
> … can cut clipping paths or otherwise extract objects from photos.  
> … have a killer online portfolio.  
> … are a personable, nice people that are good with clients.