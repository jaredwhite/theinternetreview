---
date: Tue, 24 Dec 2024 13:49:35 -0800
title: Apple could “remove Google Search” as a choice in Safari
description: Eddy Cue never fails to disappoint. This stuff is wild!
icon: rainbow-apple
funny_caption: More Big Tech Shenanigans on
---

In a [court filing on Monday](https://fingfx.thomsonreuters.com/gfx/legaldocs/zgvoalybovd/apple%20declaration.pdf), Apple Senior Vice President Eddy Cue had a lot to say about the antitrust lawsuit against Google. And surprise, surprise, Apple wants to keep the **$20 billion** revenue sharing agreement in place. Ah Eddy Cue, you never fail to disappoint.

Some of the more wild stuff in this filing:

> Google Search is currently the default search engine on Safari in the United States.

Note that phrasing "in the United States" — because guess what? Google is _not_ the default search engine in some regions outside of the U.S. like, oh I don't know…**the EU**?

> In exchange for distributing Google Search on Apple devices, Google shares with Apple a percentage of the revenue generated from Apple users search queries

Well I'm an Apple user, and I never asked for my search queries to be monetized such that they provide Apple with kickbacks. This is ethically indefensible, but that won't stop Apple from defending it!

> Apple is relentlessly focused on creating the best user experience possible and explores potential partnerships and arrangements with other companies to make that happen. If the remedies above were implemented, it would hamstring Apple's ability to continue delivering products that best serve its users needs.

You know what Apple? The "best user experience possible" would be to allow users to choose which search engine they prefer to use when setting up Safari for the first time, and perhaps even prompt them to reevaluate that choice on occasion. And oh this is rich:

> If this Court prohibits Google from sharing revenue for search distribution, Apple would have two unacceptable choices. It could still let users in the United States choose Google as a search engine for Safari, but Apple could not receive any share of the resulting revenue, so Google would obtain valuable access to Apple's users at no cost. Or Apple could remove Google Search as a choice on Safari. But because customers prefer Google, removing it as an option would harm both Apple and its customers. 

I'm so confused. If "customers prefer Google", why would Apple opt to remove it entirely as an option (in the United States)? And as for Google obtaining "valuable access to Apple's users at no cost"—wait, I thought providing users access to Google Search by default was the "best user experience possible"? So who cares if Google gets used at no cost?

Eddy Cue's word salad is ludicrous and insulting to our intelligence. You can't have your cake and eat it too. Obviously Apple is a giant money printing machine so of course they don't want to lose their very lucrative revenue sharing agreement with Google, but if that agreement were rendered null and void through antitrust remedies, it wouldn't harm _Apple users_ in the slightest. Furthermore, Apple continuing to toe the line of holding onto the Google default in the United States rather than adding a prompt like in the EU is part of the broader strategy of claiming every change foisted upon Apple **for its own misdeeds** is somehow bad for user experience. Balderdash!

Eddy Cue, you might have a chance at fooling someone in the legal system as this antitrust lawsuit moves forward, but you're not fooling Apple users. This revenue sharing agreement is distasteful at best and _illegal_ at worst. We can see right through your empty claims to be fighting on behalf of user experience.
