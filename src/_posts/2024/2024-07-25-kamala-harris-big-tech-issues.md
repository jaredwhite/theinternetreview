---
date: Thursday, July 25, 2024 at 12:40:10 PM PDT
title: Big Tech Terrified or Thrilled by Top of Ticket Kamala Harris?
description: How she fares on these issues will either vindicate or put a damper on hopes she'll go toe-to-toe with the tech industry.
icon: math
funny_caption: Tech’s Best of Times, Worst of Times for
---

<image-figure caption="AP Photo/Kayla Wolf" caption-right>
  ![Vice President Kamala Harris at a campaign stop in West Allis, Wisconsin](/2024/vp-kamala-harris-campaign.jpg)
</image-figure>

There's a lot riding on the newly formed Vice-President Harris campaign for United States president. Her meteoric rise to online stardom in the wake of President Biden's decision to step down after his first term is already the stuff of Internet legends.

**Of particular interest to this audience: where does she stand on the issues when it comes to Big Tech and the excesses of Silicon Valley?**

On the one hand, she quite literally hails from the California Bay Area. She personally knows many of the movers and shakers in the Valley. She's already received a groundswell of support from tech moguls who had cooled on the prospects of a Biden-Trump rematch and were even toying with the idea of leaning Trump. Kara Swisher, long time journalist covering the tech beat, [puts it this way](https://www.threads.net/@karaswisher/post/C9yVlANub0X?xmt=AQGzCK9mIZFBKEse2NMDnYogJuj3SBW9mzJdYiOprJAddQ):

> Big reveal: Kamala Harris actually has more big money moguls in tech than Trump, whose backers are largely aggrieved rich dudes who are disliked in SV because they are, well, jerks. Hers are likely to include Laurene Powell Jobs, Melinda Gates, Ron Conway, Vinod Khosla, Sheryl Sandberg, and many more. I would not be surprised to see MacKenzie Scott and Bill Gates give. Not sure about Mark Zuckerberg or Jeff Bezos. But watch carefully to see the shift back to her and away from Trump.

On the other hand, Harris has a record of going toe-to-toe with the tech industry on certain issues in the past. [Politico reported back in 2020](https://www.politico.com/news/2020/08/11/what-kamala-harris-believes-key-issues-positions-and-votes-393807):

> Harris has been critical of Facebook and other social media companies’ handling of political speech, hate speech and misinformation online. But like Biden, she’s offered few proposals to address those issues. She’s also taken a more moderate stance on antitrust concerns, stopping short of embracing Elizabeth Warren’s call to break up tech giants — though she’s advocated for closer scrutiny from regulators.
> 
> When she was attorney general of a tech-heavy state, Harris played a central role in the fight against online sex harassment, pressing tech companies to make their platforms safer and advocating for greater leeway to prosecute cybercrimes. That effort culminated, most notably, in the arrest of the heads of Backpage.com over allegations that the classified ad website had knowingly become a hub for online prostitution.

And in [more recent Politico reporting](https://www.politico.com/news/2024/07/24/kamala-harris-tech-donors-00170780):

> She has been an industry critic in the past, calling out tech companies for “revenge porn” as California attorney general and going after Facebook as a senator during the Cambridge Analytica privacy scandal. And as Biden’s vice president, she faces questions about whether she’d continue his legacy of pressing lawsuits against some of the biggest tech firms, including Amazon, Google and Apple.

However, Box CEO Aaron Levie indicated she might need to soften her tough-on-tech perception to win SV over:

> Levie suggested that some industry players are looking for her to make the first move: “If by the end of the week she had a tech policy framework out there, a 10-point plan for pro-business, pro-tech, pro-entrepreneurship, and it was credible,” he said, “I think she could very quickly rally a significant portion of the ecosystem.”
> 
> Levie added that he’s talked to “a dozen-plus tech CEOs [and] leaders that are very hopeful that she does, and are absolutely willing to support her if she does.”

_Jared's Take:_ Pro-entrepreneurship? Sure, nobody's going to argue with that. Pro-business/pro-tech? If by that Levie means turning a blind eye to the numerous missteps and _enshittification_ plauging our modern digital landscape thanks to the lack of federal oversight and—yes—regulation to date of the tech industry, then one can only hope Harris trends carefully here indeed.

[Brandon Vigliarolo writing for The Register](https://www.theregister.com/2024/07/23/kamala_harris_technology/):

> During her time as California Attorney General, Harris was responsible for a number of lawsuits filed against major tech companies. She won a $4 million settlement from eBay pertaining to its no-poach staff agreement with Intuit in 2014, and the next year secured a $33 million payout from Comcast for privacy violations.
> 
> Harris has also suggested that breaking up Facebook would be on the table had she the power, and the Vice President has expressed support for Biden's attempts to regulate the AI industry. That won't go over well with the growing crowd of machine learning enthusiasts who have thrown their weight behind former president Donald Trump's re-election bid – Trump has promised to kill Biden's AI executive order if he takes office.

Yet during Harris' 2020 presidential run, she received _mucho dinero_ from tech companies:

> Campaign finance records also indicate Harris has been a frequent target of giving from the tech sector, with lobbyists from Facebook, Google, Amazon, and Apple all forking over max-dollar donations for her 2020 presidential run.

The Verge seems to think Kamala Harris is a bit of an enigma—“a cipher”—[when it comes to a clearly-defined tech policy](https://www.theverge.com/24205360/kamala-harris-tech-policy-explainer-democratic-presidential-election):

> We know where she stands on climate, we have some sense of how she feels about privacy, and we have a whole array of tantalizing statements about AI, but there is a wide range of key questions that she has yet to be asked or has successfully avoided answering. She remains an enigma when it comes to tech antitrust and the TikTok ban. And she has yet to speak directly to the issues that most concern the moneyed donor class of Silicon Valley, such as crypto regulation.

It's possible Harris will emerge as tough-as-nails on privacy and deepfakes, while continuing down the more centrist path we've seen so far from the Biden-Harris administration when it comes to generative AI. [Kyle Wiggers writing for TechCrunch indicates](https://techcrunch.com/2024/07/24/this-week-in-ai-how-kamala-harris-might-regulate-ai/):

> I also spoke with AI policy experts to get their views. For the most part, they said that they’d expect consistency with a Harris administration, as opposed to a dismantling of the current AI policy and general deregulation that Donald Trump’s camp has championed. Lee Tiedrich, an AI consultant at the Global Partnership on Artificial Intelligence, told TechCrunch that Biden’s endorsement of Harris could “increase the chances of maintaining continuity” in U.S. AI policy.

[Reuters points out](https://www.reuters.com/world/us/us-vice-president-harris-views-business-issues-2024-07-21/) that when Harris was California attorney general, she took a hardline stance against "revenge porn"—an issue which one could argue most people on both sides of the aisle feel strongly about and is therefore popular to champion:

> One of her signature issues was curtailing the distribution of pornography on social media, particularly “revenge porn,” a practice involving the posting of explicit photos without the subject’s consent. She took credit for a pressure campaign that led to Facebook, Alphabet's Google, Microsoft, and others taking measures to remove certain explicit images.
> 
> “I cannot emphasize enough how leaders in technology have stepped up,” said Harris at a news conference then. “I’m not suggesting any of them were happy to get a call from the AG saying, ‘Come in, we want to talk with you.‘ But they all did. They did.”

And on the sentiment among tech executives regarding a Harris precidency, [Tim Bajarin writes for Forbes](https://www.forbes.com/sites/timbajarin/2024/07/25/how-a-harris-presidency-could-affect-big-tech/):

> One big question about a potential Harris presidency is whether she would alter the current ongoing tech antitrust cases at the DOJ and FTC. I would be surprised if she changed the current leadership of these agencies since, as Vice President, Harris has been close to these cases and has not shown any sign of opposing them.
> 
> I spent the last two days talking to senior executives of major tech companies about their views on a Harris presidency's potential impacts on the technology industry. Here is a summary of those discussions.

It's clear Harris will need to carefully thread the needle of harnessing popular anger at Big Tech for a variety of reasons as it's lost the moral high ground in recent years, while also not turning off leaders in the tech industry who would otherwise lend significant public (and financial) support for her campaign. How she fares on these issues this election cycle, as well as the lengthy list of other signifcant issues facing American democracy, will either vindicate or put a damper on hopes she'll significantly take on the tech industry if and when she holds highest office in the land.