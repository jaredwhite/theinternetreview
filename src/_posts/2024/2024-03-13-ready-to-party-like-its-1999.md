---
date: Wed, 13 Mar 2024 11:14:55 -0700
title: Why I’m Ready to Party Like It’s 1999…Again
description: Never have I ever seen so many parallels between the early days of the Internet and today. History is repeating itself.
icon: phone-atom
funny_caption: Dial-Up Modems Can Stay in the Past Tho…
---

**The Internet is going through a major upheaval.** Mega-corporations are trying to box consumers into proprietary platforms. A frothy VC market chasing after the Next Big Thing is beginning to see major warning signs. Top operating systems vendors have gotten the smackdown for their monopolistic business practices, being forced to offer real choice for access to third-party browsers and other key software. A growing backlash against technology's dominance threatens to stall the heady growth of the industry. The nerd set is fighting back against capitalist entrenchment, building new open infrastructure that respects user privacy and eliminates gatekeepers. A revolution is underway to make it even easier to publish on the web, push content and software features across networks, and find meaningful successful as an indie producer. 

*Wait, which decade am I describing here? The late 90s? Or now??*

**Exactly.** 😃

## History Repeats Itself (Like Poetry, It Rhymes)

Never have I ever seen so many parallels between the early days of the Internet, when I first started as a blogger before the term had even been invented, and today. It really feels like we've entered a whole new era, and yet **so many aspects of what is happening right now seem eerily familiar**.

Those who fail to learn from history are doomed to repeat it, we are told. Yet a great deal of the history of the early Internet is in danger of disappearing—lost on backup hard drives in weird formats at the back of closets, or simply in a state of disrepair [buried inside of Wayback Machine](/2013/05/10/thank-goodness-for-wayback-machine/).

Increasingly over the past couple of years, I've found myself drawn to **sifting through my own archives** while trying not to feel utterly gaslit when presented with absurd claims such as "web3" correcting all the failings of Web 1.0 and Web 2.0 *via a narrative that makes absolutely no sense* if you remember the true history of Web 1.0 and 2.0. Or that "podcasting" was invented as a mechanism to play internet radio shows on iPods and had nothing to do with **the technical underpinnings of the RSS format**…which supposedly means that today's exclusive audio shows can still be called podcasts even if no RSS feeds are made available. 🤨

As we grapple with the challenges of the present, such as the rise of generative AI, viral social media platforms like TikTok, increased regulatory pressure to fight entrenched device and service vendors, and an inability to curtail radicalization through disinformation; as well cheer on the positive movements which are reshaping the fabric of the web like the Activity Pub-fueled Fediverse, Smol Web / IndieWeb, and the shift from cloud services back to self-hosting and on-prem; it is clear we _must_ understand our digital history more than ever. **It is crucial that our decision-making today and tomorrow be informed by learning the lessons of the past—both our mistakes and our successes.**

## Once a Tech Blogger, Always a Tech Blogger

I don't remember the exact moment at the beginning of 2023 when the idea first popped into my head to reboot my original tech blog. It was one of those ideas that simply exploded into action from the moment it was conceived. I think it's possible the time frame between initially having the idea and coming up with one of my favorite domain names of all time (_theinternet.review_) could be measured in hours, if not minutes.

You may be wondering how could I have had a tech blog in 1996? I must admit my memory is hazy, but as a young nerdy teenager who had just logged onto "Trumpet Winsock"—my ticket to cyberspace—a couple of years earlier, building Web sites was quickly becoming one of my favorite things to do in life. I had built a site for my family's Celtic music band in 1995, and by 1996 the most logical thing I could think of for what to do with this newfangled technology called the Internet was to **write about the Internet**. 😂

So I launched my first publication, first called "Jared White's Internet Review", which quickly changed to "The Internet Review", which subsequently changed yet again to "iReview" (yes, years before Apple ever did anything with that name!).

I think that lasted a little ways into 1997, but eventually my attention drifted elsewhere and I became involved with another tech publication started by another group of teenagers (in hindsight a colossal mistake, but I digress). And so iReview left the stage.

Then in 1999, I rebooted iReview as a new publication to talk all about my then-favorite operating system, **BeOS**, along with the many media applications which were coming onto the market. Unfortunately it was a rather short-lived operation because Be imploded—in no small part thanks to Microsoft's monopolistic stranglehold on the PC market.

Later, throughout the 2000s I would run a series of tech blogs all titled "The Idea Basket"—a name which now sounds incredibly dumb to me but which afforded me the ability to publish some really nifty articles in the heady days of the Mac OS X renaissance. I even got to [interview some notable people such as Stuart Cheshire](/archived/2002/07/18/stuart-cheshire-interview/), lead inventor of Zeroconf (Bonjour) networking technology.

My tech blogging became more fractured after that, and I only started to get serious about writing about tech again on my own personal site a few years ago. But my blog at [JaredWhite.com](https://jaredwhite.com) was never intended to be a tech blog per se, and other properties I've launched in recent times such as [The Spicy Web](https://www.spicyweb.dev) and [That HTML Blog](https://thathtml.blog) are targeted specifically at web developers, not a general technology audience.

So it's safe to say I haven't seen myself as a "tech blogger" in well over a decade. But something's changed all that, and **that something is the Fediverse**.

Ejecting from the hellscape that is X-formerly-Twitter once it had been taken over by Elon Musk in 2022 was incredibly painful…and yet it was the _absolute best thing_ that's ever happened to me online since I first got online in the 90s. 🙌

**You can feel it in the air.** What's old is new again. Blogs are returning. RSS is again ascendant. Email newsletters—popularized once more by Substack but now migrating to even better services—are all the rage. Mastodon, Pixelfed, Lemmy, and other open source software are enabling new social networks to form—**yet this time no longer beholden to Big Tech**. And even the tech companies attempting to keep their grasp on an increasingly disgruntled audience are having to adapt (witness the federated future of Threads).

Sure, there will be fits and starts along the way. You can't build Rome in a day. Yet I'm more optimistic than I've been in a long, long time. **The Internet is healing.** And that process is only now getting started.

## Retro Nostalgia

One of the wilder aspects of our present moment as internet nerds attempt to take back control of their online destiny is all the nostalgia around the esthetic of the World-Wide Web of the 1990s—down to 88x31 buttons, pixel art, animated GIFs (pre-meme culture of course!), chunky 3D text with gradients and drop shadows galore, and funky textured backgrounds. And lest we forget: 🚧 **This Web site is under construction.** 🚧

While I think it's perfectly legit to embrace some historical concepts for the Web going forward without diving straight into 90s nostalgia, it's fun to see a modern retro take on this formative era. As I was contemplating the reboot of **The Internet Review**, I started thinking about how fun it would be to actually capture some aspects of the aesthetic. I could pull out colors and font choices, older ways of formatting and describing things, and even attempt a redo of the logo design itself—**all recontextualized for a modern audience**.

So that's how we arrived here, and I still have ideas to push the design even harder in that direction. Obviously there are new considerations to keep in mind as well: responsiveness across screen sizes and device types, accessibility, and readability. On that last point: **Yes, I'll be adding a toggle switch for light mode** (because I know not everyone appreciates permanent dark mode).

The vibe of old icons, GIFs, and off-the-wall art & typography may not resonate so much with young people today…yet I think it offers a window into more of a "maker" mindset, something which somehow got lost along the way. I predict the next generation will more frequently chafe against the restrictions and framing of Big Social. I already see this reflected in my own children…becoming a YouTuber as a future career path is losing its luster. How can you compete with the elongating tail of AI sludge and the handful of big-budget producers who've successfully gamed the algorithm?

**There's a better Internet out there.** We just need to build it. _Again._

[You with me?](https://intuitivefuture.com/@theinternet) 🤝
