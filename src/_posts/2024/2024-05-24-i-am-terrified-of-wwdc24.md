---
title: I Am Terrified of WWDC24. But Perhaps I Shouldn't Be
description: If anyone can get the process of platform AI-fication at least somewhat right, it's Apple. I'm choosing to remain cautiously optimistic.
date: 'Fri, 24 May 2024 11:44:10 -0700'
icon: rainbow-apple
funny_caption: Do I Believe in Magic? 
skip_figure_markdown: true
---

<image-figure markdown="span">
  ![a rainbow vortext of different sized dots](/2024/color-vortex.jpg){:style="width:100%"}
</image-figure>

## If anyone can get the process of platform AI-fication at least somewhat right, it's Apple. I'm choosing to remain cautiously optimistic.
{: style="font-size: var(--subheading-font-size-fluid)"}

It has not been all unicorns and rainbows lately regarding coverage of Big Tech's ~~Borg-style assimilation of the Internet~~ embrace of Generative AI.

> “[Google promised a better search experience — now it’s telling us to put glue on our pizza](https://www.theverge.com/2024/5/23/24162896/google-ai-overview-hallucinations-glue-in-pizza)” – The Verge

> [From The Onion to Google Search: Eating small rocks](https://www.threads.net/@crumbler/post/C7VGpYSPOgT) – Casey Newton on Threads

> [Snake fights at universities over thesis defense](https://discuss.systems/@ricci/112493842753390258) – Rob Ricci

> Google: “[You can use gasoline to make a spicy spaghetti dish](https://mastodon.social/@JoeUchill/112493317168967705)” – Joe Uchill

😂

Or how about these nuggets:

> [OpenAI is sticking to its story that it never intended to copy Scarlett Johansson's voice when seeking an actor for ChatGPT's "Sky" voice mode](https://arstechnica.com/tech-policy/2024/05/sky-voice-actor-says-nobody-ever-compared-her-to-scarjo-before-openai-drama/) – Ars Technica

> [The Low-Paid Humans Behind AI’s Smarts Ask Biden to Free Them From ‘Modern Day Slavery’](https://www.wired.com/story/low-paid-humans-ai-biden-modern-day-slavery/) – WIRED

> “[I got ahold of the Copilot+ software. It 100% does not need physical access and can be stolen](https://cyberplace.social/@GossiTheDog/112492445214914228)” – Kevin Beaumont

🤡

Like I said, media and public skepticism is clearly on the rise even as the hype cycle continues to, well, cycle.

We've been through a few big waves of announcements now by the major players: Google, OpenAI, and Microsoft. And we'll probably hear more from Meta as we approach the fall. But at this present moment, all eyes are drawn towards the direction of Cupertino as we await the great unveiling of Apple's "artificial intelligence" strategy at WWDC24.

## Two Competing Narratives

If various tech and Apple-focused pundits are to be believed, Apple _must_ come up with a coherent AI strategy to compete with the likes of Google/Microsoft/OpenAI. Siri is considered a dinosaur, and nothing short of a Her-like demo—_oops, too soon?_—will placate investors, reassure power users, and cement Apple as a credible innovator in this new "Age of AI".

**I believe this is the entirely wrong narrative.**

Here's what I believe Apple _must_ do at WWDC24: _show incredible restraint_.

Apple still has a unique legacy among the giants of computer technology. Historically—in Apple's own words—they have stood at the "intersection of technology and the liberal arts". In the words of Steve Jobs after the launch of the first iPad:

> "Technology alone is not enough. It’s technology married with the liberal arts, married with the humanities, that yields the results that makes our hearts sing."

<image-figure markdown="span">
  ![Steve Jobs at the intersection of technology and the liberal arts](/2024/technology-and-liberal-arts.jpg)
</image-figure>

And let us not forget that famous video about the "thousand no's" Apple must say in order to say "yes" to that which is truly essential:

<image-figure>
  <iframe src="https://invidious.darkness.services/embed/uZmK_92Vkh0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</image-figure>

> if everyone is busy making everything  
> how can anyone perfect anything?
> 
> we start to confuse convenience with joy  
> abundance with choice.
> 
> designing something requires focus  
> the first thing we ask is what do we want people to feel?
> 
> delight  
> surprise  
> love  
> connection
> 
> then we begin to craft around our intention  
> it takes time…
> 
> there are a thousand no's  
> for every yes.
> 
> we simplify  
> we perfect  
> we start over  
> until every thing we touch  
> enhances each life it touches.
> 
> only then do we sign our work.
> 
> Designed by Apple in California

You may be rolling your eyes at the pretentiousness of it all, but I consider this manifesto nearly as vital to understanding the DNA of Apple as the famous Think Different campaign of the late 90s. "[Here's to the crazy ones…](https://invidious.darkness.services/watch?v=-z4NS2zdrZc)"

Apple fans like myself—who witnessed the astounding and never-to-be-equalled "second coming" of Jobs, who saw him lead the company to such heights as the iMac, the iPod, the iPhone, and the iPad—recognized the fruits of this philosophy. We could see how other companies would simply release technology for the sake of technology—they forgot the _value of saying no_ until it's the right time and the right concept and the right set of principles to say *yes*.

**This is what Apple must demonstrate in 2024.** That they haven't forgotten what makes Apple, Apple. That they haven't strayed from their DNA.

The [horrifying "Crush" ad they released and then apologized for](https://www.macrumors.com/2024/05/10/apple-crush-ad-apology/) as part of the recent iPad Pro campaign is certainly cause for alarm. The very idea that the "liberal arts" & "humanities"—human creativity and flourishing over a thousand-year span—could be soullessly machine-crushed into bits just to sell a new computer stood against _everything_ we trusted Apple to value. I certainly hope that they learned their lesson from the backlash—but even so, it's pretty hard to pivot whatever they'd been planning for a long time to cook up for WWDC.

**Nevertheless, I choose to remain positive.** If there's any Big Tech company—any at all—who stands a reasonable chance of getting this AI rollout at least somewhat right, who is able to demonstrate restraint in how they frame the feature set, who is able to balance thoughtful utility with sincere ethical considerations, and who understands the deeply-held concerns of the creative community, it's Apple.

I can hear you scoffing already—trust me, I understand how skeptical you are! But we already know Google is _institutionally incapable_ of getting this right. Microsoft is constantly reminding us they have no taste. And OpenAI is…well, run by an asshat. (And don't get me started on Meta.)

 As I [wrote recently on my personal blog](https://jaredwhite.com/articles/they-forgot-its-bicycle-for-the-mind-not-jet-airplane):

> Increasingly, we find ourselves being sold tools which aren’t simply “augmentations” of the capabilities we already have, integrated into the real-world environments and habits and social connections we actually engage with, but are total worlds unto themselves. More and more, we find ourselves captive to our tools, rather than liberated by them. And more and more, we are at the mercy of the platform operators.

So we're getting down to the wire here. If Apple can't figure this out, if Apple can't thread that needle, then we're truly doomed. But perhaps they'll—dare I say it—_surprise and delight us again_. Maybe it's possible we'll behold a Macintosh of AI, an iPod of AI. Maybe they've been able to chart a course that really does take advantage of their unique mixture of hardware, software, and services with a nod to that secret-sauce intersection between tech and art.

**I guess we'll find out in two weeks.** I just fret the popular narrative once again will be all wrong. (Either they throw a bunch of spaghetti at the wall which is nothing more than flash-in-the-pan whiz-bang tech demos, making pundits and investors happy all while shredding their ultimate reputation and legacy…or they indicate a modicum of sense and dial in only enough "magic" to be fairly useful for a constrained set of specific use cases using opt-in models which were ethically trained and run locally for enhanced privacy and security—only to be pilloried by cynical talking heads for "not doing enough" and falling behind their competitors.)

In the end, it's all up to Tim Cook. Is he able to steer this ship in the right direction, bravely upholding Apple's core values and the reasons they've had such a loyal userbase for so long? Or will he complete the long-feared transformation of Apple into just another generic Silicon Valley corporate leviathan with yawn-inducing marketing and the pall of late-stage capitalism—morality be damned?

_Come back here on June 10th for the riveting conclusion!_

----

_Image credit: [Gordon Johnson on Pixabay](https://pixabay.com/vectors/vortex-circles-background-wallpaper-5268247/)_