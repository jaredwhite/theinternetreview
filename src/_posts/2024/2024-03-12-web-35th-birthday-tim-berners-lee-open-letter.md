---
date: Tue, 12 Mar 2024 11:13:07 -0700
title: Big Tech on Blast as Sir Tim Berners-Lee Reflects on the Web's 35th Birthday
description: Happy Birthday, WWW. It’s been one hell of a ride. Now we’ve got some serious work to do to guarantee another 35 years.
icon: world
funny_caption: How It Started…How It's Going…
---

<image-figure caption="Image © CERN" caption-right>
  ![an early photo of Berners-Lee in front of a NeXT computer showing the first browser ever](/2024/berners-lee.jpg)
</image-figure>

35 years ago today, the beginnings of what would become the "World-Wide Web" was published by Sir Tim Berners-Lee while working as a software engineer at CERN in Geneva, Switzerland. In a proposal modestly titled "Information Management: A Proposal", Berners-Lee presented a vision of an interconnected network of hypertext documents. The feedback he received from his boss, Mike Sendall? "Vague but exciting" 😄

Some time passed, and by October 1990, Berners-Lee had created a working prototype of the Web including foundational technologies such as HTML, URI/URL, and HTTP. For more on the early history of the web, [there's an excellent writeup over at the World Wide Web Foundation](https://webfoundation.org/about/vision/history-of-the-web/).

As we celebrate the Web turning 35 🎉, the cheers must be tempered by a moment of sober reflection, because as Berners-Lee puts it, "exploitative business models" which encourage "passive observation of content"—as well as "personal data markets" which allow for "targeted advertising"—threaten the health and safety of this global technology.

[In a new Open Letter published on the World Wide Web Foundation](https://webfoundation.org/2024/03/marking-the-webs-35th-birthday-an-open-letter/), Sir Tim Berners-Lee writes:

> Underlying [the web's] whole infrastructure was the intention to allow for collaboration, foster compassion and generate creativity — what I term the 3 C’s. It was to be a tool to empower humanity. The first decade of the web fulfilled that promise — the web was decentralised with a long-tail of content and options, it created small, more localised communities, provided individual empowerment and fostered huge value. Yet in the past decade, instead of embodying these values, the web has instead played a part in eroding them. The consequences are increasingly far reaching.

As you can see around **The Internet Review** which just so happens to have launched today (hmm, it's almost like we planned it! 😉), in the early days of the web there was an expectation that the _primary_ goal of the web was to eliminate the content gatekeepers. Instead of having to be hired by a newspaper and publish only what they permit, you could start your own news website. Instead of a record label discovering you and putting their marketing dollars behind a hit record, you could simply self-publish CDs and sell them off your band website—or even provide audio files directly! And later on as video became higher-quality and simpler to work with, instead of having to sign on to a TV crew and work with studio executives, you could just edit a video on your computer and post it online for the world to see.

**(Spoiler alert: I did all of those things, and more besides!)**

So how has the Web come to this? How did it migrate from a decentralized platform featuring long-tail content to the fiefdom of corporate silos? **Berners-Lee offers some explanatory analysis:**

> Leadership, hindered by a lack of diversity, has steered away from a tool for public good and one that is instead subject to capitalist forces resulting in monopolisation. Governance, which should correct for this, has failed to do so, with regulatory measures being outstripped by the rapid development of innovation, leading to a widening gap between technological advancements and effective oversight.

In other words, Big Tech took over _because we let them_. Collective action, resulting in meaningful regulation to curtail these monopolistic excesses, has been severely lacking.

I won't spoil the rest of Sir Tim Berners-Lee's article because you should go read it right now. But I'm glad he's speaking out so forcefully about these issues—even mentioning decentralized fediverse software like Mastodon (as well as Bluesky) by name.

As the letter states, "the time to act and embrace this transformative potential is now,"
and I couldn't agree more. We're proud to stand with the creator of the World-Wide Web on this momentous occasion here at **The Internet Review** and will endeavor to keep reporting on the new decentralization movement and the Fediverse with aplomb.
