---
date: Tue, 02 Jul 2024 09:30:52 -0700
title: Hype Cycles, Come Again No More
description: Did you ever stop and think “wait a minute, hold the phone…what if Hype Cycle® is itself a hype cycle?”
icon: phone-atom
funny_caption: Logging Off from “Big Tech” for
---

On this most auspicious occasion, I'm ecstatic to proclaim the launch of our official newsletter, [Cycles Hyped No More](https://buttondown.email/theinternet). From our first issue, [Hype Cycles, Come Again No More](https://buttondown.email/theinternet/archive/hype-cycles-come-again-no-more/):

> As a companion to my recently resurrected tech blog, The Internet Review, I felt that in order to launch this newsletter, I had to come up to a very particular hook—something that would keep _me_ excited and engaged over the long-term (let alone you dear reader!).
> 
> **It came to me in a flash** when I discovered (in true TIL fashion) that the concept of the hype cycle — or rather, Hype Cycle® — is actually a designed product of the Gartner, Inc. consulting firm. Seriously, look it up! Like, no joke: this company quite literally invented a particular way of describing technological development and happened to be so successful at it that now people think it's some inherent property of the world. Like gravity or nuclear force or the propensity of Elon Musk to utter the most juvenile dreck you've ever heard spew from the mouth of a multibillionaire.
> 
> Did you ever stop and think “_wait a minute, hold the phone…what if_ **Hype Cycle®** _is itself a hype cycle? one which perhaps never got much farther past the Trough of Disillusionment?_”
> 
> Personally I feel like I've been on a rollercoaster ride these past few years where I keep being told something is **The Future of Technology™** when in fact it turns out to be a steaming pile. Do I really need to renumerate through all the many examples?

And later on in the essay:

> We desperately need a new grassroots Internet, a digital social fabric built from the ground up to focus on the marginalized as well as the truly skilled, not the “influencers” and the grifters. We need to focus on technologies rooted in _place_, aka the real world. I deeply care about the intersection of new innovations and urbanism in order to build better cities—more humane, safer, cleaner, and more equitable. We need to wrest control back so that every individual human has the capacity to seek ethical means of augmentation—not replacement—by technological progress.

So [read the whole thing](https://buttondown.email/theinternet/archive/hype-cycles-come-again-no-more/), and if you like what you hear, [please subscribe](https://buttondown.email/theinternet) so **The Internet Review** will be downloaded to your personal computer system whenever your enjoy _cruisin' down the Information Superhighway_. 😎
