---
title: Take the Training Wheels off iPadOS, Apple!
description: iPad can do a whole batch of things Mac can't do. Yet a lowly M1 Mac mini can still perform all kinds of processes iPad can't—even with the latest Pros. Oops.
date: Wed, 08 May 2024 13:22:04 -0700
icon: rainbow-apple
funny_caption: Really Going to Kick Lucy's Football This Time on
---

**It’s time for iPadOS to grow up.**

Listen, I’ve never liked the people on the sidelines jeering iPadOS for not being a “real” OS. I’m living proof it’s _absolutely possible_ to do your job and get serious work done on iPad.

But here we are in 2024, and iPad Pro hardware (and even iPad Air hardware now!) is INSANE. Like, truly off-the-charts **wow**. The new iPad Pros will smoke the Mac I’m typing this article out on right now (which is a lowly M1 Mac mini).

And yet…my lowly M1 Mac mini can perform all sorts of complex processes which are difficult or even impossible to do even on the very latest iPads.

**It makes no sense.**

Now let it be said: iPad _can_ do a whole batch of things which aren’t fun/easy to do on Mac. In fact, certain things can _only_ be done on iPad. That’s what iPad fans (like me) find frustrating about the dialogue out there. This device can be—and is!—used for _way_ more than Netflix and email. That’s a dumb take, flat-out dumb, and you don’t need to pay attention to it.

And yet…my lowly M1 Mac mini can perform all sorts of complex processes which are difficult or even impossible to do even on the very latest iPads. 🤪

**I’m sick of this debate!** And I’m tired of having to [write out the same thing](https://theinternet.review/2024/03/29/ipad-fans-you-wont-be-happy-until-june/) and see other pundits saying the same thing year in and year out. You can lift a quote from virtually any reviewer and it’s the same quote:

“This amazing, capable, and attractive hardware is hobbled by the limitations of the OS. Maybe WWDC this year will change everything. Maybe.”

And then WWDC happens and some things change, but it’s not enough. _It’s never enough._

So this year is my last “will they/won’t they” rodeo. It’s not like I’ll be buying a new iPad any time soon anyway (my M2 iPad Pro is working just fine, thank you very much), and I’m definitely interested in what might happen this year at Apple’s developer conference. Maybe iPadOS 18 _will_ change everything. (Don’t hold your breath though.)

Meanwhile, I’m still using my iPad Pro as my only mobile computer (I don’t own a laptop), and between that and my desktop Mac I’ve figured out a workflow that’s **good enough**. iPadOS 16 + macOS Sonoma works for me. And as a fun way to tickle my nerd fancy around operating systems, I’ve been dabbling with Fedora & GNOME. If I do end up buying a laptop eventually, it might yet be an Apple MacBook…but it’ll be [running Linux](https://fedoramagazine.org/fedora-asahi-remix-40-is-now-available/). *All hail the mighty Tux.*
