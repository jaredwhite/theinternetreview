---
date: Wed, 23 Oct 2024 09:52:41 -0700
title: Apple’s Tiniest Desktop Computer is Nearly a Reality
description: Could it be we’re about to see the first redesign of the Mac mini in a long time?
icon: rainbow-apple
funny_caption: Mini yet mighty for
---

As I type these words into a draft in [Bear](https://bear.app), we’re mere days away from Apple’s “October event” (if the rumors hold true) where the next generation of some portion of the Mac lineup is typically announced. Some folks may be keen to hear about M4-powered MacBook Pros, or iMacs with speed bumps and possibly refreshed colors, or updated USB-C peripherals. But far and away my most anticipated Mac product launch of the year is what will grab my attention—that is, a much mini-er **Mac mini**.

Some context: I have used a Mac mini desktop for years. Currently I’m rocking a base-level M1, which definitely doesn’t have enough storage and memory for my liking but that’s because I originally bought it as a “home server” and a way to dip my toes into the shiny new world of Apple Silicon when it was first unveiled. At the time my primary Mac was the 16” Intel MacBook Pro, a real workhorse of a computer. I simply didn’t expect to fall in love with the speed and polished experience of using an M1 Mac. But that’s exactly what happened, and for a couple years now my combo of Mac mini desktop + iPad Pro has been the computing hardware I use exclusively.

So you can imagine my anticipation of upgrading from a base-level M1 to whatever Apple might have in store for the next-gen Mac mini. Here’s what the rumors have been telling us:

* It’ll be *noticeably smaller* than any Mac mini has ever been. [Mark Gurman of Bloomberg](https://www.bloomberg.com/news/articles/2024-08-08/mac-mini-m4-apple-plans-to-release-smallest-desktop-computer-yet) wrote back in August that its size would be reminiscent of an Apple TV box, though perhaps a bit taller.
* It may sport 5 Thunderbolt 4/USB-C ports, [according to this MacRumors report](https://www.macrumors.com/2024/09/16/mac-mini-5-usb-c-ports-leak/), with two on the front (like the Mac Studio) and the other three on the back. It’s unclear if that means the final death of USB-A ports, but given these new size constraints, that seems likely. In fact, I would hazard a guess we won’t _any_ other ports except HDMI.
* Along with the rest of the new lineup, we may _finally_ see a switch to 16GB RAM for the base-level config. All I can say to that is **thank gawd** because the pundits were _very wrong_ about 8GB being adequate in the age of Apple Silicon. I have to close down apps all the time when I have too much stuff running to avoid sluggishness, and I would gladly welcome an affordable starting point of 16GB on new Macs going forward.
* And of course, some folks will be excited about what the M4 chip lineup will provide the Mac mini in terms of faster and more capable “Apple Intelligence” though that offers no draw to me. What _is_ enticing is how much faster it will make my audio & video production work. I was able to finish up and release my new Yarred album [Subterranean](https://yarred.bandcamp.com/album/subterranean) using Logic & Reason (I love saying that every time!), but I definitely ran into occasional glitches and other performance issues with my 8GB M1. The thought of upgrading to at least a 16GB M4 makes my mouth water!

[Over at AppleInsider, Malcolm Owen is guessing](https://appleinsider.com/articles/24/10/22/inbound-m4-mac-updates-rumored-to-arrive-as-early-as-october-28) we’ll see the Mac announcements along with possibly a small event on Tuesday, October 29. There are also new OS releases coming on Monday touting new Apple Intelligence features, so it’s possible we’d see the new hardware then, but I think Malcom’s guess is likely.

Are you looking forward to new M4-powered Macs? Does a smaller redesign of the Mac mini sound intriguing? [Let us know on Mastodon!](https://intuitivefuture.com/@theinternet)
