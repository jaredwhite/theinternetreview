---
date: Tue, 19 Nov 2024 10:17:49 -0800
title: "The Web Browser for Those of Discerning Taste: Vivaldi"
description: I've been using Vivaldi in Fedora Linux, macOS, iPadOS, and iOS. Is this the browser of the hour for power users? Bruce Lawson believes so, and more besides.
icon: world
funny_caption: A Browser for “The Four Seasons” on
---

Throughout the teen years of the 21st century, I was thoroughly ensconced in the ecosystem of Apple Computers. In practice it meant that I used—and _only_ used—Safari. I was the biggest WebKit apologist you'd find anywhere, and I'd be sure to point out the ways that WebKit was in fact the _superior_ browser engine as compared to Blink or Gecko.

Well, if you're here for my _mea culpa_…I’m sorry to disappoint because I still believe WebKit to be a very good browser engine and Safari to be a very good Web browser.

**However…**

For a few years now I've used Firefox as a secondary browser—for testing sites as a Web developer, for using certain apps related to client projects, or perhaps to access a particular browser extension I wasn't able to access with Safari. And I've had nothing against Firefox technically—I've *always* rooted for it to succeed and maintain a healthy degree of marketshare.

But it's become abundantly clear in recent times that Firefox's parent org of Mozilla is [dysfunctional and woefully lagging](https://buttondown.com/theinternet/archive/who-cares-about-mozilla-that-is-a-really-important-question/) in the proper support and promotion of Firefox. And that’s a damn shame, because I've had _more_ reason to use Firefox than ever before for a couple of specific reasons:

* **I'm now a dual-ecosystem user.** I boot up Fedora Linux in a VM on my Mac mini regularly and use GNOME as my desktop. Until very recently, Firefox was the best browser option I found to use in that context.
* On a whim this past summer, I pulled my old 2015 13" Retina MacBook Pro out of cold storage, routed around its fried internal SSD by plugging in a USB stick, and got good ol' Mac OS X Sierra up and running again. And guess what? The most up-to-date browser I could find which works in that legacy environment is Firefox. **Suddenly, a 9-year-old laptop could engage with the modern Web**, letting me use it for some casual development tasks and beyond.

But in my quest to tinker endlessly with Fedora Linux as a desktop environment (which I now _prefer_ in many ways over macOS), as well as my desire to keep my nose to the grindstone in terms of what people in the Fediverse are buzzing about, my attention has turned to the Web browser which seems to be turning a great many heads all of a sudden:

**Vivaldi**.

What follows is my review of the browser, as well as a conversation I had with Vivaldi's Technical Communications Officer, **Bruce Lawson**.

<image-figure caption="Credit: Vivaldi" caption-right>
  ![screenshot of the Vivaldi web browser](/2024/Vivaldi_7-1.webp)
</image-figure>

## The Red-Haired Priest Has Returned

No longer a virtuoso violinist and composer of some of the most exhilarating music you'll come across from the Baroque era, [the Vivaldi of today is a Web browser](https://vivaldi.com/), engineered by a number of folks some of whom worked previously at Opera Software (remember _that_ browser?)—including Opera’s former CEO who is now CEO of Vivaldi, Jon von Tetzchner—and headquartered in Norway.

I asked Bruce what it’s like to be working (again) with such a tight-knit crew:

“It always used to baffle me as a Brit, how the Scandinavians would do this magical self-organizing thing. I have noticed that particularly this bunch, they argue for ages, everybody gets their say—and then magically people go away and make stuff happen. Because everybody is—to use a terrible cliché—on the same page and wants the same thing. And so people express their differences. We always have vigorous debates about open source, open standards, comms, etc. But once the consensus is reached, people just go ahead and somehow make it happen.”

While under the hood Vivaldi is a Chromium-based browser, and hence uses Blink as its engine, Vivaldi offers a distinctive and customizable user interface and a raft of features catering to power users.

Let's talk about that for a moment, because this is a key point I think often gets overlooked in browser comparisons.

**There's only one reason** why _anyone_ who's not a computer power user would go out of their way to pick a different browser than what ships by default on the operating system they use, and that's **market dominance**. To wit:

* If you use a Windows PC, if you're not using Edge you'll use Chrome.
* If you use a Mac, if you're not using Safari you'll use Chrome.
* And iPhone and Android users are pretty much stuck with Safari or Chrome respectively. (Though there are signs the mobile market will look more like the desktop market in the future…)

And that's it for the _vast_ majority of users. Furthermore, if you were to come along and suggest to those folks they should switch to another browser with low single-digit marketshare, they'd probably give you a weird stare and ask _why_ and _who cares?!_

Therefore, I consider it nearly indisputable that the people who will bother to take the time to **(a)** install a new Web browser that's **(b)** not a dominant marketshare browser with huge name recognition are _power users_.

And thus the single most important question anyone can ask as they evaluate one of the alternative Web browsers available today (and let's face it, Firefox is now one of the "alternative Web browsers") is this:

**What have you done for me lately?**

In other words, how _responsive_ are the developers of the browser to the power users who are going out of their way to champion and build workflows around that browser?

* Does the browser respect the user's unique and quirky functionality needs?
* Does the browser respect the user's requirements around security and privacy?
* Does the browser foster a sense that "there's no wrong way" to configure the browser to work across a variety of workflow styles?
* Does the browser convey the impression that the stewards of the browser will make good decisions to put the user first when controversial questions may arise during future development?

I'm not here to claim Vivaldi score big on _all_ of these points, because I simply haven't had enough time with it. But of all the alternative browsers I've heard of and taken the time to really try out in earnest, **Vivaldi does indeed seem to rise to the top**.

“A lot of people who won't consider themselves nerds who do care about their data, they do care about their privacy, they do care about being followed,” says Bruce Lawson. “And customization, it's not a sell initially, but very quickly when people switch to a new product, they think: _Oh, I wish I could do X or Y._ There's so many options, but you become accustomed to them. Theme switching, changing your icons, choosing where your tabs are, choosing how your tabs stack. These are the gateway drugs into customization. We want to show people that the software should adapt to them. They should be able to make the software theirs rather than you are subject to the machine. Too many people have become enslaved by technology, and it's time to take that back.”

## Cross-Platform Bona Fides

I've been using Vivaldi for a few months now in Fedora Linux, macOS, iPadOS, and iOS. On that last point, yes I know Vivaldi is just a basic skin over WebKit on i(Pad)OS and isn't _really_ worth using over Safari as a true alternative. But two things keep me sticking with it, and that are:

* Vivaldi offers true bottom tabs, which I have grown to appreciate immensely on both iPhone & iPad. I've never particularly cared for Safari's weird non-tabs on iPhone in particularly, so it's awesome to use a mobile browser with a good tab game. (**Note:** I sometimes experience slowness with the iPad’s interface, such as a delay when typing. I’m not sure yet what triggers this condition.)
* Once you're all in on a desktop browser, it's great to have bookmarks/history sync. Using Vivaldi on iPhone & iPad means I have access to bookmarks, history, and even passwords from my macOS and Linux browsing usage.

But back to the desktop. Vivaldi historically has not been what I would call a _beautiful_ application, but I think they made great strides in the version 7 release towards a more appealing look. While it doesn't look completely at home on macOS, or GNOME, the bottom line is Vivaldi looks like itself. It has a distinctive look because the UI is completely bespoke. And there's actually a reason for this: you can change pretty much anything about it!

* Don't like the colors? **Change 'em!**
* Don't like the size of controls? **Adjust 'em!**
* Don't like the icons? **Swap 'em!**
* Don't like where things are placed? **Move 'em!**
* Don't like the options given? **Replace 'em!**

Truly, I've never felt like I've been given such raw control over every aspect of the browsing experience before as I have using Vivaldi. This means _my_ Vivaldi might not look like _your_ Vivaldi at all, which in turn doesn't look anything like _her_ Vivaldi or _their_ Vivaldi. The Web as seen through the window of a Vivaldi browser can feel extremely personalized…which I must say is **rather intoxicating**.

As Bruce puts it: “We try to make sure that if you move to Vivaldi, you know immediately how to go about stuff. There's a recognizable interface. You might not know how to do all the customization yet, but anybody can start to use Vivaldi immediately and feel comfortable, and then customize it as you go. We'll start with something we think looks nice for most people—but go nuts. Do what you want with it. It belongs to you.”

## Apps & Access

Vivaldi also does an _excellent_ job of letting you install PWAs or create launch shortcuts of any Web site, and it works equally well on macOS and Linux. I can't say how much of this is universally true across all Chromium-based browsers, but to the degree Vivaldi is putting its own spin on this, it works very well indeed.

**I am very much in love with Vivaldi's sidebar and vertical tabs features.** (I wish the iPadOS version of Vivaldi had some of these features!) I prefer having vertical tabs on the right-hand rather than the left, and that way I can also stick a few key news feeds on the left-hand sidebar and pop those out whenever I want.

You can also tile multiple tabs across the X or Y axis (or both for a grid!), though I haven't even dived into that much yet, and you can also create tab groupings as well as organize sets of tabs across multiple workspaces.

Another fun feature: you can add a **dedicated search field** to the toolbar, so that the address bar can search via your default search engine (I use DuckDuckGo) but the search bar can be something else (like Wikipedia). There are little touches like this throughout the UI, making it seem so often like if you can simply _imagine_ a feature you might need, Vivaldi has it ready to go for you already.

On the truly esoteric front: Vivaldi offers what are called “page actions” which can modify the look and behavior of a Web page. My favorite was the monospaced option, which can change a page to use only your preferred monospaced font. If you like that “brutalist” look on the Web, you can make that happen on any Web site!

**Finally, Vivaldi seems really _fast_.** On both macOS and Linux (though harder to tell in the VM because performance isn't always buttery smooth there to begin with), I've never felt like Vivaldi is sluggish or cumbersome. I can't say how Vivaldi compares to Safari on my Mac mini M1 scientifically, but anecdotally? Every bit as capable and pleasant to use.

## Not All is Sunshine & Roses

I don't wish to rain on the parade, but I must point out a couple of downsides to using Vivaldi—one technical and one philosophical.

On the technical side, I've had some glitches in Fedora Linux related to **Wayland**. For starters, Vivaldi wouldn't boot at all with Wayland out of the box. I'd get a fuzzy pixelated image indicating it's still using X11. I eventually figured out how to go to `vivaldi://flags/#ozone-platform-hint` and set it to `auto` in order to use Wayland. However, it would completely glitch out and become unresponsive numerous times. I eventually realized there was something weird about the Start Page/Speed Dial and switched that off for new tabs. It's better now, but still occasionally glitches out and because of this I had to install Vivaldi directly from an RPM rather than through Flatpak so that I could force booting Vivaldi with GPU disabled and close whatever tab or UI element was triggering the glitches.

Is this an issue unique to my combination of VM/UTM/macOS host/Apple silicon/etc.? Or have other people experienced something similar? I'm not sure. All I know is I've never had any glitches using Firefox in this VM.

And the other issue is really just a philosophical question: **are we OK with ceding yet more ground to Chromium/Blink in the browser engine wars?** I'm not happy about that prospect. There's apparently another browser starting to make waves called [Zen Browser](https://zen-browser.app/) that folks are describing as the Firefox/Gecko equivalent of Vivaldi, but when I tried it out it was clear they've got a ways to go to approach Vivaldi's level of customization. I've also tried using the basic GNOME Web browser which is based on WebKit (once called Epiphany), but it has been a bit slow, crashy, and also has graphical glitches at times. Not really a valid daily driver in my opinion.

**Bruce Lawson agrees we need choice in browser engines.** “I personally lament anything that reduces diversity in the browser space. There's enough people on the planet for all the browsers to flourish. I was there at Opera when they abandoned the Presto rendering engine, which meant there were only three rendering engines rather than four. It would be a shame if that diversity were further reduced—but at least we can try to help that with a browser that, yeah, uses Chromium but tries to do the right thing rather than _hoover up_ people's data, or vacuum up people's personal profiles in order to sell them more crap.”

## So What's the Business Model?

This is another _possible_ negative for Vivaldi, but it's really hard to tell at this point. The company has said publicly it's not yet profitable. They don't seem to be terribly worried about it at this point, so maybe they know something we don't about how to close that gap without doing something stupid and brand-damaging. But I must say it makes me nervous. We've already seen The Browser Company, aka the makers of Arc, go totally sideways with their _horrendous_ use of AI to rob creators on the Web of their ability to provide accurate, human-sourced information. Will Vivaldi pull an Arc at some point? Or sneak in some privacy-invasive tech while claiming they're still privacy-focused? For what it's worth, they insist they won't.

It remains unclear to me at this juncture how the business of Vivaldi is aligned precisely with the needs of its power user community—which it would be if it were to, say, charge its users for extra features. Call that "Vivaldi Pro" or "Vivaldi+" or something to the effect. I genuinely don't understand why browser makers can't charge real money for advanced pro browser features. Perhaps everyone's still scarred from the Netscape vs. Microsoft wars…

There’s also the general concern that Vivaldi is itself not an open source browser…the most Vivaldi-ish bits are proprietary. “There's a vigorous internal debate about it,” Bruce says. “The vast majority of Vivaldi is open source, and the changes we make to Chromium get pushed upstream. The secret sauce, if you like, is our UI, and the way we do ad blocking. One of the reasons why you don't publish how you do ad blocking is because the bad guys who want to beat the ad blocking learn that way. It's an arms race against the advertisers.”

Bruce continues: “We're very small. We don't have a vast footprint. If you're vast and somebody forks you, it doesn't hurt. When you're a very small company and somebody forks you and potentially could do stuff that is detrimental to your brand or antagonistic towards your brand, it can really hurt. And that would potentially hurt us disproportionately. So one day we might be open source, but now is not the time.”

I doubt that answer will be completely satisfying to people who are fully committed to using open source software only, and I get that. But I _do_ still use a lot of proprietary software, and I also believe open source can itself be weaponized in unethical ways, so for me I am comfortable giving Vivaldi a degree of trust based on their track record thus far.

## Promoting the Social Web

I also think Vivaldi deserves some major kudos in how it’s supported the **Fediverse** through the introduction of [Vivaldi Social](https://social.vivaldi.net), their canonical Mastodon instance. Not only is it an easy way to onboard folks who otherwise find Mastodon confusing, it’s accessible directly through the Vivaldi browser and even integrates with Vivaldi’s sync account with single sign-on (SSO).

I created an account to test there and was pleasantly surprised that they provide the absolutely _outstanding_ Mastodon theme [Tangerine UI](https://github.com/nileane/TangerineUI-for-Mastodon) as an option. That alone is a reason to consider joining!

I’m also encouraged by Vivaldi’s __robust public commentary__ on the importance of the Fediverse. About their participation in the new [Social Web Foundation](https://socialwebfoundation.org), CEO Jon Von Tetzchner writes: “The Fediverse reminds us of the early days of the Web. We are competing against silos and corporate interests, using a W3C-based open standard and a distributed solution. Vivaldi is pleased to support the Social Web Foundation so that we can once again have a town square free of algorithms and corporate control.”

Vivaldi offers support for **RSS subscriptions** and reading news feeds. This goes directly against the trend of mainstream browsers _removing_ RSS support—a terrible regression which has harmed the open web. I asked Bruce why they care about building and promoting such features:

“Because so much of the content that people consume is pushed at them by big tech. So many algorithms are pushing stuff at you. We think it's important that you have the opportunity to curate the stuff that you want to consume and consume it in your own time rather than it being pushed at you as breaking news…_read it now, read it now, read it now, get engaged, get enraged_. You can more quietly, more mindfully, decide that you want to subscribe to this or that RSS feed, and when the time is right for you, you can go there and have a quieter read—rather than the frenetic algorithmic push which we argue isn't beneficial to individuals' mental health or society's mental health, quite frankly.

“The Web is the most important consumer worldwide technology we have. And it's important that somebody stands there and offers an alternative to the vast leviathans that currently own or control most of it. We know we're never going to be 90% market share, but it's important that we're there for the people who don’t want to be in the walled garden of the giants. Some people are happy to be, and that's fine. Everybody makes their choice, but we want to be there for the people who don't wish to be corralled by behemoths whose business interests don't necessarily line up with their personal lives.”

## Will I Switch Back to Safari?

Many people who try out new software and report about it tend to go back to what they were previously comfortable with. I could easily publish this article, then fall back into the familiar arms of Safari or Firefox.

But to be perfectly honest, I've quickly grown to _love_ Vivaldi, and it's set to my **default browser** on all platforms. This genuinely surprised me! I figured I _might_ use Vivaldi instead of Firefox in Fedora, and then only occasionally dabble with it on Apple platforms. But once I switched 100% and got used to seeing Vivaldi come up when clicking links and launching shortcuts, I realized this is actually pretty great.

**So congratulations Vivaldi!** You've made a believer out of me. But do me a solid and _please_ don't screw the pooch tomorrow, like 98.725% of all other Internet software companies to date. I don’t know if this poor heart could take it. 😂

Vivaldi is [free to download](https://vivaldi.com) for Windows, macOS, and Linux.
