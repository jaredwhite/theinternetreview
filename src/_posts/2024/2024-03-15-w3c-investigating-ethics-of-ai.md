---
date: Fri, 15 Mar 2024 11:25:21 -0700
title: The W3C is Now Investigating the Ethics of Generative AI
description: A new document describes a process to jump-start in-depth explorations and research related to how “Artificial Intelligence” is affecting the Web.
icon: math
funny_caption: Put a Warning Label on That Spicy Autocomplete for
---

Today, the World Wide Web Consortium (W3C) has published a document outlining new areas of focus for the organization. In [AI & the Web: Understanding and managing the impact of Machine Learning models on the Web](https://www.w3.org/reports/ai-web-impact/), they describe the early days of a process to jump-start in-depth explorations and research related to how "Artificial Intelligence" is affecting the Web:

> This document is intended to capture the current shared understanding of the W3C Team on the current and expected impact of developments linked to Artificial Intelligence systems on the Web, and identifying explorations the World Wide Web Consortium community has started or ought to be starting, to manage that impact. It does not represent any consensus from the W3C Membership nor is it a standardization document.
> 
> …This document aims first and foremost to help structure discussions on what may be needed at the standardization level to make the systemic impact of AI (and specifically, Machine Learning models) less harmful or more manageable.

Some of the areas of interest put forth in this report include:

* Respecting **autonomy and transparency**
* Transparency on **AI-generated content & AI-mediated services**
* Right to **privacy** and **data control**
* **Safety** and **security**
* **Sustainability**
* Balancing **content creators incentives** and **consumers rights**

The report also includes a reminder of one of the key points of W3C's [Ethical Web Principles](https://www.w3.org/TR/ethical-web-principles/#noharm): **"The web does not cause harm to society"**.

We'll be following W3C's engagement on this topic closely in the months to come. While the purpose of the W3C is to engage respectfully with a wide variety of stakeholders, some of whom may be companies we don't like building products we don't approve of, it's encouraging to see the organization take **principled stands** on pressing concerns regarding the future health of the Web, from **accessibility** and **diversity** to **safety** and **environmental impact**.

Generative AI touches on nearly every ethical code one could imagine might apply to the Web—in fact one can easily argue the Web has directly given rise to the ability for generative AI to function at all. Therefore the importance of the W3C to weigh in on matters concerning these emerging technologies cannot be overstated.

If you have specific feedback on the contents of **AI & the Web**, W3C is asking you [to file an issue on GitHub](https://github.com/w3c/ai-web-impact/issues) recommending additions or modifications. This isn't a general call to weigh in on the topic but specifically to make improvements to this document so that they can plan and prioritize future efforts accordingly. And some of these efforts have the potential to intersect with other W3C concerns more broadly. For example, Section 4.3 states:

> W3C still lacks a well-defined framework to evaluate the environmental impact of its standards.

Hopefully the timely idea of developing a framework to **address the systemic impact of generative AI** can accelerate these sorts of related tracks at the organization, making the Web better across multiple specialties and specifications.
