---
date: Wednesday, April 17, 2024 at 9:59:19 AM PDT
title: "FediUX Series: Pixelfed & Photography on iPad"
description: Welcome to the series where I lovingly critique Fediverse experiences as a UX designer. Today, let's talk about photography—specifically, the Pixelfed platform.
icon: fediverse
funny_caption: The Art of Presenting Art for Posting Art for Commenting on Art on
---

<image-figure>
  ![a fancy rotated grid of photos](/2024/pixelfed/pixelfed-article.jpg)
</image-figure>

Welcome to the series where I lovingly critique Fediverse experiences as a UX designer. We all want our time on the open web to be full of enjoyable moments and enticing interactions, and this is my attempt to add some color to the conversation.

**Today, let's talk about photography…and specifically, the [Pixelfed](https://pixelfed.social) platform as used on tablets like the iPad.**

> **Editor's Note:** I swear, I wrote this entire article and right as I was about to hit Publish, I discovered the _huge_ news that [Vernissage](https://mastodon.social/@vernissage) is relaunching as a new Fediverse social photography platform. The previous Vernissage for Pixelfed app, covered here, will be renamed so as to reduce confusion. This is big, big stuff, and you can be sure we'll cover it soon in further detail here at **The Internet Review**.

## An Introduction to Insta-ActivityPub-Gram

If _Twitter -> Mastodon_, then _Instagram -> Pixelfed_. That's the pitch. I know it's terribly reductive to keep comparing ActivityPub-powered open web platforms to their commercial silo counterparts, but it just helps to kickstart discussion.

Instagram's UX starts and ends on smartphones. While there's a web client, it's just sad on tablet/desktop-sized screens. A mesmerizing experience apart from mobile it most certainly is not!

So in _that_ sense, Pixelfed doesn't need to do anything special here, right? Instagram is a phone-first experience, Pixelfed can be too.

**Except first-party mobile apps aren't available.** 😕 I've been using Pixelfed's iOS app through the TestFlight beta for quite some time now, and it's not bad.

But honestly I don't actually care that much about Internet photography as viewed through the tiny little screen of my iPhone. What _really gets my blood pumping_ is how it looks on  my **iPad Pro's gorgeous 12.9" display**.

And this is where things get…complicated.

## Comparison Time: Glass

**The gold standard for tablet viewing of photography is Glass.** [Glass](https://glass.photo) is a social network by photographers, for photographers, and its UX is _exquisite_.

<image-figure caption="The feed view on Glass">
  ![grid of photos](/2024/pixelfed/glass-feed.jpg)
</image-figure>

<image-figure caption="The detail view on Glass">
  ![an individual photo with metadata, description, and comments](/2024/pixelfed/glass-detail.jpg)
</image-figure>

Unfortunately, Glass is a for-profit commercial platform—which I don't take any exception to per se, but it's also a closed system. Yes, they provide RSS feeds, but there's no ActivityPub support, and in 2024 _that simply won't do_.

Perhaps at some point in the future Glass will add ActivityPub support, but until then, I've stopped posting there and rarely use the app. It's a shame, because the app is simply breathtaking—and even the web interface is quite good on desktop.

Back to Pixelfed…here's what the web UI looks like on iPad:

<image-figure caption="The feed view on Pixelfed">
  ![a center column of photos with navigation on left and right](/2024/pixelfed/pixelfed-web.jpg)
</image-figure>

_Hmm._ 🧐

OK, so clearly we're dealing with an old-school website layout here rather than a content-first (aka photo-first) layout. Not terrible, but not particularly inspiring either.

Thankfully, this is the open web! **We can use third-party clients to access Pixelfed.** So let's take a look at a couple of options.

## Vernissage for iPad

I've been using [Vernissage](https://mastodon.social/@vernissage) on and off on my iPhone & iPad for a while now. I believe it's the only App Store app for Pixelfed available. I had some issues last year which led me eventually to stop using it in lieu of Pixelfed's beta client, but I'm having better luck now with the latest updates.

<image-figure caption="The feed view on Vernissage">
  ![grid of photos](/2024/pixelfed/vernissage-feed.jpg)
</image-figure>

<image-figure caption="The detail view on Vernissage">
  ![an individual photo with description and comments](/2024/pixelfed/vernissage-detail.jpg)
</image-figure>

As you can see, the interface is quite good overall and much closer to the Glass experience. I'm not thrilled with how the individual photo detail page looks once you tap a photo in the feed, but you can tap the photo again for a fullscreen view.

**Definitely switch to Dark Mode when using Vernissage.** I'm of the firm opinion the best way to engage with photography is to have a dark UI. I always keep Pixelfed's beta client set to dark UI.

You can post to Pixelfed through Vernissage as well, which I think I'll be doing more often now as I work through my backlog of Nikon Zfc photos in Adobe Lightroom.

## Phanpy PWA (Progressive Web App)

[Phanpy](https://phanpy.social) is a popular web app client for Mastodon, and it works with Pixelfed now as well. I'm not sure if that was due to changes on Pixelfed's platform, or in Phanpy, or both—but it's very cool nonetheless!

The interface definitely isn't particularly optimized for photography on larger displays, as you can see. It's more attractive than Pixelfed proper, but doesn't match up to Vernissage or Glass.

<image-figure caption="The feed view on Phanpy in dark mode">
  ![center column of photos](/2024/pixelfed/phanpy-dark-mode.jpg)
</image-figure>

Again, I recommend switching to a dark UI, as well as bumping up the display font size to widen the center column (which is still _far_ too narrow).

You can also post to Pixelfed through Phanpy, and the appeal of using Phanpy would be you can manage multiple accounts (Mastodon and Pixelfed alike) from a single interface.

If Phanpy could simply support a grid layout, instead of a single column, then I think this could be a very attractive as well as cross-OS solution.

## Conclusion

I have immense respect for Dan Supernault and everything he's accomplished building Pixelfed from scratch as an open source software project and federated platform. So I say this as gently as possible:

The Pixelfed UI feels like a late-aughts web app, but without the "gallery" feel of a site like Flickr. _I don't find it compelling_ on larger displays—especially iPad—and because of that, I don't find myself using it much at all. My access to Pixelfed is almost entirely limited to the iOS app (which isn't yet available to the public on the App Store).

**What I really want is PixelGlassFed.** 😂 From the focus on EXIF metadata (Camera, Lens, Exposure, etc.) to the way photography is laid out visually to even the typography, Glass feels like everything you'd want from a gallery-inspired, photography-first social platform. Glass isn't perfect…their refusal to support hashtags is utterly mystifying to me, and of course with ActivityPub support missing _I'm just not going to deal_ in this day and age.

Barring that, my favorite Pixelfed UX is without a doubt **Vernissage**. I remain nervous something will happen in the future to break the app again, but as long as it's working I'll be pretty happy. Thankfully, [the source code to the app](https://github.com/VernissageApp/Vernissage) is available under an Apache 2.0 license, so that means it can never truly die.

**My parting thought:** Pixelfed needs to decide what it is. If it's just a fun way to post pictures from your smartphone, then…I'm not fully sure why we need it when you can do the same thing on Mastodon—or Threads for that matter.

But if it's intended to be a "serious" platform **for photographers**, then it needs to be the _best place_ for photographers on the Internet. We desperately need it…we're not going to get it from the zombified Flickr. Glass' reach is limited. And Instagram is…lol. _Don't get me started!_

(Of course, ActivityPub being ActivityPub, there's nothing stopping some enterprising dev team from building a new ActivityPub-enabled, Glass-like professional photography platform. Future opportunity?)

> Again, I **swear** I wrote this _before_ I read the [Vernissage](https://mastodon.social/@vernissage/112225820640477556) announcement! Great minds… 😅 –JW