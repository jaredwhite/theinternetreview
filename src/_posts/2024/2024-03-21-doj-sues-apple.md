---
title: There’s a Law for That! DOJ Sues Apple
description: Apple has consolidated its monopoly power not by making its own products better but by making other products worse, according to U.S. Attorney General Merrick Garland.
date: Thu, 21 Mar 2024 09:40:46 -0700
icon: rainbow-apple
funny_caption: Somewhere Bill Gates Just Can't Stop Smirking on
---

<image-figure>
  ![DOJ press conference with an overlay caption: Apple, there's a law for that!](/2024/apple-theres-a-law-for-that.jpg)
</image-figure>

> "Monopolies like Apple's threaten the free and fair markets upon which our economy is based. They stifle innovation; they hurt producers and workers; and they increase costs for consumers.  If left unchallenged, Apple will only continue to strengthen its smartphone monopoly. **But there’s a law for that.**"

So goes Attorney General Merrick B. Garland's remarks today, as he along with 16 other state and district attorneys general have brought a suit against Apple in the New Jersey District Court. The United States Department of Justice [outlines the key aspects of antitrust law](https://www.justice.gov/opa/pr/justice-department-sues-apple-monopolizing-smartphone-markets) they believe Apple is in violation of through anticompetitive behavior, including:

* Blocking innovative super apps
* Suppressing mobile cloud streaming services
* Excluding cross-platform messaging apps
* Diminishing the functionality of non-apple smartwatches
* Limiting third party digital wallets

as well as restricting "platform participants’ ability to negotiate or compete down [Apple's] fees through alternative app stores, in-app payment processors, and more."

[In a press conference today](https://video.ibm.com/recorded/133439239), Attorney General Garland said the lawsuit alleges that "Apple has consolidated its monopoly power not by making its own products better but by making other products worse" and that "having monopoly power does not itself violate the antitrust laws, but it does when a firm acquires or maintains monopoly power…by engaging in exclusionary conduct."

Deputy Attorney General Lisa Monaco remarked that Americans "deserve and demand" accountability for companies who cross the line from "competition to exclusion" and that Apple's "conduct must stop". And Assistant Attorney General Jonathan Kanter compared these actions by the DOJ with past successes at fighting against entrenched monopolists such as Standard Oil, AT&T, and Microsoft, with a similar goal of protecting innovation and competition for the next generation of technologies. "Apple's conduct has resulted in less competition to lower the price of smartphones for American consumers. Consumers are paying more as a result for digital goods, services, and subscriptions. Smartphone users are losing out on new innovative and more secure features that reduce the need for expensive hardware."

During a brief Q&A following prepared remarks, Attorney General Garland was asked what it means that America's "biggest success stories" such as Google, Amazon, Facebook, and Apple are all accused of illegal, anticompetitive behavior. Garland responded:

> “The Justice Department does not have a different rule for the powerful as compared to the powerless. It does not have a different rule for the rich as compared to the poor. We have one rule. We look at the facts, we look at the law, we make the appropriate determinations. I will say that with respect to resource allocation, when you have an institution with lots of resources that in our view is harming the American economy and American people, it's important for us to allocate our resources to protect the American people, and that is certainly the case where individual Americans have no ability to protect themselves.”

Apple is expected to vigorously defend themselves against the lawsuit, which may result in years of litigation before a verdict or settlement is reached.

## My Take

The Apple of today does not resemble the Apple of the 1990s or the 2000s or even the 2010s. **Apple is the new Microsoft—just with better taste.** While Apple CEO Tim Cook has done a remarkable job carrying forward the legacy of Steve Jobs' Apple and cementing a formidable portfolio of hardware, software, and services which have turned Apple into a technology juggernaut and one of the world's wealthiest corporations, Cook has also turned a blind eye to the outrageous stranglehold Apple enjoys over digital commerce and developer opportunities throughout the past decade and a half.

The United States is late to the party of bringing down the ban hammer on anticompetitive and consumer choice-limiting business practices, with other countries and jurisdictions around the world including most notably the European Union having engaged in significant actions to rein in Apple's unchecked power.

The DOJ may not have gotten everything right in this lawsuit, nor is it as comprehensive as some would like (as I'll soon illustrate). But it's a major step in the right direction, and I challenge Apple fanboys who might be feeling spicy about the DOJ coming after Apple to explain in detail how limiting choice in digital commerce, third-party software, and third-party hardware accessories for consumers using Apple platforms is good for consumers rather than _good for Apple's bottom line_. **(Spoiler Alert: They can't.)**

On a personal note, **I think it’s unfortunate** that both the DOJ’s actions as well as the sweeping regulations in the EU are targeting only the iPhone market. I can’t claim I feel like I’m being meaningfully harmed by Apple’s iPhone monopoly on a daily basis—however, **I am certainly harmed by how that monopoly’s policies & practices carry over to the iPad market**.

My iPad Pro, a very capable device hardware-wise, is hobbled by its unreasonable connection to the App Store monopoly. Technically-speaking, **there is no justifiable reason** why I can’t natively run Visual Studio Code on the iPad, why I can’t run a Unix terminal, why I can’t run Fedora in a VM, why I can’t run lots of the audio software and plugins I like, etc. I can do all of these things on the Mac, and at this point in time **the Mac & iPad run on nearly identical architectures**!

The fact my Mac mini M1 is a more capable computer than my iPad Pro M2 is not a choice in hardware design, and it’s not really even a choice in software design. It’s a choice in *business policies*. I can’t run Visual Studio Code because Apple doesn’t allow Electron-style apps in the App Store. I can’t run a Unix terminal because Apple doesn’t allow access to the Unix CLI environment (because that’s not a thing on smartphones apparently). I can’t run Fedora in a VM because Apple doesn’t allow VMs in the App Store. I can’t run a lot of audio software and plugins which are Mac-only, because the economic and technical hurdles of supporting App Store-exclusive access to these products just don’t make sense. (Truly bizarre on the face of it because Audio Units plugin technology is provided on both macOS & iPadOS!)

In summary, **I am harmed every day as an iPad user** because when it comes to **software choice**, Apple’s tablet is inferior to other computing systems *including Apple’s own Mac platform*—not because Apple _can’t_ fix those problems, but because Apple _won’t_. They’ve been blinded by the locked-down-tight iPhone market and their desire to tie the iPad’s fortune to that same market. **And this simply cannot hold.**

While it's unlikely I'll see meaningful change any time soon due to the DOJ's actions and the lengthy legal process which is now underway (and we already know the iPad isn't yet affected in the EU like the iPhone is), I continue to cling to the hope that—like Charlie Brown running towards Lucy's football—Apple won't yet again prove it has no interest in opening up its platforms and would rather engage in "malicious compliance". My fear is that just as it took new leadership for a kinder, gentler Microsoft to emerge after the DOJ lawsuit in the 90s, it will take bold new leadership to steer Apple in the right direction. It's obvious **Tim Cook** [won't give up without a bloody fight](https://www.theverge.com/2024/3/21/24107784/apples-response-to-the-dojs-iphone-antitrust-lawsuit), and that's bad for the industry and _especially_ Apple users. **Make your voices heard accordingly.**
