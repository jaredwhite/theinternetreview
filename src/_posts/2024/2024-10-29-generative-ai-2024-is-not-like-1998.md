---
date: Tue, 29 Oct 2024 09:25:35 -0700
title: It's the “1998” of the AI Revolution. So Why Can I Safely Ignore It?
description: Researchers, please get in touch with me. I can be part of your control group.
icon: electricity-ani
funny_caption: I Do Not Want What I Haven’t Got on
---

Ah, I remember 1998 like it was yesterday.

**Windows 98!**

**Bondi Blue iMac!**

**The “[Cuban Missile Crisis](https://slate.com/news-and-politics/1998/09/what-exactly-is-the-cigar-story.html)”** 😏

**[You've Got Mail!](https://en.wikipedia.org/wiki/You%27ve_Got_Mail)**

But we’re not here to reminisce. We’re here to consider why the so-called “AI Revolution” of today is not like the Internet Revolution of 26 years ago.

**1998 was a pivotal moment in time for me.** It was when I’d gotten my start as a **professional Web developer**, working on projects for friends and new leads alike. And I was writing quite a bit for new online publications. (Alas, I hadn’t yet [rebooted iReview as a BeOS-themed destination](https://theinternet.review/archived/1999/06/09/introduction-to-beos/).)

1998 was also right in the middle of the first big Internet boom. AOL was riding high and acquiring companies right and left—including Netscape in a $4.2 billion deal. Microsoft had spent a few years pivoting mightily from a primary focus on big box (offline) software to a major consumer play where they hoped to fulfill their vision of “a computer in every home” connected to the nascent World-Wide Web.

Apple was also just beginning its “Second Coming of Steve Jobs” narrative arc, launching the Internet-flavored Macintosh computer that would save the company and pave the way for the successes of the **iPod, iPhone, and iPad**.

So here’s the deal.

I would argue that for most people, in the year 1998, it would have made no sense to stubbornly resist these technological advances. Imagine flat-out saying **_no_** to computers and the Internet—to the degree that you never set up an email address. No Web access. Nothing.

(OK weirdos, enough with that dreamy look in your eyes! Maybe you need to go unplug for the weekend! 🤣)

Were there people like that back then? Certainly! And even now, there’s no denying the appeal of retro tech. [Some folks still love to write on typewriters.](https://www.saturdayeveningpost.com/2022/07/why-and-how-i-use-a-typewriter/)

But on the whole, you could argue that people in the late 1990s who completely shunned personal computing were limiting their options for no clear reason. Accessing a Web site for information instead of dialing an automated telephone line was _clearly_ a superior experience. Talking to a friend via email or instant messaging was _clearly_ more akin to a face-to-face conversation than writing a letter and sending it in the post.

I remember my very tech-adverse mother becoming completely addicted to online chatrooms in order to discuss…and this is no joke…**Gàidhlig** with native speakers in Scotland and learners around the world. I even helped her set up [a Web site and mailing list called Gàidhlig 4 U](https://web.archive.org/web/20000424001346fw_/http://distantoaks.com/g4u/index.html) — and in case you’re wondering, my Scottish Gaelic persona was _Diarmaid Mac GhilleBhàin_.

The reason I’m going into all this detail is because I want to impress to you just how much of a overwhelming shift in culture the Internet was in the late 1990s.

**I see none of that same inevitability today with the so-called AI Revolution.**

## You can literally just not use it.

Researchers, _please_ get in touch with me. **I can be part of your control group.**

Because I’ve _never_ used ChatGPT. Not once. I hesitated even to access a link a client shared with me with a transcript of _their_ ChatGPT request. AI cooties! 🙅

I’ve _never_ used GitHub Copilot. Or Cursor. Or any of the other AI “pair programmers” out there. Not once.

I routinely switch off any AI tools in software I use (if that’s even possible). I never look at “answers” search engines regurgitate out, preferring to get to the genuine human-sourced information as quickly as possible.

I’m still running macOS Sonoma and iOS 17, because I have _zero_ interest in “Apple Intelligence”.

I don’t say all of this to revel in my curmudgeonly Luddism. I say it because I’m living proof that you can be a fulfilled, modern, _very online_, technical expert & creator and **completely sit out this hype cycle**.

Seriously. You can just not use any of these generative AI tools.

A while back, I wrote up an [AI Ethical Framework](https://www.whitefusion.studio/ai-ethics) for my software business Whitefusion. It even needs a bit of updating now because I once considered the environmental cost of generative AI to be a bit of a side issue compared to the main ones, but it’s becoming clear it’s actually [rather horrendous](https://www.techradar.com/pro/generative-ais-energy-demands-are-accelerating-the-climate-crisis-top-researcher-warns-of-environmental-impact-of-googles-new-search-feature).

I’m sad to say I see little evidence that we’re making any progress towards meeting the tenets of the framework. **Creators are having to take major steps** to protect their work against theft at industrial scale, and **regulation is slow or non-existent** to ensure models are trained and provided in an ethical manner.

Until there’s widespread availability of generative AI tooling which meets my criteria, I’m _refusing_ to use any at all. And again, the impact on my life has been…negligible.

I honestly don’t feel like I’m missing anything at all.

**I’m still coding and making a real impact on the projects I work on.**

**I’m still writing. I’m still podcasting.**

**I’m still taking photographs and editing them.** (with zero “generative fill”!)

**I’m still participating in my local communities.** In fact, if I ignore the few conversations I’ve had with folks IRL about what generative AI “will” do and focus on how AI has affected _anything_ I do IRL, the answer is **nothing**. AI might as well not exist when I consider all of the things I do on a daily basis out in the **real world**.

## It’s not inevitable. (Sorry Thanos!)

I’m really unable to explain to you why I would need generative AI to help me with anything I do. Now don’t get me wrong, I definitely appreciate **machine learning**. Making transcripts, translating text, searching for photos, dictating…these are all truly revolutionary and valuable computing tools. And if you want to put all of them in the broad category of “AI” and call me a hypocrite, you’re welcome to try.

But I find that there’s a wide conceptual chasm between traditional machine learning tools as described above, and this new crop of generative AI services. And unfortunately, when the lines get blurred, [it’s actually pretty terrifying](https://apnews.com/article/ai-artificial-intelligence-health-business-90020cdf5fa16c79ca2e5b6c4c9bbb14).

Computers can’t think—_and they won’t_. Anyone trying to sell you on a vision of AGI, or “powerful intelligence”, or any such nonsense, has truly drunk the kool-aid (or cynically capitalizing on the hype cycle before it bursts). Computers can’t experience the world, because there’s no _qualia_ in the lifecycle of a digital operation. Chatbots are lying to you when “they” wax philosophical about how much they appreciate the beach in the summer or that pickles taste great in a sandwich. I find it nauseating that some people willingly accept this kind of output from a chatbot. When the #1 problem with the Internet today is the rampant spread of misinformation and total bullshit everywhere at dizzying speeds, folks seem fine with using bullshit generators at scale?

**I don’t get it.**

But thankfully, I don’t need to, because I can continue to live my life perfectly fine without using _any_ of these generative AI tools.

Try it! Once you’ve weaned yourself off of these fake intelligence simulators, you just might realize they never added to your quality of life in the first place.

(But, alas, you can’t join my control group. 😉)
