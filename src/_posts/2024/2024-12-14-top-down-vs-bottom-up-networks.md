---
title: Top-Down vs. Bottom-Up in Social Network Decentralized Architecture
description: What users think they want may not actually be what they need.
date: Sat, 14 Dec 2024 18:35:19 -0800
icon: fediverse
funny_caption: Topology is the Best -ology for
---

There's been a truly fascinating back-and-forth technical conversation between Christine Lemmer-Webber, one of the authors of the ActivityPub specification, and Bryan Newbold, a core engineer at Bluesky. Check out [Christine's first post](https://dustycloud.org/blog/how-decentralized-is-bluesky/), then [Bryan's reply](https://whtwnd.com/bnewbold.net/3lbvbtqrg5t2t), then [Christine's reply](https://dustycloud.org/blog/re-re-bluesky-decentralization/). (Buckle up, these are _lo-o-o-og…_)

Think of all this as the "nerds at the back of the server farm" discourse mirroring some of the discourse we see by lay people about what is good or bad about **Mastodon vs. Bluesky**.

I won't even attempt to summarize what they talk about because, frankly, some of it goes straight over my head—and I'm a Web software programmer! But how I might explain the gist of it in incredibly simple terms is this:

* When you use a Fediverse account (using Mastodon for example), you're only ever seeing a partial "bottom-up" view of potential global network content…essentially just who you're following, plus who the other people on the server are following, plus who people have responded to, plus those who have responded to local content, plus what people have boosted, etc., etc. It _could_ be more than that too, if the server you're on connects to a relay of some kind for a larger firehose, but that's not a given.

* When you use a Bluesky account however, you are seeing a "top-down" view of global network content. There's an attempt to maintain a **godlike perspective** at all times…even when the original sources of content in various cases could come from outside Bluesky's infrastructure (the "decentralization" part of ATProto). It's been described as a bit like a search engine: content may originate from sources other than Bluesky, but when you using the Bluesky service you see "everything" because _everything has been indexed_.

Now a lot of folks—understandably so—look at these two very different systems and think _well surely we want the second option so everyone's seeing the same stuff, the same number of replies, the same stats, etc._

But with that top-down approach comes the likelihood of absolutely eye-watering infrastructure costs—and those costs keep growing. This is hard enough for Bluesky itself as a company raising money and searching for monetization solutions, but trying to imagine independent not-Bluesky operators maintaining not-Bluesky social networking infrastructure which is "federated" the way you'd expect is supremely difficult.

**There's also a cultural difference.** People who run independent servers in the Fediverse have a sense of ownership, and the people who use these servers feel a sense of identity…even tribalism (in the good way). This very site has an account on a particular server which I personally run: [@theinternet@intuitivefuture.com](https://intuitivefuture.com/@theinternet). That is *my* Mastodon instance. **Mine.** I've customized how it looks when you visit it (thanks to Niléane for the fantastic [Tangerine UI](https://github.com/nileane/TangerineUI-for-Mastodon) skin!) and the content that's presented from the homepage. It feels like a real Web application I've set up—even though this account can interact with other accounts on other services across the Fediverse.

Other accounts I have, from my personal one [@jaredwhite@indieweb.social](https://indieweb.social/@jaredwhite) where I actually know the guy who runs that instance ([Tim Chambers](https://indieweb.social/@tchambers)), to ones like [@bridgetown@ruby.social](https://ruby.social/@bridgetown) where the instance itself is themed around the programming language (Ruby) that Bridgetown is written in—they are all part of that special "fabric" of what makes the Fediverse tick. Other parts of the "Social Web" are quite different still. Accounts hosted by Flipboard are going to look and feel a very particular way. Threads is…a little strange, but I think they'll manage to get full Fediverse interop reasonably working eventually.

I just don't understand how ATProto will allow vastly different organizations and types of online tribalism at scales large and small to co-exist and make sense. It really feels like ATProto optimizes for: (a) letting users host their own data so they feel good about privacy and identity (certainly not a bad thing I'll admit), and (b) providing an "escape hatch" for overlords-gone-rogue so Bluesky as a corporate entity can't forever capture and hoard all the value and functionality of the global network—as you routinely see in the Instagrams and Xes and TikToks of the world. This is called the "credible exit" in the discussion linked above, and while I'm glad Bluesky's planning for the idea of a "credible exit", I simply don't see how that solves the kinds of problems _I am interested in solving_ when I look at the promise of decentralized social media.

I think Christine puts it best: **truly decentralized & federated social media means power is diffused across the whole network**. Thousands upon thousands of nodes, all directly or indirectly connecting and passing messages back and forth (content and profiles). No top-down, godlike view—which means that yes, your hashtags won't always include everything out there and your replies won't always include everyone out there and your stats won't always include all of the activity out there and your search results won't always include all of the matches out there.

Because "there's only one global network of consistent content" isn't a goal of ActivityPub, it doesn't work like users may expect at first, coming from the godlike worlds of commercial social networks. And Bluesky is fully transparent in wanting to give users what they think they want in this regard.

The problem is: many of us, certainly myself, **no longer trust this world**. Such "decentralized" protocols which end up actually looking pretty damn centralized and/or are cost-prohibitive for any fully-independent operators to use might as well be completely proprietary. And that seems to be what is playing out _in real life_ thus far. In mainstream people's minds, in the world of "microblogging" there's X, there's Threads, and there's Bluesky. They are all competitors on a level playing field, more or less. Mastodon is the "weird" one some folks seem not to understand or believe is dead or something. 🙄

What's ironic about that is 50 years from now, Mastodon (or whatever open source codebases might replace it) will be the only one standing—because it's completely and truly open and accessible by any independent operator, running off of official Web networking standards. I very much doubt X, Threads, and Bluesky will be anything anyone remembers using in 50 years, by and large.

Think I'm nuts? Well consider that email newsletter you just read, and that other email you replied to from your co-worker. **Yep, you just used 50+ year-old technology.** All those other forms of "electronic mail" which came along to compete (Google Wave, anyone)? Nobody remembers they ever existed.
