---
title: iPad Professional Postmortem
description: Apple marketing in 2024 for computer power users is clear as mud. I'm ready for substantial change—and it appears that won't be forthcoming from Apple.
date: 'Mon, 17 Jun 2024 13:13:30 -0700'
icon: rainbow-apple
funny_caption: Well That Aged Well for 
---

<image-figure caption="Tim Cook. AZQuotes.com. https://www.azquotes.com/quote/1352690">
  ![The iPad is the clearest expression of our vision of the future of personal computing. Tim Cook, 2015](/2024/quote-the-ipad-is-the-clearest-expression-of-our-vision-of-the-future-of-personal-computing-tim-cook-135-26-90.jpg)
</image-figure>

## Apple marketing in 2024 for computer power users is clear as mud. I'm ready for substantial change—and it appears that won't be forthcoming from Apple.
{: style="font-size: var(--subheading-font-size-fluid)"}

Nearly nine years ago, Apple CEO Tim Cook put a stake in the ground. The iPad wasn't merely a fun, whimsical companion product sitting next to the juggernaut of iPhone and the lasting endurance of professional platform Macintosh. It in fact represented Apple's aspirations to build the next great computer system for the widest possible market of users. "What's a computer?" a kid of the future would ask (according to the Apple of 2017)—because all they've ever known is iPad. 

And all I ever wanted to know, too, was iPad. When this product was first unveiled in 2010, I was over the moon. [I'd been wanting a tablet computer for years](https://theinternet.review/archived/2010/04/02/ipad-hands-on/) at that point, and was quite literally waiting for Apple to "do it right" (as opposed to the awful toaster-fridges that were Tablet PCs).

The first iPad—while extremely impressive—didn't quite deliver what we ultimately desired. And so we waited longer still for the iPad to grow into a fully-featured computing platform. The dream of the **iPad Professional** beat in the hearts of iPad enthusiasts everywhere.

With every new hardware model, with every new release of what would eventually be termed _iPadOS_, we eagerly upgraded and tried to move over as many of our workflows and applications and setups and habits and processes as we could. **Some** of that worked out—and surprisingly well at times. But in other cases, it was simply one failure after another.

Yet we trusted Apple to come through for us eventually. 2015-2017 was a heady time, as Apple kept cranking on new hardware literally called "iPad Pro". Surely now that Apple had "pro" tablet hardware with a dedicated OS that wasn't simply iPhoneOS blown up, we'd soon see the "clearest expression" of **Apple's vision of the future of personal computing**.

## The Beginning of the End: Apple Silicon

The great irony of Apple Silicon is that as incredible as its been for the Mac—turning a languishing and frustrating product line (Apple laptops during the rise of iPad Pro were uniquely awful in a variety of ways) into the crown jewel of Apple's professional offerings—it's proven to highlight just how absurdly hamstrung iPadOS is.

Read every single iPad Pro review over the last several years—and especially earlier this year—and the refrain is the same: "This is absolutely incredible hardware! Unfortunately **the software is still lagging way behind**, and depending onto the task at hand renders this device useless."

Fast forward a wee bit to this year's WWDC where surely—_surely!_—[Apple would address the myriad of complaints](https://theinternet.review/2024/03/29/ipad-fans-you-wont-be-happy-until-june/) by showcasing the next major revision of iPadOS and making it even more powerful and capable for the **iPad Professional**, and…

…_crickets_.

If you're wondering why Apple would do this, if you're musing that perhaps it's all a **cynical ploy** to get professional users with advanced workflow requirements to just buy a Mac, the answer was made crystal clear during this year's live [Talk Show with John Gruber](https://daringfireball.net/thetalkshow/2024/06/11/ep-402). Gruber essentially led the witnesses by intimating that people who want the iPad Pro to solve all of their professional computing needs should just use a Mac, to which Greg Jozwiak and Craig Federighi essentially responded _yep, you got that right!_

**Ouch.**

In this era of Apple Silicon, where iPad hardware and Mac hardware is nearly identical on so many fronts, apparently the "clearest expression" of Apple's vision of "the future of personal computing" is…the Mac? (Or perhaps it's Vision, but I digress…)

## The Mac Ain't It

Sorry to burst your bubble, Mac fans, but I don't find anything particularly noteworthy or visionary about macOS at this point in time.

This was truly underscored by me resurrecting my 2015 MacBook Pro from the dead and playing around with an old OS (Sierra) since I couldn't upgrade it any further using my external thumb drive (the internal SSD died long ago).

**Honestly? Not much has changed.**

With few exceptions, macOS seems to be mainly a dumping ground for technologies Apple develops for its primary platform, iPhone. The only reason the Mac can withstand Apple's iPhone-first development strategy better than the iPad is because macOS was _already_ professional-grade. Can you imagine if Apple tried to iPhone/iPad-ify their laptops today? There'd be riots in the streets!

Catalyst, Apple's attempt to make it easier to bring new apps to the Mac by reusing iPad app code, **was largely a UX failure**. Nearly every Catalyst app I've tried using in the last few years I ended up disliking immensely. SwiftUI is _clearly_ the better route, yet it's strange how SwiftUI apps are uglier on the Mac than on either iPhone or iPad.

I have little interest in building my own future of professional computing around a desktop OS that's the uglier, kludgier variant of Apple's primary platform iPhoneOS.

I'd tried using both Stage Manager and Spaces together in macOS Sonoma to help me with window management and workflow, and it was a buggy exercise in frustration. And when I say buggy, I mean _how in the hell did this ever ship in a production build?!_ buggy. I'm willing to believe there are improvements in macOS Sequoia, but I'm not holding my breath.

## The Future of Personal Computing is…Linux?

I know, I know…roll your eyes all you want at one more proclamation that this is truly the year of "Linux on the desktop"—**but hear me out**.

If I'm looking ahead over the next few years and resigning myself to the fact that I can only expect and appreciate the most straightforward usage habits of the iPad Pro I have *right now*—because it would be **madness** to wait any longer for Apple to get it together here—then the question simply becomes this:

**Should I buy a Mac laptop?**

**Or is there another emerging platform which aligns better with my values, interests, and professional needs?**

(Obviously Windows is out of the question. I'd quit the biz before I ever daily drive a Microsoft operating system.)

Well, I'm here to report that after several months of running modern Fedora + GNOME in a VM on my Mac mini, I have come to a surprising (to me anyway!) conclusion:

**I really love this environment.** 😍 Not only do I get the appeal of using open source software built less around capitalistic enterprise and more around the desires of grassroots computing enthusiasts, I've come to realize I also care about the modularity and physical longevity of the hardware I use.

Which is why my next laptop will _not_ be a MacBook Pro. **It will be a Framework.**

This isn't an endorsement of Framework as I haven't bought one yet! But I absolutely _love_ the idea. Not only does it seem like a great machine to run Fedora on, but I'm sold on the idea I can buy a modestly-spec'd machine up-front and then slowly upgrade it over time.

It makes no sense to me why I would buy more Mac laptops—a completely closed all-in-one system—when I already buy iPad Pros—a completely closed all-in-one system. Apple's central argument seems to be that I, as an iPad Professional, need yet another computer to do all the things I can't do on iPad for inexplicable reasons (again, the hardware is nearly the same!). Yet **Apple's failed to convince me** why that truly-pro pro computer should be made by Apple. If I'm going to spend my hard-earned cash on the "truck" to iPad's "car", then I want a goddamn truck.

## Pickup Line

Framework actually seems to understand what it means to make "truck" computers. And I'm ready for a substantial change. [As I recently wrote on Mastodon](https://indieweb.social/@jaredwhite/112590315856086050), there seems to be a genuine renaissance of GNOME application development these days thanks to the major advances of GTK 4. I haven't been this excited about a non-Apple desktop OS environment since BeOS 25 years ago. I'm even tempted to start writing a GNOME app of my own!

I'm not saying a Framework running Fedora can actually give me _all_ the capabilities I need. I'll still rely on my Mac mini as an escape hatch, and I still intend to use my iPad Pro extensively. But as the "nerve center" of my professional activities going forward, I'm ready to go all in on Linux.

And if nothing else, it'll make for good content here on **The Internet Review**. Stay tuned. 🤓