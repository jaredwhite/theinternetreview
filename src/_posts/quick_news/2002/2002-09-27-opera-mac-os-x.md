---
title: "Opera 6.0 Beta for OS X: Potential Killer Browser"
category: archived
archive_url: "http://web.archive.org/web/20021007214817/http://gaeldesign.com/ib/be/#82203015"
---

I've been using the new <a href="http://web.archive.org/web/20021007214817/http://www.opera.com/pressreleases/en/2002/09/20020925.html" target="_blank">Opera 6.0 Beta for Mac OS X</a> for a while now, and though it has some drawing problems and is a bit slow (hey, it's a beta afterall), I think it's shaping up to be a mighty fine browser. The Quartz-enabled text rendering is among the best I've seen, which surprises me since I've always been disappointed with the text display of Opera on any platform in the past. Opera also sports a beautiful new interface that puts both Internet Explorer and even OmniWeb to shame in my opinion. Not only does it look good, but it has some great new innovations like multiple customizable search bars that allow you to perform searches using a variety of Web services from Amazon to eBay to Download.com Mac to Google all with only a click or two. Of course, these features are also available to users of Opera on other platforms as well. In fact, it's rather amusing to me to see that Opera on Windows now looks a bit like OS X!
