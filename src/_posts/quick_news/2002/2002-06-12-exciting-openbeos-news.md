---
title: "Exciting News from the Intrepid Programmers at OpenBeOS"
category: archived
archive_url: https://web.archive.org/web/20020818025756/http://www.gaeldesign.com/ib/we.6.11.02.php?item=4
---

_[2023 Editor's Note: OpenBeOS was subsequently renamed to Haiku, and it's still around…[and still a beta!](https://www.haiku-os.org) 😄 Here's a [neat overview on YouTube](https://www.youtube.com/watch?v=-KoI1Wz7wE8).]_

You may be wondering why I'm interested in <a href="https://web.archive.org/web/20020818025756/http://www.openbeos.org/" target="_blank">OpenBeOS</a> when I just got though raking a Linux company over the coals for copying Windows XP. The big difference here is that the folks at OpenBeOS are trying to create an open-source re-creation of BeOS because BeOS is no longer being developed or commercially supported — which is one of the greatest losses the computer industry has ever seen. Once OpenBeOS is a shipping product that is as full-featured and useable as the real BeOS, they'll be taking OpenBeOS in new and exciting directions — partially continuing where Be left off, partially working on new OS concepts that have never been seen before by anyone.
<br><br>
Now the exciting news I have to share with you today comes from the OpenBeOS Web site. They claim to have a working BFS (Be File System) clone that is nearly complete. Not only that, but in some cases it's <i>faster</i> than the real BFS running under BeOS R5. Creating a real, journaling filesystem supporting live queries and indexed attributes is no small task, and I suggest we all give the OpenBeOS team a big round of applause. If you look at the OpenBeOS <a href="https://web.archive.org/web/20020818025756/http://open-beos.sourceforge.net/tmstat.php" target="_blank">Team Status</a> page, you'll see that many of the OS "kits" have already arrived at "pre-alpha" stage, with "alpha" stage coming soon for several of them. I have a feeling we'll start seeing real OS builds coming from the OpenBeOS team in the not-so-distant future. I can hardly wait!
