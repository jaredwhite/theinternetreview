---
title: "Dell: Please Leave the Printer Market Alone!"
category: archived
archive_url: "https://web.archive.org/web/20020701105024/http://www.gaeldesign.com/ib/we.6.17.02.php?item=4"
---

There's a been a lot of talk in the computer industry about the possibility that Dell is eyeing the lucrative ink cartridge...whoops, I mean printer market. If this is true, then, as far as I can tell, there is no reason for Dell to be entering this market other than to try to make lots of money. Now I'm not saying that trying to make money isn't a good goal for a business — obviously, that's a very important goal. But the innocent, idealistic side of me can't help but think that providing high-quality, innovative products that enrich people's lives is just as important a goal. And, in my opinion, Dell just doesn't seem to get this at all. I mean, my impression of their products is that they are really just cheap, mass-market, slap-it-together computers that offer nothing new or compelling. Anyone could make a "Dell" computer. Why would Dell printers be any different? HP, Epson, Canon, and others have been in the business a long time and are constantly trying to improve the quality of their printers. I have an Epson Stylus Photo 820, and the output is just phenomenal. It really can print nearly photo-quality images. Will cheap Dell printers be able to hold up to this level of quality? Will Dell be truly committed to innovating in the printer market, or will they just rip off other people's ideas and technologies and then undersell the competition?

Only time will tell. But I can tell you one thing now: when Dell enters the printer market, I'll be shopping in another store — probably one that has a glowing fruit logo on the front.
