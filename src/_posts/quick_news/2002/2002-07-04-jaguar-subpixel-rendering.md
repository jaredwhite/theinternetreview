---
title: Apple Releases Sub-pixel Font Rendering for "Jaguar"
category: archived
archive_url: "http://web.archive.org/web/20020818142752/http://gaeldesign.com/ib/be/#78576234"
---

I've been following threads on multiple message boards around the Web about the sub-pixel font rendering that has just been introduced in the latest builds of Jaguar, the next major release of Mac OS X, and I definitely have mixed feelings about the news. On one hand, this is one less thing for folks to whine and moan about. On the other hand, I find sub-pixel font rendering to be extremely ugly. I'm <i>very</i> sensitive to the color fringing, which appears on every LCD screen that I've tried. Even if there is a slight detail enhancement gained using the technology (which is only apparent to me when the font size is quite small), the color bleed is far too noticeable. I want my black text to be <i>black</i>, not "sort of black with red, green, and blue dots swirling around it".

Thankfully, Apple has given us the choice of what type of font rendering to use. So you can pick sub-pixel font rendering if you like, and I'll stick with good, ol' fashioned solid color fonts, thank you very much.
