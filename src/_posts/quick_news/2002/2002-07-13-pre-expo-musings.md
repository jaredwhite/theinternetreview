---
title: Pre-Expo Musings
category: archived
archive_url: "https://web.archive.org/web/20020818030559/http://www.gaeldesign.com/ib/mw0702_musings.php"
---

Forget about Apple and IDG — <i>I'm</i> getting sick of rumors and speculation. It seems that everyone and their mother-in-law has an opinion on what our Fearless Leader will be talking about on Wednesday next. Here's an idea: let's just wait and see what Mr. Jobs says, OK? I admit that I'm as guilty as anyone when it comes to eating this stuff up, especially if it has anything to do with Jaguar, but sometimes a man has just gotta say "enough is too much!" and forget about the whole thing. There will be new PowerMacs, there won't be new PowerMacs. There will be new iBooks, there won't be new iBooks. There will be a new digital lifestyle device, there won't be a new digital lifestyle device. Jaguar will be released, Jaguar won't be released. It will be the best of times, it will be the...oh, never mind.

Sufferin succotash! At this point I've heard so many conflicting stories that I have no idea what will be announced at Macworld, although it's reasonable to assume that Jaguar presentations will comprise a large part of the proceedings. And since I have little doubt that Jobs' keynote at this Macworld will follow a similar pattern to his previous performances, I think we can also count on seeing plenty of third-party companies strutting their stuff on stage this time around.

My main hope is that, whatever happens, the Mac community doesn't flip out next week because their favorite [fill in the blank] rumor didn't come to pass. I remember the keynote at Macworld last year in New York was lambasted by the Mac press because Apple "only" announced Mac OS X 10.1, iDVD 2, and new "Quicksilver" PowerMac G4s (this was the first round that topped off at dual-800MHz). Maybe I was just green and impressionable at the time since I had only been really involved in the Mac community a short while, but to me the keynote was plenty exciting. Not as exciting as the one at Macworld this January, when the iMac G4 was announced, but certainly no snorer. It seems that Apple has to come to grips with the fact that its reputation as an innovater is <i>too</i> good. Now people expect Apple to come out with killer products constantly, when in fact no company can possibly manage to keep up the pace.

Perhaps I'm just flappin' my gums over nothing, and Ol' Unkle Steve's gonna knock us all dead Wednesday. We'll see. In the meantime, I'm going to have to use all of my willpower to resist checking the latest news on the rumor sites.

...

Excuse me while I go visit ThinkSecret one last time...
