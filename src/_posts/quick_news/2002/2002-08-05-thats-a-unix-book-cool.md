---
title: "That's a UNIX Book...Cool"
category: archived
archive_url: "http://web.archive.org/web/20021007214817/http://gaeldesign.com/ib/be/#79836155"
---

All right, OK, so I just finished watching Wayne's World 2. Excellent!

But seriously, forks, it's interesting to think about the fact that, back in 1993 when WW2 was made, the prevailing view was that UNIX was for geeks. You used UNIX, you were a geek. 1+1=2. But a lot has happened in the last few years. For one thing, the definition of UNIX has blurred considerably. What is UNIX? Linux isn't UNIX, technically (and, after all, GNU=GNU is Not Unix), but for all intents and purposes it is UNIX (or should that be Unix?). Mac OS X claims to be UNIX, but I'm not sure if Apple has licensed the official UNIX trademark from <a href="http://web.archive.org/web/20021007214817/http://www.unix-systems.org/" target="_blank">The Open Group</a>. And, besides, OS X/Darwin does have a few bizarre un-UNIX-like add-on technologies, such as NetInfo (ugh!). FreeBSD seems to be a pretty nice UNIX-like OS — and it's interesting to see the BSD'ers "winning" with Apple's adoption of the OS as a starting point for OS X/Darwin. Oooh, my hair will probably burst into flames when I start reading all the hate mail I'll get from Linux zealots over this one. (Don't you hate it when tech writers say things like that?)

Anyway, the second point I'd like to make is that the computing world is moving towards a bizarre dichotomy between UNIX and Windows. I don't know if this was predicted by some computer science professor ten years ago (or a geek with a UNIX manual under his/her arm), but the legion of desktop OSes, big and small, has gone the way of the dodo. Now we just have Windows and Mac OS X. Oh, and desktop Linux. (Sheah, and monkeys might fly out of my butt!)

The question is, is Windows is going to become more UNIX-like, or are Mac OS X and Linux going to become more Windows-like? Both trends are currently in vogue, so what will be the end result? Complete look-alike copy cat OSes on both sides of the divide? Dear God, I hope not. That's the day I'll swear off computing.

Nyet!
