---
title: "Apple Leads the Industry in Innovation According to Analyst"
category: archived
archive_url: "http://web.archive.org/web/20020622193942/http://gaeldesign.com/ib/we.6.4.02.php?item=2"
---

<a href="http://web.archive.org/web/20020622193942/http://www.twst.com/" target="_blank">TWST</a>, an online news and information site for investors, <a href="http://web.archive.org/web/20020622193942/http://archive.twst.com/notes/articles/nav810.html" target="_blank">recently published an interview</a> with David C. Bailey, Vice President and Research Analyst with Gerard Klauer Mattison &amp; Co., discussing the computer hardware sector and what will be driving growth for the industry over the next several quarters. In this interview, Mr. Bailey says:

> "Apple continues to lead the PC industry in innovation, in our opinion. The company completely revamped its portable product line in 2001, and we expect Apple to make significant enhancements to its desktop offerings in 2002....Sometimes overlooked, but just as important, in our opinion, is Apple's portfolio of software products. Apple has rolled out an entire suite of software products that enable consumers to capture, edit and distribute digital audio and video using very powerful but easy to use applications."

In other words, Steve Jobs' "digital hub" strategy isn't lost on investors. Now listen to what he says about the rest of the computer industry:

> "From a WinTel PC perspective, unfortunately the innovation has almost ground to a halt. There has been a lot of discussion about tablet PCs and wireless mobile computing, but we view most of these initiatives as more evolutionary than revolutionary. In addition, they may not garner a great deal of initial enthusiasm from either corporations or consumers."

Amen, brother. I couldn't have said it better myself. Basically, Apple has adopted a strategy of making computers more powerful <i>while at the same time</i> making them easier to use, more useful (small but significant distinctions), and enabling people to be more creative and productive with their computers. The "WinTel" camp doesn't seem to understand that Tablet PCs, wireless mobile computing, etc. — while admirable technological advances — aren't what's fundamentally needed in today's computing climate. People are fed up with rushed products, sloppy quality, feature-laden bloatware, and incompatible "standards" — and Apple knows it. That's why they're creating products that are simple, work well, are highly compatible with REAL standards, yet push the envelope and create new ways of getting work and play done. The "digital hub" strategy is an important part of this, as is Apple's new-found love for the UNIX market, which favors modularity, openness, and communication.

All I can say is, thank goodness that some investors aren't taken with certain PC companies' short-term profit gains at the expense of long-term growth and innovation. The response of consumers at large remains to be seen, however.
