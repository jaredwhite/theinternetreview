---
title: "And from out of the blue comes the following news: le Buzz is back!"
category: archived
archive_url: "http://web.archive.org/web/20020818142752/http://gaeldesign.com/ib/be/#78107570"
---

<a href="http://web.archive.org/web/20020818142752/http://www.lebuzz.com/" target="_blank">le Buzz</a>, a most excellent BeOS pro audio news and review site, is back online after an unnerving respite. Dane Scott, Buzzmaster of le Buzz, has been a stauch support of BeOS for years, and the fact that he is still a strong advocate of the OS and its available software is remarkable. Even in spite of the so-called death of BeOS in terms of development support, Dane Scott is continuing to post the latest "buzz" around the OS and its fabulous media abilities. He's even helped develop a full-featured radio automation system for BeOS called <a href="http://web.archive.org/web/20020818142752/http://www.tunetrackersystems.com/" target="_blank">TuneTracker</a>, targeted at small radio stations of all kinds around the globe. I wish Dane the best of luck with le Buzz "2.0", and I hope he continues to find success with TuneTracker.