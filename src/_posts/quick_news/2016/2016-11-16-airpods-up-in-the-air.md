---
date: '2016-11-16 08:32:19'
title: AirPods Up in the Air
---

**It seems clear that wireless audio is the future. Whether that future includes the AirPods in a big way is less certain.**

I recently attended a conference for music professionals and, in particular, a class on careers in the music technology industry. We were tasked with putting on our product manager hats and coming up with basic specs and a marketing message for new headphones.

The main takeaway from the class is everyone **loves** wireless. The idea of wireless headphones sans tangled-up cords are unreservedly a big hit, and it seems clear that (at least in the consumer space) wireless audio **is** the future. _However_, in the course of discussion, I talked with a few people about Apple’s forthcoming AirPods. I was surprised at the skepticism around their form factor. It seems there is a lot of consternation that they going to work out well in daily use. Typical comments are they look rather funny (like you took the regular EarPods and just chopped off the cords), and they seem like they'll get lost really easily. That’s my main concern as well.

Thus, while it’s highly likely we will all be using wireless headphones in short order, it might not come in the form of AirPods per se. But as we all know, Apple’s ability to convince customers of the merits of strange new form factors or interaction methods is unparalleled, so perhaps we will get used to the lopped-off-look of the AirPods and figure out ways to avoid dropping one into a storm drain.

(As an aside, I’m very curious why it’s taken so long for the AirPods to roll out. Perhaps the internal tech has proven more difficult to manufacture en masse than Apple expected. A bump in the road towards our glorious wireless future?)
