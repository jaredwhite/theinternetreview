---
title: Whither Eddy Cue?
date: '2016-06-09 21:52:00'
---

I've really, really been trying to give Apple SVP Eddy Cue the benefit of the doubt in recent times, but this is the straw that broke the camel's back. App Store's been a mess for years, then Phil Schiller takes over operations (not just marketing) and in six months major changes and improvements occur. And this is simply another item in a long list of gaffes and semi-failures (or outright no-holds-barred failures!) on Cue's watch. I think it's high time Tim and Eddy had a long talk about his future at Apple.

**Link:** [Rebooting the App Store](https://techpinions.com/rebooting-the-app-store/46242)