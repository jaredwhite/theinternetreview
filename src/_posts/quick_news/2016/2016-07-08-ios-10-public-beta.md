---
date: "2016-07-08 15:25:51"
title: Installing iOS 10 Public Beta
archive_url: "https://pygmynuthatch.com/20160708/iOS-10-Public-Beta"
---

Unlike some folks out there who simply _live_ on their smartphones, I'm a Mac + iPad guy. I love my MacBook Pro and I truly **love** my iPad Pro, and I use both for my work, so those devices will be waiting until the fall to get on the bleeding edge. Meanwhile, I really only use my iPhone for messaging and surfing the web 90% of the time, so I'm cool with going out on a limb with it.  I don't really care if some game crashes or I can't access a photo from 2012.

Assuming all goes well, I'll post a mini-review of iOS 10 on the blog soon. Top feature I'm looking forward to? Probably the new-and-improved Siri, followed closely by a better Apple Music.