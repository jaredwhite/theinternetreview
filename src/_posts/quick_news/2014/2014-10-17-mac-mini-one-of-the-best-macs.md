---
title: "Mac mini: One of Apple's Best Macs"
date: '2014-10-17 17:46:18'
---

Most people probably don’t give much thought to the [Mac mini](http://www.apple.com/mac-mini/), but I think it’s actually one of Apple’s best Macs. I or my family has owned at least one pretty much since they first came out. It’s a semi-portable desktop computer that’s inexpensive and easy to plug in any monitor/keyboard/mouse you like. When anyone ever asks me what Mac to buy, if they  are on a budget and don’t absolutely require a laptop form factor (maybe they already have a great smartphone and possibly a tablet anyway), I recommend a Mac mini hands down.

<image-figure caption="Photo credit: Apple">
  ![Mac mini product shot](/quick_news/2014/mac-mini-desktop.png){: style="box-shadow: none"}
</image-figure>

One downside in more recent times regarding the Mac mini was the slower hard drive performance as compared to the snappy SSD-based (aka flash storage) MacBook lineup. But now Apple has lower prices across the board on the Mac mini models, and a Fusion drive (which combines flash Storage and spinning disk technology for a solid balance of price/performance/capacity) upgrade is reasonably priced. You can get the entry-level model with a memory upgrade to 8GB and a 1TB Fusion Drive upgrade for only $849. **Great deal.**
