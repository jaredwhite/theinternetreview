---
title: "iPhone 5c: This is Not the Flop You're Looking For"
date: '2014-04-26 15:55:12'
---

Daniel Eran Dilger over at AppleInsider [hits it squarely on the nose](http://appleinsider.com/articles/14/04/26/apples-iphone-5c-ate-up-android-while-googles-moto-x-flopped-why-everyone-was-wrong): despite the handwringing punditry of almost everyone in the media, the iPhone 5c was actually not a flop, not even a slight success, but was in fact a major success and one of the top-selling smartphones in a variety of markets beating out flagships by well-known rivals.

> Apple was the star athlete of 2013, but rather than getting a parade, it got lots of advice to more closely mimic the lessor players that were failing around it. The most outlandish ideas gained so much traction that everything being said toward the end of 2013 pretty much boiled down to repeating the idea that Apple, long the world's most successful mobile company, was in dire trouble in various respects and doing virtually everything wrong.

> ...the reality is that iPhone 5c is selling well and stealing away Android buyers for the majority of its sales.

[Read the full article here.](http://appleinsider.com/articles/14/04/26/apples-iphone-5c-ate-up-android-while-googles-moto-x-flopped-why-everyone-was-wrong)

_(originally published on the Echoes microblog)_
{: style="margin-block-start: var(--size-8)"}