---
date: 'Fri, 16 Sep 2022 16:47:27 -0700'
title: From iPhone mini to iPhone Pro
archive_url: https://jaredwhite.com/20220916/from-mini-to-pro
---

<image-figure>
  <img src="https://jaredwhite.com/images/iphonepro-deeppurple.jpg" alt="picture of the Deep Purple version of the iPhone 14 Pro" />
</image-figure>

I've been rocking the [iPhone mini lifestyle](https://jaredwhite.com/20201115/1) since the 12 series debuted. I love my mini. I love that form factor. And I'm sad to see it go.

Nevertheless, time marches one and new iPhone models come out packed with new features. And the list of new features in the iPhone 14 Pro is impressive indeed.

* **Dynamic Island** (I always hated the notch. Byeeee!)
* **Always-On Display**
* **48MP Main Camera**
* **Deep Purple**

OK, maybe that last one isn't a "feature", but I definitely love this color. I got to hold one at my local Apple store and generally see the Dynamic Island at work, and it's fantastic. It's what the design should have been from the beginning. Farewell notch…hopefully forever.

I've heard some grumblings about the overall lack of progress in the standard iPhone lineup this year, and I get that. But for me it's not an issue, because there's no way I would _ever_ upgrade from an iPhone mini to one of the standard iPhones. If I'm forced to leave the world of the mini behind, I'm going iPhone Pro. No question about it.

_[2024 Editor's Note: As of right now, I've been rocking a Gold iPhone 14 Pro since they came out, and I couldn't be happier. It's a **great** device.]_
