---
date: 'Mon, 25 Apr 2022 16:22:22 -0700'
graph_title: Back to the Open Web
archive_url: https://jaredwhite.com/20220425/back-to-the-open-web
link_title: Twitter accepts buyout, giving Elon Musk total control of the company
link_url: https://www.theverge.com/2022/4/25/23028323/elon-musk-twitter-offer-buyout-hostile-takeover-ownership
link_content: Twitter has accepted Elon Musk’s offer to purchase the company for $44 billion, the company announced in a press release today. Musk purchased the company at $54.20 a share, the same price named in his initial offer on April 14th.
---

**Back to the open web I go.** Not that I ever left it…but to be quite frank, it's _so_ easy to post and get immediate feedback on Twitter that I spend most of my day-to-day "chit-chat energy" there and _not_ on my own website.

**No longer!** Now that Elon Musk is buying Twitter and taking it private, I'm _done_ putting serious effort into creating content for walled gardens. Everything I publish from here on out will **start on my own properties** and then get syndicated elsewhere.

I'm also in the process of switching from Revue (owned by Twitter) to [ConvertKit](https://convertkit.com) for my email newsletter. _[2024 Editor's Note: I'm still very pleased with this decision!]_

Read it and weep:
