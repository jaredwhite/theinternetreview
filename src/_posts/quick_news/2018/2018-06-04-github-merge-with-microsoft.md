---
date: 2018-06-04T08:51:39-07:00
title: Merged! Microsoft's $7.5 Billion Pull Request for GitHub
link_title: Microsoft Buys GitHub for $7.5 Billion, Going Back to Its Roots
link_content: |
  The acquisition provides a way forward for San Francisco-based GitHub, which has been trying for nine months to find a new chief executive officer and has yet to make a profit from its popular service that allows coders to share and collaborate on their work. It also helps Microsoft, which is increasingly relying on open-source software, to add programming tools and tie up with a company that has become a key part of the way Microsoft writes its own software.
link_url: https://www.bloomberg.com/news/articles/2018-06-04/microsoft-agrees-to-buy-coding-site-github-for-7-5-billion
archive_url: https://jaredwhite.com/links/20180604/1
---

So many conflicting emotions! A lot of developers are shell shocked at this news, although there's nothing really shocking about the announcement. Microsoft has gone through a huge cultural shift over the past decade, from a company with a famously antagonistic attitude towards open source software to a company that's a leading contributor and supporter of open source. It makes a heck of a lot of sense for Microsoft to acquire *the* platform that powers modern code collaboration. And financially, it's certainly a big win for GitHub. The problem here is that is been *bad enough* that so many open source repositories and development workflows have been built around a proprietary, commercial entity. Now that entity will be **Microsoft**. It's not hard to speculate that a lot of open source software teams and proponents of free (aka libre) software principles will be seriously reconsidering the tools they're using in light of this acquisition.