---
category: links
date: '2018-11-27T12:40:00-08:00'
link_url: https://medium.com/@googlersagainstdragonfly/we-are-google-employees-google-must-drop-dragonfly-4c8a30c5e5eb
title: Google Must Cancel Project Dragonfly Say Employee Advocates
link_title: We are Google employees. Google must drop Dragonfly.
link_content: "We are Google employees and we join Amnesty International in calling
  on Google to cancel project Dragonfly, Google’s effort to create a censored search
  engine for the Chinese market that enables state surveillance.\r\n\r\nWe are among
  thousands of employees who have raised our voices for months. International human
  rights organizations and investigative reporters have also sounded the alarm, emphasizing
  serious human rights concerns and repeatedly calling on Google to cancel the project.
  So far, our leadership’s response has been unsatisfactory."
archive_url: https://jaredwhite.com/links/20181127/1
---

The magnitude of their statement cannot be overstated. At the moment, this *is* the biggest story in tech. I highly recommend you read through it. It's short and it's powerful. Well done Googlers.