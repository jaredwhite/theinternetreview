---
title: "It is Confirmed: Panther Will Be Unleashed on October 24, 2003"
category: archived
archive_url: "https://web.archive.org/web/20041216151755/http://www.theideabasket.com/modules/news/article.php?storyid=52"
---

Apple today announced that Mac OS X 10.3 (Panther) will be released on October 24, 2003 in both client and server form. With over 150 new features, including many interface improvements and dramatic performance enhancements, this upgrade looks to be every bit as important as Jaguar was last year.

According to Apple's press release, Panther will be available for a suggested retail price of $129 (US) for a single user license, and $199 (US) for a single-residence, five-user license (The Mac OS X Panther Family Pack). For those folks who buy a new Mac system on or after October 8, Apple will be offering the standard Mac OS Up-To-Date upgrade package which costs $19.95 (US) for materials and S&H.; This Up-To-Date package will also be available to anyone who bought a Power Mac G5 regardless of purchase date. Very good move, Apple! 

Stay tuned for more coverage of Panther in the coming weeks.