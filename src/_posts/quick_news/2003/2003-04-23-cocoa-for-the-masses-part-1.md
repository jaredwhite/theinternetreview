---
title: "Cocoa 101: Object-Oriented Programming for the Masses - Part 1"
category: archived
archive_url: "https://web.archive.org/web/20031103004955/http://www.theideabasket.com/modules/news/article.php?storyid=29"
---

I'm happy to report that the first part of a "total newbie" Cocoa tutorial that I've started writing has just been [published over at OSNews.com](https://web.archive.org/web/20031103004955/http://www.osnews.com/story.php?news_id=3379). If you're a beginnning-to-intermediate-level programmer and wondering what all the fuss is about regarding Apple's Cocoa framework and the Objective-C language, then this is the article for you. Please check it out and let me know how you like it.
