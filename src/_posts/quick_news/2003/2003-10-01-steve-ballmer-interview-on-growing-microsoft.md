---
title: "Steve Ballmer on How to Grow Microsoft"
category: archived
archive_url: "https://web.archive.org/web/20041216152903/http://www.theideabasket.com/modules/news/article.php?storyid=49"
---

[AlwaysOn](https://web.archive.org/web/20041216152903/http://www.alwayson-network.com/), an interesting computer news/blog network I hadn't heard of before, just posted an [interview with Microsoft CEO Steve Ballmer](https://web.archive.org/web/20041216152903/http://www.alwayson-network.com/comments.php?id=1126_0_1_0_C) regarding how the company is poised for growth in emerging markets such as China, India and Brazil --- as well as the various ways in which Microsoft is convincing people to buy second or third PCs in a multitude of different form factors.

**Our Take:** Why does the interviewer sound like a complete Microsoft fanboy? I don't care how long Roger McNamee has known Steve Ballmer, and I don't care how cool it is that Steve did some nifty stock maneuver back in the 80's that nobody else thought of trying. I just want to read the facts, and I just want on-topic discussion about the issues being covered.

That being said, I enjoyed reading Mr. Ballmer's comments, and, while I remain skeptical that Microsoft's going to see very large gains in the Asian market with the threat of Linux looming high and large, I anticipate that Microsoft will have better luck with the Media Center PCs, Tablet PCs, and other useful PC form factors that the company has been experimenting with lately. Of course, you and I know that the Linux crowd will be coming up with their own versions of those devices as well. It looks like some tough days are still ahead for the boys in Redmond.