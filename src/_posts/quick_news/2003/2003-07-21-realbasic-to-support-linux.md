---
title: "Visual Basic Competitor Plans to Support Linux"
category: archived
archive_url: "https://web.archive.org/web/20040325205745/http://www.theideabasket.com/modules/news/article.php?storyid=38"
---

Here's some exciting news for you cross-platform developers out there. [REAL Software](https://web.archive.org/web/20040325205745/http://www.realsoftware.com/), makers of REALbasic, the #1 competitor to Visual Basic, have [announced plans](https://web.archive.org/web/20040325205745/http://www.realsoftware.com/company/pressreleases/linuxAnnounce.html) to support Linux in version 5.5 of REALbasic, which is due to be released in beta form later this fall. What versions of Linux, or which platforms, will be supported have not yet been revealed. But the great news is that Linux developers will finally be able to use a robust RAD tool to create application prototypes, port VB projects over to Linux, quickly develop custom in-house applications for business uses, and generally all the things that VB is used for on Windows. The VB-haters out there may snicker, but having a similar product on any non-MS platform is nothing but a good thing.