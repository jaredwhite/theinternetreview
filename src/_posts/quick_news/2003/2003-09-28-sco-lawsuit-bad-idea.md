---
title: "Caldera Co-Founder Thinks SCO Lawsuit is a Bad Idea"
category: archived
archive_url: "https://web.archive.org/web/20050414113210/http://www.theideabasket.com/modules/news/article.php?storyid=45"
---

A very interesting development in the surreal yet fascinating SCO vs. Linux war was [eWeek's recent interview](https://web.archive.org/web/20050414113210/http://www.eweek.com/article2/0,4149,1300411,00.asp) with Ransom Love, the co-founder of SCO predecessor's Caldera. In this interview, Love, in so many words, expresses his displeasure with the direction the company has taken since his departure in mid-2001, and he also sheds some light on the joint technology partnership between IBM and Caldera known as "Project Monterey" that ended in failure and left Caldera/SCO in the lurch. Not only an interesting read, it places SCO's activities in a perspective that many Linux zealots may be slightly uncomfortable with. Still, Love agrees that the SCO lawsuit is a bad idea, and it's refreshing to hear that from somebody who is intimately familiar with the company and the goals that were so important to it only a few short years ago.