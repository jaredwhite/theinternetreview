---
title: "Individual Customers Begin to Receive Dual 2GHz Power Mac G5 Shipments"
category: archived
archive_url: "https://web.archive.org/web/20050310220516/http://www.theideabasket.com:80/modules/news/article.php?storyid=44"
---

After a frustrating delay caused by rush shipments of the high-end Power Mac G5 model to educational customers, individual orders of the dual CPU behemoth are finally starting to arrive at eager Apple users' doorsteps. While a large bulk of the pre-orders have not yet been fulfilled, the next week or two will be a crucial time period as Apple steps up the delivery pace. Meanwhile, reports of deliveries and first impressions concerning the new Power Mac on various forums and blogs around the Web are helping to appease the anxious masses who await what is arguably the fastest personal computer on Earth. We'll keep you updated on the latest progress reports here at the Idea Basket.