---
title: "New Releases of Mozilla 1.5, Firebird 0.7, and Thunderbird 0.3"
category: archived
archive_url: "https://web.archive.org/web/20041218040205/http://www.theideabasket.com/modules/news/article.php?storyid=59"
---

From [mozilla.org](https://web.archive.org/web/20041218040205/http://www.mozilla.org/):

"We are pleased to announce new versions of Mozilla 1.5, the award winning Internet suite, and new Technology Preview releases of Mozilla Firebird (version 0.7) and Mozilla Thunderbird (version 0.3).

Mozilla CDs that include Mozilla 1.5, Mozilla Firebird 0.7, Mozilla Thunderbird 0.3 and more are now available worldwide for only $3.95 (plus shipping and handling)."

They're also letting people test the [beta version](https://web.archive.org/web/20041218040205/http://www.mozilla.org/website-beta/) of the upcoming Mozilla.org Web site, so please be sure to check that out as well and give them your [feedback](https://web.archive.org/web/20041218040205/mailto:webmaster@mozilla.org). The Mozilla Organization is one of the most important players in the Web community, and it's great to see the official releases and technology previews keep coming.