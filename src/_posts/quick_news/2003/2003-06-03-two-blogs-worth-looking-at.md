---
title: "Two Blogs Worth Looking At"
category: archived
archive_url: "https://web.archive.org/web/20031024183949/http://www.theideabasket.com/modules/news/article.php?storyid=34"
---

I have just a short bit of news for you today, and that is that I've just discovered a couple of blogs that I think you'll enjoy. The first one is called [ongoing](https://web.archive.org/web/20031024183949/http://www.tbray.org/ongoing/), and it's by Tim Bray, a veteran of the Web community who has been involved in developing search engines, XML, and other important staples of the Internet for quite a number of years now. The topics he covers are broad and fascinating, and I appreciate his unique brand of intelligent discourse, humor, and humility.

The [other blog I found](https://web.archive.org/web/20031024183949/http://norman.walsh.name/) is by Norman Walsh, and, in fact, his site links to ongoing (and visa versa), so there's a bit of a connection. Norman writes about computer topics, yes, but other subjects as well, and he has an easy, personable style to his writing.

I'm not very much into the whole blog phenomenon personally, but I do appreciate interesting, informative, and articulate blogs covering mostly computer topics, and these two blogs certainly fit the bill. If you know of some other blogs along these lines that I should be aware of, please let me know! Just post a comment below. It's that easy. :)