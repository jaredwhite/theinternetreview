---
title: "Cocoa 101: Object-Oriented Programming for the Masses - Part 2"
category: archived
archive_url: "https://web.archive.org/web/20031024183917/http://www.theideabasket.com/modules/news/article.php?storyid=31"
---

[Part 2 of my introductory article series](https://web.archive.org/web/20031024183917/http://www.osnews.com/story.php?news_id=3490) on Cocoa programming has been posted on [OSNews.com](https://web.archive.org/web/20031024183917/http://www.osnews.com/). I've gotten a lot of positive feedback on parts 1 and 2 so far, so it seems like it's a hit. Will there be a Part 3? You'll just have to wait and see. :)
