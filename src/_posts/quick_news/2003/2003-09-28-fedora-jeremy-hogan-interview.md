---
title: "LinuxQuestions.org Interviews Jeremy Hogan of Red Hat"
category: archived
archive_url: "https://web.archive.org/web/20050414114051/http://www.theideabasket.com/modules/news/article.php?storyid=46"
---

If you've been wondering what all the hubbub about Red Hat's new [Fedora project](https://web.archive.org/web/20050414114051/http://fedora.redhat.com/) is all about, [read this enlightening interview](https://web.archive.org/web/20050414114051/http://www.linuxquestions.org/questions/showthread.php?threadid=96722) with Jeremy Hogan which makes Fedora's goals and Red Hat's position on the project a bit more clear. I'm excited about any additional community involvement in Red Hat's development process for Linux since the company has, IMHO, been at the receiving end of a lot of unnecessary flack for trying to "take over" the Linux market. Projects like these will help improve Red Hat's standing in the community and prove that they do indeed want to be a caring citizen in Tux Land.