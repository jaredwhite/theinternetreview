---
title: "Mac OS X 10.3 (Panther): Newest Rumored Launch Date to be in Last Week of October"
category: archived
archive_url: "https://web.archive.org/web/20041216154413/http://www.theideabasket.com/modules/news/article.php?storyid=51"
---

According to [Mac Rumors](https://web.archive.org/web/20041216154413/http://www.macrumors.com/), reliable sources have indicated that the upcoming release of Panther is scheduled for sometime during the last week of October. Previous information regarding an Apple Store launch event pinpointed an October 24 date, but the latest notes aren't so specific.

Also, it appears that Apple has just changed to higher build numbers for internal builds of Panther, perhaps indicating that the previous 7B85 build of Panther is indeed the "Gold Master" version as recently [reported by Mac Rumors](https://web.archive.org/web/20041216154413/http://www.macrumors.com/pages/2003/10/20031001164435.shtml).
