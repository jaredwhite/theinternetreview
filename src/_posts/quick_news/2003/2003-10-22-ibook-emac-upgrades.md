---
title: "iBook Gets G4 Upgrade, Faster eMac Gets Price Cut"
category: archived
archive_url: "https://web.archive.org/web/20041218045703/http://www.theideabasket.com/modules/news/article.php?storyid=66"
---

Today Apple released the [iBook G4](https://web.archive.org/web/20041218045703/http://www.apple.com/ibook/) that everyone's been waiting for, and, while it looks pretty much the same as before, there are several internal improvements besides the processor upgrade. The new slot-loading drive is a welcome change, as is the inclusion of AirPort Extreme and USB 2.0 support. Check out the specs at [Apple's Web site](https://web.archive.org/web/20041218045703/http://www.apple.com/ibook/specs.html).

Apple also slashed prices on the "high-end" [eMac](https://web.archive.org/web/20041218045703/http://www.apple.com/emac/) models, with the $799 model now being the one with the 1GHz Processor. These aren't upgraded machines, mind you --- it's just that you can now get more power for the same price as before. In the market of cheap PCs, the eMac is a bit more competitive than it used to be, though it's certainly no speed demon. But it's a Mac, and for some people, that's good enough reason to buy one.