---
title: "Multitouch and Javascript"
category: archived
archive_url: "https://web.archive.org/web/20110208225813/http://netnotes.siteshine.com/2010/03/#post-180"
---

I've been thinking a lot about how touch screens and multitouch gestures will impact Web design, particularly with tablet devices ala the iPad coming out ([see my previous article here](/archived/2010/01/26/apple-tablet-rumors/) & [also here](/archived/2010/01/27/who-will-buy-the-ipad/)). It didn't occur to me until today that the iPhone's Safari browser has a multitouch API, which means that the iPad will also come with multitouch support in the browser. What does this mean?

It means that **Web sites that cater to iPad users will be able to craft amazing new experiences** that simply aren't possible on desktops and notebooks. One such example (only viewable on iPhone at present) is [a virtual light table for interacting with photos](https://web.archive.org/web/20110208225813/http://tlrobinson.net/projects/iphone-light-table/). This is only a demo, but can you imagine Facebook, Picasa, or some other online site providing an in-browser light table for arranging and grouping photos right in the browser using Javascript, CSS3, and multitouch? That's just one use case. There are many others.

Now I realize that native iPhone/iPad apps are able to do incredible things, but ultimately, I'm looking ahead to how Web sites will be transformed by multitouch. It's coming. And, as a Web designer, I want to make sure I'm ready for the transition.
