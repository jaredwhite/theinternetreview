---
date: "April 7, 2010 – 10:07 am"
title: "iPad Hot for Games, but What About Education?"
category: archived
archive_url: "https://web.archive.org/web/20100504205040/http://ipadartistry.com/2010/04/ipad-hot-for-games-but-what-about-education/"
---

According to some [estimates cleaned from data captured by Distimo](https://web.archive.org/web/20100504205040/http://techcrunch.com/2010/04/07/distimo-ipad-stats/), games are clearly the most popular category of application for the iPad (in terms of developer focus that is), accounting for over a third of apps in the App Store. Educational titles came in at less than 9%. I hope that number increases significantly in the coming months.

> Distimo tracked 2,385 unique iPad applications in the marketplace as of 6 April\[...\]
>
> Out of those 2.385 iPad-specific apps, Distimo found that the 'Games' category is by far the largest with 833 titles (35%), followed by 'Entertainment' with 260 apps (11%) and 'Education' with 205 titles (8.6%), respectively. The -- relatively -- smaller categories include 'Weather' (17 apps), 'Navigation' (18 apps) and, perhaps surprisingly, 'Finance' (with 21 apps).

I remember the killer app for the CDROM and multimedia PCs back in the early 90's was educational titles like Animals!, Microsoft Art Gallery, and the Dorling-Kindersley programs such as History of the World, Stowaway!, and Eyewitness Birds. I grew up on such experiences. I know the Web ended up killing that kind of software for the most part, but to be honest, the Web has a ways to go before that sort of immersive experience is really possible. Meanwhile, we now have the iPad. It's fast, graphical, and has a decent amount of local storage. I could easily imagine downloading a 250MB app and traveling into the world of the San Diego Zoo once again. So what are you waiting for, developers? Bring on the educational apps!