---
date: "April 3, 2010 – 7:04 am"
title: "Michael Arrington of TechCrunch Reviews the iPad"
category: archived
archive_url: "https://web.archive.org/web/20100505002424/http://ipadartistry.com/2010/04/michael-arrington-of-techcrunch-reviews-the-ipad/"
---

Well, well...despite Apple's mind-boggling security measures, Arrington manage to snag some time using the iPad at a few third-party developer locations. And [his review is, well, surprisingly positive](https://web.archive.org/web/20100505002424/http://techcrunch.com/2010/04/02/the-unauthorized-techcrunch-ipad-review). He's an Apple fan to a degree but has been known to blast Apple on some of their more questionable moves (specifically the tight level of App Store control). So with this kind of glowing recommendation, I can imagine some of TechCrunch regulars as well as other well-known tech communities out there may have their ears perked.

> But one thing I have had the chance to do is test iPads at developers who've been willing to bend the rules a little. Well, actually, a lot. This is exactly what Apple didn't want -- bloggers and other outsiders to get access to and play with the devices.
>
> But play I did. I've surfed the net on the iPad. I've played games on the iPad. And I've done email on the iPad. Yes, those iPads were chained to desks and in a bolted on steel case. And even so, the experience was stunning. It's a nearly flawless device.
>
> And the iPad beats even my most optimistic expectations. This is a new category of device. But it also will replace laptops for many people. It does basic computer stuff, like email and web surfing, very well. Applications load quickly and are very responsive -- think iPhone 3GS with a 50% speed boost.

[Read the full review here.](https://web.archive.org/web/20100505002424/http://techcrunch.com/2010/04/02/the-unauthorized-techcrunch-ipad-review)