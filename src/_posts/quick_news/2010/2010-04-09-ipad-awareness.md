---
date: "April 9, 2010 – 12:39 pm"
title: "Real-world iPad Awareness"
category: archived
archive_url: "https://web.archive.org/web/20100504205045/http://ipadartistry.com/2010/04/real-world-ipad-awareness/"
---

I was typing up a white paper for my company on the iPad the other day while sitting outside of a local market. It was a very interesting experience. A couple of school kids walked by, and I overheard them chattering all of a sudden: "wow, there's an iPad! Cool! It's like a big iPod!" I noticed other people walking by now and then glancing over their shoulder like "wait, is that a...?" The final surprise was an older gentleman who, on his way into the market, stopped dead in his tracks, walked over to me, and introduced himself. "Is that an iPad?" "Yep." We chatted for a bit, and he informed me that he was waiting to buy the next version or so after all the bugs are worked out.

I don't live in Silicon Valley. This was just a grocery store in suburban Santa Rosa. I think I can safely say that Apple has enormous mindshare these days with the success of the iPod, iPhone, and to a lesser extent, OS X-era Mac product lines. If the iPad is not an huge success, it will be because this type of product is simply unwanted, not because Apple doesn't have the marketing muscle to push it into the mainstream. I hope for the sake of the entire computer industry that it does succeed.