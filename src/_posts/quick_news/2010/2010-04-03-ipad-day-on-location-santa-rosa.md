---
date: "April 3, 2010 – 9:00 am"
title: "iPad Day: On Location at the Apple Store in Santa Rosa, CA"
category: archived
archive_url: "https://web.archive.org/web/20100505002528/http://ipadartistry.com/2010/04/on-location-at-the-apple-store-in-santa-rosa-ca/"
---

I'm here at the Apple Store in the Santa Rosa Plaza mall. It's 8:51 in the morning, so we don't have long to go until the store opens. The lines have formed on both sides of the store, so it looks a little less hectic than it might be otherwise. There is a whole mix of different people here, from young dudes to older ladies.

I talked with one gentleman who has been in line for an hour. He's here with his son who's saved up money to pay for half of a shiny new iPad. I ask "so, why are you getting this?" "Movies, and also I'll probably be getting Pages too. It's on the App Store for $9.99." Well well.

----

{: style="text-align:center"}
![](/quick_news/2010/l_2048_1536_54C4C5D2-D7A7-48B6-BDE0-4FECECF24159.jpeg)

{: style="text-align:center"}
![](/quick_news/2010/p_2048_1536_EAEB5E18-9B00-4DC5-9631-1200CF1CF8FE.jpeg)
