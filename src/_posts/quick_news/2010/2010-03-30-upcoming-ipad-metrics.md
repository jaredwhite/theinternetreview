---
date: "March 30, 2010 – 6:23 pm"
title: "More iPad Metrics: Who’s Going to Buy Apple’s Latest?"
category: archived
archive_url: "https://web.archive.org/web/20100504205137/http://ipadartistry.com/2010/03/more-ipad-metrics-whos-going-to-buy-apples-latest/"
---

Over at Mashable, a [report of some interesting statistics](https://web.archive.org/web/20100504205137/http://mashable.com/2010/03/28/ipad-npd-report/) from an NPD survey shows that the likely iPad user is fairly young with a good income --- not a huge surprise. But some of the other metrics are very interesting.

> We spoke with Stephen Baker, vice president of industry analysis at NPD, about the [report](https://web.archive.org/web/20100504205137/http://npd.com/lps/pdf/iPad_Overview.pdf), which was compiled from a survey of individuals over the age of 18 from February 24 through March 3. The sample size was about 2,000 consumers.
>
> Those who show the most interest in buying the iPad, per the report, look a little something like this…

[Read the rest of the article here](https://web.archive.org/web/20100504205137/http://mashable.com/2010/03/28/ipad-npd-report/).
