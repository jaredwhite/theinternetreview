---
title: "The Comcast/Disney News"
category: archived
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/12/the-comcastdisney-news/"
---

I'm labeling this one "off topic" because it really isn't related to computers in any direct way. But it's interesting nevertheless.

Comcast has announced its $66 billion hostile takeover bid to merge with Disney, which could potentially create an entertainment and media powerhouse rivaling even Time Warner post-AOL. [News.Com has some juicy information](https://web.archive.org/web/20050308071637/http://news.com.com/2009-1026_3-5157644.html?tag=nefd_lede) on the subject, appropriately titled *A Fractured Fairy Tale*.

I think most average observers of this proposition can't help but feel it's at least a step in the right direction. That is, Disney is a sinking ship, and somebody's gotta do something about it. And that somebody is certainly not Michael Eisner. It is the opinion of many that Disney has hardly put out anything decent since *The Lion King* (me, I peg the decline squarely on anything that came after *Beauty and the Beast*), and Eisner has continually shown a reactionary bias against anything "new" or "cool" in the technology arena. Disney only started putting out reasonable DVDs recently, and its pro-disposable rentals mentality is showing not a new-found liking for modern digital tech but a predisposition towards making extra cash fast.

I agree with [Roy Disney](https://web.archive.org/web/20050308071637/http://www.savedisney.com/). Eisner needs to get kicked out **fast** or Disney is going to sink so low that it may never recover -- if not by pure business reckoning then in terms of sheer product quality. "Lion King 1/2?" Don't make me laugh.

So, bottom line, I don't know if I'm for this Comcast/Disney merger per se, but I am against stagnation. Anything that can help turn Disney around, I'm behind 110%.