---
title: "In With the Nupha"
category: archived
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/13/in-with-the-nupha/"
---

Blogintosh [reviews the Nupha Music Store](https://web.archive.org/web/20050308071637/http://www.blogintosh.com/pages/reviewmain.html) from an OS X perspective. Nupha is a new French-based music download service that supports both the Windows and Mac platforms (via WMP on Windows, iTunes on the Mac). The one major drawback is iPod support is currently lacking, but will be available soon.

My limited perusal of [Nupha](https://web.archive.org/web/20050308071637/http://ninon.nupha.net/) earlier today was quite promising. I found some very interesting music on there -- stuff that you'd be hard-pressed to find here in the USA without a lot of poking and prodding. Until my iPod is supported, however, I can't give them any business. But when that support does come, I could imagine spending quite a lot of time (and money) there. Beware: the 99 cent price is in Euros, not dollars. So you'll have to pay a bit more here, but that's not so bad is it? Real CD imports cost an arm and a leg.

It's good to see a non-iTunes music store make a serious effort to support the Mac and the iPod. Now if only Apple would officially license its DRM....