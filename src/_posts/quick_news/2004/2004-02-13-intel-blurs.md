---
title: "Intel Blurs Line Between Desktops, Notebooks"
category: archived
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/13/intel-blurs-line-between-desktops-notebooks/"
---

[From News.Com:](https://web.archive.org/web/20050308071637/http://news.com.com/2100-1005_3-5159094.html?tag=nefd_top)

> "Intel plans to unveil next week a prototype consumer portable computer that blurs the line between desktops and notebooks by shedding the traditional clamshell laptop design."

Reading the article and looking at the prototype photo, I don't see what makes this newsworthy. The idea isn't bad, but it's certainly not innovative. The prototype isn't particularly nice looking. A handle in a computer has been done before (\*cough\* Apple \*cough\*). Using Intel's Centrino mobile technology in a non-notebook computer is an interesting idea, but mainly because Intel's regular chips aren't suitable for products requiring space and power-saving components -- [unlike other leading CPUs](https://web.archive.org/web/20050308071637/http://www.forbes.com/technology/newswire/2004/02/12/rtr1259718.html).

I appreciate any large PC technology company looking into alternative form factors, but with this prototype lacking much brilliance or even originality, I wonder why Intel even bothered.
