---
title: "When Thinking Goes Wrong"
category: archived
archive_url: "https://web.archive.org/web/20050308071637/http://www.theideabasket.com/weblog/archives/2004/02/16/when-thinking-goes-wrong-spymac/"
---

Spymac has posted an [editorial](https://web.archive.org/web/20050308071637/http://www.spymac.com/news/index.php?contentid=78) defending the pricing of the [iPod mini](https://web.archive.org/web/20050308071637/http://www.apple.com/ipodmini). The mini's $249 price may seem a bit higher than its flash-based competitors, but you get a lot more storage space for only $50 more than the plethora of $199 flash players available.

Despite one price typo (you'll have no trouble spotting it), the article is right on. If you think the mini isn't a good deal and would rather get the higher-end iPod, then guess what? You should buy the higher-end iPod. You're not the customer Apple is targeting. Apple is targeting the high-end flash player market: for only a little bit more you can get a much better product. It's a temptation that many won't be able to resist.

Disagree? Come back in a year and we'll see how the iPod mini is faring in the marketplace. 😃
