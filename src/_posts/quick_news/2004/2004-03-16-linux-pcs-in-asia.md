---
title: "Forbes.com: Hewlett-Packard eyes launch of Linux PCs in Asia"
category: archived
archive_url: "https://web.archive.org/web/20050430153234/http://www.theideabasket.com/weblog/archives/2004/03/16/forbescom-hewlett-packard-eyes-launch-of-linux-pcs-in-asia/"
---

[This could prove to be](https://web.archive.org/web/20050430153234/http://www.forbes.com/newswire/2004/03/16/rtr1299850.html) a major blow to Microsoft's goals to take over the emerging computer markets in Asia. Remember, selling computer software and hardware in Asia is much more difficult than in the US, or even Europe, because the Asian markets are flooded with cheap (often pirated) software and extremely cheap local hardware. However, that gives open source software (such as Linux) a major leg up there compared to expensive, proprietary solutions like Windows.

On the other hand, how successful the major PC makers will be over there is anyone's guess. If they sell millions of computers, but with very little margin, is it really that profitable in the end? We shall see. Meanwhile, anyone that pushes Linux on the desktop, in any market, gets a big score in my book!
