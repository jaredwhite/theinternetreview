---
title: "Linux on iPod: What Will They Think of Next?"
category: archived
archive_url: "https://web.archive.org/web/20050314195623/http://www.theideabasket.com/weblog/archives/2004/04/07/linux-on-ipod-what-will-they-think-of-next/"
---

I just love reading about this stuff. It's the geek in me, I guess; finding out just how far you can push the technology that you're working with. In this case, it's Apple's iPod, to which [a group of brave and adventurous souls have ported Linux](https://web.archive.org/web/20050314195623/http://www.ipodlounge.com/ipodnews_comments.php?id=3513_0_7_0_C), that venerable open source OS that can run on anything from supercomputers to your toaster (well, almost). And now your iPod.

Many improvements have been made to this iPod "distro" lately, but I'm personally going to wait until a fool-proof Mac-compatible binary installation is available. I've been looking forward to robust Ogg Vorbis support for the iPod for some time now -- perhaps Linux is the answer?
