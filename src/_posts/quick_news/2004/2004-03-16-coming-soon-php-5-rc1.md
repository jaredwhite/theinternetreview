---
title: "Coming soon: PHP 5 RC1"
category: archived
archive_url: "https://web.archive.org/web/20041227213937/http://www.theideabasket.com/weblog/archives/2004/03/16/coming-soon-php-5-rc1/"
---

[According to this issue](https://web.archive.org/web/20041227213937/http://zend.com/zend/week/week176.php) of the PHP newsletter by Zend, the RC1 release of PHP 5 is forthcoming; when, they don't say. Hopefully by mid-April at the latest.

I had some trouble getting PHP 5 Beta 4 to compile on OS X, but after much fiddling with configure options (and I had to leave out the PostgreSQL extension), it finally worked. A few of the most obvious bugs I'd found in Beta 3 seemed to be fixed, so hopefully the RC series of releases will be pretty darn stable.

PHP 5 is an incredible leap forward for PHP, and I can hardly wait for the stable release and its subsequent adoption by ISPs!
