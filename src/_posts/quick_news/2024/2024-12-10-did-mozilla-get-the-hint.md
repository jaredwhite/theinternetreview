---
date: Tue, 10 Dec 2024 11:49:22 -0800
title: Did Mozilla Finally Get the Hint?
---

A little while back [in an issue of Cycles Hyped No More](https://buttondown.com/theinternet/archive/who-cares-about-mozilla-that-is-a-really-important-question/), I had raked Mozilla over the coals for its shockingly poor marketing job on its Web sites. My gaze zeroed in on both Mozilla.org and Mozilla Foundation, and not only was the messaging **all over the place**, it was challenging to find even a satisfactory mention of their _core_ product—y'know, the one we all think of when we think of Mozilla: **Firefox**.

_Well…_

It appears Mozilla had known they were falling down hard in this department and [recently unveiled a completely redesigned Web site](https://www.mozilla.org/en-US/), branding, logo—the whole nine yards. I won't comment too much on the design specifics…people are welcome to like it or not. What I'm particularly impressed by is just how much clearer the messaging is right from the start.

Here's a "plain text" version of the top half of the new homepage. (Note that Firefox is prominently displayed in the top nav bar as distinct from other products.)

**Welcome to Mozilla**  
From trustworthy tech to policies that defend your digital rights, we put you first — always.

**Love the internet again**  
Break free from big tech — our products put you in control of a safer, more private internet experience.

* **Firefox** Get the gold standard for browsing with speed, privacy and control.
* **Thunderbird** Go chaos-free with one app for all your emails, calendars and contacts.
* (and a number of other products/services, you get the idea)

**Donate to the Mozilla non-profit**  
Mozilla is building a movement to reclaim the internet. Together we can build a future where our privacy is protected, AI is trustworthy and irresponsible tech companies are held accountable. But that’s only possible if we do it together.

We’re proudly non-profit. Will you donate to Mozilla today?

[**DONATE BUTTON**]

_Whew._ Isn't that _so-o-o_ much better than what they had before? In just a few sentences, I understand **why** Mozilla exists, **what** Mozilla makes, and **how** I can support the cause.

I would still quibble with some of the other messages being emphasized here (lots about the future of "AI" everywhere), but overall it seems like a major step in the right direction. Hopefully with the marketing getting fixed, the product development will similarly progress onto more solid footing.
