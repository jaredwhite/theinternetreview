---
date: Wednesday, May 1, 2024 at 10:14:10 AM PDT
title: Patreon Creators Can Now Use Bunny for Video
link_url: https://bunny.net/blog/now-creators-take-charge-using-bunny-stream-with-patreon/
link_title: Now Creators Take Charge Using Bunny Stream with Patreon
link_content: |
  Until now, content creators on Patreon have lacked good video hosting service options to control their content. This turns into platform dependency, putting the creator's valuable business at risk. One wrong move and their content could vanish into a rabbit hole.
  
  With Bunny Stream, you're in the driver's seat, so you can fully own your content and say goodbye to the hassle of using the traditional video hosting platforms Vimeo or YouTube. This was possible thanks to our integration support for Player.js and because we also became an official embed.ly provider.
---

Some news which may have flown under the radar but is actually quite significant: Bunny, a popular CDN provider which offers video hosting and streaming via its **Stream** service, now offers easy embed integration with Patreon.

**Full disclosure:** I use Bunny Stream for video hosting at [The Spicy Web](https://www.spicyweb.dev) and elsewhere and have talked about it before on my [Fresh Fusion podcast](https://jaredwhite.com/podcast/107/). I'm a big fan of the service, because it effectively lets you create _your own streaming service_. Video quality is excellent, it's snappy, the automatic captioning is impressive, and customizing the video player to look native to your web platform of choice is straightforward.

Now with this news, Patreon creators have the ability to publish and monetize their video content by using Patreon for what it does best—writing posts, collecting subscription payments, encouraging comments, etc.—all while keeping full control over video hosting and distribution by using Bunny (for example, you could let patrons watch videos early, and then later publish those exact same videos on your own website for public consumption).

**Anything which helps creators wean themselves off of total YouTube dependency is a win in my book.** _Patreon + Bunny_ appears to be an attractive proposition.