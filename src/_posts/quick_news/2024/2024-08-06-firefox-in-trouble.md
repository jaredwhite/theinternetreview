---
date: Tuesday, August 6, 2024 at 10:16:01 AM PDT
title: "Like a Pitchbot: The DOJ Won Its Case Against Google. Here’s Why That’s Bad News for Mozilla Firefox"
link_url: https://fortune.com/2024/08/05/mozilla-firefox-biggest-potential-loser-google-antitrust-search-ruling/
link_title: Forget Apple, the biggest loser in the Google search ruling could be Mozilla and its Firefox web browser
link_content: |
  For Mozilla, which first rose to prominence in the late 1990s as a community-driven project to challenge Microsoft’s stranglehold on the web browser market via Internet Explorer, Monday’s ruling is the latest setback in a quest to find relevance in a market dominated by Big Tech companies. Mozilla laid off about 60 staffers earlier this year and saw its CEO step down.
---

It sounds like a funny meme, but unfortunately this is no laughing matter. Firefox, the little browser that could, could be in _serious trouble_ with the latest ruling against Google.

[Jason Del Ray reporting for Fortune:](https://fortune.com/2024/08/05/mozilla-firefox-biggest-potential-loser-google-antitrust-search-ruling/)

> “Google is a monopolist, and it has acted as one to maintain its monopoly.”
> 
> With those words from United States District judge Amit Mehta on Monday, Google suffered a historic antitrust case loss at the hands of the U.S. Department of Justice.

And the fallout could be significant:

> But while Apple would certainly take a big hit if the ruling is upheld, Apple is a large, diversified company with many sources of revenue. That’s not the case for another partner of Google’s located in the fallout zone of Monday’s ruling: Mozilla, the non-profit tech org that makes the Firefox web browser.

Jason Del Ray goes on to describe Mozilla's precarious financial position, as the overwhelming amount of income flowing into the organization comes from a deal to keep Google as the default search engine in Firefox. [Bloomberg reported it accounting for a whopping 83% of Mozilla's income in 2021](https://www.bloomberg.com/news/newsletters/2023-05-05/why-google-keeps-paying-mozilla-s-firefox-even-as-chrome-dominates), even while Firefox's global market share cratered.

It was arguably _never_ a wise decision to tie Mozilla's fortunes to Google's hip, and should a remedy eventually come about that Google is barred from making significant payments to other entities in order to maintain its monopoly in online search, the results would be catastrophic—that is if Mozilla doesn't immediately plan to take steps to find alternate sources of revenue.

Mozilla may just need to keep [Laura Chambers around as interim CEO](https://en.wikipedia.org/wiki/Laura_Chambers) a bit a longer. I can't imagine anyone _wanting_ to step into her shoes at a time like this…