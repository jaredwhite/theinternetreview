---
date: Sat, 26 Oct 2024 11:48:21 -0700
title: Ghost is Now Federating, in Private Beta for Now
link_url: https://activitypub.ghost.org/the-private-beta-begins/
link_title: The private beta begins
link_content: |
  The big news is that we just sent out invitations to the very first 3 publishers to be onboarded for beta testing. There's an old saying in software development that if you aren't embarrassed by the first version of your product, then you shipped too late. So, in this case, let's just say we're shipping extremely on time.
---

We [previously reported on the news](/2024/04/23/ghost-and-buttondown-embrace-fediverse/) that both **Ghost** & **Buttondown** (which we use to publish [Cycles Hyped No More](https://buttondown.email/theinternet)) are working on adding ActivityPub support to their respective platforms, bringing the wider world of the **Fediverse** to their networks of creators & subscribers.

Ghost has now announced their ActivityPub support is officially in private beta with a few key publishers. In addition, you can follow Ghost's ActivityPub blog directly via `@index@activitypub.ghost.org` in Mastodon or any ActivityPub-compatible social network. There's still much work to be done before this is ready for prime time, but with Ghost making waves not just as a serious alternative to Substack for Nazi-free (!!) independent publishing but as an alternative to WordPress at a crucial moment when that ecosystem is going through a painful crisis of leadership, **the timing sure feels spot on**.
