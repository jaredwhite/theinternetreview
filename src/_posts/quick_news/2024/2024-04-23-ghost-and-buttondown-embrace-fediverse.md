---
date: Tuesday, April 23, 2024 at 7:33:06 AM PDT
title: Ghost Embraces the Fediverse, Collaborating with Buttondown
link_url: https://activitypub.ghost.org
link_title: Ghost is federating over ActivityPub
link_content: |
  Email gave us private messaging technology that isn’t owned by a single company. You can communicate with anyone, whether you use Gmail or Outlook.

  ActivityPub is doing the same for social technology. It’s a protocol that allows people across different platforms to follow, like and reply to one another. No algorithms. No lock-in. No bullshit.

  The open web is coming back, and with it returns diversity. You can both publish independently and grow faster than ever before with followers from all over the world & the web.
---

The news that [Ghost is federating over ActivityPub](https://activitypub.ghost.org) and plans to release a host of Fediverse-centric features by the end of the year has taken the community building the open social web by storm.

Nilay Patel [writes for The Verge](https://www.theverge.com/2024/4/22/24137296/ghost-newsletter-activitypub-fediverse-support):

> It seems like the platforms betting on [federation] in various ways — Mastodon, Threads, Bluesky, Flipboard, and others — are where all the energy is, while attempts to rebuild closed systems keep hitting the rocks. …at this point I’m not sure any social platform that launches without an eye towards federation stands a chance, really.

Anuj Ahooja writes in [Ghost is about to beat Substack in Discoverability](https://www.augment.ink/ghost-substack-discoverability/):

> The problem with Substack's Inbox is that it only includes Substack newsletters, RSS feeds, and their walled-garden "Notes" that you cannot access anywhere except their feed. Ghost, on the other hand, choosing to not go the walled-garden route is now tapping into what will be the largest sharing network available on the internet.

It appears that Ghost is not only planning to allow content creators to share posts out via ActivityPub, but is building a reading experience right into Ghost itself—thus making the software a fully-fledged participant in open social networking.

Buttondown, a platform for publishing email newsletters, [is also joining the Fediverse and working alongside Ghost](https://mastodon.social/@buttondown/112315274243684638):

> We're very, very excited to work alongside Ghost to evolve and proselytize ActivityPub as a great tool for authors to stay connected with their audience without tethering themselves to Yet Another Algorithm or Yet Another Walled Garden.

John O'Nolan, Ghost's founder, [had previously posted that](https://www.threads.net/@johnonolan/post/C5ygJRVL9WP):

> Our most-requested feature in the past few years has been to federate Ghost over ActivityPub.

and with this latest news is also hoping to build bridges with rivals like Medium by [reaching out to Medium CEO Tony Stubblebine](https://www.threads.net/@johnonolan/post/C6EYpx-LQfH).

Eugen Rochko, founder of Mastodon, [praises Ghost on its federation plans](https://mastodon.social/@Gargron/112316164910569758):

> The blogging platform Ghost is working on adding ActivityPub integration. That means, among other things, being able to follow Ghost-powered blogs and comment on articles right from your Mastodon account. The website they made to explain their plans is really nice! This is what momentum looks like.

While it's clear that an increasing number of significant players in web publishing and discourse are seeing ActivityPub integration as a must-have feature in this era of social networking, it remains to be seen if these features resonate with the general populace. Many users on Threads continue to indicate their confusion over what the Fediverse is, or their lack of interest in federation regardless, and have yet to enable Thread's federation functionality on their own profiles.

**But these are early days yet for mainstream adoption of Fediverse technology**, and just as it took years for concepts like email addresses and URLs to become second nature to Internet users, it may take some time yet for the vernacular of ActivityPub to loom large in public consciousness.

Steps like we're seeing now with Ghost and Buttondown are another major step forward in this process.