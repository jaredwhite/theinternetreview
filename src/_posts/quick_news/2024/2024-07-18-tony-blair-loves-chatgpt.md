---
date: Thursday, July 18, 2024 at 8:00:14 AM PDT
title: ChatG.P.T. Thinks It's P.H.A.T. (Just Ask Tony Blair)
link_url: https://404media.co/ai-finds-that-ai-is-great-in-new-garbage-research-from-tony-blair-institute/
link_title: AI Finds That AI Is Great In New Garbage Research From Tony Blair Institute
link_content: |
  The researchers then wanted to assess which of these tasks, which are also performed by public sector workers in the UK, could be performed by AI, given the technology’s current capabilities. Amazingly, the researchers concede that answering that question by talking to actual human experts across different fields would be hard, so they just asked OpenAI’s ChatGPT-4 to answer the question instead.
---

So let me get this straight.

A research paper, authored by the Tony Blair Institute for Global Change and presented by the man himself, which claims that AI could automate as much as 40% of the work performed by employees in the public sector—"improving growth and productivity and driving value and efficiency"—was able to make these predictions because…

…they were provided by ChatGPT.

![Ryan Gosling meme: hey girl, you haven't chatted until you've chatted with my GPT](/images/hey-girl-chatgpt-ryan-gosling.jpg)
{:style="text-align:center"}

That's like researching if cats are indeed the best way to catch mice on the farm by walking up to a cat and asking it if it's the best way to catch mice on the farm. (Although most likely the cat will merely stare at you bemused, then turn around and lick its butt.)

Look, it's become clear that _some_ amount of task automation can certainly be done by machine learning models. Just ask anybody who's received a full podcast transcript in a franction of the time it would take a human to perform that work.

But as liguistics professor Emily Bender puts it, "This [research] is absurd—they might as well be shaking a Magic 8 ball and writing down the answers it displays."

_Stop! Don't give them any ideas!_ 😂