---
date: Thu, 05 Sep 2024 10:13:18 -0700
title: Hallelujah! Let Us Celebrate the End of Spotify “Podcast” Exclusives
link_url: https://www.midiaresearch.com/blog/why-spotifys-podcast-exclusivity-era-is-coming-to-an-end
link_title: Why Spotify’s podcast exclusivity era is coming to an end
link_content: |
  Spotify knew that sinking hundreds of millions of dollars into acquiring exclusive podcasts would only be sustainable if 1) they acquired more subscribers as a result and 2) they were able to generate significant revenue from the advertising that those podcasts brought in. Over the course of three years, the first part of Spotify’s calculation bore fruit; now, it is attempting to cash in on the second part. However, with podcast advertising in limbo, it is worth asking whether the second part of Spotify’s strategic calculation will pay off.

  **Podcast exclusivity is a loss leader strategy.**
---

Pour one out for the era of fake podcasting, not-a-podcast-ing…whatever you want to call it. **The Spotify "podcast" exclusive is no more**.

The writing has been on the wall for some time now, as Spotify has changed its tune and can be found rolling out their exclusive shows to open podcasting platforms at large. And with top shows like _Call Her Daddy_ not only being made available to the public, but actually **moving house** and setting up shop with competiting distributors, it's clear Spotify's gambit that they could capture the podcast market and it turn into their proprietary fiefdom **failed spectacularly**.

[Deadline reports](https://deadline.com/2024/08/alex-cooper-call-her-daddy-siriusxm-1236044728/) that "The [Call Her Daddy] deal will start in 2025 and gives SiriusXM advertising and distribution rights to the show as well as shows from Cooper’s Unwell Network. Call Her Daddy will also remain on all podcast platforms."

As I opined on Fresh Fusion's episode [An Interesting Reflection on the Nature of the Podcast](https://jaredwhite.com/podcast/88), a podcast is an **RSS feed with enclosures of audio files** which are playable _across the whole ecosystem of podcast players_. Despite the protestations of those claiming Spotify's "exclusive podcasts" were indeed podcasts even though they were only playable within Spotify's proprietary player, it is clear that the open web platform of podcasting won the day. **Hallelujah, indeed.**
