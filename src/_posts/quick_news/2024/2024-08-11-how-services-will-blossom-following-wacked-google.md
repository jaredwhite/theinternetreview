---
date: Sun, 11 Aug 2024 15:52:22 -0700
title: How Web Services Might Flourish as a Result of Google Being Trimmed Down to Size
link_url: https://www.theverge.com/2024/8/11/24216760/google-trial-monopoly-remedies-rivals
link_title: What Google rivals want after DOJ’s antitrust trial win
link_content: |
  Whatever happens, the process could be a drawn-out one. Google’s president of global affairs Kent Walker has confirmed the company plans to appeal the ruling, saying the decision “recognizes that Google offers the best search engine, but concludes that we shouldn’t be allowed to make it easily available.”
---

[We recently covered the antitrust ruling handed down to Google by the DOJ](https://theinternet.review/2024/08/06/firefox-in-trouble/), and in particular the negative effect it could possibly have on Mozilla Firefox.

There's clearly a great deal of potential upside for other Web service companies whose offerings have tended to languish in the shadow of Google's reign. [Lauren Feiner writing for The Verge](https://www.theverge.com/2024/8/11/24216760/google-trial-monopoly-remedies-rivals):

> Judge Amit Mehta’s decision on how to restore competition will be just as — if not more – important than his finding that Google violated antitrust law. The recently concluded liability phase determined that Google violated the Sherman Act through exclusionary contracts with phone and browser makers to maintain its default search engine position. In the remedies phase, Mehta will decide how to restore competition in general search services and search text advertising.

Feiner continues, quoting statements from Yelp and DuckDuckGo about the need for a level playing field in the marketplace and what sort of remedies could ensure Google's search and ads business no longer benefits from its illegal monopoly.

One of the potential remedies, requiring browsers to offer users a choice of default search engine upfront, was already adopted in the EU—but so far hasn't lived up to the hope such choice would greatly shift marketshare towards competing search engines.

It will be hard to break consumer habit, now that "searching the Web" has become synonymous with Google. But as time goes on and Google is forced to compete more fairly, there's still a chance we'll see a more robust and sustainable search ecosystem.
