---
date: Fri, 29 Mar 2024 13:24:51 -0700
title: iPad Fans, You Won’t Be Happy Until June
link_url: https://www.apple.com/newsroom/2024/03/apples-worldwide-developers-conference-returns-june-10-2024/
link_title: Apple’s Worldwide Developers Conference returns June 10, 2024
link_content: |
  Apple today announced it will host its annual Worldwide Developers Conference (WWDC) online from June 10 through 14, 2024. Developers and students will have the opportunity to celebrate in person at a special event at Apple Park on opening day.

  Free for all developers, WWDC24 will spotlight the latest iOS, iPadOS, macOS, watchOS, tvOS, and visionOS advancements. As part of Apple’s ongoing commitment to helping developers elevate their apps and games, the event will also provide them with unique access to Apple experts, as well as insight into new tools, frameworks, and features.
---

Rumor has it that new iPad Pro and iPad Air models are imminent. [MacRumors by way of Bloomberg is reporting](https://www.macrumors.com/2024/03/28/new-ipad-models-may-launch/) a switch to OLED screens for the Pros, and a larger 12.9" option for the Airs, plus all the usual improvements around moving to M3 processors.

**But you won't be happy.** If like me you're a fan of Apple's unique tablet ecosystem, you know full well the software is _always_ lagging behind the hardware. No doubt the new iPad Pros & Airs will be flashy, impressive, and boast fancy new accessories. But what we're really waiting for is _iPadOS to suck less_. (sigh)

We'll have to wait until June for that…and pray to the dogcow gods that iPadOS 18 won't disappoint. 🙇
