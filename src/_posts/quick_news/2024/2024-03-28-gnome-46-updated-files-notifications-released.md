---
title: GNOME 46 Released, Featuring Improvements to Files and Notifications
date: Thu, 28 Mar 2024 16:11:57 -0700
---

GNOME 46, code-named "Kathmandu", [has been released](https://release.gnome.org/46/) and will be part of the upcoming Fedora Linux 40 release in May 2024. Some of the noticeable improvements include:

* A global search icon in the top-left corner of the Files window, along with performance improvements and other UI enhancements around progress updates and network discovery.
* Remote login is now available using the RDP protocol.
* Many refinements made to the Settings app.
* Enhanced notifications with headings and groupings to make them easier to visually scan and manage.
* Display improvements around font rendering and scaled interfaces as well as experimental variable refresh rate (VRR) support.

[More details and screenshots are available in the release notes here.](https://release.gnome.org/46/)

**Jared's Take:** I've been running Fedora Linux in an VM (ARM) on my Mac mini M1 for a while now, and I've truly fallen in love with modern GNOME and the experiences made possible by GTK 4. In the not-so-distant past, I couldn't have imagined desiring to use _any_ desktop OS other than macOS as my daily driver. Now I'm starting to investigate Linux-compatible laptops. _The FLOSS FOMO is real!_ 😅
