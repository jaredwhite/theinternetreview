---
date: Wed, 20 Mar 2024 09:03:49 -0700
title: Start Labeling Your Deepfakes on YouTube…Unless They're for Kids?
link_url: https://arstechnica.com/gadgets/2024/03/youtube-rolls-out-warnings-for-ai-manipulated-videos/
link_title: YouTube will require disclosure of AI-manipulated videos from creators
link_content: |
  Google previewed the "misleading AI content" policy in November, but the questionnaire is now going live. Google is mostly concerned about altered depictions of real people or events, which sounds like more election-season concerns about how AI can mislead people. Just last week, Google disabled election questions for its "Gemini" chatbot.

  As always, the exact rules on YouTube are up for interpretation. Google says it's "requiring creators to disclose to viewers when realistic content—content a viewer could easily mistake for a real person, place, or event—is made with altered or synthetic media, including generative AI," but doesn't require creators to disclose manipulated content that is "clearly unrealistic, animated, includes special effects, or has used generative AI for production assistance."
---

As [reported by Ars Technica](https://arstechnica.com/gadgets/2024/03/youtube-rolls-out-warnings-for-ai-manipulated-videos/), YouTube has recently added an upload control to the Studio interface to indicate if your video contains content that "makes a real person appear to say or do something they didn't say or do" or "alters footage of a real event or place".

This is a welcome improvement, as the proliferation of AI-generated content has exploded on YouTube as it has on many platforms. While this disclosure doesn't cover all uses of generative AI, it covers the ones most damaging to news, information gathering, and celebrity coverage.

However, [as discovered by WIRED](https://www.wired.com/story/kids-cartoons-free-pass-youtube-deepfake-disclosure-rules/), the new policies [do not seem to apply](https://support.google.com/youtube/answer/14328491#zippy=%2Cexamples-of-content-creators-dont-have-to-disclose) when it comes to showing scenes which are "fantastical" (aka "someone riding a unicorn"). Presumably this means a lot of children's content being dumped on YouTube would be excempt. It's also unclear if AI-generated voiceovers would require this disclosure. The rules only mention "voice cloning someone else’s voice to use it for voiceover".

Let's hope this proves to be the **start of a series of policy updates for YouTube**, not a definitive answer to the onslaught of generative AI content.