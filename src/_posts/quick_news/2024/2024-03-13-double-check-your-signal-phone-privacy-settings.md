---
date: Wed, 13 Mar 2024 00:03:10 -0700
title: Joined Signal Specifically Because of Usernames? Double-Check Your Privacy Settings
link_url: https://signal.org/blog/phone-number-privacy-usernames/
link_content: We’re also introducing a setting that lets you control who can find you by your phone number on Signal. Up until today, anyone who had your phone number–from a party flier, a business card, or somewhere else–could look you up on Signal by phone number and message you. You can now restrict this by going to Settings > Privacy > Phone Number > Who can find me by my number and setting it to “Nobody.”
link_title: Keep your phone number private with Signal usernames
---

I set up a **Signal** account quite some time ago because I liked their privacy stance and I wanted to try it out. But I never really got into the habit of using it—mainly because most of my close friends and family were on iPhone Messages, and otherwise I'd converse with folks professionally using Slack, Discord, or even DMs on a social network.

But with the news that usernames were coming to Signal, I figured it'd be an opportune time to familiarize myself again with the platform. Knowing I'd want to provide a secure way for the public to contact me as I launched a new publication (aka **The Internet Review**!) certainly fueled my interest.

Well [usernames have launched](https://signal.org/blog/phone-number-privacy-usernames/), and I think it's a stellar feature. _Finally_, people who don't like the idea of giving out their real personal phone number can avoid the extra hassle of getting a new phone number through whatever service just to use Signal. My username is **jaredwhite.05**. _Feel free to say hi!_ (Don't be creepy.)

**BUT** there might be one last step you need to attend to. As the announcement states, if you want to ensure any rando who somehow got ahold of your phone number _still_ can't reach you that way, you need to turn that option off in Signal's Settings. I just did. **I recommend you do as well.**
