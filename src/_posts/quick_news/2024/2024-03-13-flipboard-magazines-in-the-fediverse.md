---
date: Wed, 13 Mar 2024 16:18:27 -0700
graph_title: Flipboard Magazines Entering the Fediverse
icon: fediverse
---

**Flipboard** has long been an enthusiastic proponent of the Fediverse, having stood up a Mastodon instance at [flipboard.social](https://flipboard.social) and promoted dialogue with notable figures working on ActivityPub and related technologies through CEO Mike McCue's [Dot Social podcast](https://dot-social.simplecast.com). But the biggest news is that [Flipboard magazines are themselves turning into federated properties](https://medium.com/@mmccue/federating-flipboard-magazines-575a18297bd6), with a few having [already arrived](https://about.flipboard.com/inside-flipboard/how-flipboard-is-using-our-new-federated-magazines/) in a testing phase.

The format of the handles is interesting. Because Flipboard offers a "profile" (an individual or organization) who manages one or more "magazines", the handle ends up being `@magazinename-profilename@flipboard`. For example, once federation fully expands and my magazine [Happy Cities](https://flipboard.com/@jaredcwhite/happy-cities-6retv5m2y) is available to follow, you'd be able to find it via:

> `@happy-cities-jaredcwhite@flipboard.com`

and it would feature a description such as:

> Happy Cities is a Flipboard Magazine created by @jaredcwhite.

This will be a most welcome feature. Instead of manually posting a link to a magazine "flip" from a Mastodon account, folks could simply boost the canonical flip which gets federated from Flipboard—a benefit for both readers and publishers.
