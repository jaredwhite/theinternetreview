---
date: "2017-04-11 10:21:23"
title: "The Mac is...Back?"
---

Last year was a terrible year for Mac desktops. Some might argue it wasn’t a great year for Mac laptops either. The thing is I know people who are _very_ happy with the 2016 MacBook Pro, the Touch Bar, and all that came with the refreshed package. Apple also recently reported strong sales of the laptop lineup. However, it's hard to deny that a sizable number of Apple's pro customers are less than enamored with the latest MacBook Pro.

But it's much, much worse on the desktop side of things. 2016 was an absolute trainwreck. No new iMacs. No new Mac Minis. A fairly lightweight update to macOS. (I personally like Sierra a lot, but honestly, it felt more like a point release than a real upgrade.)

The most frustrating situation by far though was _no new Mac Pros_. Not only that, Apple let it slip that they were out of the display business entirely, ceding vital ground to LG (which turned out to be yet another trainwreck, in the form of faulty units that LG was forced to recall and re-engineer).

Now here we are in the spring of 2017, and—in a strange turn of events—there are hopeful signs that [the Mac desktop line is destined to blossom once again](http://daringfireball.net/2017/04/the_mac_pro_lives).

Apple will be completely redesigning the Mac Pro from the bottom-up, with a new "modular" form factor that Apple can update on a regular basis and a renewed purpose of being the fastest, most powerful Mac computer for high-end pro users. If you got the cash and want the best CPUs, GPUs, memory, and storage money can buy in a Mac desktop, they're going to give that to you. _Finally!_ Apple also plans to release a new standalone display to compliment the Mac Pro. Buh-bye LG.

There's only one problem with this rosy new scenario: it's not going to happen this year. (cue sad trombones) No official release timeframe has been given, but I think it's safe to assume spring or summer 2018. (I doubt Apple would go past WWDC next year, as they'd have a riot on their hands!)

Thankfully, we *will* be seeing something new this year, in the form of an "iMac Pro." Updated versions of the all-in-one Mac will sport significantly beefed up internals. It'll be interesting to see how far they can push the GPU and memory capacity...if it's a better upgrade than pros might expect, that just might be enough to tide over a lot of folks this year.

No solid word yet on the Mac Mini, but Apple is on the record that it doesn't intend to drop the product line, so they're going to _have_ to upgrade it at some point soon.

**Bottom line:** as a current Mac laptop user who hasn't owned a desktop Mac in some time, I'm once again seriously excited about Mac desktops. After the wasteland that was 2016, it's a big relief to know that Apple is aware of the mistakes they've made and intend to rectify those mistakes. As the saying goes, better late than never.