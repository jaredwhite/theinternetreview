---
date: "2017-10-25 20:52:11"
title: "Don't Give Up on the Mac Mini"
archive_url: "https://pygmynuthatch.com/20171025/dont-give-up-on-mac-mini"
---

Apple CEO Tim Cook recently commented on the lack of updates to the Mac mini by responding to a customer email, stating that "while it is not time to share any details, we do plan for Mac mini to be an important part of our product line going forward." Jason Snell's take on the matter:

> Apple could survive without the Mac mini in its product line, but it would make a lot of Mac users sad. I’d argue that the Mac mini is great not because it fills a niche, but because it fills a thousand of them. That’s great for Mac users because if you need a Mac in a particular place (where a laptop would be inappropriate and an iMac won’t fit), it’ll do the job. And it’s great for Apple because the Mac mini can act as a release valve of a sort—it can do all the jobs for which other Macs with more focused designs simply aren’t suited.

[Read the full article at Six Colors.][1] Like Jason, I too am hopeful about the future of this product, and I'm glad to hear an update—albeit an extremely vague one—from Apple.

**Fun fact:** back in 2013, my out-of-warranty MacBook Pro bit the dust, and as I didn't have a lot of spare change at the time and I do a lot of heavy-duty graphics and programming work, I decided to buy a Mac mini. I actually schlepped that thing back and forth between my house and my office, so in a sense it was a portable computer. I lived without a laptop for about two years. No joke!

[1]:	https://sixcolors.com/post/2017/10/a-new-mac-mini-i-want-to-believe/