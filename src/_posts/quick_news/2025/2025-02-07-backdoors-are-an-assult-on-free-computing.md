---
date: Fri, 07 Feb 2025 10:40:52 -0800
title: Backdoors are an Assault on Free Computing, Apple Must Fight This
link_title: Could Apple Pull iCloud Services From the UK Market?
link_url: https://www.macrumors.com/2025/02/07/could-apple-pull-icloud-services-from-uk/
link_content: |
  According to The Washington Post, the British government has secretly demanded that Apple give it blanket access to all encrypted user content uploaded to iCloud. The spying order reportedly came by way of a "technical capability notice," a document sent to Apple ordering it to provide access under the sweeping UK Investigatory Powers Act (IPA) of 2016.

  According to sources that spoke to the publication, Apple is likely to stop offering encrypted storage in the UK as a result of the demand. Specifically, Apple could withdraw Advanced Data Protection, an opt-in feature that provides end-to-end encryption (E2EE) for iCloud backups, such as Photos, Notes, Voice Memos, Messages backups, and device backups.
---

The chilling news out of the UK that Apple is being pressured to provide backdoor government access to encrypted data stored in iCloud should be ringing the loudest of alarm bells for all people who believe in the rights of digital freedom, security, and privacy.

Look, [I've been plenty critical of Apple as of late](/2025/01/31/my-shifting-computing-habits-thanks-tim-cook/), but I am 100% on their side with any and all efforts they may make to fight this. Unfortunately, it sounds like their options are limited in the near term, unless they're willing to play chicken with the UK's legal system.

There's a significant difference between government regulation which protects consumers from excess monopoly control and lack of interop on the part of big tech companies, and cynical legislation which _weakens_ the protections of consumers from either corporate dominance or government overreach. Any attempts at adding blanket backdoors and eroding the protections of data encryption, _no matter who it's for_, are a fundamental attack on user freedom—not to mention **inherently insecure** because wherever there's a backdoor for "the good guys", there's now a backdoor for the bad guys. And that's patently unacceptable.
