---
date: Mon, 10 Feb 2025 20:31:15 -0800
title: Improved Moderation Features Coming Later This Month to Pixelfed
---

[According to Pixelfed founder and lead developer Daniel Supernault](https://mastodon.social/@dansup/113979358398119425), a number of new and improved moderation features will be rolling out later this month for the open source fediverse photosharing platform, including:

* New APIs for clients to access moderation features
* Crowdsourced community notes and the ability to label content
* Action limits for new accounts
* Keyword and hashtag blocking filters
* A new "mod" role and corresponding moderation dashboard

As is typical for any fast-growing social network, and especially one with a distinct ActivityPub-powered visual flavor, users have been vocal about the need for advanced content moderation and the ability to enforce safety rules. Given that Pixelfed's recent growth is in large part due to dissatisfaction with Meta's recent changes in moderation policies, any investments in moderation features will go a long way in supporting userbase expansion.

[Some however have expressed concerns](https://rknight.me/blog/doubts-about-pixelfed/) about Supernault's handling of the open source Pixelfed codebase and other interactions within the fediverse community. While new moderation controls at the software level won't necessarily assuage such fears, it will provide increased confidence in third-party instances running the Pixelfed software at the very least. There's also the hope that the [Pixelfed Kickstarter](https://www.kickstarter.com/projects/pixelfed/pixelfed-foundation-2024-real-ethical-social-networks), which has blown past its original goal by over 2X, will help establish a "Pixelfed Foundation" which would ensure the platform is able to mature past the personal oversight of Daniel Supernault alone.

Pixelfed's flagship instance, [pixelfed.social](https://pixelfed.social), recently crossed the 250,000 MAU (Monthly Active Users) threshold, making it one of the top services in the independent fediverse alongside Mastodon.
