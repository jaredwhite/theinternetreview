---
date: Wed, 22 Jan 2025 11:39:13 -0800
title: Apple Maps Hasn’t (and Shouldn’t) Change the Name of the Gulf of Mexico
link_title: Apple Maps still calls it the Gulf of Mexico, and politicians are upset
link_url: https://appleinsider.com/articles/25/01/21/apple-maps-still-calls-it-the-gulf-of-mexico-and-politicians-are-upset
link_content: |
  The Gulf of Mexico has been called as much for over 400 years. The name comes from Aztec origins since those were the people that resided in the region when the Americas were colonized. Other countries, generally ones with authoritarian rulers, have tried to coerce Apple and Google to change place names on maps. Like how Russia disagrees with what land Ukraine owns, or that China doesn't observe Taiwan as a separate entity. The standardization of names on a map has always held some importance in ensuring everyone reading the map around the world understood what they were looking at.
---

Let's be clear here. Let's be crystal clear.

**Donald Trump is a wanna-be dictator who lusts after an expanding American Empire.**

OK? With that out of the way, I was pleased to see [this reporting by US Navy veteran Wesley Hilliard for AppleInsider](https://appleinsider.com/articles/25/01/21/apple-maps-still-calls-it-the-gulf-of-mexico-and-politicians-are-upset) about MAGA getting all bent out of shape that Apple Maps still shows that the Gulf of Mexico is indeed the "Gulf of Mexico"; Republican Representative Dan Crenshaw of Texas posting on Nazi social network "X" that he had reported the location as having a "wrong name" because it should be the "Gulf of America".

This would all be rather humorous and wacky, if it weren't so deadly serious. Wars are fought and won (or lost) over place names, boundaries, and geographical features. Under normal circumstances, the global community works together to agree as much as possible on our shared understanding of all of these facts—the **United Nations Group of Experts on Geographical Names (UNGEN)** being such a deliberative body.

But some two-bit gangster who conned his way to the White House thinks he can unilaterally declare a globally-significant geographic feature shared by a number of nations to be what _he_ wants it to be. **He's wrong.**

The Gulf of Mexico will not be renamed, and Trump will look like the complete fool that he is.
