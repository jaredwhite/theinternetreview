---
date: Tue, 14 Jan 2025 19:27:48 -0800
title: "Enjoying Pixelfed? Don’t Miss Out On One Of Its Cooler Features: Portfolio"
---

I'm excited to see [Pixelfed](https://pixelfed.org) get its moment in the spotlight. It's been an unsung hero of the Fediverse for far too long, and now lots of folks are joining or starting back up posting—in part because of everything terrible going on over at Meta, and in part because of the new iOS and Android apps which just launched.

One of Pixelfed's cooler features which is easy to miss if you're not looking for it (and you might not ever know about it if you only use the mobile apps) is **Portfolio**. You should be able to find a button to launch the Portfolio application on the Web when you're logged in and viewing your profile (or just [use this link](https://pixelfed.social/i/web/my-portfolio)). Once there, you have the opportunity to select key photos from your profile you'd like to feature in your portfolio (make sure "Curated posts" is chosen from the "Portfolio Source" option), and you can also make a few customizations such as the layout, display order, and color scheme.

I have a [work-in-progress portfolio up and running here](https://portfolio.pixelfed.social/essentiallife), and the funny thing is I had been evaluating a paid subscription service to build a photography portfolio for myself _which looked nearly identical_ to what I'm able to build here on Pixelfed for free. Sure, it's missing some content options like setting up additional informational pages or a blog, but I could easily link out to a separate photo blog if I wanted to.

So there you have it. If you'd like to showcase some of your favorite photos in an attractive layout that puts the focus on your content (and not a million other social media distractions), check out **Pixelfed Portfolio**.
