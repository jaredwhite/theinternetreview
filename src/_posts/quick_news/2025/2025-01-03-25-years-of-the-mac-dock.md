---
date: Fri, 03 Jan 2025 19:44:36 -0800
title: 25 Years of the Mac OS Dock
link_url: https://tla.systems/blog/2025/01/04/i-live-my-life-a-quarter-century-at-a-time/
link_title: I Live My Life a Quarter Century at a Time
link_content: |
  I didn’t design the dock – that was Bas Ording, a talented young UI designer that Steve had personally recruited. But it was my job to take his prototypes built in Macromind Director and turn them into working code, as part of the Finder team.

  I had already written another dock – DragThing – before I worked for Apple, and that had helped me get a job there. I moved over from Scotland to Ireland in late 1996 with my future wife, with both of us joining the small software team there.
---

Hat tip to [Six Colors by Jason Snell](https://sixcolors.com/link/2025/01/25-years-since-the-docks-debut/), I've become aware that a number of very exciting 25th anniversaries are coming up for fans of Mac OS X and those early days of the Jobsian Mac renaissance. One such anniversary is the unveiling 25 years ago of **Aqua** at Macworld Expo 2000, the "lickable" 3D interface which would go on to revolutionize how user interfaces are created and rendered in modern operating systems. As part of Aqua, the **Dock** was demonstrated—a combination taskbar & launcher which was based in part on the Dock in NeXTSTEP (which of course had served as the underpinnings of the new Mac OS X architecture).

The first version of the OS X Dock was written by James Thomson, whom you may know today as the creator of PCalc, but back then his claim to fame was that he was the author of [DragThing](https://www.dragthing.com/english/about.html), a popular dock/shelf utility for classic Mac OS. Thomson reminisces over his short-lived time as an Apple engineer working out of their Irish headquarters and the effort it took to make Steve Jobs happy with the progress of that early Dock.
