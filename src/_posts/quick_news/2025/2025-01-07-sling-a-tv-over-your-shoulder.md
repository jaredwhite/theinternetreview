---
date: Tue, 07 Jan 2025 17:44:06 -0800
title: Sling a TV Over Your Shoulder? Why Not!
link_url: https://www.theverge.com/2025/1/7/24338520/lg-stanbyme-2-portable-tv-carrying-strap-ces-2025
link_title: LG’s StanbyME sequel adds a carrying strap to the portable TV
link_content: |
  The 27-inch 1440p touchscreen on the StanbyME 2 should offer a sharper picture than the original’s 1080p. It also lasts longer with a four-hour battery life, up from three and a half, but LG has not revealed pricing or availability details. You can still mount the TV to a floor stand that holds and charges it, but LG is unashamedly emphasizing its portability with the carrying strap, which you can also use to hang from a wall like a picture frame on a sturdy enough hook.
---

I must admit, I don't pay a whole lot of attention to CES each year. Most of the more outlandish tech gadgets end up way too expensive for way too little actual utility, and the stuff that's genuinely useful will show up eventually in search results on Best Buy when you actually need [fill in the blank gizmo].

But if you're super into checking out all the tech on display, The Verge (to no one's surprise) [has outstanding coverage of the latest news out of Las Vegas](https://www.theverge.com/2025/1/4/24307731/ces-2025-tvs-gaming-smart-home-wearables-news). Speaking of displays…the item which caught my attention is a portable (!!) 27-inch TV + monitor from LG. It sort of looks like a giant, chonky tablet, and even comes with a folio-style cover which heightens the impression.

I'm a massive fan of mobile-oriented technology even with things people don't normally think could be portable, so this concept is right up my alley. Will it go easy on my wallet though? That remains to be seen.
