---
date: Sat, 01 Mar 2025 22:51:17 -0800
title: "Character.AI: Harmless Fun for Teens…Or Lurking Dangerous Influence?"
link_title: In motion to dismiss, chatbot platform Character AI claims it is protected by the First Amendment
link_url: https://techcrunch.com/2025/01/24/in-motion-to-dismiss-chatbot-platform-character-ai-claims-it-is-protected-by-the-first-amendment/
link_content: |
  In October, Megan Garcia filed a lawsuit against Character AI in the U.S. District Court for the Middle District of Florida, Orlando Division, over the death of her son, Sewell Setzer III. According to Garcia, her 14-year-old developed an emotional attachment to a chatbot on Character AI, “Dany,” which he texted constantly — to the point where he began to pull away from the real world.

  Following Setzer’s death, Character AI said it would roll out a number of new safety features, including improved detection, response, and intervention related to chats that violate its terms of service. But Garcia is fighting for additional guardrails, including changes that might result in chatbots on Character AI losing their ability to tell stories and personal anecdotes.
---

As the parent of a teen who is _obsessed_ with Character.AI, a platform which lets you access chatbot friends based on near limitless permutations of storylines and fanfic-style characters, I can certainly understand the inherent appeal. And as someone who has been deeply critical of the broader generative AI movement for some while now, even I am able to admit that Character.AI seems like it would be an intriguing springboard for imaginative play much like any computer game.

But alas, even though Character.AI may fall under the umbrella of "entertainment" and thus is less subject to critical scrutiny perhaps than the major general-purpose chatbot services like ChatGPT and Gemini, there is still reason to be vary of this potentially dangerous and ill-thought-out technology. [And this recent TechCrunch report](https://techcrunch.com/2025/01/24/in-motion-to-dismiss-chatbot-platform-character-ai-claims-it-is-protected-by-the-first-amendment/) about ongoing litigation over the claim this service encourages minor users to engage in self-harm or other self-destructive behavior illustrates that parents in particular need to make sure they are regularly checking in with their teens who might have developed serious attachments to essentially unaccountable entities with unclear "motives" or a moral stance.
