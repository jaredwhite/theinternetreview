---
graph_title: New RealMedia Platform
category: archived
archive_url: "http://web.archive.org/web/19961226140526/http://www.sonic.net/~jwhite/news/oldquicknews.html"
---

Progressive Networks, the makers of RealAudio, have announced a new platform (currently in beta)
called RealMedia that allows data -- not just audio, but video, animation, text, midi, and more -- to be
compiled and delivered to the client in a real-time format (i. e., streaming data). They are working with
several partners, including <a href="http://web.archive.org/web/19961226140526/http://www.futurewave.com/" target="_top">FutureWave
Software</a> and <a href="http://web.archive.org/web/19961226140526/http://www.iterated.com/" target="_top">Iterated Systems</a>,
the makers of ClearVideo and Fractal Imager (which will be reviewed here in the near future). Visit the
<a href="http://web.archive.org/web/19961226140526/http://www.prognet.com/" target="_top">Progressive Networks Web site</a> for more
information.

