---
category: archived
archive_url: "http://web.archive.org/web/19961226140526/http://www.sonic.net/~jwhite/news/oldquicknews.html"
---

Well, we're sorry for the long delay, but we hope it was worth it. Visual Basic 5.0 Control Creation
Edition beta (from Microsoft) has now been released! Until now, ActiveX control creation required not only
a good understanding of Visual C++ -- but also a good understanding of the Component Object Model
(COM), OLE, and more. Not so any more -- Visual Basic 5.0 eliminates that with an easy-to-use
interface, the immensely popular VBA programming language, and numerous tools and pop-up help
windows. You specify properties as easily as writing a subroutine or a function, and creating
custom events is even easier! To get your hands on the future of ActiveX control creation, point
your browser to <a href="http://web.archive.org/web/19961226140526/http://www.microsoft.com/vbasic" target="_top">Visual Basic's Home Page</a> for a free download!