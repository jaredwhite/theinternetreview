---
title: Microsoft Releases a Beta Version of "Internet Studio", Now Called Visual InterDev
category: archived
archive_url: "http://web.archive.org/web/19961225074549/http://www.sonic.net/~jwhite/news/quicknews.html"
---

Visual InterDev has been around for a long time. Originally it was code-named "Blackbird", and it
was going to be a new tool for creating interactive content on MSN and, possibly, the Web. But when
Microsoft finally jumped on the Internet bandwagon and plotted their strategy, they renamed it
to "Internet Studio" -- and have been quietly working on it ever since. And now it's here, and it is part of
the Microsoft <i>Visual Tools</i> line of products (which includes Visual C++, Visual J++, and Visual
Basic 5.0). However, get ready to faint -- the compressed file to download is 68 megabytes!!
If you are using a modem, prepare to spend the whole day downloading it. Nevertheless, Visual
InterDev is the Microsoft Word and Visual Basic of the Web world, and, with it, designing a Web
site is more like building an application -- perfect for hefty intranet applications. Yet, for many people
who are only designing simple Web sites without much server-side processing, Visual InterDev
may be overkill. To find out what YOU think of it, swing by the
<a href="http://web.archive.org/web/19961225074549/http://www.microsoft.com/vinterdev" target="_top">Visual InterDev</a> Web site, and
take a look around.