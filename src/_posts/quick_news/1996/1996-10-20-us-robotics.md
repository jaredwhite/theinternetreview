---
category: archived
archive_url: "http://web.archive.org/web/19961226140526/http://www.sonic.net/~jwhite/news/oldquicknews.html"
---

US Robotics is in the process of developing a new technology called <b>X2</b> that will allow
you to download information to your modem at speeds up to 56Kbps over standard phone lines! That is
just about as fast as ISDN -- and the way it is accomplished is, since your ISP has a direct connection
to the Internet (which is totally digital), it simply feeds the digital information over the phone line to your
modem. That's right! No more analog signals to slow things down. Of course, both your ISP and you
have to have modems that are compatible with this new interface. To find out more about the X2
technology, visit <a href="http://web.archive.org/web/19961226140526/http://x2.usr.com/" target="_top">USR's X2 Web site</a>.