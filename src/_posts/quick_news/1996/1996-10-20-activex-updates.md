---
category: archived
archive_url: "http://web.archive.org/web/19961226140526/http://www.sonic.net/~jwhite/news/oldquicknews.html"
---

Iterated Systems has now released an ActiveX control of their FractalViewer. Until now, it was
only available as a plug-in or helper application, so users would have to download and install it
before they could view FIF (Fractal Image Format) images on Web pages. But now that it's an ActiveX
control, IE 3.0 users can download and install the control automatically, thus making the use of FIF
images quite desirable -- since they can dramatically reduce the file size of the graphic with almost
no loss of quality. Also, the ClearVideo viewer is now in the ActiveX format, making embedded real-time
video content easier to use. Go to Iterated Systems'
<a href="http://web.archive.org/web/19961226140526/http://www.iterated.com/" target="_top">Web Site</a> (which makes very good use
of HTML Layouts), and check out the great beta versions of both products.

The ActiveX SDK for Macintosh is now in beta! That means that ActiveX is well on its way to
becoming truly cross-platform. It also means that Internet Explorer 3.0 for Macintosh is probably
not far away. Visit the <a href="http://web.archive.org/web/19961226140526/http://www.microsoft.com/intdev/activex/sdk" target="_top">ActiveX SDK Web site</a> and find out the rest of the story.