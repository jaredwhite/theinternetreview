---
category: archived
archive_url: "http://web.archive.org/web/19961226140526/http://www.sonic.net/~jwhite/news/oldquicknews.html"
---

Prepare yourself, for Internet War IV is on its way! MS Internet Explorer 4.0 and Netscape
Navigator 4.0 are in the works right now, and only time will tell who the ultimate loser will be.
Again, Netscape has chosen to keep support for ActiveX out of Navigator 4.0, and with
Microsoft furiously trying to create ActiveX SDKs for all major operating systems (Windows 3.1, 95,
and NT; Macintosh; and UNIX), they may come out ahead with Internet Explorer 4.0 and its ActiveX
support for all platforms. On the other hand, Netscape is really focusing on intranets with their new
software package, Netscape Communicator, which includes Navigator 4.0, Composer (the new HTML authoring
module that replaces the editing mode in Navigator Gold), Messenger (the new mail program
that replaces the Navigator Mail module), Collabra (new group discussion software),
and Conference (Netscape's answer to Microsoft's NetMeeting, which allows you to collaborate
with fellow workers real-time over the Net). Also, Navigator 4.0 will include full support for Cascading
Style Sheets, which will really put Navigator 4.0 in the same class and IE 4.0, HTML-wise. Alas,
both products are still in the development stage, so there isn't much information available from
Microsoft or Netscape. We'll just have to wait and see what happens!