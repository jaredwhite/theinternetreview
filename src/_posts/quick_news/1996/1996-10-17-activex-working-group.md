---
graph_title: ActiveX Working Group
category: archived
archive_url: "http://web.archive.org/web/19961226140526/http://www.sonic.net/~jwhite/news/oldquicknews.html"
---

On October 1, 1996, Microsoft gave the ActiveX technology over to <i>The ActiveX Working
Group</i>, which is an open standards body for shaping the future of ActiveX. Many companies,
such as Netscape Communications and Sun Microsystems, are participating. Visit
The ActiveX Working Group's <a href="http://web.archive.org/web/19961226140526/http://www.activex.org/" target="_top">Web
site</a> today!