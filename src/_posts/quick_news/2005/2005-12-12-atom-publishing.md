---
date: "Monday, December 12th, 2005 at 11:32 pm"
title: "Atom Publishing: the Next Big Thing?"
category: archived
archive_url: "https://web.archive.org/web/20060620180024/http://www.theideabasket.com/2005/12/12/atom-publishing-the-next-big-thing/"
---

[Read this](https://web.archive.org/web/20060620180024/http://www.xml.com/pub/a/2005/12/07/catching-up-with-the-atom-publishing-protocol.html) and [this](https://web.archive.org/web/20060620180024/http://www.xml.com/pub/a/2005/09/21/atom-store-web-database.html), and then come back and tell me this isn't the next big thing. I think it is. I wasn't even aware there was a publishing protocol in the works based on Atom until today when a friend forwarded an article that was on eWeek a couple of months ago. After reading that I skimmed through the entire APP specification draft.

Wow.

I want it. Now. Me, and a gazillion other people out there, I'm sure. This is the next big thing in Web 2.0-style data sharing, and I intend to be in on the ground floor if possible. Make sure you are too.
