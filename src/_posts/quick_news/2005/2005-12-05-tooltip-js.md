---
date: "Monday, December 5th, 2005 at 2:31 pm"
title: "Tooltip.js: Sweet Web 2.0-style Tooltips"
category: archived
archive_url: "https://web.archive.org/web/20090314120420/http://www.theideabasket.com/2005/12/05/tooltipjs-sweet-web-20-style-tooltips/"
---

[Davey Shafik](https://web.archive.org/web/20090314120420/http://www.pixelated-dreams.com/), a well-known PHP coder, has now released some Javascript fun for us Web geeks. It's called [Tooltip.js](https://web.archive.org/web/20090314120420/http://tooltip.crtx.org/), and it allows you to create tooltips for various elements on your pages while using all the latest fun Javascript effects that you are used to seeing on Web 2.0 apps and sites (such as Netflix). I haven't had a chance yet to manually play with the code, but I'm impressed with what I see so far on the Web site.

My #1 feature request to start with: provide an option to make clicking away from the tooltip hide it. I find having to click on the original element or on a separate close link to be somewhat confusing. Otherwise, keep up the good work!
