---
title: "New HTML5 Logo Revealed"
category: archived
archive_url: "https://web.archive.org/web/20110416025034/http://netnotes.siteshine.com/2011/01/#post-263"
---

HTML5 has been around for a while now, and a lot of people in my industry understand how important it and related standards technologies such as CSS3 are for the future of the Web. However, conveying that in an eye-catching manner to the typical "lay person" has been pretty much up for grabs. [Until now](https://web.archive.org/web/20110416025034/http://www.w3.org/html/logo).

![HTML5 logo](/quick_news/2011/HTML5_Logo_128.png){: style="float: right; margin-inline-start: var(--size-2); margin-block: var(--size-1)"}
The W3C (World Wide Web Consortium), which is the main standards body in charge of drafting specifications for Web technologies, [today released a new HTML5 logo](https://web.archive.org/web/20110416025034/http://www.w3.org/html/logo) that promotes the fresh power of the latest "open Web" technologies. I think it looks fantastic. What's interesting is there's not just a single logo, but a set of icons accompanying it that reference specific aspects of the HTML5 spec or related specs (CSS3, SVG, etc.). You can create a badge showing off which of those various features are in use on your site.

It'll be interesting to see how quickly this new branding effort takes off. My guess is you'll see it used a lot by Web designers quickly, followed by more and more apps, and eventually a host of sites wanting to align themselves with the buzz surrounding HTML5. Anything that boosts awareness of what HTML5 is all about is fine by me!
