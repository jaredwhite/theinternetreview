---
date: "Tuesday, March 7th, 2006 at 8:00 pm"
title: "Clipboard for the Web"
category: archived
archive_url: "https://web.archive.org/web/20081122071530/http://www.theideabasket.com/2006/03/07/clipboard-for-the-web/"
---

You know what? Microsoft is really starting to fire on all cylinders. Maybe buying Groove and getting Ray Ozzie was a bigger event than I realized at the time.

Ray's come up with one of the most brilliant technologies ever to hit the Web in my opinion: a standardized XML format describing clipboard data formats that can be used for site-to-site, site-to-desktop, and desktop-to-site copy & paste. Whoa. Let that sink in for a minute: with this technology (pure Javascript that works in IE and Firefox to start with), you can now right-click on an image on a Web site, select "copy", right-click on another image somewhere else on that page or on a totally different Web site, select "paste", and the data is transferred through structured XML and inserted as customized HTML into that page. Even RSS feed URLs can be brought along.

[Check out](https://web.archive.org/web/20081122071530/http://spaces.msn.com/rayozzie/blog/cns!FB3017FBB9B2E142!285.entry?_c11_blogpart_blogpart=blogview&_c=blogpart#permalink) the screencasts and the code, which is still in its infancy so it can only get better from here. This is big, folks. Really big.