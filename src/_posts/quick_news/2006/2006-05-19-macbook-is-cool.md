---
date: "Friday, May 19th, 2006 at 1:30 pm"
title: "MacBook: It’s, Like, Totally Cool, Man"
category: archived
archive_url: "https://web.archive.org/web/20060614123247/http://www.theideabasket.com/2006/05/19/macbook-its-like-totally-cool-man/"
---

I checked out the new [MacBook](https://web.archive.org/web/20060614123247/http://www.apple.com/macbook) a few days ago at my local Apple store, and I was quite impressed. I thought the iBook design couldn't be beat, and I still like my iBook G4 a whole lot, but the MacBook (even with the slightly bigger width) is a small, sleek bundle of power that is really on a whole new level. A next-generation product, no doubt about it. I'll post soon with some of my observations, especially regarding the strange and unique keyboard design.

Needless to say, I believe Apple will sell millions of them this year. They're really, really cool!