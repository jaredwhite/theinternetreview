---
date: "Friday, February 17th, 2006 at 8:29 pm"
title: "Old becomes new: Camino is back!"
category: archived
archive_url: "https://web.archive.org/web/20081122065141/http://www.theideabasket.com/2006/02/17/old-becomes-new-camino-is-back/"
---


After much time of languish and neglect, [Camino](https://web.archive.org/web/20081122065141/http://www.caminobrowser.org/) is back, and back with a vengence. I'm posting this using the new Camino 1.0 browser for Mac OS X, and I'm extremely impressed. It has all the nice Gecko-y rendering power, yet has a simple Safari-like Cocoa interface and nice Cocoa-style widgets for the HTML forms. It's fast, seems stable, and has everything I need...except inline spell checking. But I think they're working on that.

If you're an OS X user, I highly recommend checking out Camino. It might make you switch from Firefox and Safari all in one go!