---
date: "Wednesday, July 19th, 2006 at 3:40 pm"
title: "1.327 Million Macs Sold in Q3 2006"
category: archived
archive_url: "https://web.archive.org/web/20060818232425/http://www.theideabasket.com/2006/07/19/1327-million-macs-sold-in-q3-2006/"
---

[Way](https://web.archive.org/web/20060818232425/http://macdailynews.com/index.php/weblog/comments/10240/%22) [to](https://web.archive.org/web/20060818232425/http://www.appleinsider.com/article.php?id=1896) [go](https://web.archive.org/web/20060818232425/http://www.thinksecret.com/news/0607q3earnings.html) [Apple](https://web.archive.org/web/20060818232425/http://www.neowin.net/index.php?act=view&id=34186)! From [my comment at OSNews](https://www.osnews.com/story/15239/apple-posts-usd-472m-profit-on-revenues-of-usd-437b/):

> What seems to be the hidden story in these Q3 results is how incredibly well the Intel switch is going at this point. Apple sold the highest number of Macs in a quarter since 2000, yet the desktop sales went DOWN, due to people waiting for new Power Macs and Xserves. The notebooks counted for 798,000 units — giving Apple a new U.S. notebook marketshare of 12%. Whoa! And that’s with Macbooks still ramping up production. 
> 
> Fast forward a couple of quarters with the Intel desktop line complete and the Macbook + Pro lineup going gangbusters. I think we’ll easily get to 1.5 million Macs soon. Then what? 2 million? It’s not out of the question. 
> 
> Apple’s going for the jugular in the PC world with this Intel switch. The pundits who predicted the demise of the Mac and Apple’s shift to consumer music + video products were wrong. Dead wrong. Dude, who cares about Dell?
