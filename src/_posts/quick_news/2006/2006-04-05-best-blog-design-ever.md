---
date: "Wednesday, April 5th, 2006 at 1:23 pm"
title: "Best Blog Design Ever?"
category: archived
archive_url: "https://web.archive.org/web/20081122061607/http://www.theideabasket.com/2006/04/05/best-blog-design-ever/"
---

This has got to be the coolest and most original blog design I have ever seen. Very, very neat.

[theocacao.com](https://web.archive.org/web/20081122061607/http://theocacao.com/)

> Scott Stevenson Says:  
> April 6th, 2006 at 1:33 am  
> **Wow, many thanks. Found this via Technorati.**